Option Explicit On

Imports System.Windows.Forms

Namespace TSG.CI.Forte
    Friend Class GlobalMethods
        Private Declare Function GetTickCount Lib "kernel32" () As Long
        Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
            "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal _
            lpString As String, ByVal lpFileName As String) As Long
        Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
            ByVal lpBuffer As String, nSize As Long) As Long
        Private Declare Sub OutputDebugString Lib "kernel32" _
          Alias "OutputDebugStringA" _
          (ByVal lpOutputString As String)

        Public Const ciMP10InternalID As Integer = 111

        Public Shared g_vCols(3, 1) As Object
        Public Shared g_iSortCol As ciListingCols
        Public Shared g_oSession As CSessionType
        Public Shared g_oError As CError
        Public Shared g_oIni As CIni
        Public Shared g_oGlobals As CGlobals
        Public Shared g_oConstants As CConstants
        Public Shared g_xBackend As String

        Public Shared Function Max(i As Double, j As Double) As Double
            If i > j Then
                Max = i
            Else
                Max = j
            End If
        End Function

        Public Shared Function Min(i As Double, j As Double) As Double
            If i > j Then
                Min = j
            Else
                Min = i
            End If
        End Function

        Function IsEven(dNum As Double) As Boolean
            If dNum / 2 = CLng(dNum / 2) Then
                IsEven = True
            Else
                IsEven = False
            End If
        End Function

        Public Shared Function xSubstitute(ByVal xString As String, _
                             ByVal xSearch As String, _
                             ByVal xReplace As String) As String
            'replaces xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer
            Dim xNewString As String

            xNewString = Nothing

            '   get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            '   remove switch all chars
            While iSeachPos
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
                iSeachPos = InStr(UCase(xString), _
                                  UCase(xSearch))

            End While

            xNewString = xNewString & xString
            xSubstitute = xNewString
        End Function

        Function CurrentTick() As Long
            CurrentTick = GetTickCount()
        End Function

        Function ElapsedTime(lStartTick As Long) As Single
            'returns the time elapsed from lStartTick-
            'precision in milliseconds
            ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
        End Function

        Public Sub DebugPrint(xOutput As String)
            Debug.Print(xOutput)
            OutputDebugString(xOutput)
        End Sub
    End Class
End Namespace