'*****************************************************
'CIForte Authors class
'created by Charlie Homo 2/19/08
'defines ci support for Forte Authors-
'implements CIO.ICIBackend and CIO.ICINumberPromptFormat
'*****************************************************
Option Explicit On

Imports System.Windows.Forms
Imports ADODB
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP

Namespace TSG.CI.Forte
    Friend Class CCIBackend

        Implements ICIBackend
#Region "*****************fields********************"
        Private m_iID As Integer
        Private m_bIsConnected As Boolean
        Private m_oCnn As ADODB.Connection
        Private m_xDisplayName As String
        Private m_oFilter As CFilter
        Private m_oUNID As CUNID
        Private m_oEvents As CEventGenerator
        Private m_iPromptType As ICINumberPromptFormat.ciNumberPromptTypes
        Private m_iNumberType As ciContactNumberTypes
        Private m_oCustomFields As CCustomFields
        Private m_xName As String
        Private m_xUserIni As String
        Private m_xPublicDB As String
        Private g_oIni As CIni
        Private m_bIsLocalDB As Boolean
        Private m_bIsForte As Boolean
        Private m_bIsForteTools As Boolean
        Private m_bIsForteLocal As Boolean

        Private Const CSIDL_LOCAL_APPDATA = &H1C&
        Private Const CSIDL_FLAG_CREATE = &H8000&
        Private Const CSIDL_COMMON_DOCUMENTS = &H2E

        Private Const SHGFP_TYPE_CURRENT = 0
        Private Const SHGFP_TYPE_DEFAULT = 1
        Private Const MAX_PATH = 260

        Private Const m_xTagEmpty As String = "<EMPTY>"

        Private Declare Function SHGetFolderPath Lib "shfolder" _
            Alias "SHGetFolderPathA" _
            (ByVal hwndOwner As Long, ByVal nFolder As Long, _
            ByVal hToken As Long, ByVal dwFlags As Long, _
            ByVal pszPath As String) As Long

        Dim xObjectID As String
#End Region
#Region "*****************initializer***************"
        Public Sub New()
            GlobalMethods.g_oGlobals = New CGlobals
            m_oCustomFields = New CCustomFields
            m_oUNID = New CUNID
            m_oEvents = GlobalMethods.g_oGlobals.CIEvents()
            g_oIni = New CIni
            GlobalMethods.g_oSession = New CSessionType
            GlobalMethods.g_oConstants = New CConstants
            GlobalMethods.g_oError = New CError
        End Sub
#End Region
#Region "*****************methods*******************"
        Private Function GetStoreListings(oStore As CStore, _
                                          Optional oFilter As CFilter = Nothing) As CListings
            'returns the collection of authors as a listings collection
            Dim oRS As ADODB.Recordset
            Dim xSQL As String
            Dim oL As CListing
            Dim oLs As CListings
            Dim xID As String
            Dim xName As String
            Dim xAdd1 As String
            Dim xAdd2 As String
            Dim xAdd3 As String
            Dim xFilter As String
            Dim xFilterSort As String
            Dim lNum As Long
            Dim bCancel As Boolean
            Dim l As Long

            xFilter = Nothing
            xFilterSort = Nothing

            ConnectIfNecessary()

            oLs = New CListings

            m_oEvents.RaiseBeforeExecutingListingsQuery()

            'set sort column to default sort column if necessary
            If oFilter.SortColumn = Nothing Then
                oFilter.SortColumn = ICIBackend_DefaultSortColumn()
            End If

            'get WHERE and ORDER BY clauses
            GetFilterSortString(oFilter, xFilter, xFilterSort)

            oRS = New ADODB.Recordset
            With oRS
                If m_bIsForteLocal Then
                    'get a count of authors that will be retrieved
                    xSQL = "SELECT COUNT(PeoplePublic.ID1) AS Num FROM PeoplePublic WHERE PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)) " & xFilter
                Else
                    'get a count of authors that will be retrieved
                    xSQL = "SELECT COUNT(people.ID1) AS Num FROM people LEFT JOIN Offices ON people.OfficeID=offices.id WHERE people.OfficeID Is Not Null AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1)) " & xFilter
                End If
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)

                lNum = CLng(oRS.Fields("Num").Value)

                .Close()

                If m_bIsForteLocal Then
                    xSQL = "SELECT PeoplePublic.ID1,offices.DisplayName as OfficeName,PeoplePublic.lastname,PeoplePublic.firstname, " & _
                            "IIf(IsNull(PeoplePublic.DisplayName),LastName + ', ' + FirstName,PeoplePublic.DisplayName) as DisplayName " & _
                            "FROM PeoplePublic LEFT JOIN Offices ON PeoplePublic.OfficeID=offices.ID " & _
                            "WHERE PeoplePublic.officeid is not null AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)) " & _
                            xFilter & " ORDER BY PeoplePublic.DisplayName"
                ElseIf m_bIsLocalDB Then
                    xSQL = "SELECT people.ID1,offices.DisplayName as OfficeName,people.lastname,people.firstname, " & _
                            "IIf(IsNull(people.DisplayName),LastName + ', ' + FirstName,people.DisplayName) as DisplayName " & _
                            "FROM people LEFT JOIN Offices ON people.OfficeID=offices.ID " & _
                            "WHERE people.officeid is not null AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1)) " & _
                            xFilter & " ORDER BY people.DisplayName"
                Else
                    xSQL = "SELECT people.ID1,offices.DisplayName as OfficeName,people.lastname,people.firstname, " & _
                            "CASE When people.DisplayName='' THEN LastName + ', ' + FirstName ELSE people.DisplayName END as DisplayName " & _
                            "FROM people LEFT JOIN Offices ON people.OfficeID=offices.ID " & _
                            "WHERE people.officeid is not null AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1)) " & _
                            xFilter & " ORDER BY people.DisplayName"
                End If
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)

                m_oEvents.RaiseAfterExecutingListingsQuery()
                m_oEvents.RaiseBeforeListingsRetrieved(lNum, bCancel)
                While Not .EOF
                    oL = New CListing
                    On Error Resume Next
                    xID = Nothing
                    xName = Nothing
                    xAdd1 = Nothing
                    xAdd2 = Nothing
                    xAdd3 = Nothing

                    xID = GlobalMethods.ciMP10InternalID & GlobalMethods.g_oConstants.UNIDSep & "1" & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & oRS.Fields("ID1").Value
                    xName = oRS.Fields("DisplayName").Value
                    xAdd1 = oRS.Fields("OfficeName").Value
                    xAdd2 = oRS.Fields("LastName").Value
                    xAdd3 = oRS.Fields("FirstName").Value
                    oLs.Add(xID, xName, xAdd1, xAdd2, xAdd3, ciListingType.ciListingType_Person)

                    .MoveNext()

                    l = l + 1
                    m_oEvents.RaiseAfterListingAdded(l, lNum, bCancel)
                End While
                .Close()
            End With

            m_oEvents.RaiseAfterListingsRetrieved(l)
            GetStoreListings = oLs
        End Function

        Private Function GetPhoneNumbers(oListing As CListing, _
                                         Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'retrieves the Phone number associated with the listing -
            'returns it as the sole item in the contact numbers collection
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xPhone As String

            'create empty collection of contact numbers
            oNums = New CContactNumbers

            If m_bIsForteLocal Then
                xSQL = "SELECT PeoplePublic.phone,addresses.phone1 FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
            Else
                xSQL = "SELECT people.phone,addresses.phone1 FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
            End If

            oRS = New ADODB.Recordset
            With oRS
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                If Not (.BOF And .EOF) Then
                    Try
                        xPhone = IIf(IsNothing(oRS.Fields("Phone").Value), oRS.Fields("Phone1").Value, oRS.Fields("Phone").Value)
                    Catch
                    End Try

                    If xPhone <> Nothing Then
                        oNums.Add(1, "Office Phone", xPhone, "", _
                            ciContactNumberTypes.ciContactNumberType_Phone, "Office Phone")
                    End If
                End If
                .Close()
            End With

            GetPhoneNumbers = oNums
        End Function

        Private Function GetFaxNumbers(oListing As CListing, _
                                       Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'retrieves the Fax number associated with the listing -
            'returns it as the sole item in the contact numbers collection
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xFax As String

            'create empty collection of contact numbers
            oNums = New CContactNumbers

            'create sql statements
            If m_bIsForteLocal Then
                xSQL = "SELECT PeoplePublic.fax,addresses.fax1 FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
            Else
                xSQL = "SELECT people.fax,addresses.fax1 FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
            End If

            oRS = New ADODB.Recordset
            With oRS
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                If Not (.BOF And .EOF) Then

                    Try
                        xFax = IIf(IsNothing(oRS.Fields("Fax").Value), oRS.Fields("Fax1").Value, oRS.Fields("Fax").Value)
                    Catch
                    End Try

                    If xFax <> Nothing Then
                        oNums.Add(1, "Office Fax", xFax, "", _
                            ciContactNumberTypes.ciContactNumberType_Fax, "Office Fax")
                    End If
                End If
                .Close()
            End With

            GetFaxNumbers = oNums
        End Function

        Private Function GetEMailNumbers(oListing As CListing, _
                                         Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'retrieves the EMail number associated with the listing -
            'returns it as the sole item in the contact numbers collection
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xEMail As String

            'create empty collection of contact numbers
            oNums = New CContactNumbers

            'create sql statements
            If m_bIsForteLocal Then
                xSQL = "SELECT EMail FROM PeoplePublic " & _
                        "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
            Else
                xSQL = "SELECT EMail FROM people WHERE ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
            End If

            oRS = New ADODB.Recordset
            With oRS
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                If Not (.BOF And .EOF) Then

                    Try
                        xEMail = oRS.Fields("EMail").Value
                    Catch
                    End Try

                    If xEMail <> Nothing Then
                        oNums.Add(1, "EMail Address", xEMail, "", _
                            ciContactNumberTypes.ciContactNumberType_EMail, "EMail Address")
                    End If
                End If
                .Close()
            End With

            GetEMailNumbers = oNums
        End Function

        Private Function GetContacts(oListing As CListing, _
                                     oAddress As CAddress, _
                                     Optional ByVal vAddressType As Object = Nothing, _
                                     Optional ByVal iIncludeData As ciRetrieveData = 1&, _
                                     Optional ByVal iAlerts As ciAlerts = 1&) As CContacts
            'returns a collection of contacts containing a single contact-
            'since there is only one address per contact,
            'we ignore address/phone/fax/email ids.
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim xPath As String
            Dim oNums As CContactNumbers
            Dim oCs As CContacts
            Dim oC As CContact
            Dim xTemplate As String

            oC = Nothing
            xTemplate = Nothing

            oCs = New CContacts

            ConnectIfNecessary()

            If m_bIsForteLocal Then
                xSQL = "SELECT * FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
            Else
                xSQL = "SELECT * FROM addresses RIGHT JOIN " & _
                        "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                        "WHERE (people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1)))"
            End If

            oRS = New ADODB.Recordset
            With oRS
                'get contact record
                .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)

                Dim i As Integer

                If Not (.BOF And .EOF) Then
                    'record exists - create a new contact
                    oC = New CContact

                    'set DisplayName by default
                    oC.DisplayName = oListing.DisplayName

                    If iIncludeData And ciRetrieveData.ciRetrieveData_Names Or _
                        iIncludeData = ciRetrieveData.ciRetrieveData_All Then
                        'fill name fields
                        oC.Company = GetFieldValue(oRS, "FirmName")
                        'oC.DisplayName = oListing.DisplayName
                        oC.FirstName = GetFieldValue(oRS, "FirstName")
                        oC.FullName = GetFieldValue(oRS, "FullName")
                        oC.GoesBy = GetFieldValue(oRS, "ShortName")
                        oC.Initials = GetFieldValue(oRS, "Initials")
                        oC.LastName = GetFieldValue(oRS, "LastName")
                        oC.MiddleName = GetFieldValue(oRS, "MI")
                        oC.Salutation = GetFieldValue(oRS, "FirstName")
                        oC.Title = GetFieldValue(oRS, "Title")
                        'oC.FirstName = oRS.Fields("FirstName").Value
                        'oC.FullName = oRS.Fields("FullName").Value
                        'oC.GoesBy = oRS.Fields("ShortName").Value
                        'oC.Initials = oRS.Fields("Initials").Value
                        'oC.LastName = oRS.Fields("LastName").Value
                        'oC.MiddleName = oRS.Fields("MI").Value
                        'oC.Salutation = oRS.Fields("FirstName").Value
                        'oC.Title = oRS.Fields("Title").Value
                    End If

                    If iIncludeData And ciRetrieveData.ciRetrieveData_Addresses Or _
                        iIncludeData = ciRetrieveData.ciRetrieveData_All Then
                        If Not IsNothing(oRS.Fields("OfficeID").Value) Then
                            'office address exists - fill address fields
                            oC.AddressTypeName = "Office"
                            oC.AddressTypeID = 1
                            oC.AddressID = 1

                            oC.City = GetFieldValue(oRS, "City")
                            oC.State = GetFieldValue(oRS, "State")
                            oC.Street1 = GetFieldValue(oRS, "Line1")
                            oC.Street2 = GetFieldValue(oRS, "Line2")
                            oC.Street3 = GetFieldValue(oRS, "Line3")
                            oC.ZipCode = GetFieldValue(oRS, "Zip")
                            oC.Country = GetFieldValue(oRS, "Country")

                            'oC.City = oRS.Fields("City").Value
                            'oC.State = oRS.Fields("State").Value
                            'oC.Street1 = oRS.Fields("Line1").Value
                            'oC.Street2 = oRS.Fields("Line2").Value
                            'oC.Street3 = oRS.Fields("Line3").Value
                            'oC.ZipCode = oRS.Fields("Zip").Value
                            'oC.Country = oRS.Fields("Country").Value

                            'get core addres by using the template field from the recordset-
                            'this needs to be modified for CI
                            xTemplate = oRS.Fields("Template").Value
                            xTemplate = Replace(xTemplate, "[Line1]", "<STREET1>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[Line2]", "<STREET2>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[Line3]", "<STREET3>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[PreCity]", "<PRECITY>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[City]", "<CITY>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[PostCity]", "<POSTCITY>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[State]", "<STATE>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[StateAbbr]", "<STATEABBR>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[Zip]", "<ZIP>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[Country]", "<COUNTRY>", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "|[CountryIfForeign]", "", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "[CountryIfForeign]", "", , , vbTextCompare)
                            xTemplate = Replace(xTemplate, "|", vbCrLf)
                        ElseIf iAlerts And ciAlerts.ciAlert_NoAddresses Or _
                            iAlerts = ciAlerts.ciAlert_All Then
                            'alert that there is no address
                            MsgBox("No addresses found for '" & oC.FullName & "'.", vbExclamation)
                        End If
                    End If

                    If iIncludeData And ciRetrieveData.ciRetrieveData_Phones Or _
                        iIncludeData = ciRetrieveData.ciRetrieveData_All Then
                        'get phone number - retrieve direct phone
                        'if available - if none, get office phone
                        Try
                            oC.Phone = IIf(IsNothing(oRS.Fields("Phone").Value), oRS.Fields("Phone1").Value, oRS.Fields("Phone").Value)
                        Catch
                        End Try

                        If oC.Phone = Nothing Then
                            If iAlerts And ciAlerts.ciAlert_NoPhones Or _
                                iAlerts = ciAlerts.ciAlert_All Then
                                'alert user to no phones
                                oNums = New CContactNumbers
                                oNums.AlertNoneIfSpecified(ciContactNumberTypes.ciContactNumberType_Phone, oC.FullName)
                            End If
                        Else
                            'phone id is '1' because there are no ids for phone info
                            oC.PhoneID = 1
                        End If
                    End If

                    If iIncludeData And ciRetrieveData.ciRetrieveData_Faxes Or _
                        iIncludeData = ciRetrieveData.ciRetrieveData_All Then
                        'get fax number - retrieve direct fax
                        'if available - if none, get office fax
                        Try
                            oC.Fax = IIf(IsNothing(oRS.Fields("Fax").Value), oRS.Fields("Fax1").Value, oRS.Fields("Fax").Value)
                        Catch
                        End Try

                        If oC.Fax = Nothing Then
                            If iAlerts And ciAlerts.ciAlert_NoFaxes Or _
                                iAlerts = ciAlerts.ciAlert_All Then
                                'alert user to no faxes
                                oNums = New CContactNumbers
                                oNums.AlertNoneIfSpecified(ciContactNumberTypes.ciContactNumberType_Fax, oC.FullName)
                            End If
                        Else
                            'fax id is '1' because there are no ids for fax info
                            oC.FaxID = 1
                        End If
                    End If

                    If iIncludeData And ciRetrieveData.ciRetrieveData_EAddresses Or _
                        iIncludeData = ciRetrieveData.ciRetrieveData_All Then
                        'get email address
                        Try
                            oC.EMailAddress = oRS.Fields("EMail").Value
                        Catch
                        End Try

                        If oC.Fax = Nothing Then
                            If iAlerts And ciAlerts.ciAlert_NoEAddresses Or _
                                iAlerts = ciAlerts.ciAlert_All Then
                                'alert user to no email addresses
                                oNums = New CContactNumbers
                                oNums.AlertNoneIfSpecified(ciContactNumberTypes.ciContactNumberType_Fax, oC.FullName)
                            End If
                        Else
                            'fax id is '1' because there are no ids for e address info
                            oC.EAddressID = 1
                        End If
                    End If

                    If (iIncludeData And ciRetrieveData.ciRetrieveData_CustomFields) Or _
                        (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                        oC.CustomFields = ICIBackend_GetCustomFields(oListing)
                    End If

                    'get core address here, as pre city and post city are custom fields,
                    'and we need to get this info for the code address
                    If (iIncludeData And ciRetrieveData.ciRetrieveData_Addresses) Or (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                        oC.CoreAddress = GetDetail(oC, xTemplate)
                    End If

                    If oAddress Is Nothing Then
                        oC.UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & oC.PhoneID & _
                            ";" & oC.FaxID & ";" & oC.EAddressID
                    Else
                        oC.UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & oAddress.ID & GlobalMethods.g_oConstants.UNIDSep & oC.PhoneID & _
                            ";" & oC.FaxID & ";" & oC.EAddressID
                    End If

                End If
                .Close()
            End With

            'add single contact to collection
            oCs.Add(oC)

            'return
            GetContacts = oCs
            oC = Nothing
            oCs = Nothing
        End Function

        Private Function GetAddresses(Optional oListing As CListing = Nothing) As CAddresses
            Dim oA As CAddress
            Dim oAs As CAddresses
            Dim xUNID As New String(GlobalMethods.g_oConstants.UNIDSep, 8)

            oAs = New CAddresses
            oA = New CAddress

            If oListing Is Nothing Then
                'UNID is backend based
                oA.UNID = GlobalMethods.ciMP10InternalID & xUNID & "1"
            Else
                'UNID is listing based
                oA.UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & "1"
            End If

            oA.Name = "Office"
            oAs.Add(oA)
            GetAddresses = oAs
        End Function

        Private Sub CreateFilter()
            'creates an empty filter for backend based on ini definition
            Dim xKey As String
            Dim iPos As Integer
            Dim i As Integer
            Dim lCol As ciListingCols

            'create empty filter
            m_oFilter = New CFilter

            For i = 1 To 4
                'get ini key for filter field of this backend
                xKey = g_oIni.GetIni("Backend" & m_iID, "Filter" & i, g_oIni.CIIni)

                If xKey = Nothing Then
                    Exit For
                End If

                'search for ',' - this delimits name from id in key
                iPos = InStr(xKey, ",")
                If iPos = 0 Or iPos = 1 Or iPos = Len(xKey) Then
                    'no delimiter present, no name specified, or no id specified
                    Throw New Exception("Key 'FilterFields" & m_iID & "\ " & _
                            CStr(i) & "' in CI.ini has an invalid value.")
                End If

                With m_oFilter.FilterFields(i - 1)
                    'get name, id, operator of filter field
                    .Name = Left(xKey, iPos - 1)
                    .ID = Mid(xKey, iPos + 1)
                    .SearchOperator = ciSearchOperators.ciSearchOperator_Contains

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oFilter.FilterFields(" & i & ")  .Name=" & .Name & "; .ID=" & .ID & _
                        "; .Operator=" & .SearchOperator, GlobalMethods.g_xBackend & ".CCIBackend.CreateFilter")
                    End If
                End With
            Next

            'get sort column
            Try
                lCol = CLng(g_oIni.GetIni("CIApplication", "Sort", g_oIni.CIUserIni))
            Catch ex As Exception
                lCol = ciListingCols.ciListingCols_DisplayName
            End Try

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("SortColumn (lCol)=" & lCol, _
                GlobalMethods.g_xBackend & ".CCIBackend.CreateFilter")
            End If

            m_oFilter.SortColumn = lCol
        End Sub


        Private Function GetFilter() As CFilter
            'prompts user for filter/sort conditions
            Dim oForm As FilterForm
            Dim oBackend As ICIBackend
            Dim i As Integer

            oForm = New FilterForm

            With oForm
                .Backend = Me

                'show filter form
                .ShowDialog()

                'branch on type of action requested
                If .Action = FilterForm.ciFilterActions.ciFilterAction_ClearSearch _
                    Or .Action = FilterForm.ciFilterActions.ciFilterAction_Search Then

                    '           set filter based on current settings in filter dlg
                    oBackend = Me
                    With oBackend.Filter
                        For i = 0 To .CountFields - 1
                            'set filter field values
                            .FilterFields(i).Value = oForm.txtFilterField(i).Text
                            .FilterFields(i).SearchOperator = oForm.cmbSearchTypes.SelectedValue
                        Next i
                    End With

                    'return filter
                    GetFilter = oBackend.Filter
                Else
                    GetFilter = Nothing
                End If

                oForm.Close()
                oForm.Dispose()
            End With
        End Function
        Private Sub ConnectIfNecessary()
            'connects to the mp10 DB if not connected -
            'raises a trappable error if connection fails
            Dim bDo As Boolean

            Try
                If m_oCnn Is Nothing Then
                    m_oCnn = New ADODB.Connection
                    bDo = True
                ElseIf m_oCnn.State = ObjectStateEnum.adStateClosed Then
                    bDo = True
                End If

                If bDo Then
                    Connect()
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Sub

        Private Sub GetFilterSortString(oFilter As CFilter, ByRef xFilter As String, ByRef xFilterSort As String)
            'returns sql WHERE and ORDER BY clauses given the specified filter
            Dim xFieldID1 As String
            Dim xValue As String
            Dim i As Integer

            xFilter = Nothing
            xFilterSort = Nothing

            With oFilter
                'build WHERE condition based on current Filter
                For i = 0 To .CountFields - 1
                    xValue = .FilterFields(i).Value
                    If xValue <> "" Then
                        xFieldID1 = .FilterFields(i).ID

                        If UCase$(xFieldID1) = "FLDDISPLAYNAME" Then
                            'change the field name to 'DisplayName'- this is
                            'required because of the nature of the SQL query -
                            'display name is a constructed field if empty
                            xFieldID1 = "DisplayName"
                        End If

                        If xValue = "^123^" Then
                            xFilter = " AND (" & xFieldID1 & " LIKE '0%') " & _
                                      " OR (" & xFieldID1 & " LIKE '1%') " & _
                                      " OR (" & xFieldID1 & " LIKE '2%') " & _
                                      " OR (" & xFieldID1 & " LIKE '3%') " & _
                                      " OR (" & xFieldID1 & " LIKE '4%') " & _
                                      " OR (" & xFieldID1 & " LIKE '5%') " & _
                                      " OR (" & xFieldID1 & " LIKE '6%') " & _
                                      " OR (" & xFieldID1 & " LIKE '7%') " & _
                                      " OR (" & xFieldID1 & " LIKE '8%') " & _
                                      " OR (" & xFieldID1 & " LIKE '9%') "
                            Exit For
                        Else

                            'add wildcards if specified by operator
                            If .FilterFields(i).SearchOperator = ciSearchOperators.ciSearchOperator_BeginsWith Then
                                xValue = xValue & "*"
                            ElseIf .FilterFields(i).SearchOperator = ciSearchOperators.ciSearchOperator_Contains Then
                                xValue = "*" & xValue & "*"
                            End If

                            'double up any apostrophes
                            xValue = GlobalMethods.xSubstitute(xValue, "'", "''")

                            xFilter = xFilter & " AND (" & xFieldID1 & " LIKE '" & xValue & "') "
                        End If

                    End If
                Next i

                If xFilter <> Nothing Then
                    xFilter = " AND " & Mid$(xFilter, 5)

                    'substitute wildcard characters
                    xFilter = GlobalMethods.xSubstitute(xFilter, "*", "%")
                    xFilter = GlobalMethods.xSubstitute(xFilter, "?", "_")
                End If

                'add sort to filter
                xFilterSort = xFilter & " ORDER BY DisplayName"
            End With
        End Sub

        Public Sub Connect()
            Dim bRet As Boolean
            Dim hKey As Long
            Dim xSQL As String
            Dim xAdminDB As String
            Dim xMsg As String
            Dim lErr As Long
            Dim xDesc As String
            Dim xUserName As String
            Dim xCnn As String
            Dim xPath As String
            Dim xPathWritable As String
            Dim xAppDirPath As String
            Dim xPrivateDB As String
            Dim xUserDir As String
            Dim xNetServer As String
            Dim xTemp As ADODB.Connection
            Dim oNetwork As CNetwork
            Dim lTimeOut As Long

            oNetwork = New CNetwork
            xNetServer = Nothing

            'get dir path with Writable directory
            xPathWritable = g_oIni.ApplicationDirectoryMacPac10()

            'GLOG : 7787 : ceh
            If xPathWritable = Nothing Then
                Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                    "Could not find location of Data Directory.")
            End If

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("xPathWritable = " & xPathWritable, _
                    GlobalMethods.g_xBackend & ".CCIBackend.Connect")
            End If

            'get dir path without Writable directory
            xPath = g_oIni.ApplicationDirectoryMacPac10(False)

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("xPath = " & xPath, _
                    GlobalMethods.g_xBackend & ".CCIBackend.Connect")
            End If

            If xPath = Nothing Then
                Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                    "Could not find location of Data Directory.")
            End If

            'GLOG2 : 8259 : ceh
            m_bIsForteLocal = g_oIni.IsForteLocal

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("IsForteLocal = " & m_bIsForteLocal, _
                    GlobalMethods.g_xBackend & ".CCIBackend.Connect")
            End If

            If Not m_bIsForteLocal Then

                'GLOG : 7787 : ceh
                m_bIsForte = (Dir$(GetEnvironVarPath(xPathWritable & "\Forte.mdb")) <> Nothing) Or _
                             (Dir$(GetEnvironVarPath(xPathWritable & "\ForteAdmin.mdb")) <> Nothing)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("IsForte = " & m_bIsForte, _
                        GlobalMethods.g_xBackend & ".CCIBackend.Connect")
                End If

                'GLOG2 : 7929 : ceh
                m_bIsForteTools = (Dir$(GetEnvironVarPath(xPathWritable & "\ForteTools.mdb")) <> Nothing) Or _
                                    (Dir$(GetEnvironVarPath(xPathWritable & "\ForteToolsAdmin.mdb")) <> Nothing)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("IsForteTools = " & m_bIsForteTools, _
                        GlobalMethods.g_xBackend & ".CCIBackend.Connect")
                End If
            End If

            'GLOG2 : 7687 : CEH
            If m_bIsForte Then
                m_xPublicDB = xPathWritable & "\Forte.mdb"

                'GLOG : 7242 : CEH
                If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                    'try looking for Forte Admin db
                    m_xPublicDB = xPathWritable & "\ForteAdmin.mdb"

                    If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                        Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                            "Could not find the file '" & xPathWritable & "\Forte.mdb" & "'.")
                    End If
                End If
                'GLOG2 : 7929 : ceh
            ElseIf m_bIsForteTools Then
                m_xPublicDB = xPathWritable & "\ForteTools.mdb"

                'GLOG : 7242 : CEH
                If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                    'try looking for Forte Admin db
                    m_xPublicDB = xPathWritable & "\ForteToolsAdmin.mdb"

                    If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                        Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                            "Could not find the file '" & xPathWritable & "\ForteTools.mdb" & "'.")
                    End If
                End If
                'GLOG2 : 8259 : ceh
            ElseIf m_bIsForteLocal Then
                m_xPublicDB = xPathWritable & "\ForteLocalPublic.mdb"

                If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                    Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                        "Could not find the file '" & m_xPublicDB & "'.")
                End If
            Else
                'try looking for pre-Forte DB instead
                m_xPublicDB = xPathWritable & "\mp10.mdb"

                If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                    If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                        'try looking for pre-Forte Admin DB instead
                        m_xPublicDB = xPathWritable & "\mp10Admin.mdb"

                        If Dir$(GetEnvironVarPath(m_xPublicDB)) = Nothing Then
                            Throw New Exception("Could not connect to " & m_xDisplayName & ".  " & _
                                "Could not find the database located in '" & xPathWritable & "'.")
                        End If

                    End If

                End If
            End If

            m_xPublicDB = GetEnvironVarPath(m_xPublicDB)

            'return connection string
            xCnn = GetConnectionString(xPath, xNetServer)

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("xPath = " & xPath & " : " & "xNetServer = " & xNetServer, _
                    GlobalMethods.g_xBackend & ".CCIBackend.Connect")
            End If

            'get TimeOut value from ini
            lTimeOut = lGetTimeOut()

            'GLOG : 5457 : ceh
            'Try to reach server before opening connection
            If m_bIsLocalDB Or oNetwork.bIsServerAvailable(xNetServer, , lTimeOut) Then
                On Error Resume Next
                m_oCnn.Open(xCnn)

                lErr = Err.Number
            Else
                lErr = ciErrs.ciErr_CouldNotExecuteSQL
            End If

            If lErr Then
                If Not m_bIsLocalDB Then
                    'try connecting locally
                    '            MsgBox "Could not connect to the network database.  The following error was returned:" & vbCr & vbCr & _
                    '            "#" & Err.Number & ": " & Err.Description & vbCr & vbCr & _
                    '            "The local database will be used instead.", vbInformation, g_oSession.AppTitle
                    'GLOG2 : 3187 : CEH
                    MsgBox("Could not retrieve the firm people list at this time.  You must be connected to the network to retrieve this list." & vbCr & vbCr & _
                    "Your Author List will be displayed instead.", vbInformation, GlobalMethods.g_oSession.AppTitle)

                    m_bIsLocalDB = True

                    If m_bIsForte Or m_bIsForteTools Or m_bIsForteLocal Then
                        xCnn = "Provider=" & General.g_xLocalConnectionProvider & ";Data Source=" & GetEnvironVarPath(m_xPublicDB) & _
                           ";Jet OLEDB:System database=" & xPath & "\Forte.mdw;User ID=mp10ProgrammaticUser;Password=Fish4Mill"
                    Else
                        xCnn = "Provider=" & General.g_xLocalConnectionProvider & ";Data Source=" & GetEnvironVarPath(m_xPublicDB) & _
                           ";Jet OLEDB:System database=" & xPath & "\mp10.mdw;User ID=mp10ProgrammaticUser;Password=Fish4Mill"
                    End If

                    'clear error & try connecting again
                    Err.Clear()
                    m_oCnn.Open(xCnn)

                    lErr = Err.Number

                    If lErr Then
                        Throw New Exception("Could not connect to the MacPac 10 database.  " & Err.Description)
                    Else
                        Exit Sub
                    End If
                End If

                Throw New Exception("Could not connect to the Forte database.  " & Err.Description)

            End If

            'GLOG2 : 7929 : ceh
            If m_bIsForte Or m_bIsForteTools Or m_bIsForteLocal Then
                GlobalMethods.g_xBackend = "CIForte"
            Else
                GlobalMethods.g_xBackend = "CIMP10"
            End If

            oNetwork = Nothing
        End Sub

        Public Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            Dim xToken As String
            Dim iPosStart As Integer
            Dim iPosEnd As Integer
            Dim xValue As String

            Try
                xValue = Nothing
                xToken = Nothing

                iPosStart = InStr(xPath, "<")
                iPosEnd = InStr(xPath, ">")

                If (iPosStart > 0) And (iPosEnd > 0) Then
                    xToken = Mid$(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)

                    If UCase$(xToken) = "USERNAME" Then
                        'we don't use environ for username
                        'because it's not an environment variable
                        'in Windows 98
                        xValue = GetLogonName()
                    ElseIf UCase$(xToken) = "COMMON_DOCUMENTS" Then
                        xValue = GetCommonDocumentsPath()
                    Else
                        xValue = Environ(xToken)
                    End If
                End If

                If xValue <> "" Then
                    GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
                Else
                    GetEnvironVarPath = xPath
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Public Function GetLogonName() As String
            'returns the current system user name
            Dim xBuf As New String(" "c, 255)

            'get logon name
            GlobalMethods.GetUserName(xBuf, 255)

            If Len(xBuf) = 0 Then
                'alert to no logon name
                Throw New Exception("UserName is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            'trim extraneous buffer chars
            Trim(xBuf)
            xBuf = RTrim$(xBuf)
            xBuf = Left$(xBuf, Len(xBuf) - 1)

            GetLogonName = xBuf
        End Function

        Public Function GetCommonDocumentsPath() As String
            'returns value for <COMMON_DOCUMENTS>
            Dim xBuf As New String(" "c, 255)

            'get common documents folder

            SHGetFolderPath(0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf)

            If Len(xBuf) = 0 Then
                'alert to no logon name
                Throw New Exception("Common_Documents is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   trim extraneous buffer chars
            xBuf = RTrim(xBuf)
            xBuf = Left(xBuf, Len(xBuf) - 1)

            '   return COMMON_DOCUMENTS value
            GetCommonDocumentsPath = xBuf
        End Function
        Private Function GetConnectionString(xPath As String, ByRef xNetServer As String) As String
            Dim xCnn As String
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim xTemp As String
            Dim iPos As Integer
            Dim xDBServer As String
            Dim xDBName As String

            m_bIsLocalDB = False
            xDBName = Nothing
            xDBServer = Nothing
            'get server & database name from local mdb
            'by getting value from keysets Table 'Firm Application Setting' record
            If m_bIsForte Or m_bIsForteTools Or m_bIsForteLocal Then
                xCnn = "Provider=" & General.g_xLocalConnectionProvider & ";Data Source=" & GetEnvironVarPath(m_xPublicDB) & _
                   ";Jet OLEDB:System database=" & xPath & "\Forte.mdw;User ID=mp10ProgrammaticUser;Password=Fish4Mill"
            Else
                xCnn = "Provider=" & General.g_xLocalConnectionProvider & ";Data Source=" & GetEnvironVarPath(m_xPublicDB) & _
                   ";Jet OLEDB:System database=" & xPath & "\mp10.mdw;User ID=mp10ProgrammaticUser;Password=Fish4Mill"
            End If

            'GLOG : 8259 : ceh
            If m_bIsForteLocal Then
                m_bIsLocalDB = True
            Else
                xSQL = "SELECT * FROM KeySets WHERE KeySets.Type = 1 AND " & _
                                                    "KeySets.ScopeID1 = 0 AND " & _
                                                    "KeySets.ScopeID2 = -999999999 AND " & _
                                                    "KeySets.OwnerID1 = -999999999 AND " & _
                                                    "KeySets.OwnerID2 = -999999999"

                oRS = New ADODB.Recordset

                With oRS
                    .Open(xSQL, xCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)

                    If Not oRS.EOF Then
                        'get Server name
                        xTemp = oRS.Fields("Values").Value
                        iPos = InStr(xTemp, "NetDBServer")
                        If iPos Then
                            xTemp = Mid(xTemp, iPos + 12, Len(xTemp))
                            iPos = InStr(xTemp, "|")
                            xDBServer = Mid(xTemp, 1, iPos - 1)
                        End If

                        'get DB name
                        xTemp = oRS.Fields("Values").Value
                        iPos = InStr(xTemp, "NetDBName")
                        If iPos Then
                            xTemp = Mid(xTemp, iPos + 10, Len(xTemp))
                            iPos = InStr(xTemp, "|")
                            xDBName = Mid(xTemp, 1, iPos - 1)
                        End If

                    End If
                End With
                oRS = Nothing

                If xDBName <> Nothing And xDBServer <> Nothing Then
                    xCnn = "Provider=SQLOLEDB;Data Source=" & xDBServer & ";Initial Catalog=" & xDBName & ";User ID=mp10User;Password=fish4wade"
                Else
                    MsgBox("Could not connect to the network database.  The local database will be used instead.", vbInformation, GlobalMethods.g_oSession.AppTitle)
                    m_bIsLocalDB = True
                End If

                xNetServer = xDBServer
            End If

            GetConnectionString = xCnn

        End Function

        Private Function lGetTimeOut() As Long
            Dim l As Long

            Try
                l = g_oIni.GetIni("Backend" & m_iID, "TimeOut", g_oIni.CIIni)
            Catch
            End Try

            lGetTimeOut = IIf(l <> 0, l, 2000)
        End Function

        Private Function GetFieldValue(oRS As ADODB.Recordset, xFieldName As String) As String
            Dim xValue As String = ""
            Try
                xValue = oRS.Fields(xFieldName).Value
            Catch
            End Try
            GetFieldValue = xValue
        End Function

        Private Function GetDetail(oC As CContact, xTemplate As String) As String
            'returns the contact detail in the format specified
            Dim xTemp As String
            Dim oCustFld As CCustomField
            Dim i As Integer
            Dim xFlds(,) As String

            xTemp = xTemplate

            With oC
                'name/value array not yet built - do it
                ReDim xFlds(31 + .CustomFields.Count, 1)

                xFlds(0, 0) = "<ADDITIONALINFORMATION>"
                xFlds(1, 0) = "<ADDRESSID>"
                xFlds(2, 0) = "<ADDRESSTYPENAME>"
                xFlds(3, 0) = "<CITY>"
                xFlds(4, 0) = "<COMPANY>"
                xFlds(5, 0) = "<COUNTRY>"
                xFlds(6, 0) = "<DEPARTMENT>"
                xFlds(7, 0) = "<DISPLAYNAME>"
                xFlds(8, 0) = "<EMAILADDRESS>"
                xFlds(9, 0) = "<FAX>"
                xFlds(10, 0) = "<FIRSTNAME>"
                xFlds(11, 0) = "<FULLNAME>"
                xFlds(12, 0) = "<FULLNAMEWITHPREFIX>"
                xFlds(13, 0) = "<FULLNAMEWITHSUFFIX>"
                xFlds(14, 0) = "<FULLNAMEWITHPREFIXANDSUFFIX>"
                xFlds(15, 0) = "<INITIALS>"
                xFlds(16, 0) = "<LASTNAME>"
                xFlds(17, 0) = "<MIDDLENAME>"
                xFlds(18, 0) = "<PHONE>"
                xFlds(19, 0) = "<PHONEEXTENSION>"
                xFlds(20, 0) = "<PREFIX>"
                xFlds(21, 0) = "<SALUTATION>"
                xFlds(22, 0) = "<STATE>"
                xFlds(23, 0) = "<STREET1>"
                xFlds(24, 0) = "<STREET2>"
                xFlds(25, 0) = "<STREET3>"
                xFlds(26, 0) = "<SUFFIX>"
                xFlds(27, 0) = "<TAG>"
                xFlds(28, 0) = "<TITLE>"
                xFlds(29, 0) = "<ZIPCODE>"
                xFlds(30, 0) = "<ZIP>"
                xFlds(31, 0) = "<STREET>"

                xFlds(0, 1) = .AdditionalInformation
                xFlds(1, 1) = .AddressID
                xFlds(2, 1) = .AddressTypeName
                xFlds(3, 1) = .City
                xFlds(4, 1) = .Company
                xFlds(5, 1) = .Country
                xFlds(6, 1) = .Department
                xFlds(7, 1) = .DisplayName
                xFlds(8, 1) = .EMailAddress
                xFlds(9, 1) = .Fax
                xFlds(10, 1) = .FirstName
                xFlds(11, 1) = .FullName
                xFlds(12, 1) = .FullNameWithPrefix
                xFlds(13, 1) = .FullNameWithSuffix
                xFlds(14, 1) = .FullNameWithPrefixAndSuffix
                xFlds(15, 1) = .Initials
                xFlds(16, 1) = .LastName
                xFlds(17, 1) = .MiddleName
                xFlds(18, 1) = .Phone
                xFlds(19, 1) = IIf(.PhoneExtension = Nothing, .PhoneExtension, "x" & .PhoneExtension)
                xFlds(20, 1) = .Prefix
                xFlds(21, 1) = .Salutation
                xFlds(22, 1) = .State
                xFlds(23, 1) = .Street1
                xFlds(24, 1) = .Street2
                xFlds(25, 1) = .Street3
                xFlds(26, 1) = .Suffix
                xFlds(27, 1) = .Tag
                xFlds(28, 1) = .Title
                xFlds(29, 1) = .ZipCode
                xFlds(30, 1) = .ZipCode
                xFlds(31, 1) = .Street1 & vbCr & .Street2 & vbCr & .Street3

                For Each oCustFld In .CustomFields
                    i = i + 1
                    xFlds(31 + i, 0) = "<" & UCase(oCustFld.Name) & ">"
                    xFlds(31 + i, 1) = oCustFld.Value
                Next oCustFld

            End With

skip:
            '   replace all detail fields with data
            Dim xReplacement As String
            For i = 0 To UBound(xFlds, 1)
                'if data is empty, replace with <EMPTY>-
                'this will be used later to delete blocks
                'specified to be deleted when the value of a
                'contact property is empty -
                'e.g. additional spaces, commas, etc.
                xReplacement = IIf(xFlds(i, 1) = Nothing, m_xTagEmpty, xFlds(i, 1))
                xTemp = Replace(xTemp, xFlds(i, 0), xReplacement)
            Next i

            xTemp = DeleteEMPTYBlocks(xTemp)

            'remove blank lines
            While InStr(xTemp, vbCr & vbCr) > 0
                xTemp = Replace(xTemp, vbCr & vbCr, vbCr)
            End While
            While InStr(xTemp, vbCrLf & vbCrLf) > 0
                xTemp = Replace(xTemp, vbCrLf & vbCrLf, vbCrLf)
            End While
            While InStr(xTemp, Chr(11) & Chr(11)) > 0
                xTemp = Replace(xTemp, Chr(11) & Chr(11), Chr(11))
            End While
            While InStr(xTemp, vbCrLf & vbCr) > 0
                xTemp = Replace(xTemp, vbCrLf & vbCr, vbCrLf)
            End While
            While InStr(xTemp, vbCrLf & Chr(11)) > 0
                xTemp = Replace(xTemp, vbCrLf & Chr(11), vbCrLf)
            End While
            While InStr(xTemp, Chr(11) & vbCr) > 0
                xTemp = Replace(xTemp, Chr(11) & vbCr, vbCr)
            End While
            While InStr(xTemp, Chr(11) & vbCrLf) > 0
                xTemp = Replace(xTemp, Chr(11) & vbCrLf, vbCrLf)
            End While
            While InStr(xTemp, vbCr & Chr(11)) > 0
                xTemp = Replace(xTemp, vbCr & Chr(11), vbCr)
            End While
            While InStr(xTemp, vbCr & vbCrLf) > 0
                xTemp = Replace(xTemp, vbCr & vbCrLf, vbCrLf)
            End While

            GetDetail = xTemp
        End Function


        Private Function DeleteEMPTYBlocks(ByVal xDetail As String) As String
            'delete blocks defined by {} that contain <EMPTY> tags-
            Dim iPosTag As Integer
            Dim iPosBlockStart As Integer
            Dim iPosBlockEnd As Integer
            Dim iPosNestedStartBrace As Integer
            Dim iPosNestedEndBrace As Integer
            Dim xTemp As String
            Dim xNestedBraces As String
            Dim xBlock As String

            xTemp = xDetail

            Do
                'search for <EMPTY> tag
                iPosTag = InStr(xTemp, m_xTagEmpty)

                If iPosTag > 0 Then
                    'empty tag exists - find surrounding
                    'block start and end i.e.{}
                    iPosBlockEnd = InStr(iPosTag, xTemp, "}", vbTextCompare)
                    iPosBlockStart = InStrRev(xTemp, "{", iPosTag, vbTextCompare)

                    If (iPosBlockStart > 0) And (iPosBlockEnd > 0) Then
                        'there exists a block to delete -
                        'get the block defined by the braces
                        xBlock = Mid$(xTemp, iPosBlockStart + 1, iPosBlockEnd - iPosBlockStart - 1)

                        'get positions of any nested braces in the block
                        iPosNestedStartBrace = InStr(xBlock, "{")
                        iPosNestedEndBrace = InStr(xBlock, "}")

                        If iPosNestedStartBrace > 0 Then
                            'there is a nested start brace
                            If iPosNestedEndBrace > 0 Then
                                'there's also a nested end brace -
                                'see which is first
                                If iPosNestedEndBrace < iPosNestedStartBrace Then
                                    'end brace before start brace
                                    xNestedBraces = "}{"
                                Else
                                    'start brace before end brace
                                    xNestedBraces = "{}"
                                End If
                            Else
                                'no end brace, just start brace
                                xNestedBraces = "{"
                            End If
                        ElseIf iPosNestedEndBrace > 0 Then
                            'end brace only
                            xNestedBraces = "}"
                        Else
                            xNestedBraces = Nothing
                        End If

                        'delete the block, keeping any nested braces
                        xTemp = Left$(xTemp, iPosBlockStart - 1) & _
                            xNestedBraces & Mid$(xTemp, iPosBlockEnd + 1)
                    ElseIf (iPosBlockStart = 0) Or (iPosBlockEnd = 0) Then
                        'no block exists - delete empty tag
                        xTemp = Left$(xTemp, iPosTag - 1) & _
                            Mid$(xTemp, iPosTag + Len(m_xTagEmpty))
                    Else
                        'FIX: raise appropriate error
                        Throw New Exception()
                    End If
                End If
                'cycle until there are no more empty tags
            Loop Until (iPosTag = 0)

            'replace remaining braces - they're not needed
            'as they define blocks that don't contain empty data
            xTemp = Replace(xTemp, "{", "")
            xTemp = Replace(xTemp, "}", "")
            xTemp = Trim$(xTemp)
            DeleteEMPTYBlocks = xTemp
        End Function

#End Region
#Region "*****************ICIBackend****************"
        Private ReadOnly Property ICIBackend_Exists() As Boolean Implements ICIBackend.Exists
            Get
                'set to always True for now
                Dim bExists As Boolean

                bExists = True

                ICIBackend_Exists = bExists

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Exists=" & bExists, _
                    GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_Exists")
                End If

            End Get
        End Property
        Private Sub ICIBackend_AddContact() Implements ICIBackend.AddContact
            Dim oWord As Word.Application

            Try
                oWord = GetObject(, "Word.Application")
            Catch
            End Try

            If Not (oWord Is Nothing) Then
                oWord.Run("zzmpAddAuthor")
            Else
                MsgBox("Word needs to be running in order to implement this feature.", _
                       vbInformation, GlobalMethods.g_oSession.AppTitle)
            End If

        End Sub

        Private ReadOnly Property ICIBackend_Col1Name() As String Implements ICIBackend.Col1Name
            Get
                ICIBackend_Col1Name = "Name"
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Col1Name=" & ICIBackend_Col1Name, _
                        GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_Col1Name")
                End If
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col2Name() As String Implements ICIBackend.Col2Name
            Get
                ICIBackend_Col2Name = "Office"
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Col2Name=" & ICIBackend_Col2Name, _
                        GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_Col2Name")
                End If
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col3Name() As String Implements ICIBackend.Col3Name
            Get
                ICIBackend_Col3Name = GlobalMethods.g_vCols(2, 0)
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Col3Name=" & ICIBackend_Col3Name, _
                        GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_Col1Name")
                End If
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col4Name() As String Implements ICIBackend.Col4Name
            Get
                ICIBackend_Col4Name = GlobalMethods.g_vCols(3, 0)
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Col4Name=" & ICIBackend_Col4Name, _
                        GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_Col4Name")
                End If
            End Get
        End Property

        Private Function ICIBackend_CustomMenuItem1() As String Implements ICIBackend.CustomMenuItem1
            ICIBackend_CustomMenuItem1 = Nothing
        End Function

        Private Function ICIBackend_CustomMenuItem2() As String Implements ICIBackend.CustomMenuItem2
            ICIBackend_CustomMenuItem2 = Nothing
        End Function

        Private Function ICIBackend_CustomMenuItem3() As String Implements ICIBackend.CustomMenuItem3
            ICIBackend_CustomMenuItem3 = Nothing
        End Function

        Private Function ICIBackend_CustomMenuItem4() As String Implements ICIBackend.CustomMenuItem4
            ICIBackend_CustomMenuItem4 = Nothing
        End Function

        Private Function ICIBackend_CustomMenuItem5() As String Implements ICIBackend.CustomMenuItem5
            ICIBackend_CustomMenuItem5 = Nothing
        End Function

        Private ReadOnly Property ICIBackend_DisplayName() As String Implements ICIBackend.DisplayName
            Get
                If m_xDisplayName = Nothing Then
                    'get from ini
                    Try
                        m_xDisplayName = g_oIni.GetIni("Backend" & m_iID, "Name", g_oIni.CIIni)
                    Catch
                    End Try

                    If m_xDisplayName = Nothing Then
                        'no value in ini - use default name
                        m_xDisplayName = ICIBackend_Name()
                    End If
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_DisplayName=" & m_xDisplayName, _
                        GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_DisplayName")
                End If

                ICIBackend_DisplayName = m_xDisplayName
            End Get
        End Property

        Private Sub ICIBackend_EditContact(oListing As CListing) Implements ICIBackend.EditContact
            Dim oWord As Word.Application

            Try
                oWord = GetObject(, "Word.Application")
            Catch
            End Try

            If Not (oWord Is Nothing) Then
                oWord.Run("zzmpEditAuthor", oListing.ID)
            Else
                MsgBox("Word needs to be running in order to implement this feature.", _
                       vbInformation, GlobalMethods.g_oSession.AppTitle)
            End If
        End Sub

        Private Function ICIBackend_Events() As CEventGenerator Implements ICIBackend.Events
            ICIBackend_Events = m_oEvents
        End Function

        Private Function ICIBackend_Filter() As CFilter Implements ICIBackend.Filter
            'returns the filter object for this backend
            Try
                If m_oFilter Is Nothing Then
                    CreateFilter()
                End If

                ICIBackend_Filter = m_oFilter
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function
        '        Friend Property ICIBackend_DefaultSortColumn As ciListingCols Implements ICIBackend.DefaultSortColumn
        '            Get
        '                On Error GoTo ProcError
        '                ICIBackend_DefaultSortColumn = ciListingCols.ciListingCols_DisplayName
        '                Exit Property
        'ProcError:
        '                GlobalMethods.g_oError.RaiseError(GlobalMethods.g_xBackend & ".CCIBackend.ICIBackend_DefaultSortColumn")
        '                Exit Property
        '            End Get
        '            Set(value As ciListingCols)
        '                m_iSortCol = value
        '            End Set
        '        End Property

        Private Function ICIBackend_GetAddresses(oListing As CListing) As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Try
                ICIBackend_GetAddresses = GetAddresses(oListing)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetAddresses() As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Try
                ICIBackend_GetAddresses = GetAddresses()
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object, _
                                                ByVal iIncludeData As ciRetrieveData, _
                                                ByVal iAlerts As ciAlerts) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, Nothing, iIncludeData, iAlerts)
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object, _
                                                ByVal iIncludeData As ciRetrieveData) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, Nothing, iIncludeData)
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress)
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress) As ICContacts Implements ICIBackend.GetContacts
            Try
                ICIBackend_GetContacts = GetContacts(oListing, oAddress)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function


        Private Function GetPublicDB() As String
            ''''returns the fully qualified file name of the macpac public db
            ''    Dim xPath As String
            ''    Static xPublicDB As String
            ''
            ''    On Error GoTo ProcError
            ''
            ''    If xPublicDB = Empty Then
            ''        xPath = g_oIni.ApplicationDirectoryMacPac10
            ''
            ''        If xPath = Empty Then
            ''            Err.Raise CIO.ciErr_CouldNotConnectToBackend, , "Could not connect to MacPac authors.  " & _
            ''                "Could not find location of Data Directory."
            ''        End If
            ''
            ''        xPublicDB = GetEnvironVarPath(xPath & "\Forte.mdb")
            ''
            ''        'deal with pre-Forte database
            ''        If Dir$(xPublicDB) = Empty Then
            ''            xPublicDB = GetEnvironVarPath(xPath & "\mp10.mdb")
            ''        End If
            ''
            ''        If Dir$(xPublicDB) = Empty Then
            ''            Err.Raise CIO.ciErr_CouldNotConnectToBackend, , "Could not connect to MacPac offices.  " & _
            ''                "Could not find the database in  '" & GetEnvironVarPath(xPath) & "'."
            ''        End If
            ''    End If
            ''    GetPublicDB = xPublicDB
            ''    Exit Function
            ''ProcError:
            ''    g_oError.RaiseError g_xBackend & ".CCIBackend.GetPublicDB"
            ''    Exit Function
            GetPublicDB = Nothing
        End Function
        '        Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields Implements ICIBackend.GetCustomFields
        '            'returns the custom fields for the specified listing
        '            On Error GoTo ProcError
        '            ICIBackend_GetCustomFields = GetCustomFields(oListing)
        '            Exit Function
        'ProcError:
        '            g_oError.RaiseError g_xBackend & ".CCIBackend.ICIBackend_GetCustomFields"
        '            Exit Function
        '        End Function

        Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields Implements ICIBackend.GetCustomFields
            'returns a custom fields collection with data for the specified listing
            Const mpThisFunction As String = "CIForte.CCIBackend.ICIBackend_GetCustomFields"
            Dim oCustFlds As CCustomFields
            Dim iIndex As Integer
            Dim i As Integer
            Dim xCustomFlds As String
            Dim xFlds(,) As String
            Dim xName As String
            Dim xID As String
            Dim iPos As Integer
            Dim oIni As CIni
            Dim oCustFld As CCustomField
            Dim vListingIDs As Object
            Dim lID As Long
            Dim lSourceID As Long
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim xOwnDirID As String
            Dim xOwnDirSourceID As String
            Dim vOwnerIDs As Object
            Dim oUNID As CUNID

            Try
                'create collection of custom fields with empty values
                oCustFlds = New CCustomFields

                'precity, postcity, stateabbr are required custom fields for this backend - add them here
                oCustFlds.Add("PreCity", "PreCity")
                oCustFlds.Add("PostCity", "PostCity")
                oCustFlds.Add("StateAbbr", "StateAbbr")

                i = 1

                'get custom fields from ini
                xCustomFlds = g_oIni.GetIni("Backend" & m_iID, "Custom" & i, g_oIni.CIIni)

                While xCustomFlds <> Nothing
                    'get position of sep in key
                    iPos = InStr(xCustomFlds, ",")

                    If iPos = 0 Then
                        'invalid ini key setup
                        Throw New Exception("Invalid key '" & "Backend" & m_iID & _
                            "\CustomField" & i & "' in ci.ini.")
                    Else
                        'get name and id of custom field
                        xName = Left(xCustomFlds, iPos - 1)
                        xID = Mid(xCustomFlds, iPos + 1)

                        If xName = Nothing Or xID = Nothing Then
                            'invalid ini setup
                            Throw New Exception("Invalid key '" & "Backend" & m_iID & _
                                "\CustomField" & i & "' in ci.ini.")
                        End If

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("xName=" & xName & ";xID=" & xID, mpThisFunction)
                        End If

                        'add to collection
                        oCustFlds.Add(xID, xName)
                    End If

                    'get next custom field from ini
                    i = i + 1
                    xCustomFlds = g_oIni.GetIni( _
                        "Backend" & m_iID, "Custom" & i, g_oIni.CIIni)
                End While

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oCustFlds.Count=" & oCustFlds.Count, mpThisFunction)
                End If

                If oCustFlds.Count = 0 Then
                    ICIBackend_GetCustomFields = Nothing
                    Exit Function
                End If

                Dim xCustFlds As String
                xCustFlds = Nothing

                'get custom fields
                For Each oCustFld In oCustFlds
                    xCustFlds = xCustFlds & oCustFld.ID & ","
                Next oCustFld

                'trim trailing ','
                xCustFlds = Left(xCustFlds, Len(xCustFlds) - 1)

                If m_bIsForteLocal Then
                    xSQL = "SELECT " & xCustFlds & " FROM addresses RIGHT JOIN " & _
                            "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
                Else
                    xSQL = "SELECT " & xCustFlds & " FROM addresses RIGHT JOIN " & _
                            "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
                End If

                oRS = New ADODB.Recordset
                oRS.Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)

                'cycle through custom fields, getting the
                'appropriate value from first record in recordset
                For Each oCustFld In oCustFlds
                    oCustFld.Value = Nothing
                    Try
                        oCustFld.Value = oRS(oCustFld.ID).Value
                    Catch
                    End Try
                Next oCustFld

                'return
                ICIBackend_GetCustomFields = oCustFlds
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function


        Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Object) As CContactNumber Implements ICIBackend.GetEMailNumber
            'retrieves the EMail number associated with the listing -
            'returns it as the sole item in the contact numbers collection-
            'this function ignores the parameter vEMailID
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xEMail As String = ""
            Dim oNum As CContactNumber

            Try
                oNum = Nothing

                'create empty collection of contact numbers
                oNums = New CContactNumbers

                'create sql statements
                If m_bIsForteLocal Then
                    xSQL = "SELECT EMail FROM PeoplePublic " & _
                            "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
                Else
                    xSQL = "SELECT EMail FROM people WHERE ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
                End If

                oRS = New ADODB.Recordset
                With oRS
                    .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                    If Not (.BOF And .EOF) Then

                        Try
                            xEMail = oRS.Fields("EMail").Value
                        Catch
                        End Try

                        If xEMail <> Nothing Then
                            oNums.Add(1, "EMail Address", xEMail, "", _
                                ciContactNumberTypes.ciContactNumberType_EMail, "EMail Address")
                        End If
                    End If
                    .Close()
                End With

                If Not oNums Is Nothing Then
                    'get the eaddress with the specified ID
                    oNum = oNums.Item(vEMailID)

                    'return
                    ICIBackend_GetEMailNumber = oNum
                End If

                ICIBackend_GetEMailNumber = oNum
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing, vAddressType As Object) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing, vAddressType)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing, vAddressType As Object) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            Try
                ICIBackend_GetFaxNumbers = GetFaxNumbers(oListing, vAddressType)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            Try
                ICIBackend_GetFaxNumbers = GetFaxNumbers(oListing)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumber(oListing As CListing, _
                                                 ByVal vFaxID As Object) As CContactNumber Implements ICIBackend.GetFaxNumber
            'retrieves the Fax number associated with the listing -
            'returns it as the sole item in the contact numbers collection
            'returns the personal fax if it exists, otherwise the office fax-
            'this function ignores the parameter vFaxID
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xFax As String = ""
            Dim oNum As CContactNumber

            Try
                oNum = Nothing

                'create empty collection of contact numbers
                oNums = New CContactNumbers

                If m_bIsForteLocal Then
                    xSQL = "SELECT PeoplePublic.fax,addresses.fax1 FROM addresses RIGHT JOIN " & _
                            "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
                Else
                    xSQL = "SELECT people.fax,addresses.fax1 FROM addresses RIGHT JOIN " & _
                            "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
                End If

                oRS = New ADODB.Recordset
                With oRS
                    .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                    If Not (.BOF And .EOF) Then
                        Try
                            xFax = IIf(IsNothing(oRS.Fields("Fax").Value), oRS.Fields("Fax1").Value, oRS.Fields("Fax").Value)
                        Catch
                        End Try

                        If xFax <> Nothing Then
                            oNums.Add(1, "Office Fax", xFax, "", _
                                ciContactNumberTypes.ciContactNumberType_Fax, "Office Fax")
                        End If
                    End If
                    .Close()
                End With

                If Not oNums Is Nothing Then
                    'get the fax with the specified ID
                    oNum = oNums.Item(vFaxID)

                    'return
                    ICIBackend_GetFaxNumber = oNum
                End If

                ICIBackend_GetFaxNumber = oNum
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder, oFilter As CFilter) As CListings Implements ICIBackend.GetFolderListings
            ICIBackend_GetFolderListings = Nothing
            Throw New Exception("ICIBackend_GetFolderListings is not implemented in CIForte.CCIBackend.")
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder) As CListings Implements ICIBackend.GetFolderListings
            ICIBackend_GetFolderListings = Nothing
            Throw New Exception("ICIBackend_GetFolderListings is not implemented in CIForte.CCIBackend.")
        End Function

        Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders Implements ICIBackend.GetFolders
            ICIBackend_GetFolders = Nothing
            Throw New Exception("ICIBackend_GetFolders is not implemented in CIForte.CCIBackend.")
        End Function

        Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Object) As CContactNumber Implements ICIBackend.GetPhoneNumber

            'retrieves the Phone number associated with the listing -
            'returns it as the sole item in the contact numbers collection-
            'returns the personal phone if it exists, otherwise the office phone-
            'this function ignores the parameter vPhoneID
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim oNums As CContactNumbers
            Dim xPhone As String = ""
            Dim oNum As CContactNumber

            Try
                'create empty collection of contact numbers
                oNums = New CContactNumbers
                oNum = Nothing

                If m_bIsForteLocal Then
                    xSQL = "SELECT PeoplePublic.phone,addresses.phone1 FROM addresses RIGHT JOIN " & _
                            "(offices RIGHT JOIN PeoplePublic ON PeoplePublic.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE (PeoplePublic.ID1 = " & oListing.ID & " AND PeoplePublic.ID2=0 AND PeoplePublic.UsageState=1 AND ((PeoplePublic.LinkedPersonID > 0) OR (PeoplePublic.DefaultOfficeRecordID1 Is Null OR PeoplePublic.DefaultOfficeRecordID1 = 0) OR (PeoplePublic.DefaultOfficeRecordID1 > 0 AND PeoplePublic.OfficeUsageState=1)))"
                Else
                    xSQL = "SELECT people.phone,addresses.phone1 FROM addresses IN RIGHT JOIN " & _
                            "(offices RIGHT JOIN people ON people.officeid=offices.id) ON addresses.id = offices.addressid " & _
                            "WHERE people.ID1 = " & oListing.ID & " AND people.ID2=0 AND people.UsageState=1 AND ((people.LinkedPersonID > 0) OR (people.DefaultOfficeRecordID1 Is Null OR people.DefaultOfficeRecordID1 = 0) OR (people.DefaultOfficeRecordID1 > 0 AND people.OfficeUsageState=1))"
                End If

                oRS = New ADODB.Recordset
                With oRS
                    .Open(xSQL, m_oCnn, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly)
                    If Not (.BOF And .EOF) Then

                        Try
                            xPhone = IIf(IsNothing(oRS.Fields("Phone").Value), oRS.Fields("Phone1").Value, oRS.Fields("Phone").Value)
                        Catch
                        End Try

                        If xPhone <> Nothing Then
                            oNums.Add(1, "Office Phone", xPhone, "", _
                                ciContactNumberTypes.ciContactNumberType_Phone, "Office Phone")
                        End If
                    End If
                    .Close()
                End With

                If Not oNums Is Nothing Then
                    'get the phone with the specified ID
                    oNum = oNums.Item(vPhoneID)

                    'return
                    ICIBackend_GetPhoneNumber = oNum
                End If
                ICIBackend_GetPhoneNumber = oNum
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, _
                                            ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers
            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing, vAddressType)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers
            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore, oFilter As CFilter) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore, oFilter)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore)
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStores() As CStores Implements ICIBackend.GetStores
            'returns the stores collection for macpac
            Dim oS As CStore
            Dim oSs As CStores

            Try
                oS = New CStore
                oSs = New CStores
                With oS
                    .UNID = GlobalMethods.ciMP10InternalID & GlobalMethods.g_oConstants.UNIDSep & "1"
                    .Name = "Forte Authors"
                End With

                oSs.Add(oS)

                ICIBackend_GetStores = oSs
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders Implements ICIBackend.GetSubFolders
            ICIBackend_GetSubFolders = Nothing
            Throw New Exception("ICIBackend_GetSubFolders is not implemented in CIForte.CCIBackend.")
        End Function

        Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean Implements ICIBackend.HasAddresses
            'all macpac authors are assigned to an office, hence they always have an address
            ICIBackend_HasAddresses = True
        End Function
        Private Property ICIBackend_ID() As Integer Implements ICIBackend.ID
            Set(value As Integer)
                m_iID = value
            End Set
            Get
                ICIBackend_ID = m_iID
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsLoadableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsLoadableEntity
            Get
                ICIBackend_IsLoadableEntity = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsSearchableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsSearchableEntity
            Get
                ICIBackend_IsSearchableEntity = True
            End Get
        End Property

        Private Function ICIBackend_SearchFolder(oFolder As CFolder, ByVal bCancel As Boolean) As CListings Implements ICIBackend.SearchFolder
            ICIBackend_SearchFolder = Nothing
            Throw New Exception("ICIBackend_SearchFolder is not implemented in CIForte.CCIBackend.")
        End Function

        Private Function ICIBackend_SearchNative() As CListings Implements ICIBackend.SearchNative
            ICIBackend_SearchNative = Nothing
            Throw New Exception("ICIBackend_SearchNative is not implemented in CIForte.CCIBackend.")
        End Function

        Private ReadOnly Property ICIBackend_SearchOperators() As ciSearchOperators Implements ICIBackend.SearchOperators
            Get
                ICIBackend_SearchOperators = ciSearchOperators.ciSearchOperator_BeginsWith + _
                    ciSearchOperators.ciSearchOperator_Contains + ciSearchOperators.ciSearchOperator_Equals
            End Get
        End Property

        Private Function ICIBackend_SearchStore(oStore As CStore) As CListings Implements ICIBackend.SearchStore
            Dim oFilter As CFilter

            Try
                oFilter = GetFilter()

                If Not (oFilter Is Nothing) Then
                    '       pass filter into function to get listings
                    ICIBackend_SearchStore = _
                        ICIBackend_GetStoreListings(oStore, oFilter)
                Else
                    ICIBackend_SearchStore = Nothing
                End If
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_SupportsContactAdd() As Boolean Implements ICIBackend.SupportsContactAdd
            Get
                ICIBackend_SupportsContactAdd = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsContactEdit() As Boolean Implements ICIBackend.SupportsContactEdit
            Get
                ICIBackend_SupportsContactEdit = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsFolders() As Boolean Implements ICIBackend.SupportsFolders
            Get
                ICIBackend_SupportsFolders = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsMultipleStores() As Boolean Implements ICIBackend.SupportsMultipleStores
            Get
                ICIBackend_SupportsMultipleStores = False
            End Get
        End Property

        Friend Property ICIBackend_DefaultSortColumn As ciListingCols Implements ICIBackend.DefaultSortColumn
            Get
                ICIBackend_DefaultSortColumn = GlobalMethods.g_iSortCol
            End Get
            Set(value As ciListingCols)
                Try
                    GlobalMethods.g_iSortCol = value
                    g_oIni.SetIni(GlobalMethods.g_xBackend & "", "SortColumn", CStr(value), g_oIni.CIUserIni())
                Catch ex As Exception
                    [LMP].[Error].Show(ex)
                End Try
            End Set
        End Property

        Private Sub ICIBackend_Initialize(iID As Integer) Implements ICIBackend.Initialize
            Dim xName As String
            Dim xKey As String
            Dim xID As String
            Dim i As Integer
            Dim iPos As Integer

            Try
                m_iID = iID
                xKey = g_oIni.GetIni("Backend" & iID, "Col1", g_oIni.CIIni)
                If xKey <> Nothing Then
                    MsgBox("Column 1 is reserved for display name in MacPac authors." & _
                        "The value '" & xKey & "' will be ignored." & vbCr & "Delete the 'Backend" & _
                        iID & "\Col1' key in the ci.ini to avoid seeing this message.", vbInformation)
                End If

                'get column names/ids
                For i = 2 To 4
                    xKey = g_oIni.GetIni("Backend" & iID, "Col" & i, g_oIni.CIIni)
                    If xKey <> Nothing Then
                        'parse value
                        iPos = InStr(xKey, ",")
                        If iPos = 0 Then
                            'no comma found - should be in form 'Name,ID'
                            Throw New Exception("Invalid value in ci.ini for Backend" & iID & _
                                "\Col" & i & ". Value must be in the form 'Name,ID'.")
                        Else
                            'get name and id from string
                            xName = Left$(xKey, iPos - 1)
                            xID = Mid$(xKey, iPos + 1)

                            If xName = Nothing Or xID = Nothing Then
                                'missing some value
                                Throw New Exception("Invalid value in ci.ini for Backend" & iID & _
                                    "\Col" & i & ".")
                            Else
                                'assign to columns array
                                GlobalMethods.g_vCols(i - 1, 0) = Trim$(xName)
                                GlobalMethods.g_vCols(i - 1, 1) = Trim$(xID)
                            End If
                        End If
                    End If
                Next i
            Catch ex As Exception
                [LMP].[Error].Show(ex)
            End Try
        End Sub

        Private ReadOnly Property ICIBackend_InternalID() As Integer Implements ICIBackend.InternalID
            Get
                ICIBackend_InternalID = GlobalMethods.ciMP10InternalID
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsConnected() As Boolean Implements ICIBackend.IsConnected
            Get
                ICIBackend_IsConnected = m_bIsConnected
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Name() As String Implements ICIBackend.Name
            Get
                ICIBackend_Name = "Firm Authors"

            End Get
        End Property

        Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat Implements ICIBackend.NumberPromptFormat
            ICIBackend_NumberPromptFormat = Me
        End Function

        Private ReadOnly Property ICIBackend_SupportsNativeSearch() As Boolean Implements ICIBackend.SupportsNativeSearch
            Get
                ICIBackend_SupportsNativeSearch = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNestedFolders() As Boolean Implements ICIBackend.SupportsNestedFolders
            Get
                ICIBackend_SupportsNestedFolders = False
            End Get
        End Property

        Private Sub ICIBackend_CustomProcedure1() Implements ICIBackend.CustomProcedure1
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure2() Implements ICIBackend.CustomProcedure2
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure3() Implements ICIBackend.CustomProcedure3
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure4() Implements ICIBackend.CustomProcedure4
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure5() Implements ICIBackend.CustomProcedure5
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

#End Region
    End Class
End Namespace