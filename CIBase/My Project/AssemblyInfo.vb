﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Contact Integration Base")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("The Sackett Group, Inc.")> 
<Assembly: AssemblyProduct("Contact Integration")> 
<Assembly: AssemblyCopyright("©1990 - 2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1bd13ae5-9512-4bc6-b663-15e8567ea861")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.0015")> 
<Assembly: AssemblyFileVersion("3.0.0.0015")> 
