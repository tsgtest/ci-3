﻿Option Explicit On

Imports System
Imports System.Collections

Namespace TSG.CI

    Public Enum ciSessionType
        ciSession_CI = 1
        ciSession_Connect = 2
    End Enum

    Public Enum ciContactTypes
        ciContactType_To = 1
        ciContactType_From = 2
        ciContactType_CC = 4
        ciContactType_BCC = 8
        ciContactType_Other = 16
    End Enum

    Public Enum ciRetrieveData
        ciRetrieveData_None = 0
        ciRetrieveData_All = 1
        ciRetrieveData_Names = 2
        ciRetrieveData_Addresses = 4
        ciRetrieveData_Phones = 8
        ciRetrieveData_Faxes = 16
        ciRetrieveData_EAddresses = 32
        ciRetrieveData_CustomFields = 64
        ciRetrieveData_Salutation = 128
    End Enum

    Public Enum ciAlerts
        ciAlert_None = 0
        ciAlert_All = 1
        ciAlert_MultiplePhones = 2
        ciAlert_NoPhones = 4
        ciAlert_MultipleFaxes = 8
        ciAlert_NoFaxes = 16
        ciAlert_MultipleEAddresses = 32
        ciAlert_NoEAddresses = 64
        ciAlert_NoAddresses = 128
    End Enum

    Public Interface ICSession
        ReadOnly Property Initialized As Boolean
        Property SessionType As ciSessionType
        Function GetContactFromUNID(ByVal xUNID As String, _
                                    ByVal iIncludeData As ciRetrieveData,
                                    ByVal iAlerts As ciAlerts) As ICContact
        Function GetContactsEx(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
                               Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
                               Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
                               Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
                               Optional ByVal DefaultSelectionList As ICI.ciSelectionlists = ICI.ciSelectionlists.ciSelectionList_To, _
                               Optional ByVal MaxContacts As Integer = 0, _
                               Optional ByVal Alerts As ciAlerts = ciAlerts.ciAlert_All, _
                               Optional ByVal xLabelTo As String = "", _
                               Optional ByVal xLabelFrom As String = "", _
                               Optional ByVal xLabelCC As String = "", _
                               Optional ByVal xLabelBCC As String = "", _
                               Optional ByVal lMaxTo As Long = 0, _
                               Optional ByVal lMaxFrom As Long = 0, _
                               Optional ByVal lMaxCC As Long = 0, _
                               Optional ByVal lMaxBCC As Long = 0) As ICContacts
        Function GetContactsFromArray(ByRef oArray As System.Array, _
                                      Optional xSep As String = "|") As ICContacts
        Function GetContacts_Custom(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal DefaultSelectionList As ICI.ciSelectionlists = ICI.ciSelectionlists.ciSelectionList_To, _
            Optional ByVal MaxContacts As Integer = 0, _
            Optional ByVal Alerts As ciAlerts = ciAlerts.ciAlert_All, _
            Optional ByVal oDataTable As DataTable = Nothing) As ICContacts
        Property FormLeft() As Single
        Property FormTop() As Single
    End Interface

    Public Interface ICContact
        Function GetDetail(xDetail As String, Optional bRemoveBlankLines As Boolean = True) As String
        Property UNID As Object
        Property ContactType As ciContactTypes
        Property AddressTypeName As String
        Property Country As String
        Property CoreAddress As String
        Property DisplayName As String
        Property Prefix As String
        Property FirstName As String
        Property MiddleName As String
        Property LastName As String
        Property Suffix As String
        Property Initials As String
        Property Street1 As String
        Property Street2 As String
        Property Street3 As String
        Property City As String
        Property State As String
        Property ZipCode As String
        Property Company As String
        Property Department As String
        Property Title As String
        Property EmailAddress As String
        Property Phone As String
        Property PhoneExtension As String
        Property Fax As String
        Property Tag As Object
        Property AdditionalInformation As String
        Property FullName As String
        Property Salutation As String
        Property CustomTypeName As String
        Property CustomFields As ICCustomFields
    End Interface

    Public Interface ICContacts
        Function Item(oIndex As Object) As ICContact
        Sub Add(oContact As ICContact, Optional Before As Long = 0)
        Function Count() As Integer
        Sub CreateDataFile(ByVal xFile As String)
    End Interface

    Public Interface ICCustomField
        Property ID As Object
        Property Name As String
        Property Value As String
    End Interface

    Public Interface ICCustomFields
        Inherits IEnumerable
        Default ReadOnly Property Item(vKey As Object) As ICCustomField
        Function Count() As Integer
        Function Add(ByVal vId As Object, _
                     ByVal xName As String, _
                     Optional ByVal xValue As String = "") As ICCustomField
        Function Delete(vKey As Object)
    End Interface

    Public Interface ICI
        Enum ciSelectionlists As Integer
            ciSelectionList_To = 1
            ciSelectionList_From = 2
            ciSelectionList_CC = 4
            ciSelectionList_BCC = 8
            ciSelectionList_Custom = 16
        End Enum
    End Interface

    Public Interface ICError
        Sub RaiseError(ByVal xNewSource As String)
        Sub ShowError()
    End Interface

End Namespace
