﻿Option Explicit On

Imports System.Windows.Forms
Imports System.Collections
Imports System.IO
Imports LMP

Namespace TSG.CI
    Public Class CI
#Region "**************fields*******************"

        Private Declare Function GetTickCount Lib "kernel32" () As Long

        Enum CIPanels
            ciPanel_folders = 1
            ciPanel_Contacts = 2
            ciPanel_Groups = 3
        End Enum

        Enum ciSearchTypes
            ciSearchType_None = 0
            ciSearchType_Dialog = 1
            ciSearchType_Native = 2
            ciSearchType_Quick = 3
            ciSearchType_Refresh = 4
        End Enum

        Private Enum ciListBoxes
            ciListBoxes_Listings = -1
            ciListBoxes_To = 0
            ciListBoxes_From = 1
            ciListBoxes_CC = 2
            ciListBoxes_BCC = 3
            ciListBoxes_GroupMembers = 4
        End Enum

        Public Enum cixErrs
            cixErr_ObjectExpected = VariantType.Error + 3512 + 1
        End Enum

        Private Const ciSecsPerDay As Long = 86400
        Private Const ciUserIniKey_LoadDefaultOnStartup As String = "LoadDefaultOnStartup"
        Private Const ciUserIniKey_DefaultNode As String = "DefaultNode"
        Private Const ciListingsNarrow As Long = 268
        Private Const ciUserIniKey_DefaultQuickFilterType As String = "DefaultQuickFilterType"

        Private m_oListings As CListings
        Private m_oAddresses As CAddresses
        Private m_oContacts As CContacts
        Private m_oUNID As CUNID
        Private m_oEntityList As DataTable
        Private WithEvents m_oEvents As CEventGenerator
        Private m_iDataTo As ciRetrieveData
        Private m_iDataFrom As ciRetrieveData
        Private m_iDataCC As ciRetrieveData
        Private m_iDataBCC As ciRetrieveData
        Private m_bShowBackendNodes As Boolean
        Private m_iAlerts As ciAlerts
        Private m_iSelectionLists As ICI.ciSelectionlists
        Private m_bMouseDown As Boolean
        Private m_bLoadCancelled As Boolean
        Private m_iMaxContacts As Integer
        Private m_bInitialized As Boolean
        Private m_iCurList As ciListBoxes
        Private m_bShowedContacts As Boolean
        Private m_xCustomFunction1 As String
        Private m_xCustomFunction2 As String
        Private m_bMenuRequested As Boolean
        Private m_lMaxTo As Long
        Private m_lMaxFrom As Long
        Private m_lMaxCC As Long
        Private m_lMaxBCC As Long
        Private m_bIsExpanding As Boolean
        Private m_bCancelled As Boolean
        Private m_dtMembers As DataTable
        Private m_ProgressForm As ProgressBoxForm
        Private m_SelectedRowID As Object

        Public Event InsertionCancelled()
        Public Event InsertionOK(oContacts As CContacts)

        Private WithEvents m_oMenuArray As HelpMenuArray

        'GLOG : 8819 : ceh
        'Public Enum ciSelectionLists
        '    ciSelectionList_To = 1
        '    ciSelectionList_From = 2
        '    ciSelectionList_CC = 4
        '    ciSelectionList_BCC = 8
        '    ciSelectionList_Custom = 16
        'End Enum

        Public Enum ciQuickFilterType
            ciQuickFilterType_Text = 1
            ciQuickFilterType_AlphaNumeric = 2
        End Enum

        Public g_iQuickFilterType As ciQuickFilterType
        Public g_iQuickFilterBtnIndex As Integer
        Public g_iQuickFilterOptionIndex As Integer

        'control arrays
        Dim m_lstContacts(3) As ListBox
        Dim m_lblContacts(3) As Label
        Dim m_btnDelete(4) As Button
        Dim m_btnAdd(4) As Button
        Dim m_optQuickFilter(3) As RadioButton
        Dim m_tsBFilter(26) As ToolStripButton
        Dim m_tsCustomMenuItem(4) As ToolStripMenuItem

        Private m_bCancelKeyPress As Boolean

#End Region
#Region "**************initializer***************"
        Public Sub New()
            Dim i As Integer

            'This call is required by the designer.
            InitializeComponent()

            'initialize control arrays
            m_lstContacts(0) = Me.lstTo
            m_lstContacts(1) = Me.lstFrom
            m_lstContacts(2) = Me.lstCC
            m_lstContacts(3) = Me.lstBCC

            m_lblContacts(0) = Me.lblTo
            m_lblContacts(1) = Me.lblFrom
            m_lblContacts(2) = Me.lblCC
            m_lblContacts(3) = Me.lblBCC

            m_btnDelete(0) = Me.btnDeleteTo
            m_btnDelete(1) = Me.btnDeleteFrom
            m_btnDelete(2) = Me.btnDeleteCC
            m_btnDelete(3) = Me.btnDeleteBCC
            m_btnDelete(4) = Me.btnDeleteEntity

            m_btnAdd(0) = Me.btnAddTo
            m_btnAdd(1) = Me.btnAddFrom
            m_btnAdd(2) = Me.btnAddCC
            m_btnAdd(3) = Me.btnAddBCC
            m_btnAdd(4) = Me.btnAddEntity

            m_optQuickFilter(0) = Me.optQuickFilter0
            m_optQuickFilter(1) = Me.optQuickFilter1
            m_optQuickFilter(2) = Me.optQuickFilter2
            m_optQuickFilter(3) = Me.optQuickFilter3

            m_tsCustomMenuItem(0) = Me.mnuFolders_Custom1
            m_tsCustomMenuItem(1) = Me.mnuFolders_Custom2
            m_tsCustomMenuItem(2) = Me.mnuFolders_Custom3
            m_tsCustomMenuItem(3) = Me.mnuFolders_Custom4
            m_tsCustomMenuItem(4) = Me.mnuFolders_Custom5

            For i = 0 To 26
                m_tsBFilter(i) = Me.tsQuickFilter.Items("tsBFilter" & i.ToString())
            Next
        End Sub
#End Region
#Region "**************properties**********************"
        Public Property MaxContacts() As Short
            Get
                MaxContacts = m_iMaxContacts
            End Get
            Set(ByVal Value As Short)
                m_iMaxContacts = Value
            End Set
        End Property

        Public Property OKButtonCaption() As String
            Get
                OKButtonCaption = Me.cmdOK.Text
            End Get
            Set(ByVal Value As String)
                Me.cmdOK.Text = Value
            End Set
        End Property

        Public Property MaxTo() As Integer
            Get
                MaxTo = m_lMaxTo
            End Get
            Set(ByVal Value As Integer)
                m_lMaxTo = Value
            End Set
        End Property

        Public Property MaxFrom() As Integer
            Get
                MaxFrom = m_lMaxFrom
            End Get
            Set(ByVal Value As Integer)
                m_lMaxFrom = Value
            End Set
        End Property

        Public Property MaxCC() As Integer
            Get
                MaxCC = m_lMaxCC
            End Get
            Set(ByVal Value As Integer)
                m_lMaxCC = Value
            End Set
        End Property

        Public Property MaxBCC() As Integer
            Get
                MaxBCC = m_lMaxBCC
            End Get
            Set(ByVal Value As Integer)
                m_lMaxBCC = Value
            End Set
        End Property

        Public ReadOnly Property Cancelled() As Boolean
            Get
                Cancelled = m_bCancelled
            End Get
        End Property

        Public Property SelectionList() As ICI.ciSelectionlists
            Get
                'returns the current selection list - i.e. the
                'list that currently receives double-clicked listings

                Try
                    With Me
                        If .btnAddFrom.Tag = "Default" Then
                            SelectionList = ICI.ciSelectionlists.ciSelectionList_From
                        ElseIf .btnAddCC.Tag = "Default" Then
                            SelectionList = ICI.ciSelectionlists.ciSelectionList_CC
                        ElseIf .btnAddBCC.Tag = "Default" Then
                            SelectionList = ICI.ciSelectionlists.ciSelectionList_BCC
                        ElseIf .btnAddEntity.Tag = "Default" Then
                            SelectionList = ICI.ciSelectionlists.ciSelectionList_Custom
                        Else
                            SelectionList = ICI.ciSelectionlists.ciSelectionList_To
                        End If
                    End With
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(ByVal Value As ICI.ciSelectionlists)
                'sets up dialog to highlight new selection list
                Dim i As Short
                Dim iSelListBoxIndex As Short
                Dim iPrevSelListBoxIndex As Short
                Static iPrevSelList As Short


                Try
                    If Value = ICI.ciSelectionlists.ciSelectionList_Custom Then
                        Exit Property
                    End If

                    If iPrevSelList = Value Then
                        'selection list has not changed - exit
                        Exit Property
                    ElseIf (Value And SelectionLists) = 0 Then
                        'the selection list that has been requested
                        'to be the default selection list is not
                        'among the selection lists that are displayed
                        'in the dialog
                        Exit Property
                    End If

                    'get list box that corresponds to selection list
                    Select Case Value
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            iSelListBoxIndex = 1
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            iSelListBoxIndex = 2
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            iSelListBoxIndex = 3
                        Case Else
                            iSelListBoxIndex = 0
                    End Select

                    'get list box that corresponds to
                    'the previous selection list
                    Select Case iPrevSelList
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            iPrevSelListBoxIndex = 1
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            iPrevSelListBoxIndex = 2
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            iPrevSelListBoxIndex = 3
                        Case Else
                            iPrevSelListBoxIndex = 0
                    End Select

                    With Me
                        'todo:unnecessary?
                        ' ''If iPrevSelList > 0 Then
                        ' ''    'remove all selections from previous listbox
                        ' ''    With .lstContacts(iPrevSelListBoxIndex)
                        ' ''        For i = 0 To .ListCount - 1
                        ' ''            .Selected(i) = False
                        ' ''        Next
                        ' ''    End With

                        ' ''    'change previous list label color to black
                        ' ''    .lblSelContacts.Item(iPrevSelListBoxIndex).ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.SystemColors.ControlText)
                        ' ''End If

                        'ensure that the appropriate
                        'add button is made the default
                        'GLOG : 8750 : ceh
                        m_btnAdd(i).Tag = "Default"

                        '' ''set new selection list label color to blue
                        ' ''.lblSelContacts.Item(iSelListBoxIndex).ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue)

                        '' ''enable/disable delete buttons
                        ' ''For i = 0 To 3
                        ' ''    .btnDelete.Item(i).Enabled = (iSelListBoxIndex = i) And (.lstContacts(i).ListCount > 0) And (.lstContacts(i).ListIndex > -1)
                        ' ''Next i

                        '' ''move up down buttons into correct position
                        ' ''With Me
                        ' ''    .btnMoveUp.Top = .lstContacts(iSelListBoxIndex).Top - .btnMoveUp.Height
                        ' ''    .btnMoveDown.Top = .btnMoveUp.Top
                        ' ''    .btnMoveUp.Visible = True
                        ' ''    .btnMoveDown.Visible = True
                        ' ''End With

                        iPrevSelList = Value
                    End With
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Set
        End Property

        Public Property LabelTo() As String
            Get
                LabelTo = Me.lblTo.Text
            End Get
            Set(ByVal Value As String)
                Me.lblTo.Text = Value
            End Set
        End Property

        Public Property LabelFrom() As String
            Get
                LabelFrom = Me.lblFrom.Text
            End Get
            Set(ByVal Value As String)
                Me.lblFrom.Text = Value
            End Set
        End Property

        Public Property LabelCC() As String
            Get
                LabelCC = Me.lblCC.Text
            End Get
            Set(ByVal Value As String)
                Me.lblCC.Text = Value
            End Set
        End Property

        Public Property LabelBCC() As String
            Get
                LabelBCC = Me.lblBCC.Text
            End Get
            Set(ByVal Value As String)
                Me.lblBCC.Text = Value
            End Set
        End Property

        Public Property ShowBackendNodes() As Boolean
            Get
                ShowBackendNodes = m_bShowBackendNodes
            End Get
            Set(ByVal Value As Boolean)
                m_bShowBackendNodes = Value
            End Set
        End Property


        Public Property DataRetrievedForTo() As ciRetrieveData
            Get
                DataRetrievedForTo = m_iDataTo
            End Get
            Set(ByVal Value As ciRetrieveData)
                m_iDataTo = Value
            End Set
        End Property


        Public Property DataRetrievedForFrom() As ciRetrieveData
            Get
                DataRetrievedForFrom = m_iDataFrom
            End Get
            Set(ByVal Value As ciRetrieveData)
                m_iDataFrom = Value
            End Set
        End Property


        Public Property DataRetrievedForCC() As ciRetrieveData
            Get
                DataRetrievedForCC = m_iDataCC
            End Get
            Set(ByVal Value As ciRetrieveData)
                m_iDataCC = Value
            End Set
        End Property


        Public Property DataRetrievedForBCC() As ciRetrieveData
            Get
                DataRetrievedForBCC = m_iDataBCC
            End Get
            Set(ByVal Value As ciRetrieveData)
                m_iDataBCC = Value
            End Set
        End Property


        Public Property Alerts() As ciAlerts
            Get
                Alerts = m_iAlerts
            End Get
            Set(ByVal Value As ciAlerts)
                m_iAlerts = Value
            End Set
        End Property


        Public Property EntityList() As DataTable
            Get
                EntityList = m_oEntityList
            End Get
            Set(ByVal Value As DataTable)
                m_oEntityList = Value
            End Set
        End Property


        Public Property SelectionLists() As ICI.ciSelectionlists
            Get
                SelectionLists = m_iSelectionLists
            End Get
            Set(ByVal Value As ICI.ciSelectionlists)
                m_iSelectionLists = Value
                SetSelectionLists(Value)
            End Set
        End Property

        Public ReadOnly Property SelectedContacts() As CContacts
            Get
                SelectedContacts = m_oContacts
            End Get
        End Property

        Private ReadOnly Property SelectionListControl() As ListBox
            Get
                Try
                    Select Case Me.SelectionList
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            SelectionListControl = Me.lstFrom
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            SelectionListControl = Me.lstCC
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            SelectionListControl = Me.lstBCC
                        Case Else
                            SelectionListControl = Me.lstTo
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
#End Region
#Region "**************shared events****************"
        Private Sub optQuickFilter_Click(sender As Object, e As EventArgs) _
            Handles optQuickFilter0.Click, optQuickFilter1.Click, optQuickFilter2.Click, optQuickFilter3.Click

            Try
                Dim ctl As RadioButton = DirectCast(sender, RadioButton)

                For i = 0 To 3
                    If m_optQuickFilter(i) Is ctl Then
                        g_iQuickFilterOptionIndex = i
                        Exit For
                    End If
                Next
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tsBFilter_Click(sender As Object, e As EventArgs) _
            Handles tsBFilter0.Click, tsBFilter1.Click, tsBFilter2.Click, tsBFilter3.Click,
                    tsBFilter4.Click, tsBFilter5.Click, tsBFilter6.Click, tsBFilter7.Click, tsBFilter8.Click,
                    tsBFilter9.Click, tsBFilter10.Click, tsBFilter11.Click, tsBFilter12.Click, tsBFilter13.Click,
                    tsBFilter14.Click, tsBFilter15.Click, tsBFilter16.Click, tsBFilter17.Click, tsBFilter18.Click,
                    tsBFilter19.Click, tsBFilter20.Click, tsBFilter21.Click, tsBFilter22.Click, tsBFilter23.Click,
                    tsBFilter24.Click, tsBFilter25.Click, tsBFilter26.Click

            Try
                Dim ctl As ToolStripButton = DirectCast(sender, ToolStripButton)

                For i = 0 To 26
                    If m_tsBFilter(i) Is ctl Then
                        g_iQuickFilterBtnIndex = i
                        Exit For
                    End If
                Next
                'GLOG : 8675 : ceh
                Me.pnlQuickFilter.Enabled = False
                LoadContacts(ciSearchTypes.ciSearchType_Quick)
                Me.pnlQuickFilter.Enabled = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstContacts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                      Handles lstTo.Click, lstBCC.Click, lstCC.Click, lstFrom.Click,
                                              lstBCC.SelectedIndexChanged, lstCC.SelectedIndexChanged, lstTo.SelectedIndexChanged, lstFrom.SelectedIndexChanged
            Dim i As Integer

            Try
                Dim lst As ListBox = DirectCast(sender, ListBox)

                With lst
                    For i = 0 To 3
                        If m_lstContacts(i) Is lst Then
                            m_btnDelete(i).Enabled = .Items.Count > 0
                            Exit Sub
                        End If
                    Next
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstContacts_GotFocus(sender As Object, e As EventArgs) _
                                        Handles lstTo.GotFocus, lstFrom.GotFocus, lstCC.GotFocus, lstBCC.GotFocus
            Dim lst As ListBox = DirectCast(sender, ListBox)
            Dim i As Integer


            Try
                'get index of seletected Contacts list
                For i = 0 To 3
                    If lst Is m_lstContacts(i) Then
                        Exit For
                    End If
                Next

                'mark that this contact list is the current list
                m_iCurList = i

                'enable irrelevant menu items
                Me.mnuContacts_MoveSelectedContactsDown.Enabled = True
                Me.mnuContacts_MoveSelectedContactsUp.Enabled = True

                'set the selection list to correspond to
                'the list box that is being clicked on
                SelectionList = SelListFromControlIndex(i)

                Me.grdListings.ClearSelection()
                lst.Focus()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstContacts_KeyDown(sender As Object, e As KeyEventArgs) _
                                        Handles lstTo.KeyDown, lstFrom.KeyDown, lstCC.KeyDown, lstBCC.KeyDown

            Try
                m_bCancelKeyPress = False

                If Main.IsPressed(Keys.ControlKey) Then
                    If e.Control + e.KeyCode = Keys.A Then
                        Application.DoEvents()
                        SelectAllContacts()
                        m_bCancelKeyPress = True
                    ElseIf e.KeyCode = Keys.Down Then
                        MoveSelContactDown()
                        m_bCancelKeyPress = True
                    ElseIf e.KeyCode = Keys.Up Then
                        MoveSelContactUp()
                        m_bCancelKeyPress = True
                    End If
                Else
                    If e.KeyCode = Keys.Delete Then
                        DeleteSelContacts()
                        m_bCancelKeyPress = True
                    ElseIf Not (e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up) Then
                        m_bCancelKeyPress = True
                    End If
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstContacts_KeyPress(sender As Object, e As KeyPressEventArgs)

            Try
                e.Handled = m_bCancelKeyPress
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Private Sub lstContacts_MouseDown(sender As Object, e As MouseEventArgs) _
                                         Handles lstTo.MouseDown, lstFrom.MouseDown, lstCC.MouseDown, lstBCC.MouseDown
            Dim sCtlTop As Single
            Dim iNewIndex As Integer
            Dim i As Integer

            Try
                'get list clicked
                Dim lst As ListBox = DirectCast(sender, ListBox)

                m_bMouseDown = True

                With lst
                    If .Items.Count = 0 Then
                        Exit Sub
                    Else
                        .Focus()
                        If e.Button = MouseButtons.Right Then
                            'delete previous selections - select item - show detail
                            sCtlTop = .Top
                            iNewIndex = Int(e.Y / 220) + .TopIndex
                            For i = 0 To .Items.Count - 1
                                .SetSelected(i, False)
                            Next i

                            If iNewIndex > .Items.Count - 1 Then
                                iNewIndex = .Items.Count - 1
                            End If

                            .SetSelected(iNewIndex, True)
                            Application.DoEvents()

                            If .SelectedIndex > -1 Then
                                ShowContactDetail(.SelectedValue)
                            End If
                        End If
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstContacst_MouseUp(sender As Object, e As MouseEventArgs) _
                                        Handles lstTo.MouseUp, lstFrom.MouseUp, lstCC.MouseUp, lstBCC.MouseUp
            m_bMouseDown = False
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) _
                                  Handles btnAddTo.Click, btnAddFrom.Click, btnAddCC.Click, btnAddBCC.Click, btnAddEntity.Click
            Dim btn As Button = DirectCast(sender, Button)

            Try
                With btn
                    If Not .Visible Then
                        Exit Sub
                    End If
                    Me.AcceptButton = btn
                    For i = 0 To 4
                        If (m_btnAdd(i) Is btn) Then
                            btn.Tag = "Default"
                        Else
                            m_btnAdd(i).Tag = ""
                        End If
                    Next
                End With
                AddSelContacts()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnDelete_Click(sender As Object, e As EventArgs) _
                                   Handles btnDeleteTo.Click, btnDeleteFrom.Click, btnDeleteCC.Click, btnDeleteBCC.Click, btnDeleteEntity.Click

            Try
                'delete the selected contact in the specified list
                Dim btn As Button = DirectCast(sender, Button)

                'test for Entities
                If btn Is m_btnDelete(4) Then
                    DeleteSelContacts_Custom()
                Else
                    'GLOG : 8750 : ceh
                    For i = 0 To 4
                        If (m_btnDelete(i) Is btn) Then
                            m_btnAdd(i).Tag = "Default"
                        Else
                            m_btnAdd(i).Tag = ""
                        End If
                    Next
                    DeleteSelContacts()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnDelete_KeyDown(sender As Object, e As KeyEventArgs) _
                     Handles btnAddTo.KeyDown, btnAddFrom.KeyDown, btnAddCC.KeyDown, btnAddBCC.KeyDown

            Try
                If e.KeyCode = Keys.Delete Then
                    DeleteSelContacts()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "**************toolbar/menu events***************"
        Private Sub mnuContacts_AddGroup_Click(sender As Object, e As EventArgs) Handles mnuContacts_AddGroup.Click
            AddGroup()
        End Sub

        Private Sub mnuContacts_DeleteGroup_Click1(sender As Object, e As EventArgs) Handles mnuContacts_DeleteGroup.Click
            DeleteGroup()
        End Sub

        Private Sub mnuFolders_ToggleQuickFilterType_Click(sender As Object, e As EventArgs) Handles mnuFolders_ToggleQuickFilterType.Click
            Try
                If g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text Then
                    g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_AlphaNumeric
                Else
                    g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text
                End If
                SetQuickFilterType()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub mnuFolders_RemoveDefaultFolder_Click(sender As Object, e As EventArgs) Handles mnuFolders_RemoveDefaultFolder.Click,
                                                                                                   tvFoldersContextMenuRemoveDefault.Click
            Try
                Main.SetUserIni("CIApplication", ciUserIniKey_DefaultNode, String.Empty)
                Me.mnuFolders_LoadCurrentFolderOnStartup.Checked = False
                Me.tvFoldersContextMenuLoadOnStartup.Checked = False
                Main.SetUserIni("CIApplication", ciUserIniKey_LoadDefaultOnStartup, False)
                Me.btnOptionsFolders.ToolTipText = "Default Folder: None specified"
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub mnuFolders_SelectCurrentFolderOnStartup_Click(sender As Object, e As EventArgs) Handles mnuFolders_SelectCurrentFolderOnStartup.Click,
                                                                                                            tvFoldersContextMenuSelectOnStartup.Click
            Try
                SetDefaultFolder()
                Me.mnuFolders_LoadCurrentFolderOnStartup.Checked = False
                Me.tvFoldersContextMenuLoadOnStartup.Checked = False
                Main.SetUserIni("CIApplication", ciUserIniKey_LoadDefaultOnStartup, False)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub mnuFolders_LoadCurrentFolderOnStartup_Click(sender As Object, e As EventArgs) Handles mnuFolders_LoadCurrentFolderOnStartup.Click,
                                                                                                          tvFoldersContextMenuLoadOnStartup.Click
            Try
                SetDefaultFolder()
                Me.mnuFolders_LoadCurrentFolderOnStartup.Checked = True
                Me.tvFoldersContextMenuLoadOnStartup.Checked = True
                Main.SetUserIni("CIApplication", ciUserIniKey_LoadDefaultOnStartup, True)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
            Try
                LoadContacts(ciSearchTypes.ciSearchType_Dialog)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnSearchNative_Click(sender As Object, e As EventArgs) Handles btnSearchNative.Click
            Try
                If CurrentBackend().SupportsNativeSearch Then
                    LoadContacts(ciSearchTypes.ciSearchType_Native)
                    SetupForInputState(True)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnAddContactInNative_Click(sender As Object, e As EventArgs) Handles btnAddContactInNative.Click
            Try
                Cursor.Current = Cursors.WaitCursor
                CurrentBackend.AddContact()
                Cursor.Current = Cursors.Default
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
            Try
                EditContact()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
            Try
                RefreshItems()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnGroups_CheckedChanged(sender As Object, e As EventArgs) Handles btnGroups.CheckedChanged
            Try
                If Me.btnGroups.Checked Then
                    'refresh groups list
                    RefreshGroupsList()
                    ShowPanel(CIPanels.ciPanel_Groups)
                Else
                    ShowPanel(CIPanels.ciPanel_Contacts)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub m_oMenuArray_Click(sender As Object, e As EventArgs) Handles m_oMenuArray.Click
            Dim xTag As String

            Try
                xTag = CStr(CType(sender, System.Windows.Forms.ToolStripItem).Tag)
                If xTag = "mnuShowHelpAbout" Then
                    ShowAbout()
                Else
                    Main.LaunchDocumentByExtension(xTag)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub tsContactsExpand_Click(sender As Object, e As EventArgs) Handles mnuContacts_Expand.Click
            Try
                ToggleListingsWidth()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub mnuFolders_CustomProcedure_Click(sender As Object, e As EventArgs) Handles mnuFolders_Custom1.Click, mnuFolders_Custom2.Click,
                                                                                               mnuFolders_Custom3.Click, mnuFolders_Custom4.Click,
                                                                                               mnuFolders_Custom5.Click
            'runs the first custom menu item
            Dim i As Integer

            Try
                Dim tsMenuItem As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)

                With tsMenuItem
                    For i = 0 To 4
                        If m_tsCustomMenuItem(i) Is tsMenuItem Then
                            Select Case i
                                Case 0
                                    CurrentBackend.CustomProcedure1()
                                Case 1
                                    CurrentBackend.CustomProcedure2()
                                Case 2
                                    CurrentBackend.CustomProcedure3()
                                Case 3
                                    CurrentBackend.CustomProcedure4()
                                Case 4
                                    CurrentBackend.CustomProcedure5()
                                Case Else
                            End Select

                            'refresh tree
                            RefreshBackendBranch()

                            Exit Sub
                        End If
                    Next
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

#End Region
#Region "**************events************************"
        Private Sub txtFilterValue1_GotFocus(sender As Object, e As EventArgs) Handles txtFilterValue1.GotFocus
            Try
                Me.AcceptButton = Me.btnQuickSearch
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub txtFilterValue1_LostFocus(sender As Object, e As EventArgs) Handles txtFilterValue1.LostFocus
            'Me.AcceptButton = Me.btnTogglePanels
        End Sub

        Private Sub mnuContacts_ShowDetail_Click(sender As Object, e As EventArgs) Handles mnuContacts_ShowDetail.Click
            Try
                If m_iCurList = ciListBoxes.ciListBoxes_Listings Then
                    ShowListingDetail()
                Else
                    ShowContactDetail(0)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub mnuContacts_SelectAll_Click(sender As Object, e As EventArgs) Handles mnuContacts_SelectAll.Click
            Try
                'GLOG : 15767 : ceh
                If m_iCurList = ciListBoxes.ciListBoxes_Listings Then
                    SelectAllListings()
                ElseIf m_iCurList = ciListBoxes.ciListBoxes_GroupMembers Then
                    SelectAllGroupMembers()
                Else
                    SelectAllContacts()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub grdEntityList_MouseDown(sender As Object, e As MouseEventArgs) Handles grdEntityList.MouseDown
            Try
                With Me.grdEntityList
                    If e.Button = MouseButtons.Right And m_oContacts.Count Then
                        'ShowContactDetail()
                    ElseIf m_oContacts.Count = 0 Then
                        Exit Sub
                    Else
                        Try
                            m_btnDelete(4).Enabled = (.HitTest(e.X, e.Y).RowIndex > -1)
                        Catch
                        End Try
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnQuickSearch_Click(sender As Object, e As EventArgs) Handles btnQuickSearch.Click
            Try
                LoadContacts(ciSearchTypes.ciSearchType_Quick)
                ResetQuickSearch()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub PanelChange(bFolderView As Boolean)
            Try
                Functions.EchoOff()

                If bFolderView Then
                    Me.Text = " " & Main.g_oSessionType.AppTitle & " - Folder View"
                    Me.btnOptionsContacts.Visible = False
                    Me.btnOptionsFolders.Visible = True
                    Me.cmdOK.Visible = False
                    Me.cmdCancel.Visible = False
                    Me.tvFolders.Select()
                Else
                    Me.Text = " " & Main.g_oSessionType.AppTitle & " - Contact View"
                    Me.btnOptionsContacts.Visible = True
                    Me.btnOptionsFolders.Visible = False
                    Me.cmdOK.Visible = True
                    Me.cmdCancel.Visible = True
                    Me.grdListings.Rows(0).Selected = True
                    Me.grdListings.Focus()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub CI_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
            Try
                m_oListings = Nothing
                m_oAddresses = Nothing
                m_oContacts = Nothing
                m_oUNID = Nothing
                m_oEntityList = Nothing
                m_oEvents = Nothing
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub CI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Dim bLoadOnStartup As Boolean
            Dim oNode As TreeNode
            Dim i As Integer
            Dim xQuickFilterType As String
            Dim bShowQuickFilterOption As Boolean


            Try
                If m_bInitialized Then Exit Sub

                'set the event generator to the event generator
                'of the backend of this store - we want to handle
                'events raised by that backend
                m_oEvents = Main.g_oGlobals.CIEvents

                m_oUNID = New CUNID
                m_oContacts = New CContacts

                m_bShowBackendNodes = True

                'set Options button tooltip to display current default folder
                Dim xDefFolder As String
                xDefFolder = Main.g_oIni.GetIni("CIApplication", "DefaultNode", Main.g_oIni.CIUserIni())

                With Me
                    If xDefFolder <> String.Empty Then
                        .btnOptionsFolders.ToolTipText = "Default Folder: " & xDefFolder
                    Else
                        .btnOptionsFolders.ToolTipText = "Default Folder: None specified"
                    End If

                    If .grdEntityList.Columns.Count Then
                        'name visible columns
                        .grdEntityList.Columns(0).HeaderText = Main.g_oIni.GetIni("CICustom", "CustomCol1Caption")
                        .grdEntityList.Columns(2).HeaderText = Main.g_oIni.GetIni("CICustom", "CustomCol2Caption")

                        'hide columns
                        .grdEntityList.Columns(1).Visible = False
                        .grdEntityList.Columns(3).Visible = False
                        .grdEntityList.Columns(4).Visible = False

                        FillLastVisibleDataGridViewColumn(.grdEntityList)

                        'enlarge/move controls
                        ' ''.pnlListings.Width = .pnlListings.Width + 50
                        ' ''.pnlContacts.Width = .pnlContacts.Width + 50
                        ' ''.pnlFolders.Width = .pnlFolders.Width + 75
                        ' ''.tvFolders.Width = .tvFolders.Width + 75
                        ' ''.cmdCancel.Left = .cmdCancel.Left + 75
                        ' ''.cmdOK.Left = .cmdOK.Left + 75
                    End If
                End With

                'set User Documentation path
                Dim directoryInfo As New IO.DirectoryInfo(Functions.AppPath())

                Main.g_xHelpPath = directoryInfo.Parent.Parent.FullName

                'assumes Forte structure
                Main.g_xHelpPath = Main.g_xHelpPath & "\CI\Tools\User Documentation\pdf"

                'setup Help menu
                BuildHelpMenu()

                'show first level of tree
                ShowTreeTop()

                'select a default node if specified
                oNode = SelectNode()

                If Not (oNode Is Nothing) Then
                    'a default node has been selected
                    Try
                        bLoadOnStartup = CBool(Main.GetUserIni("CIApplication", _
                            ciUserIniKey_LoadDefaultOnStartup))
                    Catch
                    End Try

                    If bLoadOnStartup Then
                        'user has specified to load contacts in default folder
                        Me.mnuFolders_LoadCurrentFolderOnStartup.Checked = True
                        LoadContacts(ciSearchTypes.ciSearchType_None)
                        PanelChange(False)
                    Else
                        PanelChange(True)
                    End If

                    If Not (oNode Is Nothing) Then
                        'set the proper quick search list items for the
                        'specified node - these are backend specific
                        RefreshQuickSearchControls()
                    End If

                    'reset width of various controls
                    For Each oCtl As ToolStripItem In m_tsBFilter
                        oCtl.Width = oCtl.Width * Functions.GetScalingFactor()
                    Next

                Else
                    PanelChange(True)
                End If

                'scaling fixes
                Me.pnlFolders.Top = Me.tsToolbar.Top + Me.tsToolbar.Height + 10
                Me.tvFolders.ItemHeight = 20 * Functions.GetScalingFactor()
                Me.grdListings.ColumnHeadersHeight = 22 * Functions.GetScalingFactor()
                Me.grdListings.RowTemplate.Height = 17 * Functions.GetScalingFactor()


                'temporary
                bShowQuickFilterOption = Main.bIsQuickFilterBackend(CurrentBackend.InternalID)

                If bShowQuickFilterOption Then
                    xQuickFilterType = Main.g_oIni.GetIni( _
                            "CIApplication", "DefaultQuickFilterType", Main.g_oIni.CIIni)

                    Select Case xQuickFilterType
                        Case "1", "2"
                            g_iQuickFilterType = CInt(xQuickFilterType)
                        Case Else
                            g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text
                    End Select
                Else
                    g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text
                End If

                'Main.g_iDefaultFilter = g_iQuickFilterType
                SetQuickFilterType()

                'SetUpColumns()
                m_bInitialized = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnMoveDown_Click(sender As Object, e As EventArgs) Handles btnMoveDown.Click, mnuContacts_MoveSelectedContactsDown.Click
            Try
                MoveSelContactDown()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnMoveUp_Click(sender As Object, e As EventArgs) Handles btnMoveUp.Click, mnuContacts_MoveSelectedContactsUp.Click
            Try
                MoveSelContactUp()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnAddGroup_Click(sender As Object, e As EventArgs) Handles btnAddGroup.Click
            Try
                AddGroup()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnDeleteGroup_Click(sender As Object, e As EventArgs) Handles btnDeleteGroup.Click
            Try
                DeleteGroup()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstGroupMembers_Click(sender As Object, e As EventArgs) Handles lstGroupMembers.Click, lstGroupMembers.SelectedIndexChanged
            Try
                m_iCurList = ciListBoxes.ciListBoxes_GroupMembers

                'disable irrelevant menu items
                With Me
                    Me.mnuContacts_MoveSelectedContactsDown.Enabled = False
                    Me.mnuContacts_MoveSelectedContactsUp.Enabled = False

                    Me.grdListings.ClearSelection()
                    .btnDeleteMember.Enabled = (.lstGroupMembers.SelectedIndex > -1) And (.lstGroupMembers.Items.Count > 0)
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstGroupMembers_KeyDown(sender As Object, e As KeyEventArgs) Handles lstGroupMembers.KeyDown
            Try
                If Main.IsPressed(Keys.ControlKey) Then
                    If e.Control + e.KeyCode = Keys.A Then
                        SelectAllGroupMembers()
                    End If
                Else
                    If e.KeyCode = Keys.Delete Then
                        DeleteMembers()
                    End If
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstGroupMembers_MouseDown(sender As Object, e As MouseEventArgs) Handles lstGroupMembers.MouseDown
            Dim sCtlTop As Single
            Dim iNewIndex As Integer
            Dim i As Integer

            Try
                m_bMouseDown = True

                With Me.lstGroupMembers
                    If .Items.Count = 0 Then
                        Exit Sub
                    Else
                        .Focus()
                        If e.Button = MouseButtons.Right Then
                            If .SelectedIndex > -1 Then
                                ShowMemberDetail(.SelectedValue)
                            End If
                        End If
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnAddMember_Click(sender As Object, e As EventArgs) Handles btnAddMember.Click
            Try
                AddMembers()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnDeleteMember_Click(sender As Object, e As EventArgs) Handles btnDeleteMember.Click
            Try
                DeleteMembers()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
            Try
                If Not Me.btnGroups.Checked Then
                    'not in groups mode - this button should insert contacts
                    SaveColumns()
                    m_bCancelled = False
                    Me.Hide()
                Else
                    'in groups mode - this button should load the current group
                    LoadGroup()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'GLOG : 15797 : ceh
        Private Sub grdListings_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles grdListings.CellMouseDown
            Try
                If e.RowIndex = -1 Then
                    If e.Button = MouseButtons.Left Then
                        m_SelectedRowID = grdListings.SelectedRows(0).Cells(0).Value
                    End If
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub grdListings_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles grdListings.ColumnHeaderMouseClick
            Try
                If e.RowIndex = -1 Then
                    If e.Button = MouseButtons.Left Then
                        Sort(e.ColumnIndex)
                    End If
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'GLOG : 15797 : ceh
        Private Sub grdListings_ColumnAdded(sender As Object, e As DataGridViewColumnEventArgs) Handles grdListings.ColumnAdded
            grdListings.Columns.Item(e.Column.Index).SortMode = DataGridViewColumnSortMode.NotSortable
        End Sub

        Private Sub grdListings_DoubleClick(sender As Object, e As EventArgs) Handles grdListings.DoubleClick
            Try
                If Me.grdListings.SelectedRows.Count Then
                    OnListingsDblClick()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub grdListings_GotFocus(sender As Object, e As EventArgs) Handles grdListings.GotFocus
            Dim i As Integer
            Dim j As Integer

            Try
                'mark that this is the current list
                m_iCurList = ciListBoxes.ciListBoxes_Listings

                'disable irrelevant menu items
                Me.mnuContacts_MoveSelectedContactsDown.Enabled = False
                Me.mnuContacts_MoveSelectedContactsUp.Enabled = False

                'get the contact list that is active
                With Me
                    For i = 0 To 3
                        'If m_lblContacts(i).ForeColor = Color.Blue Then
                        'this is the active list - remove
                        'selection, disable delete button
                        m_lstContacts(i).ClearSelected()
                        m_btnDelete(i).Enabled = False
                        'Exit For
                        'End If
                    Next i

                    'remove group members selections, disable delete button
                    .lstGroupMembers.ClearSelected()
                    .btnDeleteMember.Enabled = False
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub grdListings_KeyDown(sender As Object, e As KeyEventArgs) Handles grdListings.KeyDown
            Try
                If Main.IsPressed(Keys.ControlKey) Then
                    If e.Control + e.KeyCode = Keys.A Then
                        SelectAllListings()
                    End If
                Else
                    Select Case e.KeyCode
                        Case Keys.Home
                            e.Handled = True
                            Me.grdListings.ClearSelection()
                            With Me.grdListings
                                .Rows(0).Selected = True
                            End With
                        Case Keys.End
                            e.Handled = True
                            Me.grdListings.ClearSelection()
                            With Me.grdListings
                                .Rows(m_oListings.Count - 1).Selected = True
                            End With
                        Case Keys.Left, Keys.Right
                            e.Handled = True
                            'GLOG : 8793 : ceh
                        Case Keys.Enter
                            OnListingsDblClick()
                        Case Else
                    End Select
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub grdListings_KeyPress(sender As Object, e As KeyPressEventArgs) Handles grdListings.KeyPress
            'GLOG : 15731 : ceh
            Try
                Main.SelectDataGridViewRecord(grdListings, grdListings.Columns(0).Name, 0, e.KeyChar)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub grdListings_MouseDown(sender As Object, e As MouseEventArgs) Handles grdListings.MouseDown
            Dim lRow As Long
            Dim lNumSel As Long

            Try
                If e.Button = MouseButtons.Right Then
                    'get original number of selections -
                    'we'll need this to force a row change-
                    'this will give us the correct addresses
                    'for the selected listing
                    lNumSel = Me.grdListings.SelectedRows.Count

                    With Me.grdListings
                        'select listing
                        lRow = .HitTest(e.X, e.Y).RowIndex
                        If lRow > -1 Then
                            'clear existing listings
                            .ClearSelection()
                            .Rows(lRow).Selected = True
                            If lNumSel > 1 Then
                                OnListingsRowChange()
                            End If

                            Application.DoEvents()
                            ShowListingDetail()
                        End If
                    End With
                End If
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                Cursor.Current = Cursors.Default
            End Try
        End Sub

        Private Sub grdListings_SelectionChanged(sender As Object, e As EventArgs) Handles grdListings.SelectionChanged

            Try
                With Me.grdListings
                    'If (Not Main.IsPressed(Keys.ControlKey)) And _
                    '        (Not Main.IsPressed(Keys.ShiftKey)) Then
                    '    Me.grdListings.ClearSelection()
                    'End If

                    '.SelBookmarks.Add.Row +.FirstRow
                    Application.DoEvents()

                    OnListingsRowChange()
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tvFolders_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvFolders.AfterSelect
            Try
                SetupDialogForNode(e.Node)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tvFolders_BeforeCollapse(sender As Object, e As TreeViewCancelEventArgs) Handles tvFolders.BeforeCollapse
            Try
                m_bIsExpanding = False
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tvFolders_BeforeExpand(sender As Object, e As TreeViewCancelEventArgs) Handles tvFolders.BeforeExpand
            Try
                m_bIsExpanding = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tvFolders_DoubleClick(sender As Object, e As EventArgs) Handles tvFolders.DoubleClick
            Try
                If CurrentBackend.IsLoadableEntity(tvFolders.SelectedNode.Name) Then
                    LoadContacts()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tvFolders_MouseDown(sender As Object, e As MouseEventArgs) Handles tvFolders.MouseDown
            Dim info As TreeViewHitTestInfo = tvFolders.HitTest(e.X, e.Y)
            Dim oNode As TreeNode

            Try
                If e.Button = MouseButtons.Right Then
                    'right click occurred - select node, then show menu
                    If (info.Node IsNot Nothing) Then
                        oNode = info.Node
                        tvFolders.SelectedNode = oNode
                        ' ''Me.PopupMenu mnuFolders
                    End If

                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
            Try
                Try
                    SaveColumns()
                    Me.tvFolders.Focus()
                Catch
                End Try

                m_bCancelled = True
                Me.Hide()

            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
            Try
                If KeyAscii = 9 Then    'Tab
                    If m_bShowedContacts Then
                        Try
                            tvFolders.Focus()
                        Catch
                        End Try
                    End If
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub cmdCancel_LostFocus()
            Try
                If Not m_bShowedContacts Then
                    Try
                        tvFolders.Focus()
                    Catch
                    End Try
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnQuickSearch_Click()
            Try
                LoadContacts(ciSearchTypes.ciSearchType_Quick)
                ResetQuickSearch()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub cbxGroups_ValueChanged(sender As Object, e As EventArgs) Handles cbxGroups.ValueChanged
            Try
                With Me
                    .btnDeleteGroup.Enabled = .cbxGroups.SelectedIndex <> -1
                    .AcceptButton = btnAddMember
                    LoadGroupMembers()
                    .cmdOK.Enabled = True
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnEdit_Click()
            Try
                EditContact()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnSearch_Click()
            Try
                LoadContacts(ciSearchTypes.ciSearchType_Dialog)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub tbtnContacts_Click(sender As Object, e As EventArgs) Handles btnTogglePanels.Click
            Try
                If Me.pnlFolders.Visible Then
                    LoadContacts()
                Else
                    ShowPanel(CIPanels.ciPanel_folders)
                    Me.tvFolders.Focus()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'todo
        ' ''        Private Sub btnGetMofoContacts_Click()
        ' ''            Const mpThisFunction As String = "CI.CI.btnGetMofoContacts_Click"

        ' ''            Static o As Object
        ' ''            Dim oBackend As Object
        ' ''            Dim oCol As Collection
        ' ''            Dim oCIContact As CContact
        ' ''            Dim oOutlookContact As Object

        ' ''            On Error Resume Next
        ' ''            o = CreateObject("MofoCI.Contacts")
        ' ''            On Error GoTo ProcError

        ' ''            If o Is Nothing Then
        ' ''                Err.Raise(429, , "Could not create Mofo Contacts object 'MoFoCI.Contacts'")
        ' ''            End If

        ' ''            'Screen.ActiveForm.Hide
        ' ''            DoEvents()

        ' ''            oCol = o.Retrieve()

        ' ''            If oCol.Count = 0 Then
        ' ''                Exit Sub
        ' ''            Else
        ' ''                ShowPanel(ciPanel_Contacts)
        ' ''            End If

        ' ''            On Error Resume Next
        ' ''            '    Set oBackend = CreateObject("CIMAPI.CCIBackend")

        ' ''            On Error Resume Next

        ' ''            If (g_oBackend Is Nothing) Then
        ' ''                g_oBackend = CreateObject("CIOutlookOM.CCIBackend")

        ' ''                If (g_oBackend Is Nothing) Then
        ' ''                    g_oBackend = CreateObject("CIMAPI.CCIBackend")
        ' ''                End If
        ' ''            End If

        ' ''            On Error GoTo ProcError

        ' ''            Dim iDataRetrieved As Integer

        ' ''            Select Case Me.SelectionList
        ' ''                Case CI.ciSelectionList_To
        ' ''                    iDataRetrieved = Me.DataRetrievedForTo
        ' ''                Case CI.ciSelectionList_From
        ' ''                    iDataRetrieved = Me.DataRetrievedForFrom
        ' ''                Case CI.ciSelectionList_CC
        ' ''                    iDataRetrieved = Me.DataRetrievedForCC
        ' ''                Case CI.ciSelectionList_BCC
        ' ''                    iDataRetrieved = Me.DataRetrievedForBCC
        ' ''            End Select


        ' ''            For Each oOutlookContact In oCol
        ' ''                '        Set oCIContact = oBackend.GetCIContactFromOutlookContact( _
        ' ''                '            oOutlookContact, iDataRetrieved)
        ' ''                oCIContact = g_oBackend.GetCIContactFromOutlookContact( _
        ' ''                    oOutlookContact, iDataRetrieved)


        ' ''                '       set selection list for contact
        ' ''                oCIContact.ContactType = Me.SelectionList

        ' ''                '       find the last contact in collection that has the
        ' ''                '       same contact type - if none, add as first of group
        ' ''                Dim iNumTo As Integer
        ' ''                Dim iNumFrom As Integer
        ' ''                Dim iNumCC As Integer
        ' ''                Dim iNumBCC As Integer
        ' ''                Dim lInsertAt As Long
        ' ''                Dim iID As Integer

        ' ''                'get number of contacts in each group
        ' ''                With Me.lstContacts
        ' ''                    iNumTo = .Item(0).ListCount
        ' ''                    iNumFrom = .Item(1).ListCount
        ' ''                    iNumCC = .Item(2).ListCount
        ' ''                    iNumBCC = .Item(3).ListCount
        ' ''                End With

        ' ''                Select Case oCIContact.ContactType
        ' ''                    Case CI.ciSelectionList_To
        ' ''                        'insert after last 'To' contact
        ' ''                        lInsertAt = iNumTo + 1
        ' ''                    Case CI.ciSelectionList_From
        ' ''                        lInsertAt = iNumTo + iNumFrom + 1
        ' ''                    Case CI.ciSelectionList_CC
        ' ''                        lInsertAt = iNumTo + iNumFrom + iNumCC + 1
        ' ''                    Case CI.ciSelectionList_BCC
        ' ''                        lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
        ' ''                End Select

        ' ''                On Error Resume Next
        ' ''                If lInsertAt <= m_oContacts.Count Then
        ' ''                    m_oContacts.Add(oCIContact, lInsertAt)
        ' ''                Else
        ' ''                    m_oContacts.Add(oCIContact)
        ' ''                End If

        ' ''                If Err.Number = 0 Then
        ' ''                    'get next number in the sequence
        ' ''                    iID = iID + 1

        ' ''                    'tag the contact with this unique number
        ' ''                    oCIContact.Tag = iID

        ' ''                    '                   add full name to selected list
        ' ''                    With SelectionListControl
        ' ''                        .AddItem oCIContact.DisplayName
        ' ''                        .ItemData(.ListCount - 1) = iID
        ' ''                    End With
        ' ''                ElseIf Err.Number = 457 Then
        ' ''                    MsgBox(oCIContact.FullName & " has already been added.", vbExclamation, Main.g_oSessionType.AppTitle)
        ' ''                Else
        ' ''                    Err.Raise(Err.Number, , Err.Description)
        ' ''                End If
        ' ''            Next
        ' ''            'RaiseEvent InsertionOK(m_oContacts)
        ' ''            Exit Sub
        ' ''ProcError:
        ' ''            Main.g_oError.RaiseError mpThisFunction
        ' ''            Exit Sub
        ' ''        End Sub

        Private Sub mnuContacts_AddGroup_Click()
            Try
                AddGroup()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub mnuContacts_DeleteGroup_Click()
            Try
                DeleteGroup()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub mnuContacts_MoveDown_Click()
            Try
                MoveSelContactDown()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub mnuContacts_MoveUp_Click()
            Try
                MoveSelContactUp()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub mnuContacts_SelectAll_Click()
            Try
                Select Case m_iCurList
                    Case ciListBoxes.ciListBoxes_Listings
                        SelectAllListings()
                    Case ciListBoxes.ciListBoxes_GroupMembers
                        SelectAllGroupMembers()
                    Case Else
                        SelectAllContacts()
                End Select
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub mnuContacts_ToggleContactList_Click()
            Try
                ToggleListingsWidth()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub


#End Region
#Region "**************CEventGenerator**************"
        Private Sub m_oEvents_BeforeStoreSearchFolderSearch(xFolderName As String, Cancel As Boolean) Handles m_oEvents.BeforeStoreSearchFolderSearch
            '            Dim xMsg As String

            '            On Error GoTo ProcError

            '            If Not (m_ProgressForm Is Nothing) And m_ProgressForm.Visible Then
            '                Exit Sub
            '            End If

            '            Exit Sub
            'ProcError:
            '            Main.g_oError.ShowError()
            '            Exit Sub

        End Sub
        Private Sub m_oEvents_BeforeExecutingListingsQuery() Handles m_oEvents.BeforeExecutingListingsQuery
            Try
                With Me.lblStatusMsg
                    Cursor.Current = Cursors.WaitCursor
                    .Text = "Preparing to retrieve contacts.  Please wait..."
                    .Visible = True
                    Application.DoEvents()
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub m_oEvents_AfterExecutingListingsQuery() Handles m_oEvents.AfterExecutingListingsQuery
            Try
                With Me.lblStatusMsg
                    .Visible = False
                    .Text = String.Empty
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub m_oEvents_AfterListingAdded(lIndexAdded As Long, lTotal As Long, ByRef Cancel As Boolean) Handles m_oEvents.AfterListingAdded
            'update only every time the progress
            'bar would add another square.
            Dim xMsg As String
            Dim sPercentComplete As Single
            Static dEstimatedTime As Double

            Static iContactsPerBarSquare As Integer
            Static t0 As Long
            Static t1 As Long

            Try
                Application.DoEvents()

                If m_ProgressForm Is Nothing Then
                    m_ProgressForm = New ProgressBoxForm
                End If

                If lTotal = 0 Then
                    Exit Sub
                End If

                'get the estimated time to load 5% of contacts
                If lIndexAdded = 1 Then
                    Cursor.Current = Cursors.Default

                    'get the time when #1 is added
                    t0 = GetTickCount()
                    iContactsPerBarSquare = CInt(lTotal / 26)
                    dEstimatedTime = 0
                End If

                With m_ProgressForm
                    If (lIndexAdded / lTotal) >= 0.05 And dEstimatedTime = 0 Then
                        '1% have been added - get time
                        t1 = GetTickCount()

                        'get the estimated time for all
                        dEstimatedTime = CDbl((t1 - t0) / 0.05)
                        If dEstimatedTime > 1000 Then
                            'estimated time is more than a
                            'second, show progress bar
                            If Not .Visible Then
                                'show progress bar dialog
                                .Text = "Loading Contacts"
                                .ShowCancelled = True
                                .Message = "Loading contacts.  (0%) complete."
                                .Value = 0
                                .Show()
                            End If
                        End If
                    End If

                    Try
                        If .Visible Then
                            'update the progress bar
                            If lIndexAdded = lTotal Then
                                'last contact has been added - close up
                                .Message = "Loading contacts.  (100%) complete."
                                .Value = 1
                                Application.DoEvents()
                                .Visible = False
                                .Value = 0
                            ElseIf (lIndexAdded Mod iContactsPerBarSquare) = 0 Then
                                'enough contacts have been added to force
                                'the display of another progress bar square
                                sPercentComplete = (lIndexAdded / lTotal)
                                .Message = "Loading contacts.  (" & _
                                    Format(sPercentComplete, "#0%") & ") complete."
                                .Value = (sPercentComplete * 100)

                            End If

                            Application.DoEvents()

                            If .Cancelled Then
                                'user cancelled the process -
                                'm_bLoadCancelled = False
                                Cancel = True
                                .Visible = False
                                .Value = 0
                                xMsg = "You have cancelled this operation. " & lIndexAdded & _
                                    " of " & lTotal & " contacts have been loaded."
                                MsgBox(xMsg, vbInformation, Main.g_oSessionType.AppTitle)
                                .Cancelled = False
                            End If
                        End If
                    Catch
                    End Try
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub m_oEvents_AfterListingsRetrieved(lTotal As Long) Handles m_oEvents.AfterListingsRetrieved
            Try
                Me.lblEntries.Text = "&Available Contacts: (" & lTotal & ")"
                If Not m_ProgressForm Is Nothing Then
                    m_ProgressForm.Visible = False
                End If
                Cursor.Current = Cursors.Default
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "**************methods**********************"
        Private Sub AddSelContacts()
            'adds the selected listings to the specified list
            Dim oContacts As CContacts
            Dim oC As CContact
            Dim oL As CListing
            Dim oAddr As CAddress
            Dim l As Long
            Dim lNumSel As Long
            Dim lAlertThreshold As Long
            Dim iChoice As MsgBoxResult
            Dim iNumTo As Integer
            Dim iNumFrom As Integer
            Dim iNumCC As Integer
            Dim iNumBCC As Integer
            Dim lInsertAt As Long
            Dim bMaxReached As Boolean
            Dim bSelectedMaxReached As Boolean
            Dim iMaxNum As Integer
            Dim xMessage As String = ""

            'used to generate a number sequence below
            Static iID As Integer

            Try
                With Me
                    Cursor.Current = Cursors.WaitCursor

                    lNumSel = .grdListings.SelectedRows.Count

                    'get existing count and check max amount in each group
                    With Me
                        iNumTo = .lstTo.Items.Count
                        iNumFrom = .lstFrom.Items.Count
                        iNumCC = .lstCC.Items.Count
                        iNumBCC = .lstBCC.Items.Count
                    End With

                    'GLOG : 8850 : ceh
                    Select Case Me.SelectionList
                        Case ICI.ciSelectionlists.ciSelectionList_To
                            If Me.MaxTo And Me.MaxTo <= iNumTo Then
                                bMaxReached = True
                            ElseIf Me.MaxTo And Me.MaxTo < lNumSel Then
                                bSelectedMaxReached = True
                                iMaxNum = Me.MaxTo
                            End If
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            If Me.MaxFrom And Me.MaxFrom <= iNumFrom Then
                                bMaxReached = True
                            ElseIf Me.MaxFrom And Me.MaxFrom < lNumSel Then
                                bSelectedMaxReached = True
                                iMaxNum = Me.MaxFrom
                            End If
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            If Me.MaxCC And Me.MaxCC <= iNumCC Then
                                bMaxReached = True
                            ElseIf Me.MaxCC And Me.MaxCC < lNumSel Then
                                bSelectedMaxReached = True
                                iMaxNum = Me.MaxCC
                            End If
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            If Me.MaxBCC And Me.MaxBCC <= iNumBCC Then
                                bMaxReached = True
                            ElseIf Me.MaxBCC And Me.MaxBCC < lNumSel Then
                                bSelectedMaxReached = True
                                iMaxNum = Me.MaxBCC
                            End If
                            '            Case CI.ciSelectionList_Custom
                            '                'get data retrieved info from entity array
                    End Select

                    If bMaxReached Then
                        MsgBox("You have reached the maximum number of contacts allowed in this field.", vbExclamation, Main.g_oSessionType.AppTitle)
                        Cursor.Current = Cursors.Default
                        Exit Sub
                    ElseIf bSelectedMaxReached Then
                        If iMaxNum = 1 Then
                            xMessage = "Only 1 contact is allowed in this field."
                        Else
                            xMessage = "Only " & iMaxNum & " contacts are allowed in this field."
                        End If
                        MsgBox(xMessage, vbExclamation, Main.g_oSessionType.AppTitle)
                        Cursor.Current = Cursors.Default
                        Exit Sub
                    End If

                    'get the threshold number of selected contacts
                    'for which the user should be prompted.
                    If lAlertThreshold = 0 Then
                        Try
                            lAlertThreshold = Main.g_oIni.GetIni("CIApplication", "AlertThreshold")
                        Catch
                        End Try
                    End If

                    If lAlertThreshold = 0 Then
                        lAlertThreshold = 15
                    End If

                    If m_ProgressForm Is Nothing Then
                        m_ProgressForm = New ProgressBoxForm
                    End If

                    If lNumSel > lAlertThreshold Then
                        'prompt before executing - alert threshold has been triggered
                        iChoice = MsgBox("You are about to add " & lNumSel & _
                            " contacts.  Do you want to continue?", vbQuestion + vbYesNo)
                        If iChoice = vbNo Then
                            Cursor.Current = Cursors.Default
                            Exit Sub
                        Else
                            With m_ProgressForm
                                '.Cancel = False
                                .Visible = True
                            End With
                        End If
                    End If

                    SetupForInputState(False)

                    For l = lNumSel - 1 To 0 Step -1
                        'For l = 0 To lNumSel - 1
                        If lNumSel > lAlertThreshold Then
                            If l = 0 Then
                                SetupForInputState(False)
                                With m_ProgressForm
                                    .Title = "Adding Contacts"
                                    .ShowCancelled = True
                                    .Value = 0
                                    .Message = "Adding contacts. Please wait... 0 of " & lNumSel
                                    .Visible = True
                                    '.ZOrder(0)
                                End With
                            End If
                        End If

                        'check that we haven't reached the
                        'maximum number of contacts allowed
                        If Me.MaxContacts > 0 Then
                            If m_oContacts.Count = Me.MaxContacts Then
                                If Me.MaxContacts = 1 Then
                                    MsgBox("No more than 1 contact may be " & _
                                        "inserted in this situation.", vbExclamation, Main.g_oSessionType.AppTitle)
                                Else
                                    MsgBox("No more than " & Me.MaxContacts & _
                                        " contacts may be inserted in this situation.", vbExclamation, Main.g_oSessionType.AppTitle)
                                End If
                                Cursor.Current = Cursors.Default
                                SetupForInputState(True)
                                Exit Sub
                            End If
                        End If

                        'get selected listing
                        oL = m_oListings.Item(.grdListings.SelectedRows(l).Index)
                        If oL Is Nothing Then
                            Throw New Exception("Listing is invalid.")
                        End If

                        If m_oAddresses Is Nothing Then
                            MsgBox(Main.g_xUnvalidAddress, MsgBoxStyle.Information, Main.g_oSessionType.AppTitle)
                            ResetUserControlForInput()
                            Exit Sub
                        ElseIf m_oAddresses.Count > 0 Then
                            'get selected address
                            Try
                                'exit if still no type is chosen
                                If .cbxAddressType.SelectedIndex = -1 Then
                                    LoadAddresses()
                                    Cursor.Current = Cursors.Default
                                    Exit Sub
                                End If

                                oAddr = m_oAddresses.Item(.cbxAddressType.SelectedIndex + 1)
                            Catch
                            End Try
                            If oAddr Is Nothing Then
                                MsgBox(Main.g_xUnvalidAddress, MsgBoxStyle.Information, Main.g_oSessionType.AppTitle)
                                ResetUserControlForInput()
                                Exit Sub
                            End If
                        Else
                            MsgBox(Main.g_xUnvalidAddress, MsgBoxStyle.Information, Main.g_oSessionType.AppTitle)
                            ResetUserControlForInput()
                            Exit Sub
                        End If

                        'get contact from listing and address
                        Dim iDataRetrieved As ciRetrieveData
                        Select Case Me.SelectionList
                            Case ICI.ciSelectionlists.ciSelectionList_To
                                iDataRetrieved = Me.DataRetrievedForTo
                            Case ICI.ciSelectionlists.ciSelectionList_From
                                iDataRetrieved = Me.DataRetrievedForFrom
                            Case ICI.ciSelectionlists.ciSelectionList_CC
                                iDataRetrieved = Me.DataRetrievedForCC
                            Case ICI.ciSelectionlists.ciSelectionList_BCC
                                iDataRetrieved = Me.DataRetrievedForBCC
                            Case ICI.ciSelectionlists.ciSelectionList_Custom
                                'get data retrieved info from entity array
                                iDataRetrieved = Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(1)
                        End Select

                        oContacts = Main.ListingBackend(oL).GetContacts(oL, oAddr, vbNull, iDataRetrieved, Me.Alerts)

                        If (oContacts Is Nothing) Then
                            GoTo skip
                        End If

                        'add to contact collection
                        For Each oC In oContacts
                            'set selection list for contact
                            oC.ContactType = Me.SelectionList

                            'GLOG : 7764 : ceh
                            'set Address Type Name for contact
                            oC.AddressTypeName = oAddr.Name

                            'find the last contact in collection that has the
                            'same contact type - if none, add as first of group

                            'get number of contacts in each group
                            With Me
                                iNumTo = .lstTo.Items.Count
                                iNumFrom = .lstFrom.Items.Count
                                iNumCC = .lstCC.Items.Count
                                iNumBCC = .lstBCC.Items.Count
                            End With

                            Select Case oC.ContactType
                                Case ICI.ciSelectionlists.ciSelectionList_To
                                    'insert after last 'To' contact
                                    lInsertAt = iNumTo + 1
                                Case ICI.ciSelectionlists.ciSelectionList_From
                                    lInsertAt = iNumTo + iNumFrom + 1
                                Case ICI.ciSelectionlists.ciSelectionList_CC
                                    lInsertAt = iNumTo + iNumFrom + iNumCC + 1
                                Case ICI.ciSelectionlists.ciSelectionList_BCC
                                    lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
                                Case ICI.ciSelectionlists.ciSelectionList_Custom
                                    lInsertAt = m_oContacts.Count + 1
                            End Select

                            'delete existing if necessary
                            If oC.ContactType = ICI.ciSelectionlists.ciSelectionList_Custom Then
                                If Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(3) <> String.Empty Then
                                    Dim j As Integer
                                    'remove from collection - use tag property as id -
                                    'key value is concatenation of name and listing id, and
                                    'is used for uniqueness
                                    For j = 1 To m_oContacts.Count
                                        If m_oContacts.Item(j).Tag = Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(3) Then
                                            m_oContacts.Delete(j)
                                            Exit For
                                        End If
                                    Next j
                                End If
                            End If


                            Try
                                If lInsertAt <= m_oContacts.Count Then
                                    m_oContacts.Add(oC, lInsertAt)
                                Else
                                    m_oContacts.Add(oC)
                                End If

                                'get next number in the sequence
                                iID = iID + 1

                                'tag the contact with this unique number
                                oC.Tag = iID

                                'update display name & custom type/entity name
                                If oC.ContactType = ICI.ciSelectionlists.ciSelectionList_Custom Then
                                    With Me.grdEntityList
                                        oC.CustomTypeName = Me.EntityList.Rows(.SelectedRows(0).Index)(0)
                                        Me.btnDeleteEntity.Enabled = True
                                        Me.EntityList.Rows(.SelectedRows(0).Index)(2) = IIf(oC.DisplayName <> "", oC.DisplayName, oC.Company)
                                        Me.EntityList.Rows(.SelectedRows(0).Index)(3) = iID
                                        .DataSource = Me.EntityList
                                        .Refresh()
                                    End With
                                Else
                                    'add full name to selected list
                                    With SelectionListControl
                                        'GLOG : 8849 : ceh
                                        .Items.Add(New ItemData(oC.FullName, iID))
                                    End With
                                End If

                            Catch e As System.Exception
                                If Err.Number = 457 Then
                                    MsgBox(oC.FullName & " has already been added.", vbExclamation, Main.g_oSessionType.AppTitle)
                                Else
                                    Throw e
                                End If
                            End Try
                        Next oC

                        If lNumSel > lAlertThreshold Then
                            m_ProgressForm.Message = _
                                "Adding contacts. Please wait... " & l + 1 & " of " & lNumSel
                            m_ProgressForm.Value = (l + 1) / lNumSel
                        End If

                        If m_ProgressForm.Cancelled Then
                            MsgBox("You canceled this operation.  " & l + 1 & " contacts have been added.", vbExclamation)
                            'reset for next iteration
                            m_ProgressForm.Cancelled = False
                            Exit For
                        End If
skip:
                    Next l
                End With

                SetupForInputState(True)

                With Me
                    m_ProgressForm.Visible = False
                    .grdListings.Focus()
                    Cursor.Current = Cursors.Default
                End With

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                'enable insert btn only if contacts exist
                Me.cmdOK.Enabled = m_oContacts.Count

                SetupForInputState(True)

                With Me
                    m_ProgressForm.Visible = False
                    Cursor.Current = Cursors.Default
                End With
            End Try
        End Sub
        Private Sub ResetUserControlForInput()
            'enable insert btn only if contacts exist
            Me.cmdOK.Enabled = m_oContacts.Count

            SetupForInputState(True)

            With Me
                If Not (m_ProgressForm Is Nothing) Then
                    m_ProgressForm.Visible = False
                End If
                Cursor.Current = Cursors.Default
            End With

        End Sub
        Private Sub DeleteSelContacts(Optional ByVal bPrompt As Boolean = True)
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim iChoice As MsgBoxResult
            Dim oContact As CContact
            Dim xMsg As String

            Try
                With SelectionListControl()
                    If bPrompt Then
                        Select Case .SelectedItems.Count
                            Case 0
                                Exit Sub
                            Case 1
                                xMsg = "Delete selected contact?"
                            Case Else
                                xMsg = "Delete selected contacts?"
                        End Select
                        iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, Main.g_oSessionType.AppTitle)
                    End If

                    If iChoice = vbYes Or (Not bPrompt) Then
                        'remove selected items
                        Do While (.SelectedItems.Count > 0)
                            For j = 1 To m_oContacts.Count
                                If m_oContacts.Item(j).Tag = CType(.SelectedItem, ItemData).ItemData Then
                                    m_oContacts.Delete(j)
                                    Exit For
                                End If
                            Next
                            .Items.Remove(.SelectedItem)
                        Loop

                        'select new item
                        If .SelectedItems.Count Then
                            '.Selected(Main.ciMin(.SelectedItems.Count - 1, CDbl(k))) = True
                            .SelectedItem = .Items(Main.ciMin(.SelectedItems.Count - 1, CDbl(k)))
                            .Focus()
                        Else
                            Me.grdListings.Focus()
                        End If

                    End If
                End With

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                'enable insert btn only if contacts exist
                Me.cmdOK.Enabled = m_oContacts.Count
            End Try
        End Sub

        Private Sub DeleteSelContacts_Custom(Optional ByVal bPrompt As Boolean = True)
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim iChoice As MsgBoxResult
            Dim oContact As CContact
            Dim xMsg As String

            Try
                With Me.grdEntityList
                    If bPrompt Then
                        xMsg = "Delete selected contact?"
                        iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, Main.g_oSessionType.AppTitle)
                    End If

                    If iChoice = vbYes Or (Not bPrompt) Then

                        With Me.grdEntityList
                            'remove from collection - use tag property as id -
                            'key value is concatenation of name and listing id, and
                            'is used for uniqueness
                            For j = 1 To m_oContacts.Count
                                If m_oContacts.Item(j).Tag = Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(3) Then
                                    m_oContacts.Delete(j)
                                    Exit For
                                End If
                            Next j

                            'remove display name/id from entity row
                            Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(2) = ""
                            Me.EntityList.Rows(Me.grdEntityList.SelectedRows(0).Index)(3) = ""
                            .DataSource = Me.EntityList
                            .Refresh()

                        End With

                        'disable delete button since nothing is selected
                        Me.btnDeleteEntity.Enabled = False
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                'enable insert btn only if contacts exist
                Me.cmdOK.Enabled = m_oContacts.Count
            End Try
        End Sub
        Private Function LoadContacts(Optional ByVal iSearchType As ciSearchTypes = ciSearchTypes.ciSearchType_None) As Long
            'loads the contacts in the selected folder,
            'using the specified search method - note that
            'some search methods ignore the selected folder
            Dim oFolder As CFolder
            Dim oFilter As CFilter
            Dim oNode As TreeNode
            Dim oStore As CStore
            Dim iBackendID As Integer
            Dim oFld1 As CFilterField
            Dim bCancelled As Boolean
            Dim xUNID As String
            Dim xStoreName As String
            Dim oB As ICIBackend
            Dim oListings As CListings

            Static iPrevBackendID As Integer
            Static iPrevSearchType As ciSearchTypes

            Try
                oListings = Nothing

                oNode = Me.tvFolders.SelectedNode

                If oNode Is Nothing Then
                    LoadContacts = 0
                    Exit Function
                End If

                xUNID = oNode.Name
                xStoreName = oNode.Text

                iBackendID = CUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Backend)

                'exit if the search type is not a native search
                'and the node is a backend - no backend searching allowed
                If (iSearchType <> ciSearchTypes.ciSearchType_Native) And (Main.Backends.ItemFromID(iBackendID).SupportsFolders) Then
                    If m_oUNID.GetUNIDType(xUNID) = ciUNIDTypes.ciUNIDType_Backend Then
                        LoadContacts = 0
                        Exit Function
                    End If
                End If

                If (iPrevBackendID <> iBackendID) And (iSearchType = ciSearchTypes.ciSearchType_Refresh) Then
                    'can't refresh if backend was switched - alert
                    MsgBox("You cannot execute the 'Refresh' function after you " & _
                        "have changed backends.", vbExclamation, Main.g_oSessionType.AppTitle)
                    LoadContacts = 0
                    Exit Function
                ElseIf (iPrevSearchType = ciSearchTypes.ciSearchType_Native) And _
                    (iSearchType = ciSearchTypes.ciSearchType_Refresh) Then
                    MsgBox("You cannot execute the 'Refresh' function after executing " & _
                        "'Native Find'.  Please rerun 'Native Find'.", _
                        vbExclamation, Main.g_oSessionType.AppTitle)
                    LoadContacts = 0
                    Exit Function
                End If

                oB = Main.GetBackendFromID(iBackendID)

                'get filter
                oFilter = Main.GetFilter(iBackendID)

                Dim iUNIDType As ciUNIDTypes
                iUNIDType = m_oUNID.GetUNIDType(xUNID)

                Select Case iSearchType
                    Case ciSearchTypes.ciSearchType_Native
                        oListings = oB.SearchNative()
                    Case ciSearchTypes.ciSearchType_Quick
                        With Me
                            'set filter based on Filter type
                            If g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text Then
                                'Quick Filter text
                                oFld1 = oFilter.FilterFields(.cmbFilterFld1.SelectedIndex)
                                oFld1.Value = .txtFilterValue1.Text
                                oFld1.SearchOperator = .cmbOp1.SelectedValue
                            Else
                                'Quick Filter buttons
                                oFld1 = oFilter.FilterFields(g_iQuickFilterOptionIndex)
                                oFld1.Value = m_tsBFilter(g_iQuickFilterBtnIndex).Tag
                                oFld1.SearchOperator = ciSearchOperators.ciSearchOperator_BeginsWith
                            End If
                        End With

                        If iUNIDType = ciUNIDTypes.ciUNIDType_Store And _
                            oB.IsSearchableEntity(xUNID) Then
                            'selected node is a store and store searches
                            'are supported by the backend - search store
                            oStore = m_oUNID.GetStore(xUNID, xStoreName)
                            oListings = oB.GetStoreListings(oStore, oFilter)
                        ElseIf iUNIDType = ciUNIDTypes.ciUNIDType_Folder And _
                            oB.IsSearchableEntity(xUNID) Then
                            'selected node is a folder - search folder
                            oFolder = m_oUNID.GetFolder(xUNID)
                            oFolder.Name = Me.tvFolders.SelectedNode.Text
                            oListings = oB.GetFolderListings(oFolder, oFilter)
                        End If
                        If Not oFilter Is Nothing Then
                            oFilter.Reset()
                        End If
                    Case ciSearchTypes.ciSearchType_Dialog
                        If iUNIDType = ciUNIDTypes.ciUNIDType_Store And _
                            oB.IsSearchableEntity(xUNID) Then
                            'this is not allowed - must *search* in a store
                            oStore = m_oUNID.GetStore(xUNID, xStoreName)
                            oListings = oB.SearchStore(oStore)
                        ElseIf iUNIDType = ciUNIDTypes.ciUNIDType_Folder And _
                            oB.IsSearchableEntity(xUNID) Then
                            'selected node is a folder - search folder
                            oFolder = m_oUNID.GetFolder(xUNID)
                            oFolder.Name = Me.tvFolders.SelectedNode.Text
                            oListings = oB.SearchFolder(oFolder, bCancelled)
                        End If
                    Case ciSearchTypes.ciSearchType_None
                        'clear filter conditions
                        oFilter.Reset()

                        If iUNIDType = ciUNIDTypes.ciUNIDType_Store And _
                            oB.IsLoadableEntity(xUNID) Then
                            'selected node is a store and store contact loads
                            'are supported by the backend - search store
                            oStore = m_oUNID.GetStore(xUNID, xStoreName)
                            oListings = oB.GetStoreListings(oStore, oFilter)
                        ElseIf iUNIDType = ciUNIDTypes.ciUNIDType_Folder And _
                            oB.IsLoadableEntity(xUNID) Then
                            'selected node is a folder - get all listing in folder
                            oFolder = m_oUNID.GetFolder(xUNID)
                            oFolder.Name = Me.tvFolders.SelectedNode.Text
                            oListings = oB.GetFolderListings(oFolder, oFilter)
                        ElseIf iUNIDType = ciUNIDTypes.ciUNIDType_Backend And oB.IsLoadableEntity(xUNID) Then
                            oStore = m_oUNID.GetStore(xUNID, xStoreName)
                            oListings = oB.GetStoreListings(oStore, oFilter)
                        End If
                    Case ciSearchTypes.ciSearchType_Refresh
                        If oB.IsLoadableEntity(xUNID) Then
                            If iUNIDType = ciUNIDTypes.ciUNIDType_Store Then
                                oStore = m_oUNID.GetStore(xUNID, xStoreName)
                                'todo
                                ' ''oFilter.Reset()
                                oListings = oB.GetStoreListings(oStore, oFilter)
                                Cursor.Current = Cursors.Default
                            ElseIf iUNIDType = ciUNIDTypes.ciUNIDType_Folder And _
                                oB.IsLoadableEntity(xUNID) Then
                                'selected node is a folder - get all listing in folder
                                oFolder = m_oUNID.GetFolder(xUNID)
                                'todo
                                ' ''oFilter.Reset()
                                oFolder.Name = Me.tvFolders.SelectedNode.Text
                                oListings = oB.GetFolderListings(oFolder, oFilter)
                            End If

                        Else
                            MsgBox("Can't remove filter because the selected folder or filing cabinet is not loadable.", vbOKOnly + vbExclamation, Main.g_oSessionType.AppTitle)
                        End If
                End Select

                If (Not bCancelled) And (Not (oListings Is Nothing)) Then
                    'assign listings to module level listings collection, which
                    'is the collection that is displayed in the control

                    m_oListings = oListings

                    'clear out listings
                    With Me.grdListings
                        If Not .DataSource Is Nothing Then
                            .DataSource.Clear()
                            .Refresh()
                        End If

                        With Me
                            'set label showing folder of current contacts
                            .lblStatus.Text = " " & oNode.FullPath

                            'bind to list box
                            If Not m_oListings Is Nothing Then
                                With .grdListings
                                    Functions.EchoOff()
                                    .DataSource = m_oListings.ToArray
                                    '.Refresh()

                                    'stop here if there are no contact in set
                                    If m_oListings.Count = 0 Then
                                        m_oEvents_AfterListingsRetrieved(0)
                                        Cursor.Current = Cursors.Default

                                        If iSearchType <> ciSearchTypes.ciSearchType_None Then
                                            MsgBox("No contacts in '" & oNode.Text & _
                                                "' matched the supplied search criteria.", vbExclamation, Main.g_oSessionType.AppTitle)
                                        End If

                                        SetupForInputState(True)

                                        If iSearchType = ciSearchTypes.ciSearchType_Quick Then
                                            'return focus to quick search to try again
                                            Me.tvFolders.Focus()
                                        End If
                                        LoadContacts = Nothing
                                        Exit Function
                                    End If

                                    'If .Rows.Count > 0 Then
                                    '    On Error Resume Next
                                    '    If .SelectedRows.Count = 0 Then
                                    '        .Rows(0).Selected = True
                                    '    End If
                                    '    On Error GoTo ProcError
                                    'End If

                                    'show appropriate panel
                                    If Me.btnGroups.Checked Then
                                        ShowPanel(CIPanels.ciPanel_Groups)
                                    Else
                                        ShowPanel(CIPanels.ciPanel_Contacts)
                                    End If
                                End With
                                '.Refresh()
                            End If
                        End With

                        With .Columns
                            If iPrevBackendID <> iBackendID Then
                                'backend has changed - set column names
                                If oB.Col1Name = String.Empty Then
                                    Throw New Exception("No column 1 Name specified.")
                                End If

                                If oB.Col2Name = String.Empty Then
                                    .Item(1).Visible = False
                                Else
                                    .Item(1).HeaderText = oB.Col2Name
                                    .Item(1).Visible = True
                                End If

                                If oB.Col3Name = String.Empty Then
                                    .Item(2).Visible = False
                                Else
                                    .Item(2).HeaderText = oB.Col3Name
                                    .Item(2).Visible = True
                                End If

                                If oB.Col4Name = String.Empty Then
                                    .Item(3).Visible = False
                                Else
                                    .Item(3).HeaderText = oB.Col4Name
                                    .Item(3).Visible = True
                                End If
                            End If

                            'hide last columns if any
                            For index = .Count - 1 To 4 Step -1
                                .Item(index).Visible = False
                            Next

                            FillLastVisibleDataGridViewColumn(Me.grdListings)

                            SetUpColumns()

                        End With
                    End With

                    'store these for next execution of LoadContacts
                    iPrevBackendID = iBackendID
                    iPrevSearchType = iSearchType

                    'save sort column in user ini
                    Main.SetUserIni("CIApplication", "Sort", oFilter.SortColumn)

                    LoadContacts = m_oListings.Count
                    PanelChange(False)

                End If  'not bCancelled

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                SetupForInputState(True)
                Cursor.Current = Cursors.Default
            End Try
        End Function
        Private Sub LoadAddresses()
            'loads available addresses for selected listing
            Dim oListing As CListing
            Dim oAddress As CAddress
            Dim i As Integer
            Dim xOrigAddress As String
            Dim dtAddressTypes As DataTable

            Try
                SetupForInputState(False)
                Cursor.Current = Cursors.WaitCursor

                With Me
                    'get existing address selection
                    xOrigAddress = .cbxAddressType.Text

                    'clear address list
                    .cbxAddressType.ClearList()

                    If .grdListings.SelectedRows.Count > 1 Then
                        'get all address types for the backend
                        m_oAddresses = CurrentBackend.GetAddresses()
                    Else
                        'get addresses for listing
                        If .grdListings.SelectedRows.Count = 0 Then
                            'no listing is selected
                            SetupForInputState(True)
                            Exit Sub
                        End If

                        'a row has been selected - get listing from UNID, which is bound text
                        oListing = m_oUNID.GetListing(.grdListings.SelectedRows(0).Cells(ciListingCols.ciListingCols_UNID).Value.ToString())

                        If oListing Is Nothing Then
                            'couldn't get listing from bound text - something is wrong
                            Throw New Exception("Listing with Display Name '" & _
                                .grdListings.Columns(0).HeaderText & "' is invalid.")
                        End If

                        'get addresses from listing
                        m_oAddresses = Main.GetBackendFromID(oListing.BackendID) _
                                            .GetAddresses(oListing)
                    End If

                    If m_oAddresses Is Nothing Then
                        'couldn't get addresses from listing - error
                        Throw New Exception("Invalid address object from listing.")
                    End If

                    Dim bMailingAddressIndex As Boolean
                    Dim xMailingAddress As String = ""

                    dtAddressTypes = New DataTable

                    'Add columns
                    With dtAddressTypes.Columns
                        .Add("Name")
                        .Add("UNID")
                    End With

                    'GLOG : 15918 : ceh
                    'add addresses to list
                    With .cbxAddressType
                        For i = 1 To m_oAddresses.Count
                            oAddress = m_oAddresses.Item(i)
                            If Not (oAddress Is Nothing) Then
                                dtAddressTypes.Rows.Add(oAddress.Name, oAddress.UNID)
                                If InStr(UCase$(oAddress.Name), "MAILING") > 0 Then
                                    'xMailingAddress = oAddress.Name
                                    xMailingAddress = oAddress.UNID
                                End If
                            End If
                        Next i

                        'load control
                        '.SortOrder = SortOrder.Ascending
                        .SetList(dtAddressTypes)

                        'select address
                        If .Items.Count > 0 Then
                            .Refresh()

                            If xMailingAddress <> "" Then
                                Me.cbxAddressType.Value = xMailingAddress
                            Else
                                .SelectedIndex = 0
                            End If


                            'left in old address selection code, just in case we want to return
                            'If True Then
                            '    'todo
                            '    'If xMailingAddress <> "" Then
                            '    '    Me.cbxAddressType.Text = xMailingAddress
                            '    'Else
                            '    '.SelectedIndex = 0
                            '    'End If
                            '    'select first address in list
                            '    .SelectedIndex = 0
                            'Else
                            'Try
                            '    xOrigAddress = Trim$(Replace(xOrigAddress, _
                            '        "(mailing)", String.Empty, , , vbTextCompare))
                            'Catch
                            'End Try


                            '.Text = xOrigAddress

                            'If .SelectedIndex = -1 Then
                            '    'original address is not in list-
                            '    'select first address in list
                            '    .SelectedIndex = 0
                            'End If
                        End If
                            'End If
                    End With
                End With

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                SetupForInputState(True)
                Cursor.Current = Cursors.Default
            End Try
        End Sub
        Private Sub AddGroup()
            'creates a new contact group
            Dim oForm As AddGroupForm
            Dim oGroups As CGroups
            Dim oFolders As CFolders

            oForm = New AddGroupForm

            Try

                With oForm
                    'show add group dialog
                    .ShowDialog()

                    If Not .Canceled Then
                        'add group
                        oGroups = Me.GroupsBackend
                        oGroups.AddGroup(.txtGroupName.Text)

                        'refresh groups list
                        RefreshGroupsList()

                        'select new group in list
                        oFolders = Me.GroupsBackend.GetFolders(Nothing)
                        For Each oFolder As CFolder In oFolders
                            If oFolder.Name = .txtGroupName.Text Then
                                Me.cbxGroups.Value = oFolder.ID
                                Exit For
                            End If
                        Next oFolder

                        'update tree
                        RefreshTreeBranch(Me.tvFolders.Nodes.Item("b" & Me.GroupsBackend.InternalID))
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                oForm.Close()
                oForm = Nothing
            End Try
        End Sub
        Private Sub DeleteGroup()
            Dim oGroups As CGroups
            Dim iChoice As MsgBoxResult

            With Me.cbxGroups
                If .SelectedIndex <> -1 Then
                    'item is selected - check to see if this group is active (loaded)
                    oGroups = Me.GroupsBackend
                    If bCurrentGroupIsLoadedFolder() Then
                        'don't allow the folder to be deleted
                        MsgBox("You can't delete this group because it is currently your active " & _
                            "contacts folder." & vbCr & vbCr & "Switch to a different folder, " & _
                            "then try deleting this group.", vbExclamation)
                        Exit Sub
                    Else
                        'get currently selected tree node

                        'prompt to delete it
                        iChoice = MsgBox("Delete the group '" & _
                            Me.cbxGroups.Text & "'?", vbQuestion + vbYesNo, Main.g_oSessionType.AppTitle)
                    End If

                    If iChoice = vbYes Then
                        oGroups.DeleteGroup(.Value)

                        'refresh groups list
                        RefreshGroupsList()
                        'update tree
                        RefreshTreeBranch(Me.tvFolders.Nodes _
                            .Item("b" & Me.GroupsBackend.InternalID))
                    End If
                End If
            End With
        End Sub
        Private Sub RefreshGroupsList()
            'refresh the Groups combo
            Dim oFolders As CFolders
            Dim dtGroups As DataTable

            Try
                'get "groups" folder
                oFolders = Me.GroupsBackend.GetFolders(Nothing)

                dtGroups = New DataTable

                'Add columns
                With dtGroups.Columns
                    .Add("Group")
                    .Add("UNID")
                End With

                For Each oFolder As CFolder In oFolders
                    dtGroups.Rows.Add(oFolder.Name, oFolder.ID)
                Next oFolder

                'load control
                Me.cbxGroups.SetList(dtGroups)

                If dtGroups.Rows.Count = 0 Then
                    Me.lstGroupMembers.DataSource = Nothing
                    Me.lstGroupMembers.Items.Clear()
                    Me.cbxGroups.Value = ""
                End If

            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub AddMembers()
            'adds the selected listings to the specified contact group
            Dim oContacts As CContacts
            Dim oC As CContact
            Dim oL As CListing
            Dim oAddr As CAddress = Nothing
            Dim lBookmark As Long
            Dim l As Long
            Dim lGroupID As Long
            Dim lID As Long
            Dim xUNID As String
            Dim iID As Integer
            Dim oGroups As CGroups

            Try
                With Me
                    With .cbxGroups
                        'alert and exit if no group selected
                        If .SelectedIndex = -1 Then
                            MsgBox("Please select a group before adding a group member.", _
                                vbExclamation, Main.g_oSessionType.AppTitle)
                            Exit Sub
                        End If

                        'get group ID
                        lGroupID = .SelectedValue

                        'alert and don't allow if loaded folder is a group -
                        'users can't add group members to other groups
                        xUNID = Me.tvFolders.SelectedNode.Name

                        iID = CUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Backend)
                        If iID = Me.GroupsBackend.ID Then
                            MsgBox("You can't add members of one group to another group.", vbExclamation)
                            Exit Sub
                        End If
                    End With

                    If .grdListings.SelectedRows.Count = 0 Then
                        MsgBox("Please select a contact before adding a group member.", _
                            vbOKOnly + vbExclamation, Main.g_oSessionType.AppTitle)
                        Exit Sub
                    End If

                    Cursor.Current = Cursors.WaitCursor

                    For l = 0 To .grdListings.SelectedRows.Count - 1
                        lBookmark = .grdListings.SelectedRows(l).Index

                        'get selected listing
                        oL = m_oListings.Item(lBookmark)
                        If oL Is Nothing Then
                            Throw New Exception("Please select a contact before adding a group member.")
                        End If

                        If m_oAddresses.Count > 0 Then
                            'exit if still no type is chosen
                            If .cbxAddressType.SelectedIndex = -1 Then
                                Exit Sub
                            End If

                            'get selected address
                            Try
                                oAddr = m_oAddresses.Item(.cbxAddressType.SelectedIndex + 1)
                            Catch
                            End Try

                            If oAddr Is Nothing Then
                                Throw New Exception("Selected address is invalid.")
                            End If
                        End If

                        'get contact from listing and address
                        oContacts = Main.ListingBackend(oL).GetContacts(oL, oAddr, vbNull, _
                                                                        ciRetrieveData.ciRetrieveData_Names + ciRetrieveData.ciRetrieveData_Addresses, _
                                                                        ciAlerts.ciAlert_NoAddresses)

                        If Not (oContacts Is Nothing) Then
                            'add to contact collection
                            For Each oC In oContacts
                                oGroups = Me.GroupsBackend
                                Try
                                    lID = oGroups.AddMember(oC, lGroupID)
                                Catch
                                End Try

                                If (lID <> 0) Then
                                    'add full name to selected list
                                    If (oC.AddressTypeName <> String.Empty) Then
                                        m_dtMembers.Rows.Add(oC.DisplayName & "  (" & oC.AddressTypeName & ")", lID)
                                    Else
                                        m_dtMembers.Rows.Add(oC.DisplayName & "  (No Address)", lID)
                                    End If
                                End If
                            Next oC
                        End If
                    Next l
                End With

                'load control
                LoadGroupMembers()

                Me.grdListings.Focus()
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                Cursor.Current = Cursors.Default
            End Try
        End Sub
        Private Function bCurrentGroupIsLoadedFolder() As Boolean
            'returns True if the current group is also the loaded contacts folder
            Dim iBEndID As Integer
            Dim lActiveFolderID As Long
            Dim lCurGroupsFolderID As Long
            Dim xActiveFolderUNID As String

            Try
                'get UNID of loaded (active folder)
                xActiveFolderUNID = Me.tvFolders.SelectedNode.Name

                'get backend of active folder
                iBEndID = CUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields.ciUNIDFields_Backend)

                If iBEndID = Me.GroupsBackend.InternalID Then
                    'backend is groups backend - test for folder
                    lActiveFolderID = CUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields.ciUNIDFields_Folder)

                    With Me.cbxGroups
                        lCurGroupsFolderID = .SelectedValue
                    End With

                    bCurrentGroupIsLoadedFolder = lActiveFolderID = lCurGroupsFolderID
                End If

                Return bCurrentGroupIsLoadedFolder
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        Private Sub DeleteMembers()
            Dim oGroups As CGroups
            Dim iChoice As MsgBoxResult
            Dim iSelCount As Integer
            Dim iSelItem As Integer
            Dim i As Integer

            Try
                'get number of selected group members
                iSelCount = Me.lstGroupMembers.SelectedItems.Count()

                If iSelCount = 0 Then
                    MsgBox("Please select one or more group members to delete.", vbExclamation)
                    Exit Sub
                End If

                'alert and don't allow if loaded folder is the group
                'from which the user is attempting to delete members
                If bCurrentGroupIsLoadedFolder() Then
                    MsgBox("You can't edit this group because it is " & _
                        "currently your active contacts folder." & vbCr & vbCr & "Switch to a different folder, " & _
                        "then try editing this group.", vbExclamation)
                    Exit Sub
                End If

                '   prompt to delete
                If iSelCount = 1 Then
                    iChoice = MsgBox("Delete '" & Me.lstGroupMembers.Text & "' from this group?", vbYesNo + vbExclamation)
                Else
                    iChoice = MsgBox("Delete the selected group members from this group?", vbYesNo + vbExclamation)
                End If

                If iChoice = vbYes Then
                    'do delete
                    oGroups = Me.GroupsBackend

                    With Me.lstGroupMembers

                        For i = .SelectedItems.Count - 1 To 0 Step -1
                            oGroups.DeleteMember(.SelectedValue)
                            m_dtMembers.Rows(.SelectedIndices(i)).Delete()
                            'mark for later use
                        Next i

                        .Focus()
                    End With
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub EditContact()
            Dim oB As ICIBackend
            Dim oL As CListing
            Dim lBmk As Long

            Try
                With Me.grdListings.SelectedRows
                    If .Count = 0 Then
                        MsgBox("Please select a contact from the list " & _
                            "of available contacts before running this function.", _
                            vbExclamation, Main.g_oSessionType.AppTitle)
                        Exit Sub
                    End If

                    'get the backend of the contact
                    lBmk = .Item(0).Index
                End With

                'get selected listing
                oL = m_oListings.Item(lBmk)

                oB = Main.GetBackendFromID(oL.BackendID)

                If oB.SupportsContactEdit() Then
                    'run the edit method of the backend
                    oB.EditContact(oL)
                Else
                    'alert
                    MsgBox("You can't use " & Main.g_oSessionType.AppTitle & _
                        " to edit contacts stored in " & oB.Name & ".", _
                        vbExclamation, Main.g_oSessionType.AppTitle)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub ResetQuickSearch()
            Me.txtFilterValue1.Text = String.Empty
            CurrentBackend.Filter.Reset()
        End Sub
        Private Sub RefreshItems()
            'refreshes either the folder tree branch
            'or contacts depending on visible panel
            Dim oNode As TreeNode

            If Me.pnlFolders.Visible Then
                'refresh the selected folder tree branch
                oNode = Me.tvFolders.SelectedNode

                If Not oNode Is Nothing Then
                    RefreshTreeBranch(oNode)
                End If

            Else
                'refresh contacts
                LoadContacts(ciSearchTypes.ciSearchType_Refresh)
            End If
        End Sub

        Private Sub BuildHelpMenu()
            Dim xPath As String
            Dim xFiles() As String
            Dim xFile As String
            Dim item As ToolStripItem
            Dim sep As ToolStripSeparator

            With Me
                'clear menu
                btnHelpNew.DropDownItems.Clear()

                'add files to menu
                If Directory.Exists(Main.g_xHelpPath) Then
                    m_oMenuArray = New HelpMenuArray(btnHelpNew, Main.g_xHelpPath)
                End If

                If btnHelpNew.DropDownItems.Count Then
                    'add separator
                    sep = New ToolStripSeparator
                    btnHelpNew.DropDownItems.Add(sep)
                End If

                'add About... menu item
                item = New ToolStripMenuItem
                item.Text = "&About " & Main.g_oSessionType.AppTitle
                item.Tag = "mnuShowHelpAbout"

                'add menu item & click event handler
                btnHelpNew.DropDownItems.Add(item)
                AddHandler item.Click, AddressOf m_oMenuArray_Click

                'GLOG : 8765 : ceh
                'Ensure Help menu is enabled
                btnHelpNew.Enabled = True
            End With

        End Sub
        Private Sub RefreshPanel()
            'refreshes either the folder tree branch
            'or contacts depending on visible panel
            Dim oNode As TreeNode

            If Me.pnlFolders.Visible Then
                'refresh the selected folder tree branch
                oNode = Me.tvFolders.SelectedNode

                If Not oNode Is Nothing Then
                    RefreshTreeBranch(oNode)
                End If
            Else
                'refresh contacts
                LoadContacts(ciSearchTypes.ciSearchType_Refresh)
            End If

        End Sub
        Private Sub LoadGroupMembers()
            'loads the group members of the selected
            'group into the group members list

            Dim oListings As CListings
            'Dim oFolder As CFolder
            'Dim oUNID As CUNID
            Dim lID As Long
            Dim oGroups As CGroups
            Dim l As Long

            With Me.cbxGroups
                If .SelectedIndex = -1 Then
                    Exit Sub
                End If

                'get id of selected group
                lID = .SelectedValue
            End With

            'clear existing items
            Me.lstGroupMembers.DataSource = Nothing
            Me.lstGroupMembers.Items.Clear()

            'oUNID = New CUNID
            oGroups = Me.GroupsBackend
            With oGroups
                oListings = .GetMembers(lID)

                m_dtMembers = New DataTable
                'Add columns
                With m_dtMembers.Columns
                    .Add("Name")
                    .Add("ID")
                End With

                If oListings.Count Then
                    For l = 0 To oListings.Count - 1
                        m_dtMembers.Rows.Add(oListings.Item(l).DisplayName, _
                                           oListings.Item(l).ID)
                    Next l

                    'load control
                    With Me.lstGroupMembers
                        .DisplayMember = m_dtMembers.Columns(0).ColumnName
                        .ValueMember = m_dtMembers.Columns(1).ColumnName
                        .DataSource = m_dtMembers
                        .SelectedItems.Clear()
                    End With
                End If

            End With
        End Sub
        Private Sub LoadGroup()
            Dim xGroup As String
            Dim lNum As Long

            'get current group
            xGroup = Me.cbxGroups.Text

            If xGroup <> String.Empty Then
                'load the group by selecting node, then loading contacts
                SelectNode(Me.GroupsBackend.Name & "\" & xGroup)

                'load contacts
                lNum = LoadContacts()

                If lNum > 0 Then
                    'exit group mode
                    Me.btnGroups.Checked = False
                End If
            Else
                MsgBox("Please select a group to load.", vbExclamation)
            End If
        End Sub
        Private Function SelListFromControlIndex(ByVal iIndex As Integer) As Integer
            'selection list is 2 exponent lstContacts index
            SelListFromControlIndex = 2 ^ iIndex
        End Function
        Private Sub MoveSelContactDown()
            Dim oLB As ListBox
            Dim iIndex As Integer
            Dim xDisplayText As String
            Dim xItemData As Long
            Dim i As Integer

            Try
                'get list box
                oLB = SelectionListControl()

                'exit if the last item in the list is selected
                If oLB.SelectedItems.Count = 0 Then
                    Exit Sub
                ElseIf oLB.GetSelected(oLB.Items.Count - 1) Then
                    Exit Sub
                End If

                With Me
                    .btnMoveDown.Enabled = False
                    .btnMoveUp.Enabled = False
                End With

                'for each selected index, move down
                For i = oLB.Items.Count - 1 To 0 Step -1
                    If oLB.GetSelected(i) Then
                        oLB.Items.Insert(i + 2, oLB.SelectedItem)
                        oLB.Items.RemoveAt(oLB.SelectedIndex)
                        oLB.SelectedIndex = i + 1

                        'get current index of contact in collection
                        With oLB
                            Select Case .Name
                                Case lstTo.Name
                                    iIndex = i + 1
                                Case lstFrom.Name
                                    iIndex = lstTo.Items.Count + i + 1
                                Case lstCC.Name
                                    iIndex = lstTo.Items.Count + _
                                        lstFrom.Items.Count + i + 1
                                Case lstBCC.Name
                                    iIndex = lstTo.Items.Count + _
                                        lstFrom.Items.Count + lstCC.Items.Count + i + 1
                            End Select
                        End With

                        Dim oContact As CContact

                        'move contact one down in collection
                        oContact = m_oContacts.Item(iIndex)

                        'delete contact from collection
                        m_oContacts.Delete(iIndex)

                        'add contact at new position
                        If iIndex + 1 > m_oContacts.Count Then
                            'contact needs to be added to the end
                            m_oContacts.Add(oContact)
                        Else
                            m_oContacts.Add(oContact, iIndex + 1)
                        End If
                    End If
                Next i

                'give list focus again
                oLB.Focus()
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                With Me
                    .btnMoveDown.Enabled = True
                    .btnMoveUp.Enabled = True
                End With
            End Try
        End Sub
        Private Sub MoveSelContactUp()
            Dim oLB As ListBox
            Dim iIndex As Integer
            Dim xDisplayText As String
            Dim xItemData As Long
            Dim i As Integer

            Try
                'get list box
                oLB = SelectionListControl()

                'exit if the first item in the list is selected
                If oLB.SelectedItems.Count = 0 Or oLB.SelectedIndex = 0 Then
                    Exit Sub
                End If

                With Me
                    .btnMoveDown.Enabled = False
                    .btnMoveUp.Enabled = False
                End With

                'for each selected index, move down
                For i = 0 To oLB.Items.Count - 1
                    If oLB.GetSelected(i) Then
                        oLB.Items.Insert(i - 1, oLB.SelectedItem)
                        oLB.Items.RemoveAt(oLB.SelectedIndex)
                        oLB.SelectedIndex = i - 1

                        'get current index of contact in collection
                        With oLB
                            Select Case .Name
                                Case lstTo.Name
                                    iIndex = i + 1
                                Case 1
                                    iIndex = lstTo.Items.Count + i + 1
                                Case 2
                                    iIndex = lstTo.Items.Count + _
                                        lstFrom.Items.Count + i + 1
                                Case 3
                                    iIndex = lstTo.Items.Count + _
                                        lstFrom.Items.Count + lstCC.Items.Count + i + 1
                            End Select
                        End With

                        Dim oContact As CContact

                        'move contact one up in collection
                        oContact = m_oContacts.Item(iIndex)
                        m_oContacts.Delete(iIndex)
                        m_oContacts.Add(oContact, iIndex - 1)
                    End If
                Next i

                'give list focus again
                oLB.Focus()
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                With Me
                    .btnMoveDown.Enabled = True
                    .btnMoveUp.Enabled = True
                End With
            End Try
        End Sub
        Private Sub SelectAllContacts()
            Dim l As Long

            'select all
            With m_lstContacts(m_iCurList)
                For l = 0 To .Items.Count - 1
                    .SetSelected(l, True)
                Next l
            End With
        End Sub
        Private Sub SelectAllGroupMembers()
            Dim l As Long

            'select all
            With Me.lstGroupMembers
                For l = 0 To .Items.Count - 1
                    .SetSelected(l, True)
                Next l
            End With
        End Sub
        Private Sub ShowContactDetail(ByVal iUniqueTagNumber As Integer)
            'shows the detail for the contact having the specified tag number-
            'number is stored in tag property and is issued when the
            'contact is added to right side list
            Dim oForm As ContactDetailForm
            Dim oContact As CContact
            Dim i As Integer

            If m_oContacts.Count = 0 Then
                Exit Sub
            End If

            oForm = New ContactDetailForm

            With oForm
                'assign contacts to form
                .Contacts = m_oContacts

                On Error Resume Next

                If iUniqueTagNumber = 0 Then
                    'no contact is selected in the list - select the first one
                    'to display in the detail dialog
                    .SelectedContactIndex = 0
                Else
                    'select the right contact -
                    'cycle through all until the one
                    'with the specified tag is found
                    For i = 1 To .Contacts.Count
                        If .Contacts.Item(i).Tag = iUniqueTagNumber Then
                            'this is the one
                            .SelectedContactIndex = i
                            Exit For
                        End If
                    Next i
                End If

                'show contact detail form
                .ShowDialog()
                .Close()
                oForm = Nothing
            End With
        End Sub
        Private Sub ShowMemberDetail(ByVal lID As Long)
            'shows the detail form for the first selected listing
            Dim oForm As ListingDetailForm
            Dim lRow As Long
            Dim oUNID As CUNID
            Dim xUNID As String

            Cursor.Current = Cursors.WaitCursor

            'get member UNID
            oUNID = New CUNID
            xUNID = Main.Backends.GroupsBackend.InternalID & Main.g_oConstants.UNIDSep & Main.g_oConstants.UNIDSep & Main.g_oConstants.UNIDSep & lID

            'show detail form
            oForm = New ListingDetailForm
            With oForm
                .Listing = oUNID.GetListing(xUNID)
                .Addresses = Main.Backends.GroupsBackend.GetAddresses(.Listing)
                .ShowDialog()

                Cursor.Current = Cursors.Default

            End With

            oForm.Close()
            Me.lstGroupMembers.Focus()
            oForm = Nothing
        End Sub
        Private Sub OnListingsDblClick()
            Dim i As Integer
            Dim bDefaultSet As Boolean

            If Not Me.btnGroups.Checked Then
                For i = 0 To 4
                    If (m_btnAdd(i).Tag = "Default") Then
                        bDefaultSet = True
                        Exit For
                    End If
                Next

                If Not bDefaultSet Then
                    For i = 0 To 4
                        If (m_btnAdd(i).Visible) Then
                            m_btnAdd(i).Tag = "Default"
                            Exit For
                        End If
                    Next
                End If
                AddSelContacts()
            Else
                AddMembers()
            End If
        End Sub
        Private Sub Sort(ByVal ColIndex As Integer)
            'GLOG : 15797 : ceh
            With Me.grdListings
                'exit if there aren't any listings
                If grdListings.RowCount = 0 Then
                    Exit Sub
                End If

                'do sort
                m_oListings.Sort(ColIndex)

                Functions.EchoOff()

                .DataSource = m_oListings.ToArray()

                .ClearSelection()

                'set this column as the default sort column for this backend
                CurrentBackend.DefaultSortColumn = ColIndex

                Application.DoEvents()

                For Each r As DataGridViewRow In grdListings.Rows
                    If r.Cells(0).Value = m_SelectedRowID Then
                        r.Selected = True
                        .FirstDisplayedScrollingRowIndex = r.Index
                        Exit For
                    End If
                Next

                Functions.EchoOn()

            End With
        End Sub

        Private Sub SelectAllListings()
            Dim l As Long

            Try
                Application.DoEvents()

                Me.grdListings.SelectAll()

                OnListingsRowChange()
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                Cursor.Current = Cursors.Default
            End Try
        End Sub

        Private Sub ShowListingDetail()
            'shows the detail form for the first selected listing
            Dim oForm As ListingDetailForm
            Dim lRow As Long

            If m_oListings.Count = 0 Then
                Exit Sub
            ElseIf Me.grdListings.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            'show detail form
            oForm = New ListingDetailForm
            With oForm
                .Listing = m_oListings.Item(Me.grdListings.SelectedRows(0).Index)

                If Me.grdListings.SelectedRows.Count = 1 Then
                    'only one contact is selected - we can use the
                    'addresses in the address list - if there are multiple
                    'selections, we can't use these - the form will get
                    'the addresses from the supplied listing
                    .Addresses = m_oAddresses
                End If

                If Me.cbxAddressType.Items.Count > 0 Then
                    '.AddressIndex = Me.cbxAddressType.ListIndex
                    .AddressName = Me.cbxAddressType.Text
                End If
                .ShowDialog()

                Me.Cursor = Cursors.Default

                'select the address that was selected
                'in the listing detail form
                Dim iIndex As Integer
            End With

            If Not oForm.Canceled Then
                'iIndex = oForm.AddressIndex
                Try
                    Me.cbxAddressType.Text = Mid$(oForm.AddressName, 2)
                Catch
                End Try
                oForm.Close()
            End If

            Me.grdListings.Focus()
            oForm = Nothing
        End Sub

        Private Sub OnListingsRowChange()
            'executes all necessary functionality
            'when a listing is selected
            Dim xTempPath As String
            Dim xBackendName As String
            Dim xFolderPath As String
            Dim xFullPath As String
            Dim oSelectedRow As DataGridViewRow

            With Me
                If grdListings.SelectedRows.Count Then
                    'get folder path of selected listing
                    xTempPath = m_oListings.Item(.grdListings.SelectedRows(0).Index).FolderPath

                    If xTempPath <> String.Empty Then
                        xFullPath = .tvFolders.SelectedNode.FullPath
                        xBackendName = xFullPath.Substring(0, InStr(xFullPath, "\"))
                    Else
                        xBackendName = ""
                    End If

                    If xBackendName <> String.Empty Then
                        xFolderPath = xBackendName & xTempPath
                    Else
                        xFolderPath = .tvFolders.SelectedNode.FullPath
                    End If

                    .lblStatus.Text = xFolderPath

                    LoadAddresses()

                    EnableAddButtons(True)

                    'enable/disable edit button per backend support
                    .btnEdit.Enabled = CurrentBackend.SupportsContactEdit

                Else
                    EnableAddButtons(False)
                    Me.btnEdit.Enabled = False
                End If
                .grdListings.Focus()
            End With

        End Sub
        Private Sub EnableAddButtons(ByVal bEnable As Boolean)
            'enables/disables all add buttons
            Dim i As Integer

            'todo
            ' ''With Me.btnAdd
            ' ''    For i = .LBound To .UBound
            ' ''        .Item(i).Enabled = bEnable
            ' ''    Next i
            ' ''End With
            Me.btnAddMember.Enabled = bEnable
        End Sub

        Private Function ShowTreeTop() As Long
            'creates first level nodes on tree
            Dim oBackend As ICIBackend
            Dim oStores As CStores
            Dim oStore As CStore

            Try
                If Me.ShowBackendNodes Or (Main.Backends.Count > 1) Then
                    Me.tvFolders.Nodes.Clear()

                    'show backend nodes
                    'For Each oBackend In Main.Backends 'todo
                    For index = 1 To Main.Backends.Count
                        oBackend = Main.Backends.ItemFromIndex(index)
                        'Main.Backends.ItemFromIndex(index)
                        If oBackend.SupportsFolders Or oBackend.SupportsMultipleStores Then
                            'create backend node-key can not convert to an integer
                            Me.tvFolders.Nodes.Add(key:="b" & oBackend.InternalID, _
                                                   text:=oBackend.DisplayName, _
                                                   imageKey:="Backend", _
                                                   selectedImageKey:="Backend")
                        Else
                            'backend has only one store - show the store as a backend node
                            oStores = oBackend.GetStores
                            Me.tvFolders.Nodes.Add(key:=oStores.Item(1).UNID, _
                                                   text:=oBackend.DisplayName, _
                                                   imageKey:="Backend", _
                                                   selectedImageKey:="Backend")
                        End If
                    Next


                    'Next oBackend
                Else
                    'show store nodes of backend 1 - get stores
                    oBackend = Main.GetBackendFromIndex(1)
                    oStores = oBackend.GetStores

                    'populate branch
                    For Each oStore In oStores
                        Me.tvFolders.Nodes.Add(key:=oStore.UNID, _
                                               text:=oStore.Name, _
                                               imageKey:="ClosedStore", _
                                               selectedImageKey:="OpenStore")
                    Next oStore
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        Private Function ShowTreeBranch(oNode As TreeNode) As Long
            'creates next level nodes on tree
            Dim i As Integer
            Dim iNumNodes As Integer
            Dim oBackend As ICIBackend
            Dim oStore As CStore
            Dim oFolder As CFolder
            Dim oStores As CStores
            Dim oFolders As CFolders
            Dim vStoreID As Object
            Dim vFolderID As Object
            Dim xUNID As String

            Try
                'get child node based on type of node
                xUNID = oNode.Name

                Select Case m_oUNID.GetUNIDType(xUNID)
                    Case ciUNIDTypes.ciUNIDType_Folder
                        'is folder - get subfolders
                        oBackend = Main.GetBackendFromUNID(xUNID)

                        If oBackend.SupportsNestedFolders Then
                            'create folder from UNID
                            oFolder = m_oUNID.GetFolder(xUNID)
                            oFolders = oBackend.GetSubFolders(oFolder)

                            If Not oFolders Is Nothing Then
                                'populate branch
                                If Not oFolders Is Nothing Then
                                    For Each oFolder In oFolders
                                        oNode.Nodes.Add(oFolder.UNID, oFolder.Name, "ClosedFolder", "OpenFolder")
                                        'Me.tvFolders.Nodes.Add(oNode, "", oFolder.UNID, oFolder.Name, "ClosedFolder", "OpenFolder")
                                    Next oFolder
                                End If
                            End If
                        End If
                    Case ciUNIDTypes.ciUNIDType_Store
                        'is store - get folders
                        oBackend = Main.GetBackendFromUNID(xUNID)

                        If oBackend.SupportsFolders Then
                            'create store from UNID
                            oStore = m_oUNID.GetStore(xUNID)
                            oFolders = oBackend.GetFolders(oStore)

                            If Not oFolders Is Nothing Then
                                'populate branch
                                For Each oFolder In oFolders
                                    oNode.Nodes.Add(oFolder.UNID, oFolder.Name, "ClosedFolder", "OpenFolder")
                                Next oFolder
                            End If
                        End If
                    Case ciUNIDTypes.ciUNIDType_Backend
                        'is backend - get stores
                        oBackend = Main.GetBackendFromUNID(xUNID)

                        If oBackend.SupportsMultipleStores Then
                            oStores = oBackend.GetStores

                            If Not oStores Is Nothing Then
                                'populate branch
                                For Each oStore In oStores
                                    oNode.Nodes.Add(oStore.UNID, oStore.Name, "ClosedStore", "OpenStore")
                                Next oStore
                            End If
                        Else
                            oFolders = oBackend.GetFolders(Nothing)

                            If Not oFolders Is Nothing Then
                                'populate branch
                                For Each oFolder In oFolders
                                    oNode.Nodes.Add(oFolder.UNID, oFolder.Name, "ClosedFolder", "OpenFolder")
                                Next oFolder
                            End If
                        End If
                End Select
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        Private Sub DeleteBranch(ByVal node As TreeNode)
            'deletes an entire branch of the tree and all the associated
            'NodeDetail objects - does so by recursive iterations
            If node.Nodes.Count Then
                While node.Nodes.Count
                    DeleteSubBranch(node.FirstNode)
                End While
            End If
        End Sub
        Private Sub DeleteSubBranch(ByVal node As TreeNode)
            'deletes an entire branch of the tree and all the associated
            'NodeDetail objects - does so by recursive iterations

            If node.Nodes.Count Then
                While node.Nodes.Count
                    DeleteSubBranch(node.FirstNode)
                End While
            Else
                Me.tvFolders.Nodes.Remove(node)
            End If
        End Sub
        Private Sub RefreshTreeBranch(ByVal node As TreeNode)
            Dim xKey As String
            Dim iIndex As Integer

            xKey = Me.tvFolders.SelectedNode.Tag
            iIndex = Me.tvFolders.SelectedNode.Index
            'delete existing branch
            DeleteBranch(node)
            'requery for updated tree branch
            ShowTreeBranch(node)
            'reselect previously selected node
            On Error Resume Next
            Me.tvFolders.Nodes.Item(xKey).Checked = True
            If Err.Number > 0 Then
                Me.tvFolders.Nodes.Item(iIndex - 1).Checked = True
            End If
            Err.Clear()
            'refresh toolbar if the refreshed node is the currently selected node
            If node Is Me.tvFolders.SelectedNode Then
                SetupDialogForNode(node)
            End If

        End Sub
        Private Sub RefreshBackendBranch()
            'refreshes the selected backend branch of the tree
            Dim oNode As Object

            Cursor.Current = Cursors.WaitCursor

            Me.Refresh()

            'select top parent
            With Me.tvFolders
                While Not (.SelectedNode.Parent Is Nothing)
                    .SelectedNode = .SelectedNode.Parent
                End While
                oNode = .SelectedNode
            End With

            RefreshTreeBranch(oNode)
            Cursor.Current = Cursors.Default
        End Sub
        Private Sub SetQuickFilterType()
            Dim bShowQuickFilter As Boolean = False

            bShowQuickFilter = (g_iQuickFilterType <> ciQuickFilterType.ciQuickFilterType_Text)

            Me.pnlQuickFilter.Visible = bShowQuickFilter

            'hide non-alpha search controls
            Me.lblQuickFilter.Visible = Not bShowQuickFilter
            Me.cmbFilterFld1.Visible = Not bShowQuickFilter
            Me.cmbOp1.Visible = Not bShowQuickFilter
            Me.txtFilterValue1.Visible = Not bShowQuickFilter
            Me.btnQuickSearch.Visible = Not bShowQuickFilter
        End Sub
        Private Sub SetDefaultFolder()
            Dim xFolder As String

            xFolder = Me.tvFolders.SelectedNode.FullPath

            Main.SetUserIni("CIApplication", ciUserIniKey_DefaultNode, _
                xFolder)

            Me.btnOptionsFolders.ToolTipText = "Default Folder: " & xFolder
        End Sub

        Private Sub SetupDialogForNode(ByVal node As TreeNode)
            Dim iUNIDType As ciUNIDTypes
            Dim i As Integer
            Dim oCtl As Control
            Dim xContainer As String
            Dim bShowQuickFilterOption As Boolean
            Dim bIsLoadable As Boolean
            Dim bIsSearchable As Boolean
            Dim datStart As Double
            Dim iloadTimeOut As Integer

            'GLOG : 15780 : ceh
            If Not Me.pnlFolders.Visible Then Exit Sub

            Try
                With Me
                    'enable native search if current backend supports it
                    .btnSearchNative.Enabled = CurrentBackend.SupportsNativeSearch
                    .btnAddContactInNative.Enabled = CurrentBackend.SupportsContactAdd

                    'get type of node
                    iUNIDType = m_oUNID.GetUNIDType(node.Name)


                    bIsLoadable = CurrentBackend.IsLoadableEntity(node.Name)
                    bIsSearchable = CurrentBackend.IsSearchableEntity(node.Name)

                    'enable 'find' button for searchable nodes
                    .btnSearch.Enabled = bIsSearchable
                    .btnQuickSearch.Enabled = bIsSearchable
                    .txtFilterValue1.Enabled = bIsSearchable
                    .cmbFilterFld1.Enabled = bIsSearchable
                    .cmbOp1.Enabled = bIsSearchable
                    .lblQuickFilter.Enabled = bIsSearchable

                    'enable alpha/numeric quick filter controls
                    .pnlQuickFilter.Enabled = bIsSearchable
                    For i = 0 To m_tsBFilter.Count - 1
                        m_tsBFilter(i).Enabled = bIsSearchable
                    Next i

                    Dim oFilter As CFilter
                    Dim oField As CFilterField
                    oFilter = CurrentBackend.Filter
                    For i = 0 To oFilter.CountFields - 1
                        oField = oFilter.FilterFields(i)
                        'setup quick filter option control caption
                        With m_optQuickFilter(i)
                            .Text = "&" & oField.Name
                            .Visible = True
                        End With
                    Next i

                    'GLOG : 5389 : ceh
                    For i = (oFilter.CountFields + 1) To 4
                        m_optQuickFilter(i - 1).Visible = False
                    Next i

                    For i = 0 To m_optQuickFilter.Count - 1
                        m_optQuickFilter(i).Enabled = bIsSearchable
                    Next i

                    'enable 'show contacts' button for loadable nodes
                    .btnTogglePanels.Enabled = bIsLoadable

                    .mnuFolders_LoadCurrentFolderOnStartup.Enabled = bIsLoadable
                End With

                If Not node.IsExpanded Then
                    Cursor.Current = Cursors.WaitCursor
                    ExpandNode(node)
                    'Get Favorite Contacts load timeout value if necessary
                    If UCase(node.Text) Like "*MAILBOX - *" Then

                        Try
                            iloadTimeOut = Main.g_oIni.GetIni("Backend" & CurrentBackend.ID, "FavContactsLoadTimeout", Main.g_oIni.CIIni)
                        Catch
                        End Try

                        If iloadTimeOut Then
                            'mark time - give timeout for log-in
                            datStart = vbNull
                            datStart = DateTime.Now.ToOADate()
                            While DateTime.Now.ToOADate() < datStart + (iloadTimeOut / ciSecsPerDay)
                                Application.DoEvents()
                            End While
                        End If
                    End If

                End If

                'refresh quick search controls based
                'on backend of selected node
                Application.DoEvents()

                'GLOG : 5390 : ceh
                If Not Main.bIsQuickFilterBackend(CurrentBackend.InternalID) Then
                    g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_Text
                Else
                    g_iQuickFilterType = ciQuickFilterType.ciQuickFilterType_AlphaNumeric
                End If

                SetQuickFilterType()

                RefreshQuickSearchControls()

                'setup Options menu
                Select Case CurrentBackend.InternalID
                    Case 100, 104, 111      'Outlook OM, MP9, MP10
                        bShowQuickFilterOption = True
                    Case Else
                        bShowQuickFilterOption = False
                End Select

                Me.mnuFolders_ToggleQuickFilterType.Visible = bShowQuickFilterOption
                Me.mnuFolders_Separator1.Visible = bShowQuickFilterOption

                'setup custom menu items
                SetupCustomMenuItems()

            Finally
                Cursor.Current = Cursors.Default
            End Try
        End Sub
        Private Sub SetupCustomMenuItems()
            'adds/removes menu items from popup menu based on backend-
            'configuration string is of the form 'MenuCaption,FunctionName,NodeType
            Dim xMenuCaption As String
            Dim xConfigArray() As String

            'remove any previous menus
            With Me
                .mnuFolders_Custom1.Visible = False
                .mnuFolders_Custom2.Visible = False
                .mnuFolders_Custom3.Visible = False
                .mnuFolders_Custom4.Visible = False
                .mnuFolders_Custom5.Visible = False
            End With

            If CurrentBackend.IsConnected Then
                'get first custom menu item caption
                xMenuCaption = CurrentBackend.CustomMenuItem1

                With Me.mnuFolders_Custom1
                    If xMenuCaption <> String.Empty Then
                        .Visible = True
                        .Text = xMenuCaption
                    Else
                        .Text = String.Empty
                    End If
                End With

                'get second custom menu item caption
                xMenuCaption = CurrentBackend.CustomMenuItem2

                With Me.mnuFolders_Custom2
                    If xMenuCaption <> String.Empty Then
                        .Visible = True
                        .Text = xMenuCaption
                    Else
                        .Text = String.Empty
                    End If
                End With

                'get third custom menu item caption
                xMenuCaption = CurrentBackend.CustomMenuItem3

                With Me.mnuFolders_Custom3
                    If xMenuCaption <> String.Empty Then
                        .Visible = True
                        .Text = xMenuCaption
                    Else
                        .Text = String.Empty
                    End If
                End With

                'get fourth custom menu item caption
                xMenuCaption = CurrentBackend.CustomMenuItem4

                With Me.mnuFolders_Custom4
                    If xMenuCaption <> String.Empty Then
                        .Visible = True
                        .Text = xMenuCaption
                    Else
                        .Text = String.Empty
                    End If
                End With

                'get fifth custom menu item caption
                xMenuCaption = CurrentBackend.CustomMenuItem5

                With Me.mnuFolders_Custom5
                    If xMenuCaption <> String.Empty Then
                        .Visible = True
                        .Text = xMenuCaption
                    Else
                        .Text = String.Empty
                    End If
                End With

                With Me
                    .mnuFolders_Separator2.Visible = (.mnuFolders_Custom1.Text + _
                                                        .mnuFolders_Custom2.Text + .mnuFolders_Custom3.Text + _
                                                        .mnuFolders_Custom4.Text + .mnuFolders_Custom5.Text) <> String.Empty
                End With
            End If

        End Sub
        Private Sub ExpandNode(ByVal node As TreeNode)
            'expands the specified tree node
            If node.GetNodeCount(False) = 0 Then
                ShowTreeBranch(node)
            End If
            node.ExpandAll()
        End Sub
        Public Sub RefreshQuickSearchControls()
            'refreshes the quick search fields and operators
            'based on the backend of selected node
            Dim oBackend As ICIBackend
            Dim xUNID As String
            Dim xSelFilter1 As String
            Dim xSelOp1 As String
            Dim xSelFilter2 As String
            Dim xSelOp2 As String
            Dim oFilter As CFilter
            Dim iOps As ciSearchOperators
            Dim i As Integer
            Dim dtFilterFlds As DataTable
            Dim dtFilterOps As DataTable
            Dim oFld As CFilterField

            Static iPrevBackendID As Integer

            'get backend
            oBackend = CurrentBackend()

            'exit if backend didn't change
            If iPrevBackendID = oBackend.ID Or oBackend.ID = 0 Then
                Exit Sub
            Else
                'set static var for next iteration
                iPrevBackendID = oBackend.ID
            End If

            'get currently selected filter -
            'used below to reselect in new lists
            With Me
                xSelFilter1 = .cmbFilterFld1.Text
                xSelOp1 = .cmbOp1.Text
            End With

            'get filter for backend
            oFilter = oBackend.Filter

            'add each filter field from filter to datatable
            dtFilterFlds = New DataTable

            'add columns
            dtFilterFlds.Columns.Add("Name")
            dtFilterFlds.Columns.Add("ID")

            With dtFilterFlds
                For i = 0 To oFilter.CountFields - 1
                    oFld = oFilter.FilterFields(i)
                    .Rows.Add(oFld.Name, oFld.ID)
                    'setup quick filter option control caption
                    With m_optQuickFilter(i)
                        .Text = "&" & oFld.Name
                        .Visible = True
                    End With
                Next i
            End With

            'bind array to both filter fields lists
            Me.cmbFilterFld1.SetList(dtFilterFlds)

            '   add each filter operator for this backend to xarray
            dtFilterOps = New DataTable

            'add columns
            dtFilterOps.Columns.Add("Description", GetType(String))
            dtFilterOps.Columns.Add("ID", GetType(Integer))

            iOps = oBackend.SearchOperators
            With dtFilterOps
                If iOps And ciSearchOperators.ciSearchOperator_BeginsWith Then
                    .Rows.Add("Begins With", ciSearchOperators.ciSearchOperator_BeginsWith)
                End If
                If iOps And ciSearchOperators.ciSearchOperator_Contains Then
                    .Rows.Add("Contains", ciSearchOperators.ciSearchOperator_Contains)
                End If
                If iOps And ciSearchOperators.ciSearchOperator_Equals Then
                    .Rows.Add("Is", ciSearchOperators.ciSearchOperator_Equals)
                End If
            End With

            'bind array to both filter fields lists
            Me.cmbOp1.SetList(dtFilterOps)

            'return to previous selections if possible
            With Me.cmbFilterFld1
                If xSelFilter1 <> "" Then
                    .Text = xSelFilter1
                End If
                If System.String.IsNullOrEmpty(.SelectedValue) Then
                    .SelectedIndex = 0
                End If
            End With

            With Me.cmbOp1
                If xSelOp1 <> "" Then
                    .Text = xSelOp1
                End If
                If System.String.IsNullOrEmpty(.SelectedValue) Then
                    .SelectedIndex = 0
                End If
            End With
        End Sub
        Private Function CurrentBackend() As ICIBackend
            'returns the backend of the selected node
            Dim oNode As TreeNode
            Dim xUNID As String

            oNode = Me.tvFolders.SelectedNode

            If Not oNode Is Nothing Then
                'get UNID
                xUNID = GetUNIDNoPrefix(oNode.Name)

                'get backend from UNID
                CurrentBackend = Main.GetBackendFromUNID(xUNID)
            Else
                CurrentBackend = Nothing
            End If
        End Function
        Public Function GetUNIDNoPrefix(ByVal xUNID As String) As String
            'returns the supplied UNID without
            'the 'b' prefix if it exists
            If Strings.Left(xUNID, 1) = "b" Then
                GetUNIDNoPrefix = Strings.Mid(xUNID, 2)
            Else
                GetUNIDNoPrefix = xUNID
            End If
        End Function

        Private Sub txtFilterValue2_KeyPress(KeyAscii As Integer)
            Try
                If KeyAscii = 13 Then
                    LoadContacts(ciSearchTypes.ciSearchType_Quick)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Friend Function SetSelectionLists(ByVal iSelectionLists As ICI.ciSelectionlists)
            'sets right-side selection lists visible/invisible
            'based on iSelectionLists
            Const OneListPos1 As Integer = 50

            Const TwoListsPos1 As Integer = 50
            Const TwoListsPos2 As Integer = 200

            Const ThreeListsPos1 As Integer = 50
            Const ThreeListsPos2 As Integer = 155
            Const ThreeListsPos3 As Integer = 258

            Const FourListsPos1 As Integer = 50
            Const FourListsPos2 As Integer = 125
            Const FourListsPos3 As Integer = 200
            Const FourListsPos4 As Integer = 273

            Const OneListHeight As Integer = 285
            Const TwoListsHeight As Integer = 133
            Const ThreeListsHeight As Integer = 80
            Const FourListsHeight As Integer = 65

            Const ButtonAddSeparation As Integer = 12
            Const ButtonSeparation As Integer = 25
            Const LabelSeparation As Integer = 15

            Dim Lists() As Integer
            Dim iNumLists As Integer
            Dim i As Integer

            ReDim Lists(0)

            'reset controls - make invisible and clean out tags
            For i = 0 To 3
                With Me
                    m_lblContacts(i).Visible = False
                    m_btnAdd(i).Visible = False
                    m_btnDelete(i).Visible = False
                    m_lstContacts(i).Visible = False
                End With
            Next i

            With Me
                m_btnAdd(4).Visible = False
                m_btnDelete(4).Visible = False
                .grdEntityList.Visible = False
                .lblEntities.Visible = False
            End With

            If iSelectionLists = ICI.ciSelectionlists.ciSelectionList_Custom Then
                With Me
                    .grdEntityList.Visible = True
                    .grdEntityList.DataSource = Me.EntityList
                    .lblEntities.Text = "Available En&tities: (" & Me.EntityList.Rows.Count & ")"
                    .grdEntityList.Refresh()
                    m_btnAdd(4).Visible = True
                    m_btnDelete(4).Visible = True
                    .lblEntities.Top = 30 * Functions.GetScalingFactor()
                    .lblEntities.Visible = True

                    'Hide irrelevant menu items
                    .mnuContacts_MoveSelectedContactsDown.Visible = False
                    .mnuContacts_MoveSelectedContactsUp.Visible = False
                    .tsContactsSeparator1.Visible = False
                    .btnMoveDown.Visible = False
                    .btnMoveUp.Visible = False

                End With
            Else
                'get specified lists - if list is to be shown,
                'add list to array of lists
                If iSelectionLists >= ICI.ciSelectionlists.ciSelectionList_BCC Then
                    Lists(0) = 3
                    iSelectionLists = iSelectionLists - ICI.ciSelectionlists.ciSelectionList_BCC
                End If

                If iSelectionLists >= ICI.ciSelectionlists.ciSelectionList_CC Then
                    If Lists(0) <> 0 Then
                        ReDim Preserve Lists(UBound(Lists) + 1)
                    End If
                    Lists(UBound(Lists)) = 2
                    iSelectionLists = iSelectionLists - ICI.ciSelectionlists.ciSelectionList_CC
                End If

                If iSelectionLists >= ICI.ciSelectionlists.ciSelectionList_From Then
                    If Lists(0) <> 0 Then
                        ReDim Preserve Lists(UBound(Lists) + 1)
                    End If
                    Lists(UBound(Lists)) = 1
                    iSelectionLists = iSelectionLists - ICI.ciSelectionlists.ciSelectionList_From
                End If

                If iSelectionLists >= ICI.ciSelectionlists.ciSelectionList_To Then
                    If Lists(0) <> 0 Then
                        ReDim Preserve Lists(UBound(Lists) + 1)
                    End If
                    Lists(UBound(Lists)) = 0
                End If

                'count number of lists
                iNumLists = UBound(Lists) + 1

                Select Case iNumLists
                    Case 1
                        i = Lists(0)

                        m_lblContacts(i).Top = (OneListPos1 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (OneListPos1 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = OneListPos1 * Functions.GetScalingFactor()
                            .Height = OneListHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                    Case 2
                        i = Lists(0)

                        m_lblContacts(i).Top = (TwoListsPos2 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (TwoListsPos2 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = TwoListsPos2 * Functions.GetScalingFactor()
                            .Height = TwoListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With


                        i = Lists(1)

                        m_lblContacts(i).Top = (TwoListsPos1 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (TwoListsPos1 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = TwoListsPos1 * Functions.GetScalingFactor()
                            .Height = TwoListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                    Case 3
                        i = Lists(0)
                        'position controls for 3rd list
                        m_lblContacts(i).Top = (ThreeListsPos3 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (ThreeListsPos3 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = ThreeListsPos3 * Functions.GetScalingFactor()
                            .Height = ThreeListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                        i = Lists(1)
                        m_lblContacts(i).Top = (ThreeListsPos2 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (ThreeListsPos2 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = ThreeListsPos2 * Functions.GetScalingFactor()
                            .Height = ThreeListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                        i = Lists(2)
                        m_lblContacts(i).Top = (ThreeListsPos1 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (ThreeListsPos1 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = ThreeListsPos1 * Functions.GetScalingFactor()
                            .Height = ThreeListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With
                    Case 4
                        i = Lists(0)
                        m_lblContacts(i).Top = (FourListsPos4 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (FourListsPos4 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = FourListsPos4 * Functions.GetScalingFactor()
                            .Height = FourListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                        i = Lists(1)
                        m_lblContacts(i).Top = (FourListsPos3 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (FourListsPos3 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = FourListsPos3 * Functions.GetScalingFactor()
                            .Height = FourListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                        i = Lists(2)
                        m_lblContacts(i).Top = (FourListsPos2 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (FourListsPos2 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = FourListsPos2 * Functions.GetScalingFactor()
                            .Height = FourListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With

                        i = Lists(3)
                        m_lblContacts(i).Top = (FourListsPos1 - LabelSeparation) * Functions.GetScalingFactor()
                        m_btnAdd(i).Top = (FourListsPos1 + ButtonAddSeparation) * Functions.GetScalingFactor()
                        m_btnDelete(i).Top = m_btnAdd(i).Top + ButtonSeparation * Functions.GetScalingFactor()

                        m_lblContacts(i).Visible = True
                        m_btnAdd(i).Visible = True
                        m_btnDelete(i).Visible = True

                        With m_lstContacts(i)
                            .Top = FourListsPos1 * Functions.GetScalingFactor()
                            .Height = FourListsHeight * Functions.GetScalingFactor()
                            .Visible = True
                        End With
                End Select
            End If

        End Function
        Public Sub SetUpColumns()
            'sets column width and position based on ini settings
            Dim xValue As String
            Dim i As Integer
            Dim sWidth As Single
            Dim iPos As Integer
            Dim iIndex As Integer
            Dim xWidth As String

            For i = 0 To 3
                'get ini value
                xValue = Main.GetUserIni("CIColumns", i)
                If xValue <> String.Empty Then

                    'parse into width and position
                    iPos = InStr(xValue, ",")
                    If iPos > 0 Then
                        iIndex = Strings.Left(xValue, iPos - 1)
                        xWidth = Mid$(xValue, iPos + 1)

                        'localize to regional setting
                        sWidth = Main.xLocalizeNumericString(xWidth)
                    End If

                    With Me.grdListings.Columns(i)
                        .DisplayIndex = iIndex
                        If sWidth < 1500 Then
                            .Width = sWidth
                        End If
                    End With
                Else
                    Me.grdListings.Columns(i).Width = 125
                End If
            Next i

            'set custom contact grid column width based on ini settings
            'get ini value
            If Me.grdEntityList.Columns.Count Then
                For i = 0 To 2 Step 2
                    xValue = Main.GetUserIni("CICustomColumns", i)
                    If xValue <> String.Empty Then
                        'localize to regional setting
                        sWidth = Main.xLocalizeNumericString(xValue)
                        If sWidth < 1500 Then
                            Me.grdEntityList.Columns(i).Width = sWidth
                        End If
                    End If
                Next i
            End If
        End Sub
        Public Sub SaveColumns()
            'save column width and position to ini
            Dim i As Integer

            'cycle through columns
            For i = 0 To 3
                With Me.grdListings.Columns(i)
                    'write settings in form 'Order,width'
                    Main.SetUserIni("CIColumns", i, .DisplayIndex & "," & .Width)
                End With
            Next i

            If Me.grdEntityList.Columns.Count Then
                For i = 0 To 2 Step 2
                    'save custom contact grid column width
                    With Me.grdEntityList.Columns(i)
                        Main.SetUserIni("CICustomColumns", i, .Width)
                    End With
                Next i
            End If
        End Sub
        Public Function SelectNode(Optional ByVal xPath As String = "") As TreeNode
            'selects folder specified in xPath
            'if not supplied, ini default is selected-
            'if no ini default specified, first node is selected-
            'expands tree as necessary

            Dim iSepPos As Integer
            Dim i As Integer
            Dim xCurLevel As String
            Dim xRemainingLevels As String
            Dim oCurNode As TreeNode = Nothing
            Dim oChildNode As TreeNode = Nothing

            Try
                If xPath = String.Empty Then
                    'not supplied - get from ini
                    xPath = Main.GetUserIni("CIApplication", ciUserIniKey_DefaultNode)
                    If xPath = String.Empty Then
                        'no default path - select first node
                        With Me.tvFolders
                            If .Nodes.Count > 0 Then
                                .SelectedNode = .Nodes.Item(0)
                                SetupDialogForNode(.SelectedNode)
                            End If
                            SelectNode = .SelectedNode
                        End With
                        Exit Function
                    End If
                End If

                With Me.tvFolders
                    'select level while sep exists-
                    'expand tree as necessary
                    iSepPos = InStr(xPath, "\")
                    If iSepPos Then
                        'parse xPath
                        xCurLevel = xPath.Substring(0, iSepPos - 1)
                        xRemainingLevels = xPath.Substring(iSepPos)
                        For i = 0 To .Nodes.Count - 1
                            If .Nodes(i).Text = xCurLevel Then
                                oCurNode = .Nodes(i)
                                oCurNode.Checked = True
                                Exit For
                            End If
                        Next i

                        If oCurNode Is Nothing Then
                            If Me.tvFolders.Nodes.Count > 0 Then
                                Me.tvFolders.SelectedNode = Me.tvFolders.Nodes.Item(0)
                                SelectNode = Me.tvFolders.SelectedNode
                            End If

                            Throw New Exception("Default folder does not exist.")
                        End If

                        'expand tree if necessary
                        If Not (oCurNode.Nodes.Count > 0) Then
                            ShowTreeBranch(oCurNode)
                        End If

                        If oCurNode.Nodes.Count > 0 Then
                            oCurNode.Expand()
                        End If

                        If oCurNode.IsExpanded Then
                            'get next separator
                            iSepPos = InStr(xRemainingLevels, "\")

                            While iSepPos
                                'index first child
                                oChildNode = oCurNode.FirstNode

                                If oChildNode Is Nothing Then
                                    Throw New Exception("Default folder does not exist.")
                                End If

                                'parse xPath
                                xCurLevel = xRemainingLevels.Substring(0, iSepPos - 1)
                                xRemainingLevels = Mid(xRemainingLevels, iSepPos + 1)

                                'loop through child folders until xCurLevel is found
                                While oChildNode.Text <> xCurLevel
                                    oChildNode = oChildNode.NextNode
                                    If oChildNode Is Nothing Then
                                        Throw New Exception("Default folder does not exist.")
                                    End If
                                End While

                                oCurNode = oChildNode
                                oCurNode.Checked = True

                                'expand tree
                                ShowTreeBranch(oCurNode)

                                'get next separator
                                iSepPos = InStr(xRemainingLevels, "\")
                            End While

                            'select final level folder node-
                            'index first child
                            oChildNode = oCurNode.FirstNode
                            oCurNode.Checked = True

                            If oChildNode Is Nothing Then _
                                Throw New Exception("Default folder does not exist.")

                            'parse xPath- search for trailing NULL chr
                            iSepPos = InStr(xRemainingLevels, Chr(0))
                            If iSepPos Then
                                xCurLevel = xRemainingLevels.Substring(0, iSepPos - 1)
                            Else
                                xCurLevel = xRemainingLevels
                            End If

                            'loop through child folders until xCurLevel is found
                            While oChildNode.Text <> xCurLevel
                                oChildNode = oChildNode.NextNode
                                If oChildNode Is Nothing Then
                                    Throw New Exception("Default folder does not exist.")
                                End If
                            End While

                            oCurNode = oChildNode
                        End If
                    Else
                        'select first level node
                        For i = 0 To .Nodes.Count - 1
                            If UCase(.Nodes(i).Text) = UCase(xPath) Then
                                oCurNode = .Nodes(i)
                                Exit For
                            End If
                        Next i
                        If oCurNode Is Nothing Then
                            Throw New Exception("Default folder does not exist.")
                        End If
                    End If

                    'select node
                    If Not (oCurNode Is Nothing) Then
                        With oCurNode
                            .Checked = True
                            Me.tvFolders.SelectedNode = oCurNode
                            SetupDialogForNode(oCurNode)
                        End With
                    End If

                    'return selected node
                    SelectNode = oCurNode
                End With
            Catch ex As Exception
                If Err.Number = ciErrs.ciErr_DefaultFolderDoesNotExist Then
                    With Me.tvFolders
                        If Not .SelectedNode Is Nothing Then
                            RefreshQuickSearchControls()
                        End If
                    End With

                    'setup custom menu items
                    SetupCustomMenuItems()

                    MsgBox("The default folder '" & xPath & _
                        "' does not exist.  Please reset your default folder.", _
                        vbExclamation, Main.g_oSessionType.AppTitle)
                    Exit Function
                Else
                    [Error].Show(ex)
                End If
            End Try
        End Function
        Private Sub ShowPanel(iPanel As CIPanels)
            'displays the specified panel
            Dim iUNIDType As ciUNIDTypes

            Functions.EchoOff()
            With Me
                If iPanel = CIPanels.ciPanel_folders Then
                    .pnlFolders.Visible = True
                    .pnlContacts.Visible = False
                    .pnlGroups.Visible = False
                    .pnlListings.Visible = False
                    .btnTogglePanels.Text = "&Contacts"
                    PanelChange(True)

                    'get type of node
                    iUNIDType = m_oUNID.GetUNIDType(.tvFolders.SelectedNode.Name)

                    .btnTogglePanels.Enabled = (iUNIDType = ciUNIDTypes.ciUNIDType_Folder)

                    'enable 'find' button for 1)folders and
                    '2)stores that support store searching
                    .btnSearch.Enabled = CurrentBackend.IsSearchableEntity( _
                        .tvFolders.SelectedNode.Name)

                    'enable 'find' button for 1)folders and
                    '2)stores that support store loading
                    .btnTogglePanels.Enabled = CurrentBackend.IsLoadableEntity( _
                        .tvFolders.SelectedNode.Name)

                    .btnEdit.Enabled = False
                ElseIf iPanel = CIPanels.ciPanel_Contacts Then
                    m_bShowedContacts = True

                    'set up for contacts
                    .btnTogglePanels.Text = "Fol&ders"
                    .btnTogglePanels.Enabled = True
                    .btnSearch.Enabled = True
                    .cmdOK.Text = "&Insert"
                    .mnuContacts_AddGroup.Enabled = False
                    .mnuContacts_DeleteGroup.Enabled = False
                    'show contacts frame if listings box is not Wide
                    .pnlContacts.Visible = (.grdListings.Width = ciListingsNarrow * Functions.GetScalingFactor())
                    .pnlGroups.Visible = False
                    .pnlContacts.Visible = True
                    .pnlListings.Visible = True
                    Application.DoEvents()
                    .pnlFolders.Visible = False
                    PanelChange(False)
                Else
                    'set up for groups
                    .pnlFolders.Visible = False
                    .pnlListings.Visible = True
                    .pnlContacts.Visible = False
                    .pnlGroups.Visible = True
                    .btnTogglePanels.Text = "&Folders"
                    PanelChange(False)
                    .btnTogglePanels.Enabled = True
                    .btnSearch.Enabled = True
                    .cmdOK.Text = "Load Gro&up"
                    .AcceptButton = .btnAddMember
                    .btnDeleteMember.Enabled = False
                    .mnuContacts_AddGroup.Enabled = True
                    .mnuContacts_DeleteGroup.Enabled = True

                    'show groups frame if listings box is not Wide
                    .pnlGroups.Visible = (.grdListings.Width = ciListingsNarrow * Functions.GetScalingFactor())
                End If
                .btnGroups.Enabled = iPanel <> CIPanels.ciPanel_folders
            End With
        End Sub
        Private Sub ShowAbout()
            Dim oForm As AboutForm

            oForm = New AboutForm
            oForm.ShowDialog()

            oForm = Nothing
        End Sub
        Sub ToggleListingsWidth()
            'toggles the width of the left-side list box
            Dim bEnable As Boolean
            Dim i As Integer
            Dim sRightBorder As Single
            Dim sWide As Single

            With Me
                sRightBorder = .pnlContacts.Left + m_lstContacts(0).Left + m_lstContacts(0).Width
                sWide = sRightBorder - .grdListings.Left


                If .grdListings.Width = sWide Then
                    FillLastVisibleDataGridViewColumn(Me.grdListings)
                    .grdListings.Width = ciListingsNarrow * Functions.GetScalingFactor()
                    .mnuContacts_Expand.Text = "Expand Available Contact &List"
                Else
                    .grdListings.Width = sWide
                    .grdListings.Columns.GetLastColumn(DataGridViewElementStates.Visible, DataGridViewElementStates.None).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .mnuContacts_Expand.Text = "Collapse Available Contact &List"
                End If

                bEnable = (.grdListings.Width = ciListingsNarrow * Functions.GetScalingFactor())

                If Me.btnGroups.Checked = False Then
                    'show/hide contacts frame
                    .pnlContacts.Visible = bEnable
                    .pnlGroups.Visible = False
                Else
                    'show/hide groups frame
                    .pnlGroups.Visible = bEnable
                    .pnlContacts.Visible = False
                End If

                Me.btnGroups.Enabled = bEnable
            End With
            Application.DoEvents()
        End Sub
        Sub ClearContacts()
            'clears the contacts chosen - clears lists
            Dim i As Integer

            If Not m_oContacts Is Nothing Then
                m_oContacts.DeleteAll()
            End If

            For i = 0 To 3
                m_lstContacts(i).Items.Clear()
            Next

            With Me.grdListings
                Try
                    .ClearSelection()
                    .Rows(0).Selected = True
                Catch
                End Try
            End With
        End Sub
        Public Function GroupsBackend() As ICIBackend
            'returns the groups backend
            Try
                GroupsBackend = Main.Backends.GroupsBackend
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        Public Sub OnSelListChange(iListIndex As Integer)
            Dim i As Integer
            Dim j As Integer

            Try
                'deselect all entries, disable
                'delete buttons, and format controls
                'in inactive lists
                For i = 0 To m_lstContacts.Length - 1
                    If i <> iListIndex Then
                        For j = 0 To m_lstContacts(i).Items.Count - 1
                            m_lstContacts(i).SetSelected(j, False)
                        Next j
                        m_lblContacts(i).ForeColor = Color.Black
                        m_btnDelete(i).Enabled = False
                    End If
                Next i

                'set properties of active list controls
                Me.AcceptButton = m_btnAdd(iListIndex)

                'if multiple lists are visible, make active one blue
                Select Case Me.SelectionLists
                    Case ICI.ciSelectionlists.ciSelectionList_To, _
                         ICI.ciSelectionlists.ciSelectionList_From, _
                         ICI.ciSelectionlists.ciSelectionList_CC, _
                         ICI.ciSelectionlists.ciSelectionList_BCC
                        m_lblContacts(iListIndex).ForeColor = Color.Black
                    Case Else
                        m_lblContacts(iListIndex).ForeColor = Color.Blue
                End Select

                'select first item in list if no selections exist
                With m_lstContacts(iListIndex)
                    If .SelectedItems.Count = 0 And .Items.Count Then
                        .SetSelected(0, True)
                    End If
                End With

                'enable delete button if an item is selected
                m_btnDelete(iListIndex).Enabled = _
                        m_lstContacts(iListIndex).Items.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub SetupForInputState(ByVal bIsReadyForInput As Boolean)
            Try
                With Me
                    .pnlContacts.Enabled = bIsReadyForInput
                    .pnlFolders.Enabled = bIsReadyForInput
                    .pnlGroups.Enabled = bIsReadyForInput
                    .pnlListings.Enabled = bIsReadyForInput
                    .tsToolbar.Enabled = bIsReadyForInput
                    If bIsReadyForInput Then
                        .CancelButton = .cmdCancel
                    End If
                    .cmdCancel.Enabled = bIsReadyForInput
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub FillLastVisibleDataGridViewColumn(dgv As DataGridView)
            Dim iLastColIndex As Integer = 0
            Dim oCol As DataGridViewColumn
            Dim iSize As Integer

            With dgv
                If .Columns.GetColumnCount(DataGridViewElementStates.Visible) < 3 Then
                    .Columns.GetLastColumn(DataGridViewElementStates.Visible, DataGridViewElementStates.None).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                Else
                    For Each oCol In .Columns
                        If oCol.Visible And oCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill Then
                            iSize = oCol.Width
                            oCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                            oCol.Width = iSize
                            Exit For
                        End If
                    Next oCol
                End If
            End With

        End Sub
#End Region

    End Class
End Namespace
