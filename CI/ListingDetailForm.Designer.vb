﻿Namespace TSG.CI
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ListingDetailForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ListingDetailForm))
            Me.lblAddresses = New System.Windows.Forms.Label()
            Me.optAddress0 = New System.Windows.Forms.RadioButton()
            Me.optAddress1 = New System.Windows.Forms.RadioButton()
            Me.optAddress2 = New System.Windows.Forms.RadioButton()
            Me.optAddress3 = New System.Windows.Forms.RadioButton()
            Me.optAddress4 = New System.Windows.Forms.RadioButton()
            Me.optAddress5 = New System.Windows.Forms.RadioButton()
            Me.optAddress6 = New System.Windows.Forms.RadioButton()
            Me.btnClose = New System.Windows.Forms.Button()
            Me.lblDetail = New System.Windows.Forms.Label()
            Me.Panel1 = New System.Windows.Forms.Panel()
            Me.pctCopy = New System.Windows.Forms.PictureBox()
            Me.lblHeading = New System.Windows.Forms.Label()
            Me.Panel1.SuspendLayout()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lblAddresses
            '
            Me.lblAddresses.AutoSize = True
            Me.lblAddresses.Location = New System.Drawing.Point(7, 15)
            Me.lblAddresses.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblAddresses.Name = "lblAddresses"
            Me.lblAddresses.Size = New System.Drawing.Size(56, 13)
            Me.lblAddresses.TabIndex = 0
            Me.lblAddresses.Text = "Addresses"
            '
            'optAddress0
            '
            Me.optAddress0.AutoSize = True
            Me.optAddress0.Location = New System.Drawing.Point(9, 37)
            Me.optAddress0.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress0.Name = "optAddress0"
            Me.optAddress0.Size = New System.Drawing.Size(84, 17)
            Me.optAddress0.TabIndex = 1
            Me.optAddress0.TabStop = True
            Me.optAddress0.Text = "optAddress0"
            Me.optAddress0.UseVisualStyleBackColor = True
            Me.optAddress0.Visible = False
            '
            'optAddress1
            '
            Me.optAddress1.AutoSize = True
            Me.optAddress1.Location = New System.Drawing.Point(9, 60)
            Me.optAddress1.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress1.Name = "optAddress1"
            Me.optAddress1.Size = New System.Drawing.Size(84, 17)
            Me.optAddress1.TabIndex = 2
            Me.optAddress1.TabStop = True
            Me.optAddress1.Text = "optAddress1"
            Me.optAddress1.UseVisualStyleBackColor = True
            Me.optAddress1.Visible = False
            '
            'optAddress2
            '
            Me.optAddress2.AutoSize = True
            Me.optAddress2.Location = New System.Drawing.Point(9, 84)
            Me.optAddress2.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress2.Name = "optAddress2"
            Me.optAddress2.Size = New System.Drawing.Size(84, 17)
            Me.optAddress2.TabIndex = 3
            Me.optAddress2.TabStop = True
            Me.optAddress2.Text = "optAddress2"
            Me.optAddress2.UseVisualStyleBackColor = True
            Me.optAddress2.Visible = False
            '
            'optAddress3
            '
            Me.optAddress3.AutoSize = True
            Me.optAddress3.Location = New System.Drawing.Point(9, 108)
            Me.optAddress3.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress3.Name = "optAddress3"
            Me.optAddress3.Size = New System.Drawing.Size(84, 17)
            Me.optAddress3.TabIndex = 4
            Me.optAddress3.TabStop = True
            Me.optAddress3.Text = "optAddress3"
            Me.optAddress3.UseVisualStyleBackColor = True
            Me.optAddress3.Visible = False
            '
            'optAddress4
            '
            Me.optAddress4.AutoSize = True
            Me.optAddress4.Location = New System.Drawing.Point(9, 131)
            Me.optAddress4.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress4.Name = "optAddress4"
            Me.optAddress4.Size = New System.Drawing.Size(84, 17)
            Me.optAddress4.TabIndex = 5
            Me.optAddress4.TabStop = True
            Me.optAddress4.Text = "optAddress4"
            Me.optAddress4.UseVisualStyleBackColor = True
            Me.optAddress4.Visible = False
            '
            'optAddress5
            '
            Me.optAddress5.AutoSize = True
            Me.optAddress5.Location = New System.Drawing.Point(9, 155)
            Me.optAddress5.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress5.Name = "optAddress5"
            Me.optAddress5.Size = New System.Drawing.Size(84, 17)
            Me.optAddress5.TabIndex = 6
            Me.optAddress5.TabStop = True
            Me.optAddress5.Text = "optAddress5"
            Me.optAddress5.UseVisualStyleBackColor = True
            Me.optAddress5.Visible = False
            '
            'optAddress6
            '
            Me.optAddress6.AutoSize = True
            Me.optAddress6.Location = New System.Drawing.Point(9, 178)
            Me.optAddress6.Margin = New System.Windows.Forms.Padding(2)
            Me.optAddress6.Name = "optAddress6"
            Me.optAddress6.Size = New System.Drawing.Size(84, 17)
            Me.optAddress6.TabIndex = 7
            Me.optAddress6.TabStop = True
            Me.optAddress6.Text = "optAddress6"
            Me.optAddress6.UseVisualStyleBackColor = True
            Me.optAddress6.Visible = False
            '
            'btnClose
            '
            Me.btnClose.Location = New System.Drawing.Point(344, 256)
            Me.btnClose.Margin = New System.Windows.Forms.Padding(2)
            Me.btnClose.Name = "btnClose"
            Me.btnClose.Size = New System.Drawing.Size(84, 27)
            Me.btnClose.TabIndex = 10
            Me.btnClose.Text = "Close"
            Me.btnClose.UseVisualStyleBackColor = True
            '
            'lblDetail
            '
            Me.lblDetail.AutoSize = True
            Me.lblDetail.BackColor = System.Drawing.Color.White
            Me.lblDetail.Location = New System.Drawing.Point(26, 32)
            Me.lblDetail.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblDetail.Name = "lblDetail"
            Me.lblDetail.Size = New System.Drawing.Size(44, 13)
            Me.lblDetail.TabIndex = 0
            Me.lblDetail.Text = "lblDetail"
            '
            'Panel1
            '
            Me.Panel1.BackColor = System.Drawing.Color.White
            Me.Panel1.Controls.Add(Me.pctCopy)
            Me.Panel1.Controls.Add(Me.lblDetail)
            Me.Panel1.Location = New System.Drawing.Point(140, 0)
            Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
            Me.Panel1.Name = "Panel1"
            Me.Panel1.Size = New System.Drawing.Size(300, 294)
            Me.Panel1.TabIndex = 9
            '
            'pctCopy
            '
            Me.pctCopy.Image = CType(resources.GetObject("pctCopy.Image"), System.Drawing.Image)
            Me.pctCopy.Location = New System.Drawing.Point(307, 8)
            Me.pctCopy.Name = "pctCopy"
            Me.pctCopy.Size = New System.Drawing.Size(19, 20)
            Me.pctCopy.TabIndex = 46
            Me.pctCopy.TabStop = False
            '
            'lblHeading
            '
            Me.lblHeading.Location = New System.Drawing.Point(8, 250)
            Me.lblHeading.Name = "lblHeading"
            Me.lblHeading.Size = New System.Drawing.Size(85, 23)
            Me.lblHeading.TabIndex = 8
            Me.lblHeading.Text = "####"
            Me.lblHeading.Visible = False
            '
            'ListingDetailForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.ClientSize = New System.Drawing.Size(440, 293)
            Me.Controls.Add(Me.lblHeading)
            Me.Controls.Add(Me.btnClose)
            Me.Controls.Add(Me.optAddress6)
            Me.Controls.Add(Me.optAddress5)
            Me.Controls.Add(Me.optAddress4)
            Me.Controls.Add(Me.optAddress3)
            Me.Controls.Add(Me.optAddress2)
            Me.Controls.Add(Me.optAddress1)
            Me.Controls.Add(Me.optAddress0)
            Me.Controls.Add(Me.lblAddresses)
            Me.Controls.Add(Me.Panel1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Margin = New System.Windows.Forms.Padding(2)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ListingDetailForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Contact Detail"
            Me.Panel1.ResumeLayout(False)
            Me.Panel1.PerformLayout()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblAddresses As System.Windows.Forms.Label
        Friend WithEvents optAddress0 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress1 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress2 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress3 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress4 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress5 As System.Windows.Forms.RadioButton
        Friend WithEvents optAddress6 As System.Windows.Forms.RadioButton
        Friend WithEvents btnClose As System.Windows.Forms.Button
        Friend WithEvents lblDetail As System.Windows.Forms.Label
        Friend WithEvents Panel1 As System.Windows.Forms.Panel
        Friend WithEvents lblHeading As System.Windows.Forms.Label
        Private WithEvents pctCopy As System.Windows.Forms.PictureBox
    End Class
End Namespace
