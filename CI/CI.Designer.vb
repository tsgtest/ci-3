﻿Namespace TSG.CI
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CI
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
            Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CI))
            Me.tsToolbar = New System.Windows.Forms.ToolStrip()
            Me.btnTogglePanels = New System.Windows.Forms.ToolStripButton()
            Me.btnSearch = New System.Windows.Forms.ToolStripButton()
            Me.btnSearchNative = New System.Windows.Forms.ToolStripButton()
            Me.btnAddContactInNative = New System.Windows.Forms.ToolStripButton()
            Me.btnEdit = New System.Windows.Forms.ToolStripButton()
            Me.btnRefresh = New System.Windows.Forms.ToolStripButton()
            Me.btnGroups = New System.Windows.Forms.ToolStripButton()
            Me.btnOptionsFolders = New System.Windows.Forms.ToolStripDropDownButton()
            Me.mnuFolders_SelectCurrentFolderOnStartup = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_LoadCurrentFolderOnStartup = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_RemoveDefaultFolder = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Separator1 = New System.Windows.Forms.ToolStripSeparator()
            Me.mnuFolders_ToggleQuickFilterType = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Separator2 = New System.Windows.Forms.ToolStripSeparator()
            Me.mnuFolders_Custom1 = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Custom2 = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Custom3 = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Custom4 = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuFolders_Custom5 = New System.Windows.Forms.ToolStripMenuItem()
            Me.btnOptionsContacts = New System.Windows.Forms.ToolStripDropDownButton()
            Me.mnuContacts_Expand = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuContacts_SelectAll = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuContacts_ShowDetail = New System.Windows.Forms.ToolStripMenuItem()
            Me.tsContactsSeparator1 = New System.Windows.Forms.ToolStripSeparator()
            Me.mnuContacts_MoveSelectedContactsUp = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuContacts_MoveSelectedContactsDown = New System.Windows.Forms.ToolStripMenuItem()
            Me.tsContactsSeparator2 = New System.Windows.Forms.ToolStripSeparator()
            Me.mnuContacts_AddGroup = New System.Windows.Forms.ToolStripMenuItem()
            Me.mnuContacts_DeleteGroup = New System.Windows.Forms.ToolStripMenuItem()
            Me.btnHelpNew = New System.Windows.Forms.ToolStripDropDownButton()
            Me.lblAddressType = New System.Windows.Forms.Label()
            Me.pnlListings = New System.Windows.Forms.Panel()
            Me.cbxAddressType = New CIControls.ComboBox()
            Me.cmdCancel = New System.Windows.Forms.Button()
            Me.cmdOK = New System.Windows.Forms.Button()
            Me.lblStatus = New System.Windows.Forms.Label()
            Me.grdListings = New System.Windows.Forms.DataGridView()
            Me.lblEntries = New System.Windows.Forms.Label()
            Me.pnlContacts = New System.Windows.Forms.Panel()
            Me.grdEntityList = New System.Windows.Forms.DataGridView()
            Me.btnDeleteEntity = New System.Windows.Forms.Button()
            Me.btnAddEntity = New System.Windows.Forms.Button()
            Me.lstFrom = New System.Windows.Forms.ListBox()
            Me.lstCC = New System.Windows.Forms.ListBox()
            Me.lstBCC = New System.Windows.Forms.ListBox()
            Me.lstTo = New System.Windows.Forms.ListBox()
            Me.btnMoveDown = New System.Windows.Forms.Button()
            Me.btnMoveUp = New System.Windows.Forms.Button()
            Me.lblTo = New System.Windows.Forms.Label()
            Me.lblBCC = New System.Windows.Forms.Label()
            Me.lblCC = New System.Windows.Forms.Label()
            Me.lblFrom = New System.Windows.Forms.Label()
            Me.btnDeleteBCC = New System.Windows.Forms.Button()
            Me.btnAddBCC = New System.Windows.Forms.Button()
            Me.btnDeleteCC = New System.Windows.Forms.Button()
            Me.btnAddCC = New System.Windows.Forms.Button()
            Me.btnDeleteFrom = New System.Windows.Forms.Button()
            Me.btnAddFrom = New System.Windows.Forms.Button()
            Me.lblEntities = New System.Windows.Forms.Label()
            Me.btnDeleteTo = New System.Windows.Forms.Button()
            Me.btnAddTo = New System.Windows.Forms.Button()
            Me.pnlGroups = New System.Windows.Forms.Panel()
            Me.btnAddMember = New System.Windows.Forms.Button()
            Me.btnDeleteMember = New System.Windows.Forms.Button()
            Me.btnDeleteGroup = New System.Windows.Forms.Button()
            Me.cbxGroups = New CIControls.ComboBox()
            Me.btnAddGroup = New System.Windows.Forms.Button()
            Me.lblGroup = New System.Windows.Forms.Label()
            Me.lstGroupMembers = New System.Windows.Forms.ListBox()
            Me.lblGroupMembers = New System.Windows.Forms.Label()
            Me.pnlFolders = New System.Windows.Forms.Panel()
            Me.pnlQuickFilter = New System.Windows.Forms.Panel()
            Me.tsQuickFilter = New System.Windows.Forms.ToolStrip()
            Me.tsBFilter0 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter1 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter2 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter3 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter4 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter5 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter6 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter7 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter8 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter9 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter10 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter11 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter12 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter13 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter14 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter15 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter16 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter17 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter18 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter19 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter20 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter21 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter22 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter23 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter24 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter25 = New System.Windows.Forms.ToolStripButton()
            Me.tsBFilter26 = New System.Windows.Forms.ToolStripButton()
            Me.lblQuickFilterAlpha = New System.Windows.Forms.Label()
            Me.optQuickFilter3 = New System.Windows.Forms.RadioButton()
            Me.optQuickFilter2 = New System.Windows.Forms.RadioButton()
            Me.optQuickFilter1 = New System.Windows.Forms.RadioButton()
            Me.optQuickFilter0 = New System.Windows.Forms.RadioButton()
            Me.lblStatusMsg = New System.Windows.Forms.Label()
            Me.txtFilterValue1 = New System.Windows.Forms.TextBox()
            Me.btnQuickSearch = New System.Windows.Forms.Button()
            Me.cmbOp1 = New CIControls.ComboBox()
            Me.cmbFilterFld1 = New CIControls.ComboBox()
            Me.lblContactLists = New System.Windows.Forms.Label()
            Me.tvFolders = New System.Windows.Forms.TreeView()
            Me.tvFoldersContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
            Me.tvFoldersContextMenuSelectOnStartup = New System.Windows.Forms.ToolStripMenuItem()
            Me.tvFoldersContextMenuLoadOnStartup = New System.Windows.Forms.ToolStripMenuItem()
            Me.tvFoldersContextMenuRemoveDefault = New System.Windows.Forms.ToolStripMenuItem()
            Me.ilTreeIcons = New System.Windows.Forms.ImageList(Me.components)
            Me.lblQuickFilter = New System.Windows.Forms.Label()
            Me.tsToolbar.SuspendLayout()
            Me.pnlListings.SuspendLayout()
            CType(Me.grdListings, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.pnlContacts.SuspendLayout()
            CType(Me.grdEntityList, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.pnlGroups.SuspendLayout()
            Me.pnlFolders.SuspendLayout()
            Me.pnlQuickFilter.SuspendLayout()
            Me.tsQuickFilter.SuspendLayout()
            Me.tvFoldersContextMenu.SuspendLayout()
            Me.SuspendLayout()
            '
            'tsToolbar
            '
            Me.tsToolbar.BackColor = System.Drawing.Color.WhiteSmoke
            Me.tsToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
            Me.tsToolbar.CanOverflow = False
            Me.tsToolbar.Font = New System.Drawing.Font("Segoe UI", 9.0!)
            Me.tsToolbar.GripMargin = New System.Windows.Forms.Padding(0)
            Me.tsToolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
            Me.tsToolbar.ImageScalingSize = New System.Drawing.Size(28, 28)
            Me.tsToolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnTogglePanels, Me.btnSearch, Me.btnSearchNative, Me.btnAddContactInNative, Me.btnEdit, Me.btnRefresh, Me.btnGroups, Me.btnOptionsFolders, Me.btnOptionsContacts, Me.btnHelpNew})
            Me.tsToolbar.Location = New System.Drawing.Point(0, 0)
            Me.tsToolbar.Name = "tsToolbar"
            Me.tsToolbar.Padding = New System.Windows.Forms.Padding(0, 1, 0, 1)
            Me.tsToolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
            Me.tsToolbar.Size = New System.Drawing.Size(558, 52)
            Me.tsToolbar.TabIndex = 0
            Me.tsToolbar.Text = "ToolStrip1"
            '
            'btnTogglePanels
            '
            Me.btnTogglePanels.Image = Global.My.Resources.Resources.Contact
            Me.btnTogglePanels.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnTogglePanels.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnTogglePanels.Name = "btnTogglePanels"
            Me.btnTogglePanels.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnTogglePanels.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnTogglePanels.Size = New System.Drawing.Size(64, 47)
            Me.btnTogglePanels.Text = "&Contacts"
            Me.btnTogglePanels.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnSearch
            '
            Me.btnSearch.Image = Global.My.Resources.Resources.Find
            Me.btnSearch.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnSearch.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnSearch.Name = "btnSearch"
            Me.btnSearch.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnSearch.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnSearch.Size = New System.Drawing.Size(49, 47)
            Me.btnSearch.Text = "&Find..."
            Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnSearchNative
            '
            Me.btnSearchNative.Image = Global.My.Resources.Resources.Find
            Me.btnSearchNative.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnSearchNative.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnSearchNative.Name = "btnSearchNative"
            Me.btnSearchNative.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnSearchNative.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
            Me.btnSearchNative.Size = New System.Drawing.Size(74, 47)
            Me.btnSearchNative.Text = "Nati&ve Find"
            Me.btnSearchNative.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnAddContactInNative
            '
            Me.btnAddContactInNative.Image = Global.My.Resources.Resources.Contact_Add
            Me.btnAddContactInNative.ImageTransparentColor = System.Drawing.Color.White
            Me.btnAddContactInNative.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnAddContactInNative.Name = "btnAddContactInNative"
            Me.btnAddContactInNative.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnAddContactInNative.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
            Me.btnAddContactInNative.Size = New System.Drawing.Size(55, 47)
            Me.btnAddContactInNative.Text = "&New"
            Me.btnAddContactInNative.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnEdit
            '
            Me.btnEdit.Enabled = False
            Me.btnEdit.Image = Global.My.Resources.Resources.Contact_Edit
            Me.btnEdit.ImageTransparentColor = System.Drawing.Color.White
            Me.btnEdit.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnEdit.Name = "btnEdit"
            Me.btnEdit.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnEdit.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
            Me.btnEdit.Size = New System.Drawing.Size(52, 47)
            Me.btnEdit.Text = "&Edit"
            Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnRefresh
            '
            Me.btnRefresh.Image = Global.My.Resources.Resources.Refresh
            Me.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnRefresh.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnRefresh.Name = "btnRefresh"
            Me.btnRefresh.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnRefresh.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnRefresh.Size = New System.Drawing.Size(56, 47)
            Me.btnRefresh.Text = "&Refresh"
            Me.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnGroups
            '
            Me.btnGroups.CheckOnClick = True
            Me.btnGroups.Enabled = False
            Me.btnGroups.Image = Global.My.Resources.Resources.groups
            Me.btnGroups.ImageTransparentColor = System.Drawing.Color.White
            Me.btnGroups.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnGroups.Name = "btnGroups"
            Me.btnGroups.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnGroups.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnGroups.Size = New System.Drawing.Size(55, 47)
            Me.btnGroups.Text = "&Groups"
            Me.btnGroups.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'btnOptionsFolders
            '
            Me.btnOptionsFolders.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFolders_SelectCurrentFolderOnStartup, Me.mnuFolders_LoadCurrentFolderOnStartup, Me.mnuFolders_RemoveDefaultFolder, Me.mnuFolders_Separator1, Me.mnuFolders_ToggleQuickFilterType, Me.mnuFolders_Separator2, Me.mnuFolders_Custom1, Me.mnuFolders_Custom2, Me.mnuFolders_Custom3, Me.mnuFolders_Custom4, Me.mnuFolders_Custom5})
            Me.btnOptionsFolders.Image = Global.My.Resources.Resources.options
            Me.btnOptionsFolders.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnOptionsFolders.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnOptionsFolders.Name = "btnOptionsFolders"
            Me.btnOptionsFolders.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
            Me.btnOptionsFolders.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnOptionsFolders.ShowDropDownArrow = False
            Me.btnOptionsFolders.Size = New System.Drawing.Size(59, 47)
            Me.btnOptionsFolders.Text = "&Options"
            Me.btnOptionsFolders.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'mnuFolders_SelectCurrentFolderOnStartup
            '
            Me.mnuFolders_SelectCurrentFolderOnStartup.Name = "mnuFolders_SelectCurrentFolderOnStartup"
            Me.mnuFolders_SelectCurrentFolderOnStartup.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_SelectCurrentFolderOnStartup.Text = "&Select Current Folder On Startup"
            '
            'mnuFolders_LoadCurrentFolderOnStartup
            '
            Me.mnuFolders_LoadCurrentFolderOnStartup.Name = "mnuFolders_LoadCurrentFolderOnStartup"
            Me.mnuFolders_LoadCurrentFolderOnStartup.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_LoadCurrentFolderOnStartup.Text = "&Load Current Folder On Startup"
            '
            'mnuFolders_RemoveDefaultFolder
            '
            Me.mnuFolders_RemoveDefaultFolder.Name = "mnuFolders_RemoveDefaultFolder"
            Me.mnuFolders_RemoveDefaultFolder.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_RemoveDefaultFolder.Text = "&Remove Default Folder"
            '
            'mnuFolders_Separator1
            '
            Me.mnuFolders_Separator1.Name = "mnuFolders_Separator1"
            Me.mnuFolders_Separator1.Size = New System.Drawing.Size(241, 6)
            '
            'mnuFolders_ToggleQuickFilterType
            '
            Me.mnuFolders_ToggleQuickFilterType.Name = "mnuFolders_ToggleQuickFilterType"
            Me.mnuFolders_ToggleQuickFilterType.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_ToggleQuickFilterType.Text = "&Toggle Quick Filter Type"
            '
            'mnuFolders_Separator2
            '
            Me.mnuFolders_Separator2.Name = "mnuFolders_Separator2"
            Me.mnuFolders_Separator2.Size = New System.Drawing.Size(241, 6)
            '
            'mnuFolders_Custom1
            '
            Me.mnuFolders_Custom1.Name = "mnuFolders_Custom1"
            Me.mnuFolders_Custom1.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_Custom1.Text = "Custom Menu Item 1"
            '
            'mnuFolders_Custom2
            '
            Me.mnuFolders_Custom2.Name = "mnuFolders_Custom2"
            Me.mnuFolders_Custom2.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_Custom2.Text = "Custom Menu Item 2"
            '
            'mnuFolders_Custom3
            '
            Me.mnuFolders_Custom3.Name = "mnuFolders_Custom3"
            Me.mnuFolders_Custom3.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_Custom3.Text = "Custom Menu Item 3"
            '
            'mnuFolders_Custom4
            '
            Me.mnuFolders_Custom4.Name = "mnuFolders_Custom4"
            Me.mnuFolders_Custom4.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_Custom4.Text = "Custom Menu Item 4"
            '
            'mnuFolders_Custom5
            '
            Me.mnuFolders_Custom5.Name = "mnuFolders_Custom5"
            Me.mnuFolders_Custom5.Size = New System.Drawing.Size(244, 22)
            Me.mnuFolders_Custom5.Text = "Custom Menu Item 5"
            '
            'btnOptionsContacts
            '
            Me.btnOptionsContacts.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuContacts_Expand, Me.mnuContacts_SelectAll, Me.mnuContacts_ShowDetail, Me.tsContactsSeparator1, Me.mnuContacts_MoveSelectedContactsUp, Me.mnuContacts_MoveSelectedContactsDown, Me.tsContactsSeparator2, Me.mnuContacts_AddGroup, Me.mnuContacts_DeleteGroup})
            Me.btnOptionsContacts.Image = Global.My.Resources.Resources.options
            Me.btnOptionsContacts.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnOptionsContacts.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnOptionsContacts.Name = "btnOptionsContacts"
            Me.btnOptionsContacts.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
            Me.btnOptionsContacts.ShowDropDownArrow = False
            Me.btnOptionsContacts.Size = New System.Drawing.Size(59, 47)
            Me.btnOptionsContacts.Text = "&Options"
            Me.btnOptionsContacts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            Me.btnOptionsContacts.ToolTipText = "Options"
            '
            'mnuContacts_Expand
            '
            Me.mnuContacts_Expand.Name = "mnuContacts_Expand"
            Me.mnuContacts_Expand.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
            Me.mnuContacts_Expand.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_Expand.Text = "Expand Available Contacts &List"
            '
            'mnuContacts_SelectAll
            '
            Me.mnuContacts_SelectAll.Name = "mnuContacts_SelectAll"
            Me.mnuContacts_SelectAll.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
            Me.mnuContacts_SelectAll.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_SelectAll.Text = "Select &All"
            '
            'mnuContacts_ShowDetail
            '
            Me.mnuContacts_ShowDetail.Name = "mnuContacts_ShowDetail"
            Me.mnuContacts_ShowDetail.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_ShowDetail.Text = "Show &Detail"
            '
            'tsContactsSeparator1
            '
            Me.tsContactsSeparator1.Name = "tsContactsSeparator1"
            Me.tsContactsSeparator1.Size = New System.Drawing.Size(276, 6)
            '
            'mnuContacts_MoveSelectedContactsUp
            '
            Me.mnuContacts_MoveSelectedContactsUp.Name = "mnuContacts_MoveSelectedContactsUp"
            Me.mnuContacts_MoveSelectedContactsUp.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_MoveSelectedContactsUp.Text = "Move Selected Contacts &Up"
            '
            'mnuContacts_MoveSelectedContactsDown
            '
            Me.mnuContacts_MoveSelectedContactsDown.Name = "mnuContacts_MoveSelectedContactsDown"
            Me.mnuContacts_MoveSelectedContactsDown.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_MoveSelectedContactsDown.Text = "Move Selected Contacts Do&wn"
            '
            'tsContactsSeparator2
            '
            Me.tsContactsSeparator2.Name = "tsContactsSeparator2"
            Me.tsContactsSeparator2.Size = New System.Drawing.Size(276, 6)
            '
            'mnuContacts_AddGroup
            '
            Me.mnuContacts_AddGroup.Name = "mnuContacts_AddGroup"
            Me.mnuContacts_AddGroup.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
            Me.mnuContacts_AddGroup.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_AddGroup.Text = "Add &Group"
            '
            'mnuContacts_DeleteGroup
            '
            Me.mnuContacts_DeleteGroup.Name = "mnuContacts_DeleteGroup"
            Me.mnuContacts_DeleteGroup.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
            Me.mnuContacts_DeleteGroup.Size = New System.Drawing.Size(279, 22)
            Me.mnuContacts_DeleteGroup.Text = "Delete Gro&up"
            '
            'btnHelpNew
            '
            Me.btnHelpNew.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
            Me.btnHelpNew.Image = Global.My.Resources.Resources.help
            Me.btnHelpNew.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.btnHelpNew.Margin = New System.Windows.Forms.Padding(0, 1, 4, 2)
            Me.btnHelpNew.Name = "btnHelpNew"
            Me.btnHelpNew.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
            Me.btnHelpNew.ShowDropDownArrow = False
            Me.btnHelpNew.Size = New System.Drawing.Size(56, 47)
            Me.btnHelpNew.Text = "&Help"
            Me.btnHelpNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            '
            'lblAddressType
            '
            Me.lblAddressType.AutoSize = True
            Me.lblAddressType.Location = New System.Drawing.Point(7, 5)
            Me.lblAddressType.Name = "lblAddressType"
            Me.lblAddressType.Size = New System.Drawing.Size(75, 13)
            Me.lblAddressType.TabIndex = 0
            Me.lblAddressType.Text = "Address T&ype:"
            '
            'pnlListings
            '
            Me.pnlListings.Controls.Add(Me.cbxAddressType)
            Me.pnlListings.Controls.Add(Me.cmdCancel)
            Me.pnlListings.Controls.Add(Me.cmdOK)
            Me.pnlListings.Controls.Add(Me.lblStatus)
            Me.pnlListings.Controls.Add(Me.grdListings)
            Me.pnlListings.Controls.Add(Me.lblEntries)
            Me.pnlListings.Controls.Add(Me.lblAddressType)
            Me.pnlListings.Controls.Add(Me.pnlContacts)
            Me.pnlListings.Controls.Add(Me.pnlGroups)
            Me.pnlListings.Location = New System.Drawing.Point(4, 58)
            Me.pnlListings.Name = "pnlListings"
            Me.pnlListings.Size = New System.Drawing.Size(552, 407)
            Me.pnlListings.TabIndex = 2
            Me.pnlListings.Visible = False
            '
            'cbxAddressType
            '
            Me.cbxAddressType.AllowEmptyValue = False
            Me.cbxAddressType.AutoSize = True
            Me.cbxAddressType.Borderless = False
            Me.cbxAddressType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cbxAddressType.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cbxAddressType.IsDirty = False
            Me.cbxAddressType.LimitToList = True
            Me.cbxAddressType.Location = New System.Drawing.Point(6, 21)
            Me.cbxAddressType.Margin = New System.Windows.Forms.Padding(2)
            Me.cbxAddressType.MaxDropDownItems = 8
            Me.cbxAddressType.Name = "cbxAddressType"
            Me.cbxAddressType.SelectedIndex = -1
            Me.cbxAddressType.SelectedValue = Nothing
            Me.cbxAddressType.SelectionLength = 0
            Me.cbxAddressType.SelectionStart = 0
            Me.cbxAddressType.Size = New System.Drawing.Size(268, 24)
            Me.cbxAddressType.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cbxAddressType.SupportingValues = ""
            Me.cbxAddressType.TabIndex = 1
            Me.cbxAddressType.Tag2 = Nothing
            Me.cbxAddressType.Value = ""
            '
            'cmdCancel
            '
            Me.cmdCancel.Location = New System.Drawing.Point(456, 367)
            Me.cmdCancel.Name = "cmdCancel"
            Me.cmdCancel.Size = New System.Drawing.Size(84, 27)
            Me.cmdCancel.TabIndex = 39
            Me.cmdCancel.Text = "Cancel"
            Me.cmdCancel.UseVisualStyleBackColor = True
            '
            'cmdOK
            '
            Me.cmdOK.Location = New System.Drawing.Point(365, 367)
            Me.cmdOK.Name = "cmdOK"
            Me.cmdOK.Size = New System.Drawing.Size(84, 27)
            Me.cmdOK.TabIndex = 38
            Me.cmdOK.Text = "&Insert"
            Me.cmdOK.UseVisualStyleBackColor = True
            '
            'lblStatus
            '
            Me.lblStatus.Location = New System.Drawing.Point(11, 363)
            Me.lblStatus.Name = "lblStatus"
            Me.lblStatus.Size = New System.Drawing.Size(266, 23)
            Me.lblStatus.TabIndex = 37
            Me.lblStatus.Text = "Hello"
            '
            'grdListings
            '
            Me.grdListings.AllowUserToAddRows = False
            Me.grdListings.AllowUserToDeleteRows = False
            Me.grdListings.AllowUserToOrderColumns = True
            Me.grdListings.AllowUserToResizeRows = False
            Me.grdListings.BackgroundColor = System.Drawing.Color.White
            Me.grdListings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.grdListings.CausesValidation = False
            DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
            DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
            DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
            DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
            DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
            Me.grdListings.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
            Me.grdListings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
            DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
            DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
            DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
            DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
            DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
            DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
            Me.grdListings.DefaultCellStyle = DataGridViewCellStyle4
            Me.grdListings.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
            Me.grdListings.GridColor = System.Drawing.Color.White
            Me.grdListings.Location = New System.Drawing.Point(6, 68)
            Me.grdListings.Name = "grdListings"
            Me.grdListings.ReadOnly = True
            Me.grdListings.RowHeadersVisible = False
            Me.grdListings.RowTemplate.Height = 17
            Me.grdListings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
            Me.grdListings.Size = New System.Drawing.Size(268, 290)
            Me.grdListings.StandardTab = True
            Me.grdListings.TabIndex = 3
            '
            'lblEntries
            '
            Me.lblEntries.AutoSize = True
            Me.lblEntries.Location = New System.Drawing.Point(7, 54)
            Me.lblEntries.Name = "lblEntries"
            Me.lblEntries.Size = New System.Drawing.Size(52, 13)
            Me.lblEntries.TabIndex = 2
            Me.lblEntries.Text = "Contacts:"
            '
            'pnlContacts
            '
            Me.pnlContacts.Controls.Add(Me.grdEntityList)
            Me.pnlContacts.Controls.Add(Me.btnDeleteEntity)
            Me.pnlContacts.Controls.Add(Me.btnAddEntity)
            Me.pnlContacts.Controls.Add(Me.lstFrom)
            Me.pnlContacts.Controls.Add(Me.lstCC)
            Me.pnlContacts.Controls.Add(Me.lstBCC)
            Me.pnlContacts.Controls.Add(Me.lstTo)
            Me.pnlContacts.Controls.Add(Me.btnMoveDown)
            Me.pnlContacts.Controls.Add(Me.btnMoveUp)
            Me.pnlContacts.Controls.Add(Me.lblTo)
            Me.pnlContacts.Controls.Add(Me.lblBCC)
            Me.pnlContacts.Controls.Add(Me.lblCC)
            Me.pnlContacts.Controls.Add(Me.lblFrom)
            Me.pnlContacts.Controls.Add(Me.btnDeleteBCC)
            Me.pnlContacts.Controls.Add(Me.btnAddBCC)
            Me.pnlContacts.Controls.Add(Me.btnDeleteCC)
            Me.pnlContacts.Controls.Add(Me.btnAddCC)
            Me.pnlContacts.Controls.Add(Me.btnDeleteFrom)
            Me.pnlContacts.Controls.Add(Me.btnAddFrom)
            Me.pnlContacts.Controls.Add(Me.lblEntities)
            Me.pnlContacts.Controls.Add(Me.btnDeleteTo)
            Me.pnlContacts.Controls.Add(Me.btnAddTo)
            Me.pnlContacts.Location = New System.Drawing.Point(283, 21)
            Me.pnlContacts.Margin = New System.Windows.Forms.Padding(2)
            Me.pnlContacts.Name = "pnlContacts"
            Me.pnlContacts.Size = New System.Drawing.Size(269, 334)
            Me.pnlContacts.TabIndex = 36
            '
            'grdEntityList
            '
            Me.grdEntityList.AllowUserToAddRows = False
            Me.grdEntityList.AllowUserToDeleteRows = False
            Me.grdEntityList.AllowUserToResizeRows = False
            Me.grdEntityList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
            Me.grdEntityList.BackgroundColor = System.Drawing.Color.White
            Me.grdEntityList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.grdEntityList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.grdEntityList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
            Me.grdEntityList.Location = New System.Drawing.Point(39, 46)
            Me.grdEntityList.Margin = New System.Windows.Forms.Padding(2)
            Me.grdEntityList.MultiSelect = False
            Me.grdEntityList.Name = "grdEntityList"
            Me.grdEntityList.ReadOnly = True
            Me.grdEntityList.RowHeadersVisible = False
            Me.grdEntityList.RowTemplate.Height = 24
            Me.grdEntityList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
            Me.grdEntityList.Size = New System.Drawing.Size(223, 295)
            Me.grdEntityList.StandardTab = True
            Me.grdEntityList.TabIndex = 59
            '
            'btnDeleteEntity
            '
            Me.btnDeleteEntity.BackgroundImage = CType(resources.GetObject("btnDeleteEntity.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteEntity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteEntity.FlatAppearance.BorderSize = 0
            Me.btnDeleteEntity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteEntity.Image = CType(resources.GetObject("btnDeleteEntity.Image"), System.Drawing.Image)
            Me.btnDeleteEntity.Location = New System.Drawing.Point(4, 89)
            Me.btnDeleteEntity.Name = "btnDeleteEntity"
            Me.btnDeleteEntity.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteEntity.TabIndex = 57
            Me.btnDeleteEntity.UseVisualStyleBackColor = True
            '
            'btnAddEntity
            '
            Me.btnAddEntity.BackgroundImage = CType(resources.GetObject("btnAddEntity.BackgroundImage"), System.Drawing.Image)
            Me.btnAddEntity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddEntity.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddEntity.FlatAppearance.BorderSize = 0
            Me.btnAddEntity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddEntity.Image = CType(resources.GetObject("btnAddEntity.Image"), System.Drawing.Image)
            Me.btnAddEntity.Location = New System.Drawing.Point(7, 64)
            Me.btnAddEntity.Name = "btnAddEntity"
            Me.btnAddEntity.Size = New System.Drawing.Size(21, 21)
            Me.btnAddEntity.TabIndex = 56
            Me.btnAddEntity.UseVisualStyleBackColor = True
            '
            'lstFrom
            '
            Me.lstFrom.FormattingEnabled = True
            Me.lstFrom.Location = New System.Drawing.Point(40, 121)
            Me.lstFrom.Name = "lstFrom"
            Me.lstFrom.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
            Me.lstFrom.Size = New System.Drawing.Size(222, 43)
            Me.lstFrom.TabIndex = 50
            '
            'lstCC
            '
            Me.lstCC.FormattingEnabled = True
            Me.lstCC.Location = New System.Drawing.Point(40, 197)
            Me.lstCC.Name = "lstCC"
            Me.lstCC.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
            Me.lstCC.Size = New System.Drawing.Size(222, 43)
            Me.lstCC.TabIndex = 54
            '
            'lstBCC
            '
            Me.lstBCC.FormattingEnabled = True
            Me.lstBCC.Location = New System.Drawing.Point(40, 270)
            Me.lstBCC.Name = "lstBCC"
            Me.lstBCC.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
            Me.lstBCC.Size = New System.Drawing.Size(222, 43)
            Me.lstBCC.TabIndex = 55
            '
            'lstTo
            '
            Me.lstTo.FormattingEnabled = True
            Me.lstTo.Location = New System.Drawing.Point(40, 48)
            Me.lstTo.Name = "lstTo"
            Me.lstTo.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
            Me.lstTo.Size = New System.Drawing.Size(222, 43)
            Me.lstTo.TabIndex = 48
            '
            'btnMoveDown
            '
            Me.btnMoveDown.BackgroundImage = Global.My.Resources.Resources.DownArrow
            Me.btnMoveDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnMoveDown.FlatAppearance.BorderSize = 0
            Me.btnMoveDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnMoveDown.Location = New System.Drawing.Point(239, 27)
            Me.btnMoveDown.Margin = New System.Windows.Forms.Padding(2)
            Me.btnMoveDown.Name = "btnMoveDown"
            Me.btnMoveDown.Size = New System.Drawing.Size(20, 22)
            Me.btnMoveDown.TabIndex = 61
            Me.btnMoveDown.UseVisualStyleBackColor = True
            '
            'btnMoveUp
            '
            Me.btnMoveUp.BackgroundImage = Global.My.Resources.Resources.UpArrow
            Me.btnMoveUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnMoveUp.FlatAppearance.BorderSize = 0
            Me.btnMoveUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnMoveUp.Location = New System.Drawing.Point(221, 27)
            Me.btnMoveUp.Margin = New System.Windows.Forms.Padding(0)
            Me.btnMoveUp.Name = "btnMoveUp"
            Me.btnMoveUp.Size = New System.Drawing.Size(20, 21)
            Me.btnMoveUp.TabIndex = 60
            Me.btnMoveUp.UseVisualStyleBackColor = True
            '
            'lblTo
            '
            Me.lblTo.AutoSize = True
            Me.lblTo.Location = New System.Drawing.Point(38, 31)
            Me.lblTo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblTo.Name = "lblTo"
            Me.lblTo.Size = New System.Drawing.Size(23, 13)
            Me.lblTo.TabIndex = 47
            Me.lblTo.Text = "&To:"
            '
            'lblBCC
            '
            Me.lblBCC.AutoSize = True
            Me.lblBCC.Location = New System.Drawing.Point(38, 253)
            Me.lblBCC.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblBCC.Name = "lblBCC"
            Me.lblBCC.Size = New System.Drawing.Size(31, 13)
            Me.lblBCC.TabIndex = 54
            Me.lblBCC.Text = "&BCC:"
            '
            'lblCC
            '
            Me.lblCC.AutoSize = True
            Me.lblCC.Location = New System.Drawing.Point(38, 180)
            Me.lblCC.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblCC.Name = "lblCC"
            Me.lblCC.Size = New System.Drawing.Size(24, 13)
            Me.lblCC.TabIndex = 53
            Me.lblCC.Text = "&CC:"
            '
            'lblFrom
            '
            Me.lblFrom.AutoSize = True
            Me.lblFrom.Location = New System.Drawing.Point(38, 105)
            Me.lblFrom.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblFrom.Name = "lblFrom"
            Me.lblFrom.Size = New System.Drawing.Size(33, 13)
            Me.lblFrom.TabIndex = 49
            Me.lblFrom.Text = "Fro&m:"
            '
            'btnDeleteBCC
            '
            Me.btnDeleteBCC.BackgroundImage = CType(resources.GetObject("btnDeleteBCC.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteBCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteBCC.FlatAppearance.BorderSize = 0
            Me.btnDeleteBCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteBCC.Image = CType(resources.GetObject("btnDeleteBCC.Image"), System.Drawing.Image)
            Me.btnDeleteBCC.Location = New System.Drawing.Point(4, 297)
            Me.btnDeleteBCC.Name = "btnDeleteBCC"
            Me.btnDeleteBCC.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteBCC.TabIndex = 47
            Me.btnDeleteBCC.UseVisualStyleBackColor = True
            '
            'btnAddBCC
            '
            Me.btnAddBCC.BackgroundImage = CType(resources.GetObject("btnAddBCC.BackgroundImage"), System.Drawing.Image)
            Me.btnAddBCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddBCC.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddBCC.FlatAppearance.BorderSize = 0
            Me.btnAddBCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddBCC.Image = CType(resources.GetObject("btnAddBCC.Image"), System.Drawing.Image)
            Me.btnAddBCC.Location = New System.Drawing.Point(7, 275)
            Me.btnAddBCC.Name = "btnAddBCC"
            Me.btnAddBCC.Size = New System.Drawing.Size(21, 21)
            Me.btnAddBCC.TabIndex = 46
            Me.btnAddBCC.UseVisualStyleBackColor = True
            '
            'btnDeleteCC
            '
            Me.btnDeleteCC.BackgroundImage = CType(resources.GetObject("btnDeleteCC.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteCC.FlatAppearance.BorderSize = 0
            Me.btnDeleteCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteCC.Image = CType(resources.GetObject("btnDeleteCC.Image"), System.Drawing.Image)
            Me.btnDeleteCC.Location = New System.Drawing.Point(4, 225)
            Me.btnDeleteCC.Name = "btnDeleteCC"
            Me.btnDeleteCC.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteCC.TabIndex = 45
            Me.btnDeleteCC.UseVisualStyleBackColor = True
            '
            'btnAddCC
            '
            Me.btnAddCC.BackgroundImage = CType(resources.GetObject("btnAddCC.BackgroundImage"), System.Drawing.Image)
            Me.btnAddCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddCC.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddCC.FlatAppearance.BorderSize = 0
            Me.btnAddCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddCC.Image = CType(resources.GetObject("btnAddCC.Image"), System.Drawing.Image)
            Me.btnAddCC.Location = New System.Drawing.Point(7, 203)
            Me.btnAddCC.Name = "btnAddCC"
            Me.btnAddCC.Size = New System.Drawing.Size(21, 21)
            Me.btnAddCC.TabIndex = 44
            Me.btnAddCC.UseVisualStyleBackColor = True
            '
            'btnDeleteFrom
            '
            Me.btnDeleteFrom.BackgroundImage = CType(resources.GetObject("btnDeleteFrom.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteFrom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteFrom.FlatAppearance.BorderSize = 0
            Me.btnDeleteFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteFrom.Image = CType(resources.GetObject("btnDeleteFrom.Image"), System.Drawing.Image)
            Me.btnDeleteFrom.Location = New System.Drawing.Point(4, 149)
            Me.btnDeleteFrom.Name = "btnDeleteFrom"
            Me.btnDeleteFrom.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteFrom.TabIndex = 43
            Me.btnDeleteFrom.UseVisualStyleBackColor = True
            '
            'btnAddFrom
            '
            Me.btnAddFrom.BackgroundImage = CType(resources.GetObject("btnAddFrom.BackgroundImage"), System.Drawing.Image)
            Me.btnAddFrom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddFrom.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddFrom.FlatAppearance.BorderSize = 0
            Me.btnAddFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddFrom.Image = CType(resources.GetObject("btnAddFrom.Image"), System.Drawing.Image)
            Me.btnAddFrom.Location = New System.Drawing.Point(7, 127)
            Me.btnAddFrom.Name = "btnAddFrom"
            Me.btnAddFrom.Size = New System.Drawing.Size(21, 21)
            Me.btnAddFrom.TabIndex = 42
            Me.btnAddFrom.UseVisualStyleBackColor = True
            '
            'lblEntities
            '
            Me.lblEntities.AutoSize = True
            Me.lblEntities.Location = New System.Drawing.Point(38, 4)
            Me.lblEntities.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblEntities.Name = "lblEntities"
            Me.lblEntities.Size = New System.Drawing.Size(44, 13)
            Me.lblEntities.TabIndex = 58
            Me.lblEntities.Text = "En&tities:"
            '
            'btnDeleteTo
            '
            Me.btnDeleteTo.BackgroundImage = CType(resources.GetObject("btnDeleteTo.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteTo.FlatAppearance.BorderSize = 0
            Me.btnDeleteTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteTo.Image = CType(resources.GetObject("btnDeleteTo.Image"), System.Drawing.Image)
            Me.btnDeleteTo.Location = New System.Drawing.Point(4, 76)
            Me.btnDeleteTo.Name = "btnDeleteTo"
            Me.btnDeleteTo.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteTo.TabIndex = 36
            Me.btnDeleteTo.UseVisualStyleBackColor = True
            '
            'btnAddTo
            '
            Me.btnAddTo.BackgroundImage = CType(resources.GetObject("btnAddTo.BackgroundImage"), System.Drawing.Image)
            Me.btnAddTo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddTo.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddTo.FlatAppearance.BorderSize = 0
            Me.btnAddTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddTo.Image = CType(resources.GetObject("btnAddTo.Image"), System.Drawing.Image)
            Me.btnAddTo.Location = New System.Drawing.Point(7, 54)
            Me.btnAddTo.Name = "btnAddTo"
            Me.btnAddTo.Size = New System.Drawing.Size(21, 21)
            Me.btnAddTo.TabIndex = 35
            Me.btnAddTo.UseVisualStyleBackColor = True
            '
            'pnlGroups
            '
            Me.pnlGroups.Controls.Add(Me.btnAddMember)
            Me.pnlGroups.Controls.Add(Me.btnDeleteMember)
            Me.pnlGroups.Controls.Add(Me.btnDeleteGroup)
            Me.pnlGroups.Controls.Add(Me.cbxGroups)
            Me.pnlGroups.Controls.Add(Me.btnAddGroup)
            Me.pnlGroups.Controls.Add(Me.lblGroup)
            Me.pnlGroups.Controls.Add(Me.lstGroupMembers)
            Me.pnlGroups.Controls.Add(Me.lblGroupMembers)
            Me.pnlGroups.Location = New System.Drawing.Point(284, 0)
            Me.pnlGroups.Margin = New System.Windows.Forms.Padding(2)
            Me.pnlGroups.Name = "pnlGroups"
            Me.pnlGroups.Size = New System.Drawing.Size(267, 359)
            Me.pnlGroups.TabIndex = 5
            '
            'btnAddMember
            '
            Me.btnAddMember.BackgroundImage = CType(resources.GetObject("btnAddMember.BackgroundImage"), System.Drawing.Image)
            Me.btnAddMember.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddMember.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.btnAddMember.FlatAppearance.BorderSize = 0
            Me.btnAddMember.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddMember.Image = CType(resources.GetObject("btnAddMember.Image"), System.Drawing.Image)
            Me.btnAddMember.Location = New System.Drawing.Point(5, 83)
            Me.btnAddMember.Name = "btnAddMember"
            Me.btnAddMember.Size = New System.Drawing.Size(21, 21)
            Me.btnAddMember.TabIndex = 23
            Me.btnAddMember.UseVisualStyleBackColor = True
            '
            'btnDeleteMember
            '
            Me.btnDeleteMember.BackgroundImage = CType(resources.GetObject("btnDeleteMember.BackgroundImage"), System.Drawing.Image)
            Me.btnDeleteMember.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteMember.FlatAppearance.BorderSize = 0
            Me.btnDeleteMember.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteMember.Image = CType(resources.GetObject("btnDeleteMember.Image"), System.Drawing.Image)
            Me.btnDeleteMember.Location = New System.Drawing.Point(2, 108)
            Me.btnDeleteMember.Name = "btnDeleteMember"
            Me.btnDeleteMember.Size = New System.Drawing.Size(21, 21)
            Me.btnDeleteMember.TabIndex = 24
            Me.btnDeleteMember.UseVisualStyleBackColor = True
            '
            'btnDeleteGroup
            '
            Me.btnDeleteGroup.BackColor = System.Drawing.Color.Transparent
            Me.btnDeleteGroup.BackgroundImage = Global.My.Resources.Resources.Remove
            Me.btnDeleteGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnDeleteGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnDeleteGroup.ForeColor = System.Drawing.Color.WhiteSmoke
            Me.btnDeleteGroup.Location = New System.Drawing.Point(237, 23)
            Me.btnDeleteGroup.Margin = New System.Windows.Forms.Padding(2)
            Me.btnDeleteGroup.Name = "btnDeleteGroup"
            Me.btnDeleteGroup.Size = New System.Drawing.Size(16, 16)
            Me.btnDeleteGroup.TabIndex = 28
            Me.btnDeleteGroup.UseVisualStyleBackColor = False
            '
            'cbxGroups
            '
            Me.cbxGroups.AllowEmptyValue = False
            Me.cbxGroups.AutoSize = True
            Me.cbxGroups.Borderless = False
            Me.cbxGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cbxGroups.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cbxGroups.IsDirty = False
            Me.cbxGroups.LimitToList = True
            Me.cbxGroups.Location = New System.Drawing.Point(37, 21)
            Me.cbxGroups.Margin = New System.Windows.Forms.Padding(2)
            Me.cbxGroups.MaxDropDownItems = 8
            Me.cbxGroups.Name = "cbxGroups"
            Me.cbxGroups.SelectedIndex = -1
            Me.cbxGroups.SelectedValue = Nothing
            Me.cbxGroups.SelectionLength = 0
            Me.cbxGroups.SelectionStart = 0
            Me.cbxGroups.Size = New System.Drawing.Size(174, 23)
            Me.cbxGroups.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cbxGroups.SupportingValues = ""
            Me.cbxGroups.TabIndex = 26
            Me.cbxGroups.Tag2 = Nothing
            Me.cbxGroups.Value = ""
            '
            'btnAddGroup
            '
            Me.btnAddGroup.BackColor = System.Drawing.Color.Transparent
            Me.btnAddGroup.BackgroundImage = Global.My.Resources.Resources.Add
            Me.btnAddGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.btnAddGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.btnAddGroup.ForeColor = System.Drawing.Color.WhiteSmoke
            Me.btnAddGroup.Location = New System.Drawing.Point(216, 23)
            Me.btnAddGroup.Margin = New System.Windows.Forms.Padding(2)
            Me.btnAddGroup.Name = "btnAddGroup"
            Me.btnAddGroup.Size = New System.Drawing.Size(16, 16)
            Me.btnAddGroup.TabIndex = 27
            Me.btnAddGroup.UseVisualStyleBackColor = False
            '
            'lblGroup
            '
            Me.lblGroup.AutoSize = True
            Me.lblGroup.Location = New System.Drawing.Point(37, 6)
            Me.lblGroup.Name = "lblGroup"
            Me.lblGroup.Size = New System.Drawing.Size(39, 13)
            Me.lblGroup.TabIndex = 25
            Me.lblGroup.Text = "Grou&p:"
            '
            'lstGroupMembers
            '
            Me.lstGroupMembers.FormattingEnabled = True
            Me.lstGroupMembers.Location = New System.Drawing.Point(38, 68)
            Me.lstGroupMembers.Name = "lstGroupMembers"
            Me.lstGroupMembers.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
            Me.lstGroupMembers.Size = New System.Drawing.Size(223, 290)
            Me.lstGroupMembers.TabIndex = 30
            '
            'lblGroupMembers
            '
            Me.lblGroupMembers.AutoSize = True
            Me.lblGroupMembers.Location = New System.Drawing.Point(39, 53)
            Me.lblGroupMembers.Name = "lblGroupMembers"
            Me.lblGroupMembers.Size = New System.Drawing.Size(53, 13)
            Me.lblGroupMembers.TabIndex = 29
            Me.lblGroupMembers.Text = "&Members:"
            '
            'pnlFolders
            '
            Me.pnlFolders.Controls.Add(Me.pnlQuickFilter)
            Me.pnlFolders.Controls.Add(Me.lblStatusMsg)
            Me.pnlFolders.Controls.Add(Me.txtFilterValue1)
            Me.pnlFolders.Controls.Add(Me.btnQuickSearch)
            Me.pnlFolders.Controls.Add(Me.cmbOp1)
            Me.pnlFolders.Controls.Add(Me.cmbFilterFld1)
            Me.pnlFolders.Controls.Add(Me.lblContactLists)
            Me.pnlFolders.Controls.Add(Me.tvFolders)
            Me.pnlFolders.Controls.Add(Me.lblQuickFilter)
            Me.pnlFolders.Location = New System.Drawing.Point(2, 56)
            Me.pnlFolders.Name = "pnlFolders"
            Me.pnlFolders.Size = New System.Drawing.Size(555, 395)
            Me.pnlFolders.TabIndex = 0
            '
            'pnlQuickFilter
            '
            Me.pnlQuickFilter.Controls.Add(Me.tsQuickFilter)
            Me.pnlQuickFilter.Controls.Add(Me.lblQuickFilterAlpha)
            Me.pnlQuickFilter.Controls.Add(Me.optQuickFilter3)
            Me.pnlQuickFilter.Controls.Add(Me.optQuickFilter2)
            Me.pnlQuickFilter.Controls.Add(Me.optQuickFilter1)
            Me.pnlQuickFilter.Controls.Add(Me.optQuickFilter0)
            Me.pnlQuickFilter.Location = New System.Drawing.Point(-4, 339)
            Me.pnlQuickFilter.Margin = New System.Windows.Forms.Padding(2)
            Me.pnlQuickFilter.Name = "pnlQuickFilter"
            Me.pnlQuickFilter.Size = New System.Drawing.Size(537, 62)
            Me.pnlQuickFilter.TabIndex = 3
            '
            'tsQuickFilter
            '
            Me.tsQuickFilter.BackColor = System.Drawing.Color.WhiteSmoke
            Me.tsQuickFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
            Me.tsQuickFilter.CanOverflow = False
            Me.tsQuickFilter.Dock = System.Windows.Forms.DockStyle.None
            Me.tsQuickFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!)
            Me.tsQuickFilter.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
            Me.tsQuickFilter.ImageScalingSize = New System.Drawing.Size(23, 23)
            Me.tsQuickFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsBFilter0, Me.tsBFilter1, Me.tsBFilter2, Me.tsBFilter3, Me.tsBFilter4, Me.tsBFilter5, Me.tsBFilter6, Me.tsBFilter7, Me.tsBFilter8, Me.tsBFilter9, Me.tsBFilter10, Me.tsBFilter11, Me.tsBFilter12, Me.tsBFilter13, Me.tsBFilter14, Me.tsBFilter15, Me.tsBFilter16, Me.tsBFilter17, Me.tsBFilter18, Me.tsBFilter19, Me.tsBFilter20, Me.tsBFilter21, Me.tsBFilter22, Me.tsBFilter23, Me.tsBFilter24, Me.tsBFilter25, Me.tsBFilter26})
            Me.tsQuickFilter.Location = New System.Drawing.Point(20, 3)
            Me.tsQuickFilter.Name = "tsQuickFilter"
            Me.tsQuickFilter.Padding = New System.Windows.Forms.Padding(0)
            Me.tsQuickFilter.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
            Me.tsQuickFilter.Size = New System.Drawing.Size(526, 25)
            Me.tsQuickFilter.Stretch = True
            Me.tsQuickFilter.TabIndex = 3
            Me.tsQuickFilter.Text = "ToolStrip1"
            '
            'tsBFilter0
            '
            Me.tsBFilter0.AutoSize = False
            Me.tsBFilter0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter0.Image = CType(resources.GetObject("tsBFilter0.Image"), System.Drawing.Image)
            Me.tsBFilter0.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter0.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter0.Name = "tsBFilter0"
            Me.tsBFilter0.Size = New System.Drawing.Size(29, 22)
            Me.tsBFilter0.Tag = "^123^"
            Me.tsBFilter0.Text = "123"
            '
            'tsBFilter1
            '
            Me.tsBFilter1.AutoSize = False
            Me.tsBFilter1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter1.Image = CType(resources.GetObject("tsBFilter1.Image"), System.Drawing.Image)
            Me.tsBFilter1.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter1.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter1.Name = "tsBFilter1"
            Me.tsBFilter1.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter1.Tag = "a"
            Me.tsBFilter1.Text = "A"
            '
            'tsBFilter2
            '
            Me.tsBFilter2.AutoSize = False
            Me.tsBFilter2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter2.Image = CType(resources.GetObject("tsBFilter2.Image"), System.Drawing.Image)
            Me.tsBFilter2.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter2.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter2.Name = "tsBFilter2"
            Me.tsBFilter2.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter2.Tag = "b"
            Me.tsBFilter2.Text = "B"
            '
            'tsBFilter3
            '
            Me.tsBFilter3.AutoSize = False
            Me.tsBFilter3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
            Me.tsBFilter3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter3.Image = CType(resources.GetObject("tsBFilter3.Image"), System.Drawing.Image)
            Me.tsBFilter3.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter3.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter3.Name = "tsBFilter3"
            Me.tsBFilter3.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter3.Tag = "c"
            Me.tsBFilter3.Text = "C"
            '
            'tsBFilter4
            '
            Me.tsBFilter4.AutoSize = False
            Me.tsBFilter4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter4.Image = CType(resources.GetObject("tsBFilter4.Image"), System.Drawing.Image)
            Me.tsBFilter4.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter4.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter4.Name = "tsBFilter4"
            Me.tsBFilter4.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter4.Tag = "d"
            Me.tsBFilter4.Text = "D"
            '
            'tsBFilter5
            '
            Me.tsBFilter5.AutoSize = False
            Me.tsBFilter5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter5.Image = CType(resources.GetObject("tsBFilter5.Image"), System.Drawing.Image)
            Me.tsBFilter5.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter5.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter5.Name = "tsBFilter5"
            Me.tsBFilter5.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter5.Tag = "e"
            Me.tsBFilter5.Text = "E"
            '
            'tsBFilter6
            '
            Me.tsBFilter6.AutoSize = False
            Me.tsBFilter6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter6.Image = CType(resources.GetObject("tsBFilter6.Image"), System.Drawing.Image)
            Me.tsBFilter6.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter6.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter6.Name = "tsBFilter6"
            Me.tsBFilter6.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter6.Tag = "f"
            Me.tsBFilter6.Text = "F"
            '
            'tsBFilter7
            '
            Me.tsBFilter7.AutoSize = False
            Me.tsBFilter7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter7.Image = CType(resources.GetObject("tsBFilter7.Image"), System.Drawing.Image)
            Me.tsBFilter7.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter7.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter7.Name = "tsBFilter7"
            Me.tsBFilter7.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter7.Tag = "g"
            Me.tsBFilter7.Text = "G"
            '
            'tsBFilter8
            '
            Me.tsBFilter8.AutoSize = False
            Me.tsBFilter8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter8.Image = CType(resources.GetObject("tsBFilter8.Image"), System.Drawing.Image)
            Me.tsBFilter8.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter8.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter8.Name = "tsBFilter8"
            Me.tsBFilter8.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter8.Tag = "h"
            Me.tsBFilter8.Text = "H"
            '
            'tsBFilter9
            '
            Me.tsBFilter9.AutoSize = False
            Me.tsBFilter9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter9.Image = CType(resources.GetObject("tsBFilter9.Image"), System.Drawing.Image)
            Me.tsBFilter9.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter9.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter9.Name = "tsBFilter9"
            Me.tsBFilter9.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter9.Tag = "i"
            Me.tsBFilter9.Text = "I"
            '
            'tsBFilter10
            '
            Me.tsBFilter10.AutoSize = False
            Me.tsBFilter10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter10.Image = CType(resources.GetObject("tsBFilter10.Image"), System.Drawing.Image)
            Me.tsBFilter10.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter10.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter10.Name = "tsBFilter10"
            Me.tsBFilter10.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter10.Tag = "j"
            Me.tsBFilter10.Text = "J"
            '
            'tsBFilter11
            '
            Me.tsBFilter11.AutoSize = False
            Me.tsBFilter11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter11.Image = CType(resources.GetObject("tsBFilter11.Image"), System.Drawing.Image)
            Me.tsBFilter11.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter11.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter11.Name = "tsBFilter11"
            Me.tsBFilter11.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter11.Tag = "k"
            Me.tsBFilter11.Text = "K"
            '
            'tsBFilter12
            '
            Me.tsBFilter12.AutoSize = False
            Me.tsBFilter12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter12.Image = CType(resources.GetObject("tsBFilter12.Image"), System.Drawing.Image)
            Me.tsBFilter12.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter12.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter12.Name = "tsBFilter12"
            Me.tsBFilter12.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter12.Tag = "l"
            Me.tsBFilter12.Text = "L"
            '
            'tsBFilter13
            '
            Me.tsBFilter13.AutoSize = False
            Me.tsBFilter13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter13.Image = CType(resources.GetObject("tsBFilter13.Image"), System.Drawing.Image)
            Me.tsBFilter13.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter13.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter13.Name = "tsBFilter13"
            Me.tsBFilter13.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter13.Tag = "m"
            Me.tsBFilter13.Text = "M"
            '
            'tsBFilter14
            '
            Me.tsBFilter14.AutoSize = False
            Me.tsBFilter14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter14.Image = CType(resources.GetObject("tsBFilter14.Image"), System.Drawing.Image)
            Me.tsBFilter14.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter14.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter14.Name = "tsBFilter14"
            Me.tsBFilter14.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter14.Tag = "n"
            Me.tsBFilter14.Text = "N"
            '
            'tsBFilter15
            '
            Me.tsBFilter15.AutoSize = False
            Me.tsBFilter15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter15.Image = CType(resources.GetObject("tsBFilter15.Image"), System.Drawing.Image)
            Me.tsBFilter15.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter15.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter15.Name = "tsBFilter15"
            Me.tsBFilter15.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter15.Tag = "o"
            Me.tsBFilter15.Text = "O"
            '
            'tsBFilter16
            '
            Me.tsBFilter16.AutoSize = False
            Me.tsBFilter16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter16.Image = CType(resources.GetObject("tsBFilter16.Image"), System.Drawing.Image)
            Me.tsBFilter16.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter16.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter16.Name = "tsBFilter16"
            Me.tsBFilter16.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter16.Tag = "p"
            Me.tsBFilter16.Text = "P"
            '
            'tsBFilter17
            '
            Me.tsBFilter17.AutoSize = False
            Me.tsBFilter17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter17.Image = CType(resources.GetObject("tsBFilter17.Image"), System.Drawing.Image)
            Me.tsBFilter17.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter17.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter17.Name = "tsBFilter17"
            Me.tsBFilter17.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter17.Tag = "q"
            Me.tsBFilter17.Text = "Q"
            '
            'tsBFilter18
            '
            Me.tsBFilter18.AutoSize = False
            Me.tsBFilter18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter18.Image = CType(resources.GetObject("tsBFilter18.Image"), System.Drawing.Image)
            Me.tsBFilter18.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter18.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter18.Name = "tsBFilter18"
            Me.tsBFilter18.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter18.Tag = "r"
            Me.tsBFilter18.Text = "R"
            '
            'tsBFilter19
            '
            Me.tsBFilter19.AutoSize = False
            Me.tsBFilter19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter19.Image = CType(resources.GetObject("tsBFilter19.Image"), System.Drawing.Image)
            Me.tsBFilter19.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter19.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter19.Name = "tsBFilter19"
            Me.tsBFilter19.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter19.Tag = "s"
            Me.tsBFilter19.Text = "S"
            '
            'tsBFilter20
            '
            Me.tsBFilter20.AutoSize = False
            Me.tsBFilter20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter20.Image = CType(resources.GetObject("tsBFilter20.Image"), System.Drawing.Image)
            Me.tsBFilter20.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter20.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter20.Name = "tsBFilter20"
            Me.tsBFilter20.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter20.Tag = "t"
            Me.tsBFilter20.Text = "T"
            '
            'tsBFilter21
            '
            Me.tsBFilter21.AutoSize = False
            Me.tsBFilter21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter21.Image = CType(resources.GetObject("tsBFilter21.Image"), System.Drawing.Image)
            Me.tsBFilter21.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter21.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter21.Name = "tsBFilter21"
            Me.tsBFilter21.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter21.Tag = "u"
            Me.tsBFilter21.Text = "U"
            '
            'tsBFilter22
            '
            Me.tsBFilter22.AutoSize = False
            Me.tsBFilter22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter22.Image = CType(resources.GetObject("tsBFilter22.Image"), System.Drawing.Image)
            Me.tsBFilter22.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter22.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter22.Name = "tsBFilter22"
            Me.tsBFilter22.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter22.Tag = "v"
            Me.tsBFilter22.Text = "V"
            '
            'tsBFilter23
            '
            Me.tsBFilter23.AutoSize = False
            Me.tsBFilter23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter23.Image = CType(resources.GetObject("tsBFilter23.Image"), System.Drawing.Image)
            Me.tsBFilter23.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter23.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter23.Name = "tsBFilter23"
            Me.tsBFilter23.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter23.Tag = "w"
            Me.tsBFilter23.Text = "W"
            '
            'tsBFilter24
            '
            Me.tsBFilter24.AutoSize = False
            Me.tsBFilter24.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter24.Image = CType(resources.GetObject("tsBFilter24.Image"), System.Drawing.Image)
            Me.tsBFilter24.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter24.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter24.Name = "tsBFilter24"
            Me.tsBFilter24.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter24.Tag = "x"
            Me.tsBFilter24.Text = "X"
            '
            'tsBFilter25
            '
            Me.tsBFilter25.AutoSize = False
            Me.tsBFilter25.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter25.Image = CType(resources.GetObject("tsBFilter25.Image"), System.Drawing.Image)
            Me.tsBFilter25.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter25.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter25.Name = "tsBFilter25"
            Me.tsBFilter25.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter25.Tag = "y"
            Me.tsBFilter25.Text = "Y"
            '
            'tsBFilter26
            '
            Me.tsBFilter26.AutoSize = False
            Me.tsBFilter26.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.tsBFilter26.Image = CType(resources.GetObject("tsBFilter26.Image"), System.Drawing.Image)
            Me.tsBFilter26.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.tsBFilter26.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
            Me.tsBFilter26.Name = "tsBFilter26"
            Me.tsBFilter26.Size = New System.Drawing.Size(18, 22)
            Me.tsBFilter26.Tag = "z"
            Me.tsBFilter26.Text = "Z"
            '
            'lblQuickFilterAlpha
            '
            Me.lblQuickFilterAlpha.AutoSize = True
            Me.lblQuickFilterAlpha.Location = New System.Drawing.Point(55, 37)
            Me.lblQuickFilterAlpha.Name = "lblQuickFilterAlpha"
            Me.lblQuickFilterAlpha.Size = New System.Drawing.Size(52, 13)
            Me.lblQuickFilterAlpha.TabIndex = 4
            Me.lblQuickFilterAlpha.Text = "Filter On: "
            '
            'optQuickFilter3
            '
            Me.optQuickFilter3.AutoSize = True
            Me.optQuickFilter3.Location = New System.Drawing.Point(407, 35)
            Me.optQuickFilter3.Name = "optQuickFilter3"
            Me.optQuickFilter3.Size = New System.Drawing.Size(90, 17)
            Me.optQuickFilter3.TabIndex = 8
            Me.optQuickFilter3.TabStop = True
            Me.optQuickFilter3.Text = "Display Name"
            Me.optQuickFilter3.UseVisualStyleBackColor = True
            '
            'optQuickFilter2
            '
            Me.optQuickFilter2.AutoSize = True
            Me.optQuickFilter2.Location = New System.Drawing.Point(316, 35)
            Me.optQuickFilter2.Name = "optQuickFilter2"
            Me.optQuickFilter2.Size = New System.Drawing.Size(69, 17)
            Me.optQuickFilter2.TabIndex = 7
            Me.optQuickFilter2.TabStop = True
            Me.optQuickFilter2.Text = "Company"
            Me.optQuickFilter2.UseVisualStyleBackColor = True
            '
            'optQuickFilter1
            '
            Me.optQuickFilter1.AutoSize = True
            Me.optQuickFilter1.Location = New System.Drawing.Point(223, 35)
            Me.optQuickFilter1.Name = "optQuickFilter1"
            Me.optQuickFilter1.Size = New System.Drawing.Size(75, 17)
            Me.optQuickFilter1.TabIndex = 6
            Me.optQuickFilter1.TabStop = True
            Me.optQuickFilter1.Text = "First Name"
            Me.optQuickFilter1.UseVisualStyleBackColor = True
            '
            'optQuickFilter0
            '
            Me.optQuickFilter0.AutoSize = True
            Me.optQuickFilter0.Checked = True
            Me.optQuickFilter0.Location = New System.Drawing.Point(120, 35)
            Me.optQuickFilter0.Name = "optQuickFilter0"
            Me.optQuickFilter0.Size = New System.Drawing.Size(76, 17)
            Me.optQuickFilter0.TabIndex = 5
            Me.optQuickFilter0.TabStop = True
            Me.optQuickFilter0.Text = "Last Name"
            Me.optQuickFilter0.UseVisualStyleBackColor = True
            '
            'lblStatusMsg
            '
            Me.lblStatusMsg.Location = New System.Drawing.Point(267, 5)
            Me.lblStatusMsg.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblStatusMsg.Name = "lblStatusMsg"
            Me.lblStatusMsg.Size = New System.Drawing.Size(279, 13)
            Me.lblStatusMsg.TabIndex = 41
            Me.lblStatusMsg.Text = "                        "
            Me.lblStatusMsg.TextAlign = System.Drawing.ContentAlignment.TopRight
            '
            'txtFilterValue1
            '
            Me.txtFilterValue1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.txtFilterValue1.Location = New System.Drawing.Point(240, 369)
            Me.txtFilterValue1.Margin = New System.Windows.Forms.Padding(2)
            Me.txtFilterValue1.Name = "txtFilterValue1"
            Me.txtFilterValue1.Size = New System.Drawing.Size(162, 21)
            Me.txtFilterValue1.TabIndex = 12
            '
            'btnQuickSearch
            '
            Me.btnQuickSearch.Image = Global.My.Resources.Resources.Search
            Me.btnQuickSearch.Location = New System.Drawing.Point(405, 368)
            Me.btnQuickSearch.Margin = New System.Windows.Forms.Padding(2)
            Me.btnQuickSearch.Name = "btnQuickSearch"
            Me.btnQuickSearch.Size = New System.Drawing.Size(33, 22)
            Me.btnQuickSearch.TabIndex = 13
            Me.btnQuickSearch.UseVisualStyleBackColor = True
            '
            'cmbOp1
            '
            Me.cmbOp1.AllowEmptyValue = False
            Me.cmbOp1.AutoSize = True
            Me.cmbOp1.Borderless = False
            Me.cmbOp1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cmbOp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cmbOp1.IsDirty = False
            Me.cmbOp1.LimitToList = True
            Me.cmbOp1.Location = New System.Drawing.Point(128, 369)
            Me.cmbOp1.Margin = New System.Windows.Forms.Padding(2)
            Me.cmbOp1.MaxDropDownItems = 8
            Me.cmbOp1.Name = "cmbOp1"
            Me.cmbOp1.SelectedIndex = -1
            Me.cmbOp1.SelectedValue = Nothing
            Me.cmbOp1.SelectionLength = 0
            Me.cmbOp1.SelectionStart = 0
            Me.cmbOp1.Size = New System.Drawing.Size(110, 24)
            Me.cmbOp1.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cmbOp1.SupportingValues = ""
            Me.cmbOp1.TabIndex = 11
            Me.cmbOp1.Tag2 = Nothing
            Me.cmbOp1.Value = ""
            '
            'cmbFilterFld1
            '
            Me.cmbFilterFld1.AllowEmptyValue = False
            Me.cmbFilterFld1.AutoSize = True
            Me.cmbFilterFld1.Borderless = False
            Me.cmbFilterFld1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cmbFilterFld1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cmbFilterFld1.IsDirty = False
            Me.cmbFilterFld1.LimitToList = True
            Me.cmbFilterFld1.Location = New System.Drawing.Point(15, 369)
            Me.cmbFilterFld1.Margin = New System.Windows.Forms.Padding(2)
            Me.cmbFilterFld1.MaxDropDownItems = 8
            Me.cmbFilterFld1.Name = "cmbFilterFld1"
            Me.cmbFilterFld1.SelectedIndex = -1
            Me.cmbFilterFld1.SelectedValue = Nothing
            Me.cmbFilterFld1.SelectionLength = 0
            Me.cmbFilterFld1.SelectionStart = 0
            Me.cmbFilterFld1.Size = New System.Drawing.Size(110, 24)
            Me.cmbFilterFld1.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cmbFilterFld1.SupportingValues = ""
            Me.cmbFilterFld1.TabIndex = 10
            Me.cmbFilterFld1.Tag2 = Nothing
            Me.cmbFilterFld1.Value = ""
            '
            'lblContactLists
            '
            Me.lblContactLists.AutoSize = True
            Me.lblContactLists.Location = New System.Drawing.Point(9, 5)
            Me.lblContactLists.Name = "lblContactLists"
            Me.lblContactLists.Size = New System.Drawing.Size(139, 13)
            Me.lblContactLists.TabIndex = 1
            Me.lblContactLists.Text = "Cont&act Folders/Directories:"
            '
            'tvFolders
            '
            Me.tvFolders.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.tvFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.tvFolders.ContextMenuStrip = Me.tvFoldersContextMenu
            Me.tvFolders.HideSelection = False
            Me.tvFolders.ImageIndex = 0
            Me.tvFolders.ImageList = Me.ilTreeIcons
            Me.tvFolders.Indent = 22
            Me.tvFolders.ItemHeight = 20
            Me.tvFolders.Location = New System.Drawing.Point(8, 22)
            Me.tvFolders.Name = "tvFolders"
            Me.tvFolders.SelectedImageIndex = 0
            Me.tvFolders.ShowRootLines = False
            Me.tvFolders.Size = New System.Drawing.Size(538, 316)
            Me.tvFolders.TabIndex = 2
            '
            'tvFoldersContextMenu
            '
            Me.tvFoldersContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tvFoldersContextMenuSelectOnStartup, Me.tvFoldersContextMenuLoadOnStartup, Me.tvFoldersContextMenuRemoveDefault})
            Me.tvFoldersContextMenu.Name = "cmFolders"
            Me.tvFoldersContextMenu.Size = New System.Drawing.Size(245, 70)
            '
            'tvFoldersContextMenuSelectOnStartup
            '
            Me.tvFoldersContextMenuSelectOnStartup.Name = "tvFoldersContextMenuSelectOnStartup"
            Me.tvFoldersContextMenuSelectOnStartup.Size = New System.Drawing.Size(244, 22)
            Me.tvFoldersContextMenuSelectOnStartup.Text = "&Select Current Folder On Startup"
            '
            'tvFoldersContextMenuLoadOnStartup
            '
            Me.tvFoldersContextMenuLoadOnStartup.Name = "tvFoldersContextMenuLoadOnStartup"
            Me.tvFoldersContextMenuLoadOnStartup.Size = New System.Drawing.Size(244, 22)
            Me.tvFoldersContextMenuLoadOnStartup.Text = "&Load Current Folder On Startup"
            '
            'tvFoldersContextMenuRemoveDefault
            '
            Me.tvFoldersContextMenuRemoveDefault.Name = "tvFoldersContextMenuRemoveDefault"
            Me.tvFoldersContextMenuRemoveDefault.Size = New System.Drawing.Size(244, 22)
            Me.tvFoldersContextMenuRemoveDefault.Text = "&Remove Default Folder"
            '
            'ilTreeIcons
            '
            Me.ilTreeIcons.ImageStream = CType(resources.GetObject("ilTreeIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
            Me.ilTreeIcons.TransparentColor = System.Drawing.Color.Transparent
            Me.ilTreeIcons.Images.SetKeyName(0, "Backend")
            Me.ilTreeIcons.Images.SetKeyName(1, "ClosedStore")
            Me.ilTreeIcons.Images.SetKeyName(2, "OpenStore")
            Me.ilTreeIcons.Images.SetKeyName(3, "ClosedFolder")
            Me.ilTreeIcons.Images.SetKeyName(4, "OpenFolder")
            '
            'lblQuickFilter
            '
            Me.lblQuickFilter.AutoSize = True
            Me.lblQuickFilter.Location = New System.Drawing.Point(13, 352)
            Me.lblQuickFilter.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblQuickFilter.Name = "lblQuickFilter"
            Me.lblQuickFilter.Size = New System.Drawing.Size(110, 13)
            Me.lblQuickFilter.TabIndex = 9
            Me.lblQuickFilter.Text = "Find Contacts &Where:"
            '
            'CI
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.AutoSize = True
            Me.BackColor = System.Drawing.Color.WhiteSmoke
            Me.ClientSize = New System.Drawing.Size(558, 467)
            Me.Controls.Add(Me.pnlFolders)
            Me.Controls.Add(Me.tsToolbar)
            Me.Controls.Add(Me.pnlListings)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "CI"
            Me.ShowIcon = False
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.Text = "Contact Integration"
            Me.tsToolbar.ResumeLayout(False)
            Me.tsToolbar.PerformLayout()
            Me.pnlListings.ResumeLayout(False)
            Me.pnlListings.PerformLayout()
            CType(Me.grdListings, System.ComponentModel.ISupportInitialize).EndInit()
            Me.pnlContacts.ResumeLayout(False)
            Me.pnlContacts.PerformLayout()
            CType(Me.grdEntityList, System.ComponentModel.ISupportInitialize).EndInit()
            Me.pnlGroups.ResumeLayout(False)
            Me.pnlGroups.PerformLayout()
            Me.pnlFolders.ResumeLayout(False)
            Me.pnlFolders.PerformLayout()
            Me.pnlQuickFilter.ResumeLayout(False)
            Me.pnlQuickFilter.PerformLayout()
            Me.tsQuickFilter.ResumeLayout(False)
            Me.tsQuickFilter.PerformLayout()
            Me.tvFoldersContextMenu.ResumeLayout(False)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents tsToolbar As System.Windows.Forms.ToolStrip
        Friend WithEvents btnTogglePanels As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnSearch As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnSearchNative As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnAddContactInNative As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnEdit As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnRefresh As System.Windows.Forms.ToolStripButton
        Friend WithEvents btnGroups As System.Windows.Forms.ToolStripButton
        Friend WithEvents lblAddressType As System.Windows.Forms.Label
        Friend WithEvents pnlListings As System.Windows.Forms.Panel
        Friend WithEvents grdListings As System.Windows.Forms.DataGridView
        Friend WithEvents lblEntries As System.Windows.Forms.Label
        Friend WithEvents pnlContacts As System.Windows.Forms.Panel
        Friend WithEvents grdEntityList As System.Windows.Forms.DataGridView
        Friend WithEvents lblTo As System.Windows.Forms.Label
        Friend WithEvents lblBCC As System.Windows.Forms.Label
        Friend WithEvents lblCC As System.Windows.Forms.Label
        Friend WithEvents lblFrom As System.Windows.Forms.Label
        Friend WithEvents lstBCC As System.Windows.Forms.ListBox
        Friend WithEvents lstCC As System.Windows.Forms.ListBox
        Friend WithEvents lstFrom As System.Windows.Forms.ListBox
        Friend WithEvents lstTo As System.Windows.Forms.ListBox
        Friend WithEvents btnDeleteBCC As System.Windows.Forms.Button
        Friend WithEvents btnAddBCC As System.Windows.Forms.Button
        Friend WithEvents btnDeleteCC As System.Windows.Forms.Button
        Friend WithEvents btnAddCC As System.Windows.Forms.Button
        Friend WithEvents btnDeleteFrom As System.Windows.Forms.Button
        Friend WithEvents btnAddFrom As System.Windows.Forms.Button
        Friend WithEvents lblEntities As System.Windows.Forms.Label
        Friend WithEvents btnDeleteTo As System.Windows.Forms.Button
        Friend WithEvents btnAddTo As System.Windows.Forms.Button
        Friend WithEvents lblGroup As System.Windows.Forms.Label
        Friend WithEvents btnAddGroup As System.Windows.Forms.Button
        Friend WithEvents lblGroupMembers As System.Windows.Forms.Label
        Friend WithEvents lstGroupMembers As System.Windows.Forms.ListBox
        Friend WithEvents pnlGroups As System.Windows.Forms.Panel
        Friend WithEvents pnlFolders As System.Windows.Forms.Panel
        Friend WithEvents lblContactLists As System.Windows.Forms.Label
        Friend WithEvents tvFolders As System.Windows.Forms.TreeView
        Friend WithEvents btnMoveDown As System.Windows.Forms.Button
        Friend WithEvents btnMoveUp As System.Windows.Forms.Button
        Friend WithEvents btnHelpNew As System.Windows.Forms.ToolStripDropDownButton
        Friend WithEvents btnOptionsFolders As System.Windows.Forms.ToolStripDropDownButton
        Friend WithEvents mnuFolders_SelectCurrentFolderOnStartup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_LoadCurrentFolderOnStartup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_RemoveDefaultFolder As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_Separator1 As System.Windows.Forms.ToolStripSeparator
        Friend WithEvents mnuFolders_ToggleQuickFilterType As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents btnOptionsContacts As System.Windows.Forms.ToolStripDropDownButton
        Friend WithEvents mnuContacts_Expand As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuContacts_SelectAll As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuContacts_ShowDetail As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents tsContactsSeparator1 As System.Windows.Forms.ToolStripSeparator
        Friend WithEvents mnuContacts_MoveSelectedContactsUp As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuContacts_MoveSelectedContactsDown As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents tsContactsSeparator2 As System.Windows.Forms.ToolStripSeparator
        Friend WithEvents mnuContacts_AddGroup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuContacts_DeleteGroup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents tvFoldersContextMenu As System.Windows.Forms.ContextMenuStrip
        Friend WithEvents tvFoldersContextMenuSelectOnStartup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents tvFoldersContextMenuLoadOnStartup As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents tvFoldersContextMenuRemoveDefault As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents cbxGroups As CIControls.ComboBox
        Friend WithEvents cmbOp1 As CIControls.ComboBox
        Friend WithEvents cmbFilterFld1 As CIControls.ComboBox
        Friend WithEvents pnlQuickFilter As System.Windows.Forms.Panel
        Friend WithEvents lblQuickFilterAlpha As System.Windows.Forms.Label
        Friend WithEvents optQuickFilter3 As System.Windows.Forms.RadioButton
        Friend WithEvents optQuickFilter2 As System.Windows.Forms.RadioButton
        Friend WithEvents tsQuickFilter As System.Windows.Forms.ToolStrip
        Friend WithEvents tsBFilter0 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter1 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter2 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter3 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter4 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter5 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter6 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter7 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter8 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter9 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter10 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter11 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter12 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter13 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter14 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter15 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter16 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter17 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter18 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter19 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter20 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter21 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter22 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter23 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter24 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter25 As System.Windows.Forms.ToolStripButton
        Friend WithEvents tsBFilter26 As System.Windows.Forms.ToolStripButton
        Friend WithEvents optQuickFilter1 As System.Windows.Forms.RadioButton
        Friend WithEvents optQuickFilter0 As System.Windows.Forms.RadioButton
        Friend WithEvents txtFilterValue1 As System.Windows.Forms.TextBox
        Friend WithEvents btnQuickSearch As System.Windows.Forms.Button
        Friend WithEvents lblQuickFilter As System.Windows.Forms.Label
        Friend WithEvents lblStatus As System.Windows.Forms.Label
        Friend WithEvents lblStatusMsg As System.Windows.Forms.Label
        Friend WithEvents mnuFolders_Separator2 As System.Windows.Forms.ToolStripSeparator
        Friend WithEvents mnuFolders_Custom1 As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_Custom2 As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_Custom3 As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_Custom4 As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents mnuFolders_Custom5 As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents cmdCancel As System.Windows.Forms.Button
        Friend WithEvents cmdOK As System.Windows.Forms.Button
        Friend WithEvents btnDeleteGroup As System.Windows.Forms.Button
        Friend WithEvents ilTreeIcons As System.Windows.Forms.ImageList
        Friend WithEvents cbxAddressType As CIControls.ComboBox
        Friend WithEvents btnAddMember As System.Windows.Forms.Button
        Friend WithEvents btnDeleteMember As System.Windows.Forms.Button
        Friend WithEvents btnDeleteEntity As System.Windows.Forms.Button
        Friend WithEvents btnAddEntity As System.Windows.Forms.Button
    End Class
End Namespace
