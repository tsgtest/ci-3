﻿Namespace TSG.CI

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AddGroupForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddGroupForm))
            Me.lblGroup = New System.Windows.Forms.Label()
            Me.txtGroupName = New System.Windows.Forms.TextBox()
            Me.btnOK = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.SuspendLayout()
            '
            'lblGroup
            '
            Me.lblGroup.AutoSize = True
            Me.lblGroup.Location = New System.Drawing.Point(9, 16)
            Me.lblGroup.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblGroup.Name = "lblGroup"
            Me.lblGroup.Size = New System.Drawing.Size(35, 13)
            Me.lblGroup.TabIndex = 0
            Me.lblGroup.Text = "&Name"
            '
            'txtGroupName
            '
            Me.txtGroupName.Location = New System.Drawing.Point(10, 32)
            Me.txtGroupName.Margin = New System.Windows.Forms.Padding(2)
            Me.txtGroupName.Name = "txtGroupName"
            Me.txtGroupName.Size = New System.Drawing.Size(329, 20)
            Me.txtGroupName.TabIndex = 1
            '
            'btnOK
            '
            Me.btnOK.Location = New System.Drawing.Point(164, 70)
            Me.btnOK.Margin = New System.Windows.Forms.Padding(2)
            Me.btnOK.Name = "btnOK"
            Me.btnOK.Size = New System.Drawing.Size(84, 27)
            Me.btnOK.TabIndex = 2
            Me.btnOK.Text = "O&K"
            Me.btnOK.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.Location = New System.Drawing.Point(253, 70)
            Me.btnCancel.Margin = New System.Windows.Forms.Padding(2)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(84, 27)
            Me.btnCancel.TabIndex = 3
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'AddGroupForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.ClientSize = New System.Drawing.Size(356, 107)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnOK)
            Me.Controls.Add(Me.txtGroupName)
            Me.Controls.Add(Me.lblGroup)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Margin = New System.Windows.Forms.Padding(2)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "AddGroupForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Add Group"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblGroup As System.Windows.Forms.Label
        Friend WithEvents txtGroupName As System.Windows.Forms.TextBox
        Friend WithEvents btnOK As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
    End Class
End Namespace