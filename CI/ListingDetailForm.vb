﻿Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class ListingDetailForm
#Region "******************fields***********************"

        Private m_oListing As CListing
        Private m_oAddresses As CAddresses
        Private m_bPromptForMissingPhones As Boolean
        Private m_bCanceled As Boolean
        Private m_iTopAddressIndex As Integer
        Private m_bReDrawing As Boolean
        Private m_xCurAddrName As String

        'control arrays
        Dim m_optAddress(6) As RadioButton

#End Region
#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            InitializeComponent()

            Try
                'Load icon
                With Main.g_oSessionType
                    If (.SessionType = ciSessionType.ciSession_Connect) Then
                        .SetIcon(Me.Handle, "CONNECTICON", False)
                    Else
                        .SetIcon(Me.Handle, "TSGICON", False)
                    End If
                End With

                'initialize control arrays
                m_optAddress(0) = Me.optAddress0
                m_optAddress(1) = Me.optAddress1
                m_optAddress(2) = Me.optAddress2
                m_optAddress(3) = Me.optAddress3
                m_optAddress(4) = Me.optAddress4
                m_optAddress(5) = Me.optAddress5
                m_optAddress(6) = Me.optAddress6
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub
#End Region
#Region "******************events***********************"
        Private Sub pctCopy_Click(sender As Object, e As EventArgs) Handles pctCopy.Click
            Dim xSummary As String

            Try
                xSummary = Me.lblDetail.Text

                'remove all double paragraphs
                While xSummary Like "*" & vbCrLf & vbCrLf & "*"
                    xSummary = Replace(xSummary, vbCrLf & vbCrLf, vbCrLf)
                End While

                While xSummary Like "*" & vbCr & vbCr & "*"
                    xSummary = Replace(xSummary, vbCr & vbCr, vbCrLf)
                End While

                'copy to clipboard
                Clipboard.SetDataObject(xSummary, True)
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Private Sub pctCopy_MouseDown(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseDown
            Try
                'show borders
                pctCopy.BorderStyle = BorderStyle.FixedSingle
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctCopy_MouseUp(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseUp
            Try
                'hide borders
                pctCopy.BorderStyle = BorderStyle.None
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub optAddress_Click(sender As Object, e As EventArgs) _
    Handles optAddress0.Click, optAddress1.Click, optAddress2.Click, optAddress3.Click, optAddress4.Click, optAddress5.Click, optAddress6.Click

            Try
                'Dim rdb As RadioButton = DirectCast(sender, RadioButton)

                'update listing detail to reflect new selection -
                'don't do while redrawing - the redraw method
                'handles this
                If Not m_bReDrawing Then
                    ShowListingDetail()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Private Sub ListingDetailForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Dim oAddress As CAddress
            '   get addresses from listing

            Try
                m_bCanceled = True

                If m_oAddresses Is Nothing Then
                    'addresses have not already been specified -
                    'retrieve from listing
                    m_oAddresses = Main.GetBackendFromID(Me.Listing.BackendID) _
                                                                  .GetAddresses(Me.Listing)
                End If

                If m_oAddresses Is Nothing Then
                    'couldn't get addresses from listing - error
                    Throw New Exception("Invalid address object from listing.")
                End If

                If m_oAddresses.Count = 0 Then
                    ShowListingDetail()
                    Me.lblAddresses.Text = "No Addresses"
                Else
                    'get index of specified address
                    Dim i As Integer
                    Dim iSel As Integer

                    For i = 1 To m_oAddresses.Count
                        If m_oAddresses.Item(i).Name = Mid$(Me.AddressName, 2) Then
                            'this is the specified address
                            iSel = i - 1
                            Exit For
                        End If
                    Next i

                    'add addresses to list box - scroll to the appropriate start position
                    'todo
                    RedrawAddressList(Main.ciMax(0, iSel - m_optAddress.Count + 1), "")

                    'todo
                    ''If m_oAddresses.Count > Me.optAddress.Count Then
                    ''    'there are more addresses to display - show scroll bar
                    ''    Me.vsbAddresses.Visible = True
                    ''    Me.vsbAddresses.Max = m_oAddresses.Count - 1
                    ''End If

                End If

            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
            Try
                m_bCanceled = False
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private Sub ListingDetailForm_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
            'todo
            ' ''If e.KeyCode = Keys.C And Shift = 2 Then
            ' ''    imgCopySummary_Click()
            ' ''End If
        End Sub

        'unused?
        '        Private Sub lstAddressTypes_DblClick()
        '            'do what happens when OK is clicked
        '            On Error GoTo ProcError
        '            btnClose_Click()
        '            Exit Sub
        'ProcError:
        '            Main.g_oError.ShowError()
        '            Exit Sub
        '        End Sub

        Private Sub imgCopySummary_Click()
            Dim xSummary As String

            Try
                xSummary = Me.lblDetail.Text

                'remove all double paragraphs
                While xSummary Like "*" & vbCrLf & vbCrLf & "*"
                    xSummary = Replace(xSummary, vbCrLf & vbCrLf, vbCrLf)
                End While

                While xSummary Like "*" & vbCr & vbCr & "*"
                    xSummary = Replace(xSummary, vbCr & vbCr, vbCrLf)
                End While

                Clipboard.Clear()
                Clipboard.SetText(Trim$(xSummary))
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'todo?
        ' ''Private Sub imgCopySummary_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
        ' ''    Me.imgCopySummary.BorderStyle = 1
        ' ''End Sub

        ' ''Private Sub imgCopySummary_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
        ' ''    Me.imgCopySummary.BorderStyle = 0
        ' ''End Sub

        Private Sub optAddress_Click(Index As Integer)
            Try
                'update listing detail to reflect new selection -
                'don't do while redrawing - the redraw method
                'handles this
                If Not m_bReDrawing Then
                    ShowListingDetail()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'todo?
        ' ''Private Sub vsbAddresses_Change()
        ' ''    RedrawAddressList(Me.vsbAddresses.Value, m_xCurAddrName)
        ' ''End Sub

#End Region
#Region "******************methods***********************"
        Private Sub RedrawAddressList(ByVal iStartIndex As Integer, ByVal xSelAddrName As String)
            Dim i As Integer
            Dim iCtlIndex As Integer
            Dim iSelIndex As Integer

            m_bReDrawing = True

            iSelIndex = -1

            'add all addresses from starting index - stop when available option controls are used
            For i = iStartIndex To Main.ciMin(m_oAddresses.Count - 1, iStartIndex + m_optAddress.Count - 1)
                'get the option control index to fill
                iCtlIndex = i - iStartIndex
                With m_optAddress(iCtlIndex)
                    'set option control
                    .Visible = True
                    .Text = "&" & m_oAddresses.Item(i + 1).Name

                    If .Text = xSelAddrName Then
                        'this is the address to select
                        iSelIndex = iCtlIndex
                    End If
                End With
            Next i

            'hide unused option controls
            For i = iCtlIndex + 1 To m_optAddress.Count - 1
                m_optAddress(i).Visible = False
            Next i

            'select index
            If iSelIndex = -1 Then
                For i = 0 To m_optAddress.Count - 1
                    m_optAddress(i).Checked = False
                Next i
                m_optAddress(0).Checked = True
                ShowListingDetail()
            Else
                m_optAddress(iSelIndex).Checked = True
            End If

            'if new selected address is different than the
            'previous selected address, update displayed address
            If (xSelAddrName <> m_xCurAddrName) And (xSelAddrName <> String.Empty) Then
                ShowListingDetail()
            End If

            m_iTopAddressIndex = iStartIndex
            m_bReDrawing = False
        End Sub

        Private Sub ShowListingDetail()
            'displays detail form with detail for
            'specified listing with selected address
            Dim oAddress As CAddress = Nothing
            Dim iIndex As Integer
            Dim oL As CListing
            Dim oContact As CContact
            Dim xTemp As String = ""
            Dim oB As ICIBackend


            Try
                'get address type from dlg
                iIndex = Main.Max(Me.AddressIndex + m_iTopAddressIndex, 0)

                Me.Cursor = Cursors.WaitCursor

                oL = Me.Listing
                oB = Main.GetBackendFromID(oL.BackendID)

                If m_oAddresses.Count Then
                    If iIndex > -1 Then
                        If iIndex > m_oAddresses.Count Then
                            iIndex = 0
                        End If
                        oAddress = m_oAddresses.Item(iIndex + 1)
                    End If
                End If


                Try
                    oContact = oB.GetContacts(oL, oAddress, vbNull, _
                              ciRetrieveData.ciRetrieveData_Names + ciRetrieveData.ciRetrieveData_Addresses + ciRetrieveData.ciRetrieveData_CustomFields, _
                              ciAlerts.ciAlert_None).Item(1)
                Catch
                End Try

                If Not (oContact Is Nothing) Then
                    With oContact
                        'build detail string
                        If .Prefix <> "" Then _
                            xTemp = xTemp & .Prefix
                        If .FirstName <> "" Then _
                            xTemp = xTemp & " " & .FirstName
                        If .MiddleName <> "" Then _
                            xTemp = xTemp & " " & .MiddleName
                        If .LastName <> "" Then _
                            xTemp = xTemp & " " & .LastName
                        If .Suffix <> "" Then _
                            xTemp = xTemp & " " & .Suffix
                        If xTemp <> "" Then _
                            xTemp = xTemp & vbCrLf

                        If m_oAddresses.Count > 0 Then
                            Dim xAddrType As String
                            xAddrType = UCase(.AddressTypeName)

                            If xAddrType Like "BUSINESS" Or xAddrType Like "OFFICE" Or _
                                xAddrType Like "WORK" Or xAddrType Like "MAIN" Or _
                                xAddrType Like "DIRECT" Or xAddrType Like "MAILING" Or _
                                xAddrType = String.Empty Or xAddrType Like "DEFAULT MAILING" Then
                                If .Title <> String.Empty Then _
                                    xTemp = xTemp & .Title & vbCrLf

                                If .Company <> String.Empty Then _
                                    xTemp = xTemp & .Company & vbCrLf
                            End If

                            If .CoreAddress <> String.Empty Then
                                'use core address if there is one
                                xTemp = xTemp & .CoreAddress
                            Else
                                If .Street1 <> "" Then _
                                    xTemp = xTemp & .Street1 & vbCrLf
                                If .Street2 <> "" Then _
                                    xTemp = xTemp & .Street2 & vbCrLf
                                If .Street3 <> "" Then _
                                    xTemp = xTemp & .Street3 & vbCrLf
                                If .AdditionalInformation <> "" Then _
                                    xTemp = xTemp & .AdditionalInformation & vbCrLf
                                If .City <> "" Then _
                                    xTemp = xTemp & .City
                                If .State <> "" Then _
                                    xTemp = xTemp & ", " & .State
                                If .ZipCode <> "" Then _
                                    xTemp = xTemp & " " & .ZipCode
                                If .Country <> "" Then _
                                    xTemp = xTemp & vbCrLf & .Country
                            End If

                            If xTemp <> "" Then _
                                xTemp = xTemp & vbCrLf & vbCrLf
                        End If

                        xTemp = xTemp & New String(vbCrLf, Main.ciMax(9 - Main.lCountChrs(xTemp, vbCrLf), 0))
                        Dim oNums As CContactNumbers

                        If oContact.AddressTypeID = Nothing Then
                            oNums = oB.GetPhoneNumbers(oL)
                        Else
                            oNums = oB.GetPhoneNumbers(oL, oContact.AddressTypeID)
                        End If

                        If oNums.Count = 0 Then
                            xTemp = xTemp & "Phone:  " & vbCrLf
                        ElseIf oNums.Count = 1 Then
                            'display phone if there is exactly one
                            With oNums.Item(1)
                                xTemp = xTemp & "Phone:  " & .Number
                                xTemp = xTemp & IIf(.Extension <> String.Empty, _
                                    "  x" & .Extension, "")
                            End With
                            xTemp = xTemp & vbCrLf
                        Else
                            'display "multiple numbers"
                            xTemp = xTemp & "Phone:  Multiple Numbers" & vbCrLf
                        End If

                        If oContact.AddressTypeID = vbNull Then
                            oNums = oB.GetFaxNumbers(oL)
                        Else
                            oNums = oB.GetFaxNumbers(oL, oContact.AddressTypeID)
                        End If

                        If oNums.Count = 0 Then
                            xTemp = xTemp & "Fax:  " & vbCrLf
                        ElseIf oNums.Count = 1 Then
                            xTemp = xTemp & "Fax:  " & oNums.Item(1).Number & vbCrLf
                        Else
                            xTemp = xTemp & "Fax:  Multiple Numbers" & vbCrLf
                        End If

                        If oContact.AddressTypeID = vbNull Then
                            oNums = oB.GetEMailNumbers(oL)
                        Else
                            oNums = oB.GetEMailNumbers(oL, oContact.AddressTypeID)
                        End If

                        If oNums.Count = 0 Then
                            xTemp = xTemp & "E-Mail:  " & vbCrLf
                        ElseIf oNums.Count = 1 Then
                            xTemp = xTemp & "E-Mail:  " & oNums.Item(1).Number & vbCrLf
                        Else
                            xTemp = xTemp & "E-Mail:  Multiple Addresses" & vbCrLf
                        End If
                    End With

                    'set and show form - ensure "&" appear as "&", not as hotkeys
                    With Me
                        .Text = "Contact Detail" & IIf(oContact.DisplayName <> String.Empty, " - " & Replace(oContact.DisplayName, "&", "&&"), "")
                        .lblDetail.Text = Trim$(Replace(xTemp, "&", "&&"))
                        Application.DoEvents()
                    End With
                Else
                    Me.Text = "Contact doesn't exist"
                End If

                If m_oAddresses.Count > 0 Then
                    m_xCurAddrName = m_optAddress(Me.AddressIndex).Text
                End If

            Finally
                Me.Cursor = Cursors.Default
            End Try

        End Sub
#End Region
#Region "******************properties***********************"
        Public Property Listing As CListing
            Set(oNew As CListing)
                m_oListing = oNew
            End Set
            Get
                Listing = m_oListing
            End Get
        End Property

        Public Property AddressIndex() As Integer
            Set(iNew As Integer)
                If iNew = -1 Then iNew = 0
            End Set
            Get
                Dim i As Integer
                'errorhandling?
                AddressIndex = -1
                For i = 0 To m_optAddress.Count - 1
                    If m_optAddress(i).Checked = True Then
                        AddressIndex = i
                        Exit For
                    End If
                Next i
            End Get
        End Property

        Public ReadOnly Property Canceled() As Boolean
            Get
                Canceled = m_bCanceled
            End Get
        End Property

        Public Property AddressName() As String
            Set(xNew As String)
                Dim i As Integer

                m_xCurAddrName = "&" & xNew
                If m_optAddress(0).Text <> String.Empty Then
                    'addresses have been loaded - select the specified address
                    For i = 0 To m_optAddress.Count - 1
                        If m_optAddress(i).Text = m_xCurAddrName Then
                            'this is the one
                            m_optAddress(i).Text = True
                            Exit For
                        End If
                    Next i
                End If
            End Set
            Get
                AddressName = m_xCurAddrName
            End Get
        End Property

        Public Property Addresses() As CAddresses
            Get
                Addresses = m_oAddresses
            End Get
            Set(oAddresses As CAddresses)
                m_oAddresses = oAddresses
            End Set
        End Property

#End Region
    End Class
End Namespace
