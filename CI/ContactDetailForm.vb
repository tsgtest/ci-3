﻿Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class ContactDetailForm
        Private m_oContacts As CContacts

#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            InitializeComponent()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property Contacts() As CContacts
            Get
                Contacts = m_oContacts
            End Get
            Set(oNew As CContacts)
                Try
                    m_oContacts = oNew
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Set
        End Property

        Public WriteOnly Property SelectedContactIndex() As Integer
            Set(iIndex As Integer)
                Me.lstContacts.SelectedIndex = iIndex
            End Set
        End Property
#End Region
#Region "******************events***********************"
        Private Sub pctCopy_Click(sender As Object, e As EventArgs) Handles pctCopy.Click
            Dim xSummary As String

            Try
                xSummary = Me.txtSummary.Text

                'remove all double paragraphs
                While xSummary Like "*" & vbCrLf & vbCrLf & "*"
                    xSummary = Replace(xSummary, vbCrLf & vbCrLf, vbCrLf)
                End While

                While xSummary Like "*" & vbCr & vbCr & "*"
                    xSummary = Replace(xSummary, vbCr & vbCr, vbCrLf)
                End While

                'copy to clipboard
                Clipboard.SetDataObject(xSummary, True)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctCopy_MouseDown(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseDown
            Try
                'show borders
                pctCopy.BorderStyle = BorderStyle.FixedSingle
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctCopy_MouseUp(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseUp
            Try
                'hide borders
                pctCopy.BorderStyle = BorderStyle.None
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
            Try
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ContactDetailForm_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
            Try
                m_oContacts = Nothing
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ContactDetailForm_Leave(sender As Object, e As EventArgs) Handles Me.Leave
        End Sub

        Private Sub ContactDetailForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Dim oDT As DataTable

            Try
                'get DataTable
                oDT = Me.Contacts.GetList()

                'set control
                With Me.lstContacts
                    .DisplayMember = oDT.Columns(0).ColumnName
                    .ValueMember = oDT.Columns(1).ColumnName
                    Application.DoEvents()
                    .DataSource = oDT
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ContactDetailForm_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
            Try
                'Load icon
                With Main.g_oSessionType
                    If (.SessionType = ciSessionType.ciSession_Connect) Then
                        .SetIcon(Me.Handle, "CONNECTICON", False)
                    Else
                        .SetIcon(Me.Handle, "TSGICON", False)
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        ' ''Private Sub lstContactDetail_MouseDown(sender As Object, e As MouseEventArgs)
        ' ''    On Error Resume Next
        ' ''    Me.lstContactDetail.SelectedItem = Me.lstContactDetail.IndexFromPoint(e.X, e.Y)
        ' ''End Sub

        Private Sub lstContacts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstContacts.SelectedIndexChanged
            Dim oContact As CContact
            Dim iFirstRow As Integer
            Static vPrevID As Object

            Try
                If (Not IsNothing(vPrevID)) And (Me.lstContacts.SelectedIndex = vPrevID) Then
                    Exit Sub
                Else
                    vPrevID = Me.lstContacts.SelectedIndex
                End If

                If Me.lstContacts.SelectedIndex >= 0 Then
                    oContact = m_oContacts.Item(CInt(Me.lstContacts.SelectedIndex + 1))

                    'get first row in detail list
                    'On Error Resume Next
                    'iFirstRow = Me.lstContactDetail.Items(0)
                    'On Error GoTo ProcError

                    If Not oContact Is Nothing Then
                        Application.DoEvents()
                        GetSummary(oContact)
                        Application.DoEvents()
                        GetDetail(oContact)
                    End If

                    'Me.lstContactDetail.Items(0) = iFirstRow
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCopySummary_Click()
            Try
                Clipboard.SetText(Me.txtSummary.Text)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub imgCopySummary_Click()
            Dim xSummary As String

            Try
                xSummary = Me.txtSummary.Text

                'remove all double paragraphs
                While xSummary Like "*" & vbCrLf & vbCrLf & vbCrLf & "*"
                    xSummary = Replace(xSummary, vbCrLf & vbCrLf & vbCrLf, vbCrLf)
                End While

                Clipboard.Clear()
                Clipboard.SetText(Trim$(xSummary))
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'todo
        ' ''Private Sub imgCopySummary_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
        ' ''    Me.imgCopySummary.BorderStyle = 1
        ' ''End Sub

        ' ''Private Sub imgCopySummary_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
        ' ''    Me.imgCopySummary.BorderStyle = 0
        ' ''End Sub

        Private Sub Form_Activate()
            Try
                Me.tbControlDetails.TabPages(0).Select()

                'Me.lstContacts.SeletedIndex = Me.SelectedContactIndex
                If Me.lstContacts.Items.Count > 0 Then
                    Me.lstContacts.Focus()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
            Try
                If KeyCode = Keys.C And Shift = 2 Then
                    imgCopySummary_Click()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************methods***********************"
        Function lCountChrs(xSource As String, xSearch As String) As Long
            'returns the number of instances
            'of xSearch in xSource

            Dim iPos As Integer
            Dim l As Long

            iPos = InStr(xSource, xSearch)
            While iPos
                l = l + 1
                iPos = InStr(iPos + 1, xSource, xSearch)
            End While
            lCountChrs = l
        End Function

        Public Sub GetSummary(oContact As CContact)
            'gets a summary string for the specified contact
            Dim xToken As String

            Try
                xToken = "{<ADDRESSTYPENAME> Address}"
                Me.lblAddressType.Text = oContact.GetDetail(xToken)

                If Me.lblAddressType.Text = String.Empty Then
                    Me.lblAddressType.Text = vbCrLf
                End If

                Me.txtSummary.Text = Main.GetSummary(oContact)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub GetDetail(oContact As CContact)
            'loads the field values for the specified contact
            Dim xTemp As String
            Dim oCustFld As CCustomField
            Dim i As Integer
            Dim xVal0 As String
            Dim oDetail As DataTable


            Try
                With oContact
                    'name/value array not yet built - do it
                    oDetail = New DataTable
                    ' Add two columns
                    oDetail.Columns.Add("Property")
                    oDetail.Columns.Add("Value")

                    oDetail.Rows.Add("Display Name", .DisplayName)
                    oDetail.Rows.Add("Prefix", .Prefix)
                    oDetail.Rows.Add("First Name", .FirstName)
                    oDetail.Rows.Add("Middle Name", .MiddleName)
                    oDetail.Rows.Add("Last Name", .LastName)
                    oDetail.Rows.Add("Suffix", .Suffix)
                    oDetail.Rows.Add("Full Name", .FullName)
                    oDetail.Rows.Add("Initials", .Initials)
                    oDetail.Rows.Add("Salutation", .Salutation)
                    oDetail.Rows.Add("Title", .Title)
                    oDetail.Rows.Add("GoesBy", .GoesBy)     'GLOG : 15855 : ceh
                    oDetail.Rows.Add("Department", .Department)
                    oDetail.Rows.Add("Company", .Company)
                    oDetail.Rows.Add("Address Type", .AddressTypeName)
                    oDetail.Rows.Add("Street1", .Street1)
                    oDetail.Rows.Add("Street2", .Street2)
                    oDetail.Rows.Add("Street3", .Street3)
                    oDetail.Rows.Add("Additional", .AdditionalInformation)
                    oDetail.Rows.Add("City", .City)
                    oDetail.Rows.Add("State", .State)
                    oDetail.Rows.Add("Zip Code", .ZipCode)
                    oDetail.Rows.Add("Country", .Country)
                    oDetail.Rows.Add("Phone", .Phone)
                    oDetail.Rows.Add("Phone Extension", .PhoneExtension)
                    oDetail.Rows.Add("Fax", .Fax)
                    oDetail.Rows.Add("Email Address", .EMailAddress)

                    For Each oCustFld In .CustomFields
                        i = i + 1
                        oDetail.Rows.Add(oCustFld.Name, oCustFld.Value)
                    Next oCustFld
                End With

                With Me.grdContactDetail
                    .DataSource = oDetail
                    .Refresh()
                    .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                    .Columns(0).Width = 128
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ClearContacts()
            m_oContacts = Nothing
        End Sub
#End Region
    End Class
End Namespace