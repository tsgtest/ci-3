﻿Namespace TSG.CI
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class FilterForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FilterForm))
            Me.fraFilter = New System.Windows.Forms.GroupBox()
            Me.lblFilterField3 = New System.Windows.Forms.Label()
            Me.lblFilterField2 = New System.Windows.Forms.Label()
            Me.lblFilterField1 = New System.Windows.Forms.Label()
            Me.lblFilterField0 = New System.Windows.Forms.Label()
            Me.txtFilterField3 = New System.Windows.Forms.TextBox()
            Me.txtFilterField2 = New System.Windows.Forms.TextBox()
            Me.txtFilterField1 = New System.Windows.Forms.TextBox()
            Me.txtFilterField0 = New System.Windows.Forms.TextBox()
            Me.btnRemove = New System.Windows.Forms.Button()
            Me.lblType = New System.Windows.Forms.Label()
            Me.lblSortBy = New System.Windows.Forms.Label()
            Me.btnSearch = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.cmbSearchType = New CIControls.ComboBox()
            Me.cmbSortBy = New CIControls.ComboBox()
            Me.fraFilter.SuspendLayout()
            Me.SuspendLayout()
            '
            'fraFilter
            '
            Me.fraFilter.Controls.Add(Me.lblFilterField3)
            Me.fraFilter.Controls.Add(Me.lblFilterField2)
            Me.fraFilter.Controls.Add(Me.lblFilterField1)
            Me.fraFilter.Controls.Add(Me.lblFilterField0)
            Me.fraFilter.Controls.Add(Me.txtFilterField3)
            Me.fraFilter.Controls.Add(Me.txtFilterField2)
            Me.fraFilter.Controls.Add(Me.txtFilterField1)
            Me.fraFilter.Controls.Add(Me.txtFilterField0)
            Me.fraFilter.Location = New System.Drawing.Point(21, 20)
            Me.fraFilter.Name = "fraFilter"
            Me.fraFilter.Size = New System.Drawing.Size(297, 201)
            Me.fraFilter.TabIndex = 0
            Me.fraFilter.TabStop = False
            Me.fraFilter.Text = "Display Contacts Where:"
            '
            'lblFilterField3
            '
            Me.lblFilterField3.AutoSize = True
            Me.lblFilterField3.Location = New System.Drawing.Point(14, 145)
            Me.lblFilterField3.Name = "lblFilterField3"
            Me.lblFilterField3.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField3.TabIndex = 6
            Me.lblFilterField3.Text = "##"
            '
            'lblFilterField2
            '
            Me.lblFilterField2.AutoSize = True
            Me.lblFilterField2.Location = New System.Drawing.Point(14, 103)
            Me.lblFilterField2.Name = "lblFilterField2"
            Me.lblFilterField2.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField2.TabIndex = 4
            Me.lblFilterField2.Text = "##"
            '
            'lblFilterField1
            '
            Me.lblFilterField1.AutoSize = True
            Me.lblFilterField1.Location = New System.Drawing.Point(14, 60)
            Me.lblFilterField1.Name = "lblFilterField1"
            Me.lblFilterField1.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField1.TabIndex = 2
            Me.lblFilterField1.Text = "##"
            '
            'lblFilterField0
            '
            Me.lblFilterField0.AutoSize = True
            Me.lblFilterField0.Location = New System.Drawing.Point(14, 21)
            Me.lblFilterField0.Name = "lblFilterField0"
            Me.lblFilterField0.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField0.TabIndex = 0
            Me.lblFilterField0.Text = "##"
            '
            'txtFilterField3
            '
            Me.txtFilterField3.Location = New System.Drawing.Point(14, 162)
            Me.txtFilterField3.Name = "txtFilterField3"
            Me.txtFilterField3.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField3.TabIndex = 7
            '
            'txtFilterField2
            '
            Me.txtFilterField2.Location = New System.Drawing.Point(14, 120)
            Me.txtFilterField2.Name = "txtFilterField2"
            Me.txtFilterField2.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField2.TabIndex = 5
            '
            'txtFilterField1
            '
            Me.txtFilterField1.Location = New System.Drawing.Point(14, 77)
            Me.txtFilterField1.Name = "txtFilterField1"
            Me.txtFilterField1.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField1.TabIndex = 3
            '
            'txtFilterField0
            '
            Me.txtFilterField0.Location = New System.Drawing.Point(14, 38)
            Me.txtFilterField0.Name = "txtFilterField0"
            Me.txtFilterField0.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField0.TabIndex = 1
            '
            'btnRemove
            '
            Me.btnRemove.Location = New System.Drawing.Point(337, 92)
            Me.btnRemove.Name = "btnRemove"
            Me.btnRemove.Size = New System.Drawing.Size(82, 25)
            Me.btnRemove.TabIndex = 1
            Me.btnRemove.Text = "Cl&ear Search"
            Me.btnRemove.UseVisualStyleBackColor = True
            '
            'lblType
            '
            Me.lblType.AutoSize = True
            Me.lblType.Location = New System.Drawing.Point(36, 243)
            Me.lblType.Name = "lblType"
            Me.lblType.Size = New System.Drawing.Size(75, 13)
            Me.lblType.TabIndex = 2
            Me.lblType.Text = "F&ind Contacts:"
            '
            'lblSortBy
            '
            Me.lblSortBy.AutoSize = True
            Me.lblSortBy.Location = New System.Drawing.Point(36, 288)
            Me.lblSortBy.Name = "lblSortBy"
            Me.lblSortBy.Size = New System.Drawing.Size(44, 13)
            Me.lblSortBy.TabIndex = 4
            Me.lblSortBy.Text = "&Sort By:"
            '
            'btnSearch
            '
            Me.btnSearch.Location = New System.Drawing.Point(337, 270)
            Me.btnSearch.Name = "btnSearch"
            Me.btnSearch.Size = New System.Drawing.Size(82, 25)
            Me.btnSearch.TabIndex = 6
            Me.btnSearch.Text = "O&K"
            Me.btnSearch.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.btnCancel.Location = New System.Drawing.Point(337, 301)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(82, 25)
            Me.btnCancel.TabIndex = 7
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'cmbSearchType
            '
            Me.cmbSearchType.AllowEmptyValue = False
            Me.cmbSearchType.AutoSize = True
            Me.cmbSearchType.Borderless = False
            Me.cmbSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cmbSearchType.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cmbSearchType.IsDirty = False
            Me.cmbSearchType.LimitToList = True
            Me.cmbSearchType.Location = New System.Drawing.Point(35, 258)
            Me.cmbSearchType.Margin = New System.Windows.Forms.Padding(2)
            Me.cmbSearchType.MaxDropDownItems = 8
            Me.cmbSearchType.Name = "cmbSearchType"
            Me.cmbSearchType.SelectedIndex = -1
            Me.cmbSearchType.SelectedValue = Nothing
            Me.cmbSearchType.SelectionLength = 0
            Me.cmbSearchType.SelectionStart = 0
            Me.cmbSearchType.Size = New System.Drawing.Size(260, 23)
            Me.cmbSearchType.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cmbSearchType.SupportingValues = ""
            Me.cmbSearchType.TabIndex = 3
            Me.cmbSearchType.Tag2 = Nothing
            Me.cmbSearchType.Value = ""
            '
            'cmbSortBy
            '
            Me.cmbSortBy.AllowEmptyValue = False
            Me.cmbSortBy.AutoSize = True
            Me.cmbSortBy.Borderless = False
            Me.cmbSortBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cmbSortBy.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cmbSortBy.IsDirty = False
            Me.cmbSortBy.LimitToList = True
            Me.cmbSortBy.Location = New System.Drawing.Point(35, 303)
            Me.cmbSortBy.Margin = New System.Windows.Forms.Padding(2)
            Me.cmbSortBy.MaxDropDownItems = 8
            Me.cmbSortBy.Name = "cmbSortBy"
            Me.cmbSortBy.SelectedIndex = -1
            Me.cmbSortBy.SelectedValue = Nothing
            Me.cmbSortBy.SelectionLength = 0
            Me.cmbSortBy.SelectionStart = 0
            Me.cmbSortBy.Size = New System.Drawing.Size(261, 23)
            Me.cmbSortBy.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cmbSortBy.SupportingValues = ""
            Me.cmbSortBy.TabIndex = 5
            Me.cmbSortBy.Tag2 = Nothing
            Me.cmbSortBy.Value = ""
            '
            'FilterForm
            '
            Me.AcceptButton = Me.btnSearch
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.AutoSize = True
            Me.CancelButton = Me.btnCancel
            Me.ClientSize = New System.Drawing.Size(435, 347)
            Me.Controls.Add(Me.cmbSearchType)
            Me.Controls.Add(Me.cmbSortBy)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnSearch)
            Me.Controls.Add(Me.lblSortBy)
            Me.Controls.Add(Me.lblType)
            Me.Controls.Add(Me.btnRemove)
            Me.Controls.Add(Me.fraFilter)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "FilterForm"
            Me.ShowIcon = False
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Find"
            Me.fraFilter.ResumeLayout(False)
            Me.fraFilter.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents fraFilter As System.Windows.Forms.GroupBox
        Friend WithEvents txtFilterField3 As System.Windows.Forms.TextBox
        Friend WithEvents txtFilterField2 As System.Windows.Forms.TextBox
        Friend WithEvents txtFilterField1 As System.Windows.Forms.TextBox
        Friend WithEvents txtFilterField0 As System.Windows.Forms.TextBox
        Friend WithEvents lblFilterField0 As System.Windows.Forms.Label
        Friend WithEvents btnRemove As System.Windows.Forms.Button
        Friend WithEvents lblType As System.Windows.Forms.Label
        Friend WithEvents lblSortBy As System.Windows.Forms.Label
        Friend WithEvents btnSearch As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents lblFilterField1 As System.Windows.Forms.Label
        Friend WithEvents lblFilterField3 As System.Windows.Forms.Label
        Friend WithEvents lblFilterField2 As System.Windows.Forms.Label
        Friend WithEvents cmbSortBy As CIControls.ComboBox
        Friend WithEvents cmbSearchType As CIControls.ComboBox
    End Class
End Namespace
