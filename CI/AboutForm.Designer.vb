﻿Namespace TSG.CI
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AboutForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AboutForm))
            Me.lblCompatibility1 = New System.Windows.Forms.Label()
            Me.lblVersion = New System.Windows.Forms.Label()
            Me.lblCopyright = New System.Windows.Forms.Label()
            Me.lblAppName1 = New System.Windows.Forms.Label()
            Me.lblAppName2 = New System.Windows.Forms.Label()
            Me.pictureBox3 = New System.Windows.Forms.PictureBox()
            Me.pctTSGLogo = New System.Windows.Forms.PictureBox()
            Me.pctCFLogo = New System.Windows.Forms.PictureBox()
            Me.pctClose = New System.Windows.Forms.PictureBox()
            Me.pictureBox1 = New System.Windows.Forms.PictureBox()
            Me.pictureBox2 = New System.Windows.Forms.PictureBox()
            Me.pctCopy = New System.Windows.Forms.PictureBox()
            CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pctTSGLogo, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pctCFLogo, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pctClose, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lblCompatibility1
            '
            Me.lblCompatibility1.BackColor = System.Drawing.Color.White
            Me.lblCompatibility1.Cursor = System.Windows.Forms.Cursors.Default
            Me.lblCompatibility1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblCompatibility1.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lblCompatibility1.Location = New System.Drawing.Point(247, 194)
            Me.lblCompatibility1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblCompatibility1.Name = "lblCompatibility1"
            Me.lblCompatibility1.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.lblCompatibility1.Size = New System.Drawing.Size(297, 72)
            Me.lblCompatibility1.TabIndex = 20
            Me.lblCompatibility1.Text = "Compatible with " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Office 2016/365 ProPlus (32-bit and 64-bit) and " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Office 2013/3" & _
        "65 ProPlus and 2010 (32-bit)"
            Me.lblCompatibility1.TextAlign = System.Drawing.ContentAlignment.TopCenter
            '
            'lblVersion
            '
            Me.lblVersion.BackColor = System.Drawing.Color.White
            Me.lblVersion.Cursor = System.Windows.Forms.Cursors.Default
            Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lblVersion.Location = New System.Drawing.Point(272, 134)
            Me.lblVersion.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblVersion.Name = "lblVersion"
            Me.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.lblVersion.Size = New System.Drawing.Size(247, 28)
            Me.lblVersion.TabIndex = 15
            Me.lblVersion.Text = "Version automatic"
            Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter
            '
            'lblCopyright
            '
            Me.lblCopyright.BackColor = System.Drawing.Color.White
            Me.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default
            Me.lblCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lblCopyright.Location = New System.Drawing.Point(272, 167)
            Me.lblCopyright.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblCopyright.Name = "lblCopyright"
            Me.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.lblCopyright.Size = New System.Drawing.Size(247, 18)
            Me.lblCopyright.TabIndex = 13
            Me.lblCopyright.Text = "©1990 - 2017"
            Me.lblCopyright.TextAlign = System.Drawing.ContentAlignment.TopCenter
            '
            'lblAppName1
            '
            Me.lblAppName1.BackColor = System.Drawing.Color.White
            Me.lblAppName1.Cursor = System.Windows.Forms.Cursors.Default
            Me.lblAppName1.Font = New System.Drawing.Font("Microsoft YaHei", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblAppName1.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lblAppName1.Location = New System.Drawing.Point(272, 19)
            Me.lblAppName1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblAppName1.Name = "lblAppName1"
            Me.lblAppName1.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.lblAppName1.Size = New System.Drawing.Size(247, 55)
            Me.lblAppName1.TabIndex = 14
            Me.lblAppName1.Text = "Contact"
            Me.lblAppName1.TextAlign = System.Drawing.ContentAlignment.TopCenter
            '
            'lblAppName2
            '
            Me.lblAppName2.BackColor = System.Drawing.Color.White
            Me.lblAppName2.Cursor = System.Windows.Forms.Cursors.Default
            Me.lblAppName2.Font = New System.Drawing.Font("Microsoft YaHei", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblAppName2.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lblAppName2.Location = New System.Drawing.Point(243, 64)
            Me.lblAppName2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblAppName2.Name = "lblAppName2"
            Me.lblAppName2.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.lblAppName2.Size = New System.Drawing.Size(305, 59)
            Me.lblAppName2.TabIndex = 19
            Me.lblAppName2.Text = "Integration"
            Me.lblAppName2.TextAlign = System.Drawing.ContentAlignment.TopCenter
            '
            'pictureBox3
            '
            Me.pictureBox3.Image = Global.My.Resources.Resources.tsg_inc
            Me.pictureBox3.Location = New System.Drawing.Point(8, 255)
            Me.pictureBox3.Name = "pictureBox3"
            Me.pictureBox3.Size = New System.Drawing.Size(241, 43)
            Me.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            Me.pictureBox3.TabIndex = 53
            Me.pictureBox3.TabStop = False
            '
            'pctTSGLogo
            '
            Me.pctTSGLogo.BackColor = System.Drawing.SystemColors.Window
            Me.pctTSGLogo.Cursor = System.Windows.Forms.Cursors.Default
            Me.pctTSGLogo.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.pctTSGLogo.ForeColor = System.Drawing.SystemColors.WindowText
            Me.pctTSGLogo.Image = CType(resources.GetObject("pctTSGLogo.Image"), System.Drawing.Image)
            Me.pctTSGLogo.Location = New System.Drawing.Point(8, 11)
            Me.pctTSGLogo.Margin = New System.Windows.Forms.Padding(2)
            Me.pctTSGLogo.Name = "pctTSGLogo"
            Me.pctTSGLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.pctTSGLogo.Size = New System.Drawing.Size(238, 238)
            Me.pctTSGLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
            Me.pctTSGLogo.TabIndex = 25
            Me.pctTSGLogo.TabStop = False
            '
            'pctCFLogo
            '
            Me.pctCFLogo.BackColor = System.Drawing.SystemColors.Window
            Me.pctCFLogo.Cursor = System.Windows.Forms.Cursors.Default
            Me.pctCFLogo.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.pctCFLogo.ForeColor = System.Drawing.SystemColors.WindowText
            Me.pctCFLogo.Image = CType(resources.GetObject("pctCFLogo.Image"), System.Drawing.Image)
            Me.pctCFLogo.Location = New System.Drawing.Point(8, 10)
            Me.pctCFLogo.Margin = New System.Windows.Forms.Padding(2)
            Me.pctCFLogo.Name = "pctCFLogo"
            Me.pctCFLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.pctCFLogo.Size = New System.Drawing.Size(241, 240)
            Me.pctCFLogo.TabIndex = 23
            Me.pctCFLogo.TabStop = False
            '
            'pctClose
            '
            Me.pctClose.BackColor = System.Drawing.SystemColors.Window
            Me.pctClose.Cursor = System.Windows.Forms.Cursors.Default
            Me.pctClose.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.pctClose.ForeColor = System.Drawing.SystemColors.WindowText
            Me.pctClose.Image = CType(resources.GetObject("pctClose.Image"), System.Drawing.Image)
            Me.pctClose.Location = New System.Drawing.Point(524, 11)
            Me.pctClose.Margin = New System.Windows.Forms.Padding(2)
            Me.pctClose.Name = "pctClose"
            Me.pctClose.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.pctClose.Size = New System.Drawing.Size(18, 19)
            Me.pctClose.TabIndex = 18
            Me.pctClose.TabStop = False
            '
            'pictureBox1
            '
            Me.pictureBox1.Image = Global.My.Resources.Resources.TSG_Tagline
            Me.pictureBox1.Location = New System.Drawing.Point(8, 287)
            Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New System.Drawing.Size(274, 26)
            Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            Me.pictureBox1.TabIndex = 54
            Me.pictureBox1.TabStop = False
            '
            'pictureBox2
            '
            Me.pictureBox2.Image = Global.My.Resources.Resources.tsg_logo_smallest2
            Me.pictureBox2.Location = New System.Drawing.Point(246, 247)
            Me.pictureBox2.Name = "pictureBox2"
            Me.pictureBox2.Size = New System.Drawing.Size(38, 28)
            Me.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            Me.pictureBox2.TabIndex = 55
            Me.pictureBox2.TabStop = False
            '
            'pctCopy
            '
            Me.pctCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.pctCopy.Image = CType(resources.GetObject("pctCopy.Image"), System.Drawing.Image)
            Me.pctCopy.Location = New System.Drawing.Point(500, 11)
            Me.pctCopy.Name = "pctCopy"
            Me.pctCopy.Size = New System.Drawing.Size(19, 20)
            Me.pctCopy.TabIndex = 56
            Me.pctCopy.TabStop = False
            '
            'AboutForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.BackColor = System.Drawing.SystemColors.Window
            Me.ClientSize = New System.Drawing.Size(553, 325)
            Me.ControlBox = False
            Me.Controls.Add(Me.pctCopy)
            Me.Controls.Add(Me.pctTSGLogo)
            Me.Controls.Add(Me.lblAppName1)
            Me.Controls.Add(Me.lblAppName2)
            Me.Controls.Add(Me.pictureBox2)
            Me.Controls.Add(Me.pictureBox1)
            Me.Controls.Add(Me.pictureBox3)
            Me.Controls.Add(Me.pctCFLogo)
            Me.Controls.Add(Me.pctClose)
            Me.Controls.Add(Me.lblCompatibility1)
            Me.Controls.Add(Me.lblVersion)
            Me.Controls.Add(Me.lblCopyright)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Margin = New System.Windows.Forms.Padding(2)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "AboutForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pctTSGLogo, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pctCFLogo, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pctClose, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Public WithEvents pctCFLogo As System.Windows.Forms.PictureBox
        Public WithEvents pctClose As System.Windows.Forms.PictureBox
        Public WithEvents lblCompatibility1 As System.Windows.Forms.Label
        Public WithEvents lblVersion As System.Windows.Forms.Label
        Public WithEvents lblCopyright As System.Windows.Forms.Label
        Public WithEvents lblAppName1 As System.Windows.Forms.Label
        Public WithEvents lblAppName2 As System.Windows.Forms.Label
        Public WithEvents pctTSGLogo As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox3 As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox1 As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox2 As System.Windows.Forms.PictureBox
        Private WithEvents pctCopy As System.Windows.Forms.PictureBox
    End Class
End Namespace
