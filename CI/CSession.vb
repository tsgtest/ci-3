'**********************************************************
'   CSession Class
'   created 01/06/01 by Daniel Fisherman
'   Contains properties and methods defining a
'   CI session
'**********************************************************
Option Explicit On

Imports TSG.CI
Imports TSG.CI.CConstants
Imports LMP

Namespace TSG.CI
    Public Class CSession
        Implements ICSession
#Region "******************fields***********************"
        ' ''Private m_oForm As CI.frmCI
        Private m_oForm As CI
        Private m_bInitialized As Boolean
        Private m_sFormLeft As Single
        Private m_sFormTop As Single
        Private m_xLabelTo As String
        Private m_xLabelFrom As String
        Private m_xLabelCC As String
        Private m_xLabelBCC As String
        Private m_xFormCaption As String
#End Region
#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            Try
                Main.GetBackends()

                General.SetConnectionProvider()
                'set this once a session
                General.g_bAlertNoContactNumbers = True
                General.LoadPrefixSuffixOptions()

                m_bInitialized = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

#End Region
#Region "******************properties***********************"
        Public ReadOnly Property Initialized() As Boolean Implements ICSession.Initialized
            Get
                Initialized = m_bInitialized
            End Get
        End Property

        Public Property LabelTo() As String
            Get
                LabelTo = m_xLabelTo
            End Get
            Set(xNew As String)
                m_xLabelTo = xNew
            End Set
        End Property

        Public Property LabelFrom() As String
            Get
                LabelFrom = m_xLabelFrom
            End Get
            Set(xNew As String)
                m_xLabelFrom = xNew
            End Set
        End Property

        Public Property LabelCC() As String
            Get
                LabelCC = m_xLabelCC
            End Get
            Set(xNew As String)
                m_xLabelCC = xNew
            End Set
        End Property

        Public Property LabelBCC() As String
            Get
                LabelBCC = m_xLabelBCC
            End Get
            Set(xNew As String)
                m_xLabelBCC = xNew
            End Set
        End Property

        Public Property FormLeft() As Single Implements ICSession.FormLeft
            Get
                FormLeft = m_sFormLeft
            End Get
            Set(sNew As Single)
                m_sFormLeft = sNew
            End Set
        End Property

        Public Property FormTop() As Single Implements ICSession.FormTop
            Get
                FormTop = m_sFormTop
            End Get
            Set(sNew As Single)
                m_sFormTop = sNew
            End Set
        End Property

        Public Property FormCaption() As String
            Get
                FormCaption = m_xFormCaption
            End Get
            Set(xNew As String)
                m_xFormCaption = xNew
            End Set
        End Property

        Public Property SessionType() As ciSessionType Implements ICSession.SessionType
            Get
                SessionType = Main.g_oSessionType.SessionType
            End Get
            Set(iNew As ciSessionType)
                Main.g_oSessionType.SessionType = iNew
            End Set
        End Property
#End Region
#Region "******************methods***********************"
        Public Function GetSummary(oContact As CContact, Optional ByVal xFormatTokenPhrase As String = "") As String
            'returns a default summary of the supplied contact
            GetSummary = GetSummary(oContact, xFormatTokenPhrase)
        End Function

        Public Sub ShowContactDetail(ByVal xContactUNID As String)
            'displays the detail of the contact with the specified UNID
            Dim oUNID As CUNID
            Dim oForm As ListingDetailForm
            Dim oContact As CContact
            Dim xName As String

            oUNID = New CUNID
            oForm = New ListingDetailForm

            oContact = Me.GetContactFromUNID(xContactUNID, ciRetrieveData.ciRetrieveData_Names, ciAlerts.ciAlert_None)
            If Not oContact Is Nothing Then
                xName = oContact.DisplayName
            End If


            Try
                oForm.Listing = oUNID.GetListing(xContactUNID, xName)
            Catch
            End Try

            oForm.AddressIndex = 0
            oForm.ShowDialog()

            oForm = Nothing
        End Sub

        Public Function Backends() As CBackends
            Try
                Backends = Backends
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetContactFromUNID(ByVal xUNID As String, _
                                           ByVal iIncludeData As ciRetrieveData,
                                           ByVal iAlerts As ciAlerts) As ICContact Implements ICSession.GetContactFromUNID
            'returns the contact having the specified UNID
            Dim oB As ICIBackend
            Dim oUNID As CUNID

            'GLOG : 15781 : ceh
            oUNID = New CUNID
            oB = Main.GetBackendFromUNID(xUNID)

            If Not oB Is Nothing Then
                Dim oCs As CContacts

                oCs = oB.GetContacts(oUNID.GetListing(xUNID), _
                                     oUNID.GetAddress(xUNID), vbNull, iIncludeData, iAlerts)

                If Not (oCs Is Nothing) Then
                    If oCs.Count > 0 Then
                        GetContactFromUNID = oCs.Item(1)
                    Else
                        Throw New Exception("No contact with UNID '" & xUNID & "' was found.")
                    End If
                ElseIf iAlerts = ciAlerts.ciAlert_None Then
                    Throw New Exception("No contact with UNID '" & xUNID & "' was found.")
                End If
            Else
                'no backend with specified ID exists - alert
                Dim xID As String
                xID = CUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Backend)
                Throw New Exception("No Backend found with ID '" & xID & "'")
            End If
        End Function

        Public Function GetFilter(ByVal iBackendID As Integer) As CFilter
            'returns the collection of filter fields for specified backend
            Try
                GetFilter = GetFilter(iBackendID)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub CreateDataFile(ByVal xFile As String)
            'creates a merge data file from selected contacts
            Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
            Dim oContacts As CContacts

            Try
                oContacts = GetContacts(ciRetrieveData.ciRetrieveData_All, ciRetrieveData.ciRetrieveData_None, _
                    ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None, , , ciAlerts.ciAlert_All)

                oContacts.CreateDataFile(xFile)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub CreateDataFileOLD(ByVal xFile As String)
            'creates a merge data file from selected contacts
            ' ''            Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
            ' ''            Dim iFile As Integer
            ' ''            Dim xHeader As String
            ' ''            Dim oCustFlds As CCustomFields
            ' ''            Dim i As Integer
            ' ''            Dim j As Integer
            ' ''            Dim xRecord As String
            ' ''            Dim oContacts As CContacts

            ' ''            On Error GoTo ProcError

            ' ''            oContacts = GetContacts(ciRetrieveData.ciRetrieveData_All, ciRetrieveData.ciRetrieveData_None, _
            ' ''                ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None, , , ciAlert_All)

            ' ''            If oContacts Is Nothing Then
            ' ''                Exit Sub
            ' ''            End If

            ' ''            'get file channel
            ' ''            iFile = FreeFile()

            ' ''            'create file
            ' ''    Open xFile For Output As #iFile

            ' ''            'create header
            ' ''            xHeader = ciDataFileHeader

            ' ''            'get custom fields - we can use the first backend since each backend has the
            ' ''            'same custom fields (although some fields may not be linked to a data field)
            ' ''            oCustFlds = oContacts.Item(1).CustomFields
            ' ''            For j = 1 To oCustFlds.Count
            ' ''                xHeader = xHeader & "|" & oCustFlds(j).Name
            ' ''            Next j

            ' ''    Print #iFile, xHeader

            ' ''            'cycle through contacts, writing to file
            ' ''            For i = 1 To oContacts.Count
            ' ''                With oContacts.Item(i)
            ' ''                    xRecord = """" & .AddressTypeName & """" & _
            ' ''                                "|" & .ContactType & _
            ' ''                                "|""" & .DisplayName & """" & _
            ' ''                                "|""" & .Prefix & """" & _
            ' ''                                "|""" & .FirstName & """" & _
            ' ''                                "|""" & .MiddleName & """" & _
            ' ''                                "|""" & .LastName & """" & _
            ' ''                                "|""" & .Suffix & """" & _
            ' ''                                "|""" & .Initials & """" & _
            ' ''                                "|""" & .Street1 & """" & _
            ' ''                                "|""" & .Street2 & """" & _
            ' ''                                "|""" & .Street3 & """" & _
            ' ''                                "|""" & .City & """" & _
            ' ''                                "|""" & .State & """" & _
            ' ''                                "|""" & .ZipCode & """" & _
            ' ''                                "|""" & .Country & """" & _
            ' ''                                "|""" & .Company & """" & _
            ' ''                                "|""" & .Department & """" & _
            ' ''                                "|""" & .Title & """" & _
            ' ''                                "|""" & .EMailAddress & """" & _
            ' ''                                "|""" & .Phone & """" & _
            ' ''                                "|""" & .PhoneExtension & """" & _
            ' ''                                "|""" & .Fax & """"

            ' ''                    For j = 1 To .CustomFields.Count
            ' ''                        xRecord = xRecord & "|""" & .CustomFields(j).Value & """"
            ' ''                    Next j

            ' ''                    xRecord = Replace(xRecord, vbCrLf, "<1310>")
            ' ''                    xRecord = Replace(xRecord, vbCr, "<13>")
            ' ''                    xRecord = Replace(xRecord, Chr$(11), "<11>")

            ' ''            Print #iFile, xRecord
            ' ''                End With
            ' ''            Next i

            ' ''            'close file
            ' ''    Close #iFile
            ' ''            Exit Sub
            ' ''ProcError:
            ' ''             Main.g_oError.RaiseError("CI.CSession.CreateDataFile")
            ' ''            Exit Sub
        End Sub

        'v2.4.4007
        Public Function GetContactsEx(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal DefaultSelectionList As ICI.ciSelectionlists = ICI.ciSelectionlists.ciSelectionList_To, _
            Optional ByVal MaxContacts As Integer = 0, _
            Optional ByVal Alerts As ciAlerts = ciAlerts.ciAlert_All, _
            Optional ByVal xLabelTo As String = "", _
            Optional ByVal xLabelFrom As String = "", _
            Optional ByVal xLabelCC As String = "", _
            Optional ByVal xLabelBCC As String = "", _
            Optional ByVal lMaxTo As Long = 0, _
            Optional ByVal lMaxFrom As Long = 0, _
            Optional ByVal lMaxCC As Long = 0, _
            Optional ByVal lMaxBCC As Long = 0) As ICContacts Implements ICSession.GetContactsEx

            Dim oContacts As ICContacts

            Try
                Main.g_oSessionType.SessionType = ciSessionType.ciSession_CI

                If m_oForm Is Nothing Then
                    m_oForm = New CI
                End If

                With m_oForm
                    '.Width = 574 * Functions.GetScalingFactor()
                    .ClearContacts()
                    .MaxContacts = MaxContacts
                    .LabelTo = IIf(xLabelTo <> "", IIf(Main.bHasHotKey(xLabelTo), xLabelTo, "&" & xLabelTo), .LabelTo)
                    .LabelFrom = IIf(xLabelFrom <> "", IIf(Main.bHasHotKey(xLabelFrom), xLabelFrom, "&" & xLabelFrom), .LabelFrom)
                    .LabelCC = IIf(xLabelCC <> "", IIf(Main.bHasHotKey(xLabelCC), xLabelCC, "&" & xLabelCC), .LabelCC)
                    .LabelBCC = IIf(xLabelBCC <> "", IIf(Main.bHasHotKey(xLabelBCC), xLabelBCC, "&" & xLabelBCC), .LabelBCC)

                    'v2.4.007
                    .MaxTo = lMaxTo
                    .MaxFrom = lMaxFrom
                    .MaxCC = lMaxCC
                    .MaxBCC = lMaxBCC

                    'GLOG : 15752 : ceh
                    Main.MoveToLastPosition(m_oForm, Me.FormTop, Me.FormLeft)

                    Main.g_iDefaultMaxContacts = MaxContacts

                    Dim iSelLists As ICI.ciSelectionlists

                    If RetrieveDataForTo <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_To
                    End If

                    If RetrieveDataForFrom <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_From
                    End If

                    If RetrieveDataForCC <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_CC
                    End If

                    If RetrieveDataForBCC <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_BCC
                    End If

                    .SelectionLists = iSelLists
                    .SelectionList = DefaultSelectionList
                    .DataRetrievedForTo = RetrieveDataForTo
                    .DataRetrievedForFrom = RetrieveDataForFrom
                    .DataRetrievedForCC = RetrieveDataForCC
                    .DataRetrievedForBCC = RetrieveDataForBCC
                    .Alerts = Alerts

                    .Enabled = True
                    .ShowDialog()
                    If Not .Cancelled Then
                        oContacts = .SelectedContacts
                        Me.FormLeft = .Left
                        Me.FormTop = .Top
                        GetContactsEx = oContacts
                        oContacts = Nothing
                    Else
                        oContacts = New CContacts
                        GetContactsEx = oContacts
                        oContacts = Nothing
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetContacts(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal DefaultSelectionList As ICI.ciSelectionlists = ICI.ciSelectionlists.ciSelectionList_To, _
            Optional ByVal MaxContacts As Integer = 0, _
            Optional ByVal Alerts As ciAlerts = ciAlerts.ciAlert_All) As CContacts

            Dim iSelLists As ICI.ciSelectionlists

            Try
                Main.g_oSessionType.SessionType = ciSessionType.ciSession_CI

                If m_oForm Is Nothing Then
                    m_oForm = New CI
                End If

                Dim oContacts As CContacts
                With m_oForm
                    .ClearContacts()
                    .MaxContacts = MaxContacts

                    'GLOG : 15752 : ceh
                    Main.MoveToLastPosition(m_oForm, Me.FormTop, Me.FormLeft)

                    Main.g_iDefaultMaxContacts = MaxContacts

                    .LabelTo = IIf(Me.LabelTo <> "", Me.LabelTo, .LabelTo)
                    .LabelFrom = IIf(Me.LabelFrom <> "", Me.LabelFrom, .LabelFrom)
                    .LabelCC = IIf(Me.LabelCC <> "", Me.LabelCC, .LabelCC)
                    .LabelBCC = IIf(Me.LabelBCC <> "", Me.LabelBCC, .LabelBCC)


                    If RetrieveDataForTo <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_To
                    End If

                    If RetrieveDataForFrom <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_From
                    End If

                    If RetrieveDataForCC <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_CC
                    End If

                    If RetrieveDataForBCC <> ciRetrieveData.ciRetrieveData_None Then
                        iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_BCC
                    End If

                    .SelectionLists = iSelLists
                    .SelectionList = DefaultSelectionList
                    .DataRetrievedForTo = RetrieveDataForTo
                    .DataRetrievedForFrom = RetrieveDataForFrom
                    .DataRetrievedForCC = RetrieveDataForCC
                    .DataRetrievedForBCC = RetrieveDataForBCC
                    .Alerts = Alerts

                    .Enabled = True

                    .ShowDialog()

                    If Not .Cancelled Then
                        oContacts = .SelectedContacts
                        Me.FormLeft = .Left
                        Me.FormTop = .Top
                        GetContacts = oContacts
                        oContacts = Nothing
                    Else
                        oContacts = New CContacts
                        GetContacts = oContacts
                        oContacts = Nothing
                    End If

                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetContacts_Custom(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
            Optional ByVal DefaultSelectionList As ICI.ciSelectionlists = ICI.ciSelectionlists.ciSelectionList_To, _
            Optional ByVal MaxContacts As Integer = 0, _
            Optional ByVal Alerts As ciAlerts = ciAlerts.ciAlert_All, _
            Optional ByVal oDataTable As DataTable = Nothing) As ICContacts Implements ICSession.GetContacts_Custom

            Dim iSelLists As ICI.ciSelectionlists
            Dim i As Integer
            Dim j As Integer
            Dim oC As ICContact

            Try
                If m_oForm Is Nothing Then
                    m_oForm = New CI
                End If

                With m_oForm
                    With m_oForm
                        '.Width = 650 * Functions.GetScalingFactor()
                        .ClearContacts()
                        .MaxContacts = MaxContacts

                        'GLOG : 15752 : ceh
                        Main.MoveToLastPosition(m_oForm, Me.FormTop, Me.FormLeft)

                        Main.g_iDefaultMaxContacts = MaxContacts

                        If Not (oDataTable Is Nothing) Then

                            For i = 0 To oDataTable.Rows.Count - 1
                                'check for UNID value
                                If oDataTable.Rows(i)(4).ToString() <> String.Empty Then
                                    'add as reuse contact


                                    Try
                                        .SelectedContacts.Add(GetContactFromUNID(oDataTable.Rows(i)(4).ToString(),
                                                                                 oDataTable.Rows(i)(1).ToString(), ciAlerts.ciAlert_None))
                                    Catch ex As Exception
                                        Err.Clear()
                                        GoTo skip
                                    End Try

                                    oC = .SelectedContacts(.SelectedContacts.Count)

                                    With oC
                                        .Tag = i + 1
                                        .CustomTypeName = oDataTable.Rows(i)(0).ToString()
                                    End With
                                End If
skip:
                            Next i
                            .EntityList = oDataTable
                            .SelectionLists = ICI.ciSelectionlists.ciSelectionList_Custom
                            .Alerts = Alerts

                        Else

                            If RetrieveDataForTo <> ciRetrieveData.ciRetrieveData_None Then
                                iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_To
                            End If

                            If RetrieveDataForFrom <> ciRetrieveData.ciRetrieveData_None Then
                                iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_From
                            End If

                            If RetrieveDataForCC <> ciRetrieveData.ciRetrieveData_None Then
                                iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_CC
                            End If

                            If RetrieveDataForBCC <> ciRetrieveData.ciRetrieveData_None Then
                                iSelLists = iSelLists + ICI.ciSelectionlists.ciSelectionList_BCC
                            End If

                            .SelectionLists = iSelLists
                            .SelectionList = DefaultSelectionList
                            .DataRetrievedForTo = RetrieveDataForTo
                            .DataRetrievedForFrom = RetrieveDataForFrom
                            .DataRetrievedForCC = RetrieveDataForCC
                            .DataRetrievedForBCC = RetrieveDataForBCC
                            .Alerts = Alerts

                        End If
                    End With
                    .Text = " " & Main.g_oSessionType.AppTitle
                    .ShowDialog()
                    If Not .Cancelled Then
                        GetContacts_Custom = .SelectedContacts
                        Me.FormTop = .Top
                        Me.FormLeft = .Left
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function


        Public Function GetContactsFromArray(ByRef oArray As System.Array, _
                                             Optional xSep As String = "|") As ICContacts Implements ICSession.GetContactsFromArray
            Dim oDT As DataTable

            Try
                'turn oArray into xArrayDB
                oDT = Main.xarStringToDataTable(oArray)

                'call custom function
                GetContactsFromArray = GetContacts_Custom(oDataTable:=oDT)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace
