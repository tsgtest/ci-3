﻿Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class FilterForm
#Region "******************fields***********************"
        Private m_bCancelled As Boolean
        Private m_bSearchNow As Boolean
        Private m_iSortCol As Integer
        Private xMsg As String
        Private m_oBackend As ICIBackend

        'control arrays
        Public txtFilterField(3) As TextBox
        Dim lblFilterField(3) As Label
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()

            'initialize control arrays
            txtFilterField(0) = Me.txtFilterField0
            txtFilterField(1) = Me.txtFilterField1
            txtFilterField(2) = Me.txtFilterField2
            txtFilterField(3) = Me.txtFilterField3

            lblFilterField(0) = Me.lblFilterField0
            lblFilterField(1) = Me.lblFilterField1
            lblFilterField(2) = Me.lblFilterField2
            lblFilterField(3) = Me.lblFilterField3

        End Sub
#End Region
#Region "******************properties***********************"
        Public WriteOnly Property Backend()
            Set(oNew)
                m_oBackend = oNew
            End Set
        End Property

        Public ReadOnly Property Cancelled() As Boolean
            Get
                Cancelled = m_bCancelled
            End Get
        End Property

        Public Property Sortcolumn() As ciListingCols
            Get
                Sortcolumn = Me.cmbSortBy.SelectedIndex
            End Get
            Set(iNew As ciListingCols)
                m_iSortCol = iNew
            End Set
        End Property
#End Region
#Region "******************events***********************"
        Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
            Try
                For Each Ctrl As TextBox In Me.fraFilter.Controls.OfType(Of TextBox)()
                    Ctrl.Text = ""
                Next
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub txtFilterField_Click(sender As Object, e As EventArgs) _
            Handles txtFilterField0.Click, txtFilterField1.Click, txtFilterField2.Click, txtFilterField3.Click


            Try
                Dim txt As TextBox = DirectCast(sender, TextBox)

                txt.SelectAll()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub FilterForm_Load(sender As Object, e As EventArgs) Handles Me.Load
            Dim i As Integer
            Dim iDisplay As Integer
            Dim iNumFields As Integer
            Dim oFilter As CFilter
            Dim dAdjustment As Double
            Dim dtSortType As DataTable

            Try
                m_bCancelled = True
                With m_oBackend.Filter
                    iNumFields = .CountFields

                    'show only those controls corresponding
                    'to existing filter fields
                    For i = 0 To iNumFields - 1
                        Me.lblFilterField(i).Text = "&" & .FilterFields(i).Name & ":"
                        Me.lblFilterField(i).Visible = i + 1 <= iNumFields
                        Me.txtFilterField(i).Visible = i + 1 <= iNumFields
                    Next

                    With Me.fraFilter
                        'get amount to move up controls - we'll have to move up controls
                        'if not all 4 of the filter fields are being used
                        dAdjustment = (((4 - iNumFields) * (.Height / 4)) + _
                                  Math.Min((4 - iNumFields), 1) * 50)

                        .Height = .Height - dAdjustment
                    End With

                    'adjust vertical position of all controls
                    Me.lblSortBy.Top = Me.lblSortBy.Top - dAdjustment
                    Me.lblType.Top = Me.lblType.Top - dAdjustment
                    Me.cmbSortBy.Top = Me.cmbSortBy.Top - dAdjustment
                    Me.cmbSearchType.Top = Me.cmbSearchType.Top - dAdjustment
                    Me.btnCancel.Top = Me.btnCancel.Top - dAdjustment
                    Me.btnSearch.Top = Me.btnSearch.Top - dAdjustment
                    Me.Height = Me.Height - dAdjustment

                    'load sort columns
                    With Me.cmbSortBy
                        For i = 0 To 3
                            .Items.Add(Main.g_vCols(i, 0))
                        Next i
                    End With

                    'select previous sort column
                    If m_iSortCol = -1 Then
                        m_iSortCol = ciListingCols.ciListingCols_DisplayName
                    End If

                    Me.cmbSortBy.SelectedIndex = m_iSortCol

                    'load sort type
                    dtSortType = New DataTable

                    With dtSortType
                        'define columns
                        .Columns.Add("Display", GetType(String))
                        .Columns.Add("Operator", GetType(Integer))

                        'add rows
                        .Rows.Add("That begin with...", ciSearchOperators.ciSearchOperator_BeginsWith)
                        .Rows.Add("That contain...", ciSearchOperators.ciSearchOperator_Contains)
                        .Rows.Add("That have the value...", ciSearchOperators.ciSearchOperator_Equals)
                    End With

                    With Me.cmbSearchType
                        .SetList(dtSortType)
                        .SelectedIndex = 0
                    End With

                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub FilterForm_Activated(sender As Object, e As EventArgs) Handles Me.Activated
            Dim i As Integer

            Try
                With Me.txtFilterField0
                    .Focus()
                    .SelectAll()
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
            Try
                Me.Hide()
                m_bCancelled = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnSearch_Click1(sender As Object, e As EventArgs) Handles btnSearch.Click
            Try
                m_bCancelled = False
                Me.Hide()
                Application.DoEvents()
            Catch
            End Try
        End Sub

#End Region
#Region "******************methods***********************"
#End Region
    End Class
End Namespace

