'**********************************************************
'   ICIBackends CollectionClass
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci backends
'**********************************************************
Option Explicit On

Imports System.Runtime.Remoting
Imports System.Reflection
Imports System.Collections
Imports LMP

Namespace TSG.CI
    Public Class CBackends
        Inherits CollectionBase
        'Implements IEnumerable
#Region "******************fields***********************"
        Private m_oCol As Collection
        Private m_oGroups As ICIBackend
#End Region
#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            m_oCol = New Collection
        End Sub
#End Region
#Region "******************properties***********************"
        'Public ReadOnly Property Item(ByVal index As Integer) As ICIBackend
        '    Get
        '        Return CType(List.Item(index), ICIBackend)
        '    End Get
        'End Property

        'Public Function GetEnumerator() As IEnumerator
        '    GetEnumerator = m_oCol.GetEnumerator
        'End Function
#End Region
#Region "******************methods***********************"
        Public Function ProgID(ByVal iBackendID As Integer) As String
            Dim xProgID As String
            Dim xDesc As String

            Try
                'get from ini
                xProgID = Main.g_oIni.GetIni("Backend" & iBackendID,
                                             "ProgID",
                                             Main.g_oIni.CIIni)

                'raise error if ini value is missing
                If Len(xProgID) = 0 Then
                    xDesc = "Invalid Backend" & iBackendID & "\ProgID key in ci.ini."
                    Throw New Exception(xDesc)
                End If

                ProgID = xProgID
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function

        Public Function AssemblyName(ByVal iBackendID As Integer) As String
            Dim xAssemblyName As String
            Dim xDesc As String

            Try
                'get from ini
                xAssemblyName = Main.g_oIni.GetIni("Backend" & iBackendID,
                                             "AssemblyName",
                                             Main.g_oIni.CIIni)

                'raise error if ini value is missing
                If Len(xAssemblyName) = 0 Then
                    xDesc = "Invalid Backend" & iBackendID & "\AssemblyName key in ci.ini."
                    Throw New Exception(xDesc)
                End If

                AssemblyName = xAssemblyName
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function

        Friend Function Add(ByVal iID As Integer) As ICIBackend
            'creates a new backend with id = iID, then
            'adds it to the collection.
            'returns the new backend
            Dim xClassName As String
            Dim xFullTypeName As String
            Dim xAsmName As String = ""
            Dim xAsmDllFullName As String = ""
            Dim xDesc As String
            Dim oB As ICIBackend
            Dim xPrompt As String
            Dim oObj As Object
            Dim oType As Type
            Dim oAsm As Assembly


            Try
                'get ini required ini values

                Try
                    xClassName = Me.ProgID(iID)
                    xAsmName = Me.AssemblyName(iID)
                Catch
                End Try

                'get full path to assembly
                xAsmDllFullName = Functions.AppPath + xAsmName

                'load assembly
                Try
                    oAsm = Assembly.LoadFrom(xAsmDllFullName)
                Catch
                End Try

                If oAsm Is Nothing Then
                    Throw New Exception("Could not add backend " & iID + 1 & ".  " & _
                            "Could find " & xAsmDllFullName)
                    Add = Nothing
                    Exit Function
                End If

                'get type
                xFullTypeName = Me.GetType().Namespace.ToString() & "." & xClassName
                oType = oAsm.GetType(xFullTypeName, True)

                If oType Is Nothing Then
                    Throw New Exception("Could not add backend " & iID & ".  " & _
                            "Could not create object with ProgID '" & xClassName & "'.")
                    Add = Nothing
                    Exit Function
                End If

                'get instance from type & cast to ICIBackend object
                Try
                    oB = DirectCast(Activator.CreateInstance(oType), ICIBackend)
                Catch
                End Try


                xPrompt = Main.g_oIni.GetIni("CIApplication",
                                             "PromptForNonExistingBackend",
                                             Main.g_oIni.CIIni)

                If oB Is Nothing Then
                    'GLOG : 5502 : ceh
                    If UCase(xPrompt) = "TRUE" Then
                        Throw New Exception("Could not add backend " & iID & ".  " & _
                            "Could not create object with ProgID '" & xClassName & "'.")
                    End If
                    Add = Nothing
                    Exit Function
                End If

                If xClassName = "CGroups" Then
                    'tag this as the "Groups" backend
                    m_oGroups = oB
                End If

                'initialize
                oB.Initialize(iID)

                'add to collection
                Err.Clear()


                Try
                    If oB.Exists Then
                        m_oCol.Add(oB, CStr(oB.InternalID))
                    Else
                        If UCase(xPrompt) = "TRUE" Then
                            MsgBox("You have not been given access to '" & oB.Name & "' for MacPac Contact Integration." & _
                                    "  Please contact your MacPac administrator if you have any questions.", _
                                    vbInformation, Main.g_oSessionType.AppTitle)
                        End If
                        Add = Nothing
                        Exit Function
                    End If
                Catch ex As Exception
                    'could not add to collection - alert and exit
                    xDesc = "Could not add backend " & iID & " to collection of backends: " & _
                        vbCrLf & vbCrLf & Err.Description.ToString()
                    MsgBox(xDesc, MsgBoxStyle.Exclamation)
                    Add = Nothing
                    Exit Function
                End Try


                'GLOG 2 : 5699 : CEH
                ''GLOG: 8724 : ceh
                'If Err.Number Then
                '    'could not add to collection - alert and exit
                '    xDesc = "Could not add backend " & iID & " to collection of backends: " & _
                '        vbCrLf & vbCrLf & Err.Description.ToString()
                '    MsgBox(xDesc, MsgBoxStyle.Exclamation)
                '    Add = Nothing
                '    Exit Function
                'Else
                '    On Error GoTo ProcError
                '    Add = oB
                'End If

                Add = oB

            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function ItemFromID(ByVal iID As Integer) As ICIBackend
            'will return Nothing if an error is generated
            Try
                ItemFromID = m_oCol.Item(CStr(iID))
            Catch
            End Try
        End Function

        Public Function ItemFromIndex(ByVal iIndex As Integer) As ICIBackend
            'will return Nothing if an error is generated
            Try
                ItemFromIndex = m_oCol.Item(iIndex)
            Catch
            End Try
        End Function

        Public Function GroupsBackend() As ICIBackend
            'returns the "Groups" backend
            GroupsBackend = m_oGroups
        End Function

        Public Function Count() As Integer
            Count = m_oCol.Count
        End Function

#End Region
    End Class

End Namespace
