Option Explicit On

Imports LMP

Namespace TSG.CI
    Friend Class Main
#Region "******************fields***********************"
        Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Integer) As Short

        Declare Function GetPrivateProfileString Lib "kernel32" Alias _
            "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
            ByVal lpKeyName As Object, ByVal lpDefault As String, ByVal _
            lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
            As String) As Long

        Declare Function WritePrivateProfileString Lib "kernel32" Alias _
            "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
            ByVal lpKeyName As Object, ByVal lpString As Object, ByVal lpFileName As _
            String) As Long

        Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
            ByVal lpBuffer As String, nSize As Long) As Long

        Private Declare Sub OutputDebugString Lib "kernel32" _
          Alias "OutputDebugStringA" _
          (ByVal lpOutputString As String)

        Declare Function ShellExecuteA Lib "shell32.dll" ( _
           ByVal hWnd As IntPtr, _
           ByVal lpOperation As String, _
           ByVal lpFile As String, _
           ByVal lpParameters As String, _
           ByVal lpDirectory As String, _
           ByVal nShowCmd As Integer) As IntPtr

        Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" _
            (lpVersionInformation As OSVERSIONINFO) As Long

        Structure OSVERSIONINFO
            Public OSVSize As Long
            Public dwVerMajor As Long
            Public dwVerMinor As Long
            Public dwBuildNumber As Long
            Public PlatformID As Long
            'Private szCSDVersion As String * 128
            'todo?
            <VBFixedString(258), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=258)> Public szCSDVersion As String
        End Structure

        Private Const SW_SHOWNORMAL = 1

        Public Const KEY_SHIFT = &H10
        Public Const KEY_CTL = &H11
        Public Const KEY_MENU = &H12
        Public Const KEY_F1 = &H70
        Public Const KEY_F5 = &H74
        Public Const KEY_a = 65
        Public Const KEY_DEL = 46
        Public Const KEY_ENTER = 13

        'HTML help
        Public Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" _
                                (ByVal hwndCaller As Long, ByVal pszFile As String, _
                                ByVal uCommand As Long, ByVal dwData As Long) As Long

        Public Const HH_DISPLAY_TOPIC = &H0         ' select last opened tab,[display a specified topic]
        Public Const HH_DISPLAY_TOC = &H1           ' select contents tab, [display a specified topic]
        Public Const HH_DISPLAY_INDEX = &H2         ' select index tab and searches for a keyword
        Public Const HH_DISPLAY_SEARCH = &H3        ' select search tab and perform a search


        Public Shared g_oError As CError = New CError
        Public Shared g_oSessionType As CSessionType = New CSessionType
        Public Shared g_oIni As CIni = New CIni
        Public Shared g_vCols(3, 1) As Object
        Public Shared m_oBackends As CBackends
        Public Shared g_oGlobals As CGlobals = New CGlobals
        Public Shared g_oConstants As CConstants = New CConstants
        Public Shared g_oBackend As Object
        Public Shared g_iDefaultMaxContacts As Integer
        Public Shared g_xHelpPath As String
        Public Shared g_xHelpFiles() As String
        Public Shared g_iDefaultFilter As Integer

        Public Const g_xUnvalidAddress As String = "A valid Address type does not exist.  Please contact your administrator."
#End Region
#Region "******************initializer***********************"
        Shared Sub New()
            'remove old debug
            Try
                Kill(g_oIni.ApplicationDirectory & "\ciDebug.log")
            Catch
            End Try
        End Sub
#End Region
#Region "******************methods***********************"
        'GLOG : 15731 : ceh
        Public Shared Sub SelectDataGridViewRecord(ByVal dgv As DataGridView,
                                     ByVal columnName As String,
                                     ByVal columnPosition As Integer,
                                     ByVal letterTyped As Char)
            'Selects first row matching char typed
            Dim dt As DataTable = dgv.DataSource
            Dim letter As Char = letterTyped
            Dim dv As DataView = New DataView(dt)
            Dim hasCount As Boolean = False

            If dgv.RowCount Then
                dv.Sort = columnName
                dv.RowFilter = columnName & " like '" & letter & "%'"
                If dv.Count > 0 Then
                    hasCount = True
                    Dim x As String = dv(0)(columnPosition).ToString()
                    Dim bs As New BindingSource
                    bs.DataSource = dt
                    dgv.BindingContext(bs).Position = bs.Find(columnName, x)
                    dgv.CurrentCell = dgv(0, bs.Position)
                End If
            End If
        End Sub

        Public Shared Function GetSummary(oContact As CContact, Optional ByVal xFormatTokenPhrase As String = "") As String
            'gets a summary string for the specified contact
            Dim xToken As String

            If xFormatTokenPhrase <> String.Empty Then
                GetSummary = oContact.GetDetail(xFormatTokenPhrase)
            Else
                xToken = "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf

                Dim xType As String
                xType = UCase$(oContact.AddressTypeName)
                If InStr(xType, "BUSINESS") Or InStr(xType, "OFFICE") Or InStr(xType, "MAIN") Then
                    xToken = xToken & "{<TITLE>}" & vbCrLf & "{<COMPANY>}" & vbCrLf
                End If

                xToken = xToken & "{<STREET1>}" & vbCrLf & _
                        "{<STREET2>}" & vbCrLf & _
                        "{<STREET3>}" & vbCrLf & _
                        "{<ADDITIONALINFORMATION>}" & vbCrLf & _
                        "{<CITY>{, }<STATE>}{  <ZIPCODE>}"
                GetSummary = oContact.GetDetail(xToken) & _
                            vbCrLf & vbCrLf & vbCrLf

                xToken = "Phone:  {<PHONE>}{  x<PHONEEXTENSION>}" & vbCrLf & _
                         "Fax:  {<FAX>}" & vbCrLf & _
                         "EMail:  {<EMAILADDRESS>}"

                GetSummary = GetSummary & oContact.GetDetail(xToken)

            End If
        End Function

        Public Shared Function GetFilter(ByVal iBackendID As Integer) As CFilter
            'returns the collection of filter fields for specified backend
            GetFilter = Main.GetBackendFromID(iBackendID).Filter()
        End Function

        Public Shared Function Backends() As CBackends
            If m_oBackends Is Nothing Then
                GetBackends()
            End If
            Backends = m_oBackends
        End Function

        Public Shared Sub GetBackends()
            Dim i As Integer = 1
            Dim bExists As Boolean

            m_oBackends = New CBackends

            'cycle through backends in ini,
            'creating a backend for each one
            Do
                'check to see if there's an ini
                'section for a backend with this index
                'todo
                bExists = Main.g_oIni.SectionExists( _
                    "Backend" & i, Main.g_oIni.CIIni)
                If bExists Then
                    'backend exists - get id of backend
                    Dim iID As Integer
                    m_oBackends.Add(i)
                    i = i + 1
                End If
            Loop While bExists

            If m_oBackends.Count = 0 Then
                Throw New Exception("Could not connect to any contact sources.")
            End If
        End Sub

        Public Shared Function GetBackendFromUNID(ByVal xUNID As String) As ICIBackend
            Dim oUNID As CUNID
            Dim iID As Integer

            'trim preceding 'b' - this is done for the backend tree node keys
            If Left$(xUNID, 1) = "b" Then
                xUNID = Mid$(xUNID, 2)
            End If

            oUNID = New CUNID
            iID = oUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Backend)
            GetBackendFromUNID = Backends.ItemFromID(iID)
        End Function

        Public Shared Function GetBackendFromID(ByVal iID As Integer) As ICIBackend
            GetBackendFromID = Backends.ItemFromID(iID)
        End Function

        Public Shared Function GetBackendFromIndex(ByVal iIndex As Integer) As ICIBackend
            GetBackendFromIndex = Backends.ItemFromIndex(iIndex)
        End Function

        Public Shared Function IsPressed(Key As Object) As Boolean
            IsPressed = (GetKeyState(Key) < 0)
        End Function

        Public Shared Sub ResizeTDBCombo(tdbCBX As ComboBox, Optional iMaxRows As Integer = 0)
            'resizes the dropdown of the true db combo
            'to display the specified number of rows.
            Dim oArr As DataTable
            Dim iRows As Integer

            With tdbCBX
                oArr = .DataSource
                If oArr Is Nothing Then
                    Throw New Exception("No XArray has been assigned to " & tdbCBX.Name & ".")
                End If

                If oArr.Rows.Count < iMaxRows Then
                    iRows = oArr.Rows.Count
                Else
                    iRows = iMaxRows
                End If

                On Error Resume Next
                'todo
                ' ''.DropDownHeight = ((iRows) * .RowHeight + 8)

            End With
        End Sub

        Public Shared Function lCountChrs(xSource As String, xSearch As String) As Long
            'returns the number of instances
            'of xSearch in xSource

            Dim iPos As Integer
            Dim l As Long

            iPos = InStr(xSource, xSearch)
            While iPos
                l = l + 1
                iPos = InStr(iPos + 1, xSource, xSearch)
            End While
            lCountChrs = l
        End Function

        Public Shared Function ciMin(i As Double, j As Double) As Double
            If i > j Then
                ciMin = j
            Else
                ciMin = i
            End If
        End Function
        Public Shared Function ciMax(i As Double, j As Double) As Double
            If i > j Then
                ciMax = i
            Else
                ciMax = j
            End If
        End Function

        Public Shared Function GetUserIni(ByVal xSection As String, ByVal xKey As String)
            'returns the value for the specified key in the User.ini
            Static xUserIni As String
            Dim bUserIniExists As Boolean

            On Error Resume Next

            If xUserIni = String.Empty Then
                xUserIni = Main.g_oIni.CIUserIni
            End If
            GetUserIni = Main.g_oIni.GetIni(xSection, xKey, xUserIni)
        End Function

        Public Shared Sub SetUserIni(ByVal xSection As String, _
                               ByVal xKey As String, _
                               ByVal xValue As String)
            'sets the value of the specified key in User.ini
            Static xUserIni As String

            If xUserIni = String.Empty Then
                xUserIni = Main.g_oIni.CIUserIni
            End If

            Main.g_oIni.SetIni(xSection, xKey, xValue, xUserIni)
        End Sub

        Public Shared Sub Pause(lSecs As Long)
            'pauses the code for specified duration
            Dim datStart As Date
            Dim datEnd As Date

            datStart = DateTime.Now()
            datEnd = datStart + DateTime.FromOADate(lSecs / 86200)

            While DateTime.Now() <= datEnd
                Application.DoEvents()
            End While
        End Sub

        Public Shared Sub CorrectTDBComboMismatch(oTDBCombo As ComboBox, iReposition As Integer)
            'resets the tdb combo value to the previous match -
            'this procedure should be called in the
            'TDBCombo Mismatch even procedure only
            'todo
            ' ''Dim bytStart As Byte

            ' ''iReposition = False

            ' ''With oTDBCombo
            ' ''           get current selection start
            ' ''    bytStart = .SelStart

            ' ''           reset the text to the current list text
            ' ''    If .ListField = String.Empty Then
            ' ''        .Text = .Columns(0)
            ' ''    Else
            ' ''        .Text = .Columns(.ListField)
            ' ''    End If

            ' ''           return selection to original selection
            ' ''    .SelStart = Max(CDbl(bytStart - 1), 0)
            ' ''    .SelLength = Len(.Text)
            ' ''End With
        End Sub

        Public Shared Function Max(i As Double, j As Double) As Double
            If i > j Then
                Max = i
            Else
                Max = j
            End If
        End Function

        Public Shared Function bHasHotKey(xString As String) As Boolean
            If xString <> "" Then
                bHasHotKey = CBool(InStr(xString, "&"))
            End If
        End Function

        Public Shared Function Min(i As Double, j As Double) As Double
            If i > j Then
                Min = j
            Else
                Min = i
            End If
        End Function

        Public Shared Function bValidateBoundTDBCombo(oTDBCombo As ComboBox) As Boolean
            'returns FALSE if invalid author has been specified, else TRUE
            With oTDBCombo
                If .Text = "" Then
                    MsgBox("Invalid entry.  Please select an item " & _
                           "from the list.", vbExclamation, Main.g_oSessionType.AppTitle)
                Else
                    'set text of control equal to the
                    'display text of the selected row
                    '.Text = .Columns(.ListField)
                    .Text = .SelectedText
                    bValidateBoundTDBCombo = True
                End If
            End With
        End Function

        Public Shared Function ListingBackend(oListing As CListing) As ICIBackend
            'returns the backend of the listing
            ListingBackend = Backends.ItemFromID(oListing.BackendID)
        End Function

        Public Shared Sub QuitSession()
            Main.g_oError = Nothing
            g_oIni = Nothing
            m_oBackends = Nothing
            g_oConstants = Nothing
            g_oSessionType = Nothing
            g_oBackend = Nothing
        End Sub

        Public Shared Function xLocalizeNumericString(ByVal xString As String) As String
            'replaces decimal separator with the current one set for the OS
            Const mpThisFunction As String = "CI.Main.xLocalizeNumericString"
            Dim xTemp As String
            Dim xDecimal As String

            xTemp = xString

            If xTemp = String.Empty Then
                Exit Function
            End If

            'get whether decimal sep is a comma or period
            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)

            If xDecimal = "." Then
                'localize to period
                xLocalizeNumericString = Replace(xTemp, ",", ".")
            Else
                'localize to comma
                xLocalizeNumericString = Replace(xTemp, ".", ",")
            End If
        End Function


        Public Shared Function xarStringToDataTable(ByRef xString As System.Array) As DataTable
            Dim xDT As DataTable

            xDT = New DataTable

            'xDT.LoadRows(xString(), False)

            xarStringToDataTable = xDT
        End Function

        Public Shared Sub MoveToLastPosition(oForm As Object, _
                                               sT As Single, _
                                               sL As Single)
            'positions dialog to last set position-
            'ensures dlg is on the screen
            Dim lXPix As Long
            Dim lYPix As Long
            Dim sW As Single
            Dim sH As Single

            'move dialog to last position
            On Error Resume Next

            With oForm
                'get screen size in pixels
                lYPix = Screen.PrimaryScreen.Bounds.Size.Height
                lXPix = Screen.PrimaryScreen.Bounds.Size.Width

                'get form size
                sW = .Width
                sH = .Height

                If sT + sL <> 0 Then
                    'place form - ensure that entire form is on screen
                    .Left = Min(CDbl(sL), lXPix - sW)
                    .Top = Min(CDbl(sT), lYPix - sH)
                Else
                    'place form - ensure that entire form is on screen
                    .Left = (lXPix / 2) - (sW / 2)
                    .Top = (lYPix / 2) - (sH / 2)
                End If

            End With

        End Sub

        Public Shared Sub DebugPrint(xOutput As String)
            Debug.Print(xOutput)
            OutputDebugString(xOutput)
        End Sub

        Public Shared Sub LaunchDocumentByExtension(xFileName As String)
            Dim iRet As Integer

            Try
                iRet = ShellExecuteA(0, "open", xFileName, "", "", SW_SHOWNORMAL)
                If iRet <= 32 Then
                    MsgBox("The document could not be opened.  " & _
                        "There may not be an installed application associated with this extension.", vbExclamation)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'temporary
        Public Shared Function bIsQuickFilterBackend(iInternalID As Integer) As Boolean
            If g_iDefaultFilter = 1 Then    'force text filtering
                bIsQuickFilterBackend = False
            Else
                Select Case iInternalID
                    Case 100, 104, 111      'Outlook OM, MP9, MP10
                        bIsQuickFilterBackend = True
                    Case Else
                        bIsQuickFilterBackend = False
                End Select
            End If
        End Function

        'Returns the version of Windows that the user is running
        Public Shared Function GetWindowsVersion() As String
            Dim osVer As Version

            osVer = Environment.OSVersion.Version

            GetWindowsVersion = osVer.Major & "." & osVer.Minor

        End Function
#End Region
    End Class
End Namespace
