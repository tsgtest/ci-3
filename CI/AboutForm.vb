﻿Option Explicit On

Imports System.Reflection
Imports LMP

Namespace TSG.CI
    Public Class AboutForm
#Region "**************declarations***************"
        Private m_xMP90Ver As String
#End Region
#Region "**************fields***************"
        Public WriteOnly Property MacPac90Version() As String
            Set(ByVal Value As String)
                m_xMP90Ver = Value
            End Set
        End Property

        Public ReadOnly Property AssemblyVersion() As String
            Get
                AssemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString()
            End Get
        End Property
#End Region
#Region "**************events*************"
        Private Sub AboutForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Try
                If Main.g_oSessionType.SessionType = ciSessionType.ciSession_Connect Then
                    Me.pctCFLogo.Visible = True
                    'Me.pctTSGLogo_small.Visible = True
                    Me.pctTSGLogo.Visible = False
                    Me.lblAppName1.Text = "Connect"
                    Me.lblAppName2.Text = "Forms"
                Else
                    Me.pctCFLogo.Visible = False
                    'Me.pctTSGLogo_small.Visible = False
                    Me.pctTSGLogo.Visible = True
                End If

                Try
                    Me.lblVersion.Text = "Version " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision
                    Me.lblVersion.Text = System.String.Format("Version {0}", AssemblyVersion)
                Catch
                End Try
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctClose_Click(sender As Object, e As EventArgs) Handles pctClose.Click
            Try
                Me.Close()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctTSGLogo_Click(sender As Object, e As EventArgs) Handles pctTSGLogo.Click
            Try
                MsgBox("ProgramDirectory Dir - " & My.Application.Info.DirectoryPath)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub AboutForm_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
            Try
                If e.KeyCode = Keys.Escape Then
                    Me.Close()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        'GLOG : 8757 : ceh
        Private Sub pctCopy_Click(sender As Object, e As EventArgs) Handles pctCopy.Click
            Try
                'copy to clipboard
                Clipboard.SetDataObject(Me.lblVersion.Text, True)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctCopy_MouseDown(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseDown
            Try
                'show borders
                pctCopy.BorderStyle = BorderStyle.FixedSingle
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub pctCopy_MouseUp(sender As Object, e As MouseEventArgs) Handles pctCopy.MouseUp
            Try
                'Show borders
                pctCopy.BorderStyle = BorderStyle.None
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

#End Region
    End Class
End Namespace
