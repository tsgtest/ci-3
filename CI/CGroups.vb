Option Explicit On

Imports LMP

Namespace TSG.CI
    Friend Class CGroups
        Implements ICIBackend
#Region "******************fields***********************"
        Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
                        ByVal lpBuffer As String, nSize As Long) As Long

        Private Const ciGroupsInternalID As Integer = 99

        Private m_iID As Integer
        Private m_bIsConnected As Boolean
        Private m_bExists As Boolean
        Private m_oCnn As ADODB.Connection
        Private m_oFilter As CFilter
        Private m_oEvents As CEventGenerator
        Private m_oError As CError
        Private m_iSortCol As Integer
#End Region
#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            m_oEvents = Main.g_oGlobals.CIEvents()
            m_oError = New CError
        End Sub
#End Region
#Region "******************properties***********************"
        Public ReadOnly Property ID() As Integer
            Get
                ID = m_iID
            End Get
        End Property
#End Region
#Region "******************methods***********************"
        Private Function GetContacts(oListing As CListing, _
                                     oAddress As CAddress, _
                                     Optional ByVal vAddressType As Object = vbNull, _
                                     Optional ByVal iIncludeData As ciRetrieveData = 1&, _
                                     Optional ByVal iAlerts As ciAlerts = 1&) As CContacts
            'returns the contact pointed to by the specified listing and address
            Dim oContacts As CContacts
            Dim oRS As ADODB.Recordset
            Dim xSQL As String
            Dim oNativeAddress As CAddress
            Dim oNativeListing As CListing
            Dim oUNID As CUNID

            Try
                oNativeAddress = Nothing
                oContacts = New CContacts
                oUNID = New CUNID

                'get unid for the group listing
                xSQL = "SELECT fldUNID FROM tblContactGroupMembers WHERE fldID =" & oListing.ID

                oRS = New ADODB.Recordset

                With oRS
                    .Open(xSQL, m_oCnn, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)
                    If Not (.BOF And .EOF) Then
                        'get unid for listing
                        oListing.UNID = .Fields("fldUNID").Value

                        'create the unid of the listing and address
                        'as it exists in its native application
                        Try
                            oNativeAddress = oUNID.GetAddress(.Fields("fldUNID").Value, oAddress.Name)
                        Catch
                        End Try

                        oNativeListing = oUNID.GetListing(.Fields("fldUNID").Value, oListing.DisplayName)

                        oContacts = Main.ListingBackend(oListing).GetContacts(oNativeListing, oNativeAddress, vAddressType, iIncludeData, iAlerts)
                    Else
                        Throw New Exception("No UNID found for groups listing with ID=" & oListing.ID.ToString())
                    End If
                End With
                GetContacts = oContacts
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function PromptForFilter() As CFilter
            'prompts user for filter/sort conditions
            Dim oForm As FilterForm
            Dim oBackend As ICIBackend
            Dim i As Integer

            Try
                oForm = New FilterForm

                With oForm
                    .Backend = Me
                    .Sortcolumn = ICIBackend_DefaultSortColumn()

                    'show filter form
                    .ShowDialog()

                    If Not .Cancelled Then
                        oBackend = Me

                        'set sort column
                        ICIBackend_DefaultSortColumn = .cmbSortBy.SelectedIndex

                        With oBackend.Filter
                            For i = 0 To .CountFields - 1
                                'set filter field values
                                .FilterFields(i).Value = oForm.txtFilterField(i).Text
                                .FilterFields(i).SearchOperator = oForm.cmbSearchType.SelectedValue
                            Next i
                            .SortColumn = ICIBackend_DefaultSortColumn
                        End With

                        'return filter
                        PromptForFilter = oBackend.Filter
                    End If
                    .Close()
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try

            '                Exit Function
            'ProcError:
            '                Dim lErr As Long
            '                Dim xDesc As String

            '                lErr = Err.Number
            '                xDesc = Err.Description

            '                'todo
            '                'Unload oForm

            '                Err.Number = lErr
            '                Err.Description = xDesc
            '                Main.g_oError.RaiseError("CI.CGroups.PromptForFilter")
            '                Exit Function
        End Function
        Private Function GetFilterSort(oFilter As CFilter) As String
            'sets filter for supplied messages collection
            Dim i As Integer
            Dim xFilterSort As String
            Dim xVal As String

            Try
                For i = 1 To 4
                    With oFilter.FilterFields(i - 1)
                        If .ID <> String.Empty And .Value <> String.Empty Then

                            Try
                                'modify value with wildcards if specified
                                If .SearchOperator = ciSearchOperators.ciSearchOperator_BeginsWith Then
                                    xVal = .Value & "%"
                                    xFilterSort = xFilterSort & _
                                        " AND " & .ID & " LIKE '" & xVal & "'"
                                ElseIf .SearchOperator = ciSearchOperators.ciSearchOperator_Contains Then
                                    xVal = "%" & .Value & "%"
                                    xFilterSort = xFilterSort & _
                                        " AND " & .ID & " LIKE '" & xVal & "'"
                                Else
                                    xVal = .Value
                                    xFilterSort = xFilterSort & _
                                        " AND " & .ID & "= """ & xVal & """ "
                                End If
                            Catch
                            End Try
                        End If
                    End With
                Next i

                If oFilter.SortColumn <> 0 Then
                    'sort descending by specified field
                    xFilterSort = xFilterSort & " ORDER BY " & _
                          Main.g_vCols(oFilter.SortColumn, 1)
                Else
                    If ICIBackend_DefaultSortColumn <> 0 Then
                        xFilterSort = xFilterSort & " ORDER BY " & _
                              Main.g_vCols(ICIBackend_DefaultSortColumn(), 1)
                    Else
                        xFilterSort = xFilterSort & " ORDER BY fldDisplayName"
                    End If
                End If

                GetFilterSort = xFilterSort
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function

        Public Sub AddGroup(ByVal xName As String)
            Dim iRecs As Integer
            Try
                If Not ICIBackend_IsConnected Then
                    Connect()
                End If

                m_oCnn.Execute("INSERT INTO tblContactGroups (fldName) VALUES ('" & xName & "')", iRecs)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub DeleteGroup(ByVal lID As Long)
            Try
                With m_oCnn
                    'delete both the members of the group and the group itself
                    .Execute("DELETE * FROM tblContactGroupMembers WHERE fldGroup =" & lID)
                    .Execute("DELETE * FROM tblContactGroups WHERE fldID =" & lID)
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function AddMember(oContact As CContact, ByVal fldGroupID As Long) As Long
            'adds contact as member of specified group - returns the ID of member record
            Dim xSQL As String
            Dim lRecs As Long
            Dim oRS As ADODB.Recordset

            Try
                With oContact
                    xSQL = "INSERT INTO tblContactGroupMembers(fldGroup,fldUNID,fldDisplayName," & _
                        "fldCompany,fldLastName,fldFirstName,fldTitle,fldAddressType,fldAddressTypeName) VALUES(" & _
                        fldGroupID & ",""" & .UNID & """,""" & .DisplayName & """,""" & .Company & """,""" & _
                        .LastName & """,""" & .FirstName & """,""" & .Title & """,""" & .AddressTypeID & """,""" & .AddressTypeName & """)"

                    Try
                        m_oCnn.Execute(xSQL, lRecs)
                    Catch
                    End Try

                    If lRecs = 0 Then
                        Throw New Exception("Could not add the " & _
                            "contact with display name '" & .DisplayName & "' to the group.")
                    End If

                    'get ID of added record
                    xSQL = "SELECT TOP 1 fldID FROM tblContactGroupMembers ORDER BY fldID DESC"
                    oRS = New ADODB.Recordset
                    oRS.Open(xSQL, m_oCnn, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    'return ID of newest record
                    AddMember = oRS.Fields(0).Value
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub DeleteMember(ByVal lID As Long)
            Dim xSQL As String
            Dim lNumAffected As Long

            Try
                xSQL = "DELETE FROM tblContactGroupMembers WHERE fldID=" & lID
                m_oCnn.Execute(xSQL, lNumAffected)
                If lNumAffected = 0 Then
                    Throw New Exception("Could not delete the contact with ID=" & lID.ToString())
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Public Function GetMembers(ByVal lGroupID As Long) As CListings
            'returns the collection of listings that match the filter criteria and
            'are stored in the specified folder
            Dim oRS As ADODB.Recordset
            Dim oListings As CListings
            Dim i As Long
            Dim lRecs As Long
            Dim Cancel As Boolean
            Dim xSQL As String
            Dim xFilterSort As String
            Dim oConst As CConstants

            Try
                'get new listings
                oListings = New CListings
                oConst = New CConstants

                xSQL = "SELECT fldDisplayName & '  (' & iif(fldAddressTypeName=''," & _
                    "'No Address',fldAddressTypeName) & ')' as " & _
                    "fldDisplayName,fldID,fldCompany,fldFirstName,fldLastName,fldTitle," & _
                    "fldAddressTypeName,fldAddressType FROM tblContactGroupMembers WHERE fldGroup=" & lGroupID & _
                    " ORDER BY fldDisplayName"

                'get member records
                oRS = m_oCnn.Execute(xSQL, lRecs)

                If Not (oRS.BOF And oRS.EOF) Then
                    'create a new listing for each record returned
                    If Not Cancel Then
                        oRS.MoveFirst()
                        While Not oRS.EOF
                            AddListing(oRS, oListings, ciGroupsInternalID & oConst.UNIDSep & _
                                oConst.UNIDSep & CStr(lGroupID), False)
                            i = i + 1
                            If Cancel Then
                                GetMembers = oListings
                                Exit Function
                            End If
                            oRS.MoveNext()
                        End While
                    End If
                Else
                    'return empty listings collection
                    oListings = New CListings
                End If

                GetMembers = oListings
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Sub CreateFilter()
            'creates an empty filter for backend based on ini definition
            Dim xKey As String
            Dim iPos As Integer
            Dim i As Integer
            Dim lCol As ciListingCols

            Try
                'create empty filter
                m_oFilter = New CFilter

                For i = 1 To 4
                    'get ini key for filter field of this backend
                    xKey = Trim$(Main.g_oIni.GetIni("Backend" & m_iID, "Filter" & i, Main.g_oIni.CIIni))

                    If xKey = String.Empty Then
                        Exit For
                    End If

                    'search for ',' - this delimits name from id in key
                    iPos = InStr(xKey, ",")
                    If iPos = 0 Or iPos = 1 Or iPos = Len(xKey) Then
                        'no delimiter present, no name specified, or no id specified
                        Throw New Exception("Key 'Backend" & m_iID.ToString() & "\FilterFields" & CStr(i) & "' in CI.ini has an invalid value.")
                    End If

                    With m_oFilter.FilterFields(i - 1)
                        'get name, id, operator of filter field
                        .Name = Left(xKey, iPos - 1)
                        .ID = Mid(xKey, iPos + 1)
                        .SearchOperator = ciSearchOperators.ciSearchOperator_Contains And _
                            ciSearchOperators.ciSearchOperator_BeginsWith And ciSearchOperators.ciSearchOperator_Equals
                    End With
                Next

                'get sort column
                Try
                    lCol = CLng(Main.g_oIni.GetIni("CIApplication", "Sort", Main.g_oIni.CIUserIni))
                Catch
                End Try

                If Err.Number > 0 Then
                    lCol = ciListingCols.ciListingCols_DisplayName
                End If

                m_oFilter.SortColumn = lCol
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub Connect()
            'connects to the groups db
            Dim xPath As String
            Dim xAppPath As String = ""
            Dim xUserIni As String = ""

            Try
                'get path to db - if ci.ini is empty see if
                'mpPrivate exists - if so, use it for connection
                xPath = Main.g_oIni.GetIni("Backend" & m_iID, "DB", Main.g_oIni.CIIni)

                If xPath = String.Empty Then
                    xPath = Main.g_oIni.MacPacUserFilesDir & "mpPrivate.mdb"

                    xPath = GetEnvironVarPath(xPath)

                    If Dir(xPath) = String.Empty Then
                        'look in personal path
                        xPath = Main.g_oIni.CIUserFilesDir & "ci.mdb"

                        xPath = GetEnvironVarPath(xPath)

                        If Dir(xPath) = String.Empty Then
                            'use default path
                            xPath = Main.g_oIni.ApplicationDirectory & "ci.mdb"
                            xPath = GetEnvironVarPath(xPath)
                        End If

                    End If
                Else
                    xPath = GetEnvironVarPath(xPath)
                End If

                If Dir(xPath) = String.Empty Then
                    'groups db file is missing
                    Throw New Exception("'" & xPath & "' could not be found.  Please contact your administrator.")
                End If

                'create connection
                m_oCnn = New ADODB.Connection
                m_oCnn.ConnectionString = "Provider=" & General.g_xLocalConnectionProvider & ";" & _
                    "User ID=Admin;Data Source=" & xPath

                'open connection
                m_oCnn.Open()

                'create 'groups' tables if necessary
                CreateTablesIfNecessary()

                m_bIsConnected = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            Dim xToken As String = ""
            Dim iPosStart As Integer
            Dim iPosEnd As Integer
            Dim xValue As String

            Try
                iPosStart = InStr(xPath, "<")
                iPosEnd = InStr(xPath, ">")

                If (iPosStart > 0) And (iPosEnd > 0) Then
                    xToken = Mid$(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)

                    If UCase$(xToken) = "USERNAME" Then
                        'we don't use environ for username because
                        'it's not an environment variable in Windows 98
                        xValue = GetLogonName()
                    Else
                        xValue = Environ(xToken)
                    End If
                End If

                If xValue <> "" Then
                    GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
                Else
                    GetEnvironVarPath = xPath
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetLogonName() As String
            'returns the current system user name
            Dim xBuf As String

            Try
                'get logon name
                xBuf = Space(255)
                GetUserName(xBuf, 255)

                If Len(xBuf) = 0 Then
                    'alert to no logon name
                    Throw New Exception("UserName is empty.  You might not be logged on to the system. " & _
                            "Please log off, and log on again.")
                End If

                'trim extraneous buffer chars
                Trim(xBuf)
                xBuf = RTrim$(xBuf)
                xBuf = Left$(xBuf, Len(xBuf) - 1)

                GetLogonName = xBuf
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Sub CreateTablesIfNecessary()
            'creates the two 'groups' tables if they
            'don't exist in the connected db
            Dim oRS As ADODB.Recordset
            Dim xSQL As String

            Try
                With m_oCnn
                    'test for tblContactGroups
                    oRS = .OpenSchema(ADODB.SchemaEnum.adSchemaTables, _
                                      New [Object]() {Nothing, Nothing, "tblContactGroups"})

                    If oRS.BOF And oRS.EOF Then
                        'table doesn't exist - create it
                        xSQL = "CREATE TABLE tblContactGroups ( " & _
                            "fldID int NOT NULL IDENTITY(1,1), " & _
                            "fldName varchar(50) NOT NULL)"
                        oRS = m_oCnn.Execute(xSQL)
                    End If

                    'test for tblContactGroupMembers
                    oRS = .OpenSchema(ADODB.SchemaEnum.adSchemaTables, _
                                      New [Object]() {Nothing, Nothing, "tblContactGroupMembers"})

                    If oRS.BOF And oRS.EOF Then
                        'table doesn't exist - create it
                        xSQL = "CREATE TABLE tblContactGroupMembers (" & _
                            "fldID int NOT NULL IDENTITY(1,1)," & _
                            "fldGroup int NOT NULL," & _
                            "fldUNID memo NOT NULL, " & _
                            "fldDisplayName varchar(100) NOT NULL," & _
                            "fldCompany varchar(100)," & _
                            "fldFirstName varchar(30)," & _
                            "fldLastName varchar(70)," & _
                            "fldTitle varchar(100)," & _
                            "fldAddressTypeName varchar(30), " & _
                            "fldAddressType varchar(40))"

                        oRS = m_oCnn.Execute(xSQL)
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Function AddListing(oRS As ADODB.Recordset, oListings As CListings, ByVal xFolderUNID As String, ByVal bTrimParens As Boolean)
            Dim oConst As CConstants
            Dim xID As String
            Dim xDisplayName As String
            Dim xProp1 As String
            Dim xProp2 As String
            Dim xProp3 As String
            Dim iPos As Integer

            Try
                With oRS
                    'clear out vars
                    xID = String.Empty
                    xDisplayName = String.Empty
                    xProp1 = String.Empty
                    xProp2 = String.Empty
                    xProp3 = String.Empty

                    Try
                        'add entry to list
                        xDisplayName = .Fields(Main.g_vCols(0, 1)).Value

                        If bTrimParens Then
                            'trim parens from display name

                            'get last (
                            iPos = InStrRev(xDisplayName, "  (")
                            If iPos > 0 Then
                                xDisplayName = Left$(xDisplayName, iPos - 1)
                            End If
                        End If

                        xProp1 = .Fields(Main.g_vCols(1, 1)).Value
                        xProp2 = .Fields(Main.g_vCols(2, 1)).Value
                        xProp3 = .Fields(Main.g_vCols(3, 1)).Value
                    Catch
                    End Try

                    oConst = New CConstants
                    xID = xFolderUNID & oConst.UNIDSep & .Fields("fldID").Value

                    oListings.Add(xID, xDisplayName, xProp1, _
                        xProp2, xProp3, ciListingType.ciListingType_Person)
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

#End Region
#Region "******************ICIBackend***********************"
        Private Function ICIBackend_CustomMenuItem1() As String Implements ICIBackend.CustomMenuItem1
            ICIBackend_CustomMenuItem1 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem2() As String Implements ICIBackend.CustomMenuItem2
            ICIBackend_CustomMenuItem2 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem3() As String Implements ICIBackend.CustomMenuItem3
            ICIBackend_CustomMenuItem3 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem4() As String Implements ICIBackend.CustomMenuItem4
            ICIBackend_CustomMenuItem4 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem5() As String Implements ICIBackend.CustomMenuItem5
            ICIBackend_CustomMenuItem5 = String.Empty
        End Function

        Private Sub ICIBackend_CustomProcedure1() Implements ICIBackend.CustomProcedure1
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure2() Implements ICIBackend.CustomProcedure2
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure3() Implements ICIBackend.CustomProcedure3
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure4() Implements ICIBackend.CustomProcedure4
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure5() Implements ICIBackend.CustomProcedure5
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub


        Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders Implements ICIBackend.GetFolders
            Dim oFolder As CFolder
            Dim oFolders As CFolders
            Dim xSQL As String
            Dim oRS As ADODB.Recordset
            Dim lRecs As Long
            Dim l As Long

            Try
                'create new, empty folders collection
                oFolders = New CFolders

                'connect to db if necessary
                If Not ICIBackend_IsConnected Then
                    Connect()
                End If

                'get contact groups from db
                xSQL = "SELECT fldName, fldID FROM tblContactGroups ORDER by fldName"

                oRS = m_oCnn.Execute(xSQL, lRecs)

                With oRS
                    If Not (.BOF And .EOF) Then
                        'there are groups - cycle through,
                        'adding a folder for each group
                        While Not .EOF
                            oFolder = New CFolder
                            oFolder.UNID = ciGroupsInternalID & Main.g_oConstants.UNIDSep & Main.g_oConstants.UNIDSep & .Fields(1).Value
                            oFolder.Name = .Fields(0).Value
                            oFolders.Add(oFolder)
                            .MoveNext()
                        End While
                    End If
                End With

                'return collection
                ICIBackend_GetFolders = oFolders
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Object) As CContactNumber Implements ICIBackend.GetPhoneNumber

        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'calls ICIBackend_GetPhoneNumbers in backend of specified listing
            Try
                ICIBackend_GetPhoneNumbers = Main.ListingBackend(oListing).GetPhoneNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'calls ICIBackend_GetPhoneNumbers in backend of specified listing
            Try
                ICIBackend_GetPhoneNumbers = Main.ListingBackend(oListing).GetPhoneNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore, oFilter As CFilter) As CListings Implements ICIBackend.GetStoreListings
            Throw New System.NotImplementedException("GetStoreListings is not implemented for Groups backend.")
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore) As CListings Implements ICIBackend.GetStoreListings
            Throw New System.NotImplementedException("GetStoreListings is not implemented for Groups backend.")
        End Function

        Private Function ICIBackend_GetStores() As CStores Implements ICIBackend.GetStores
            If Not ICIBackend_IsConnected Then
                Connect()
            End If
        End Function

        Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders Implements ICIBackend.GetSubFolders
            Throw New System.NotImplementedException("GetSubFolders is not implemented for Groups backend.")
        End Function

        Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean Implements ICIBackend.HasAddresses

        End Function

        Private Property ICIBackend_ID() As Integer Implements ICIBackend.ID
            Get
                ICIBackend_ID = m_iID
            End Get
            Set(RHS As Integer)
                m_iID = RHS
            End Set
        End Property

        Private Sub ICIBackend_Initialize(iID As Integer) Implements ICIBackend.Initialize
            Dim xName As String
            Dim xKey As String
            Dim xID As String
            Dim i As Integer
            Dim iPos As Integer

            Try
                m_iID = iID

                'get column names/ids
                For i = 1 To 4
                    xKey = Main.g_oIni.GetIni("Backend" & iID, "Col" & i, Main.g_oIni.CIIni)
                    If xKey <> String.Empty Then
                        'parse value
                        iPos = InStr(xKey, ",")
                        If iPos = 0 Then
                            'no comma found - should be in form 'Name,ID'
                            Throw New Exception("Invalid value in ci.ini for Backend" & iID.ToString() & _
                                                "\Col" & i.ToString() & ".")
                        Else
                            'get name and id from string
                            xName = Left$(xKey, iPos - 1)
                            xID = Mid$(xKey, iPos + 1)

                            If xName = String.Empty Or xID = String.Empty Then
                                'missing some value
                                Throw New Exception("Invalid value in ci.ini for Backend" & iID.ToString() & _
                                                    "\Col" & i.ToString() & ".")
                            Else
                                'assign to columns array
                                Main.g_vCols(i - 1, 0) = Trim$(xName)
                                Main.g_vCols(i - 1, 1) = Trim$(xID)
                            End If
                        End If
                    End If
                Next i
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private ReadOnly Property ICIBackend_InternalID() As Integer Implements ICIBackend.InternalID
            Get
                ICIBackend_InternalID = ciGroupsInternalID
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsConnected() As Boolean Implements ICIBackend.IsConnected
            Get
                ICIBackend_IsConnected = m_bIsConnected
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Exists() As Boolean Implements ICIBackend.Exists
            Get
                'force True value
                ICIBackend_Exists = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Name() As String Implements ICIBackend.Name
            Get
                Dim xName As String
                Dim xDesc As String

                Try
                    'get from ini
                    xName = Main.g_oIni.GetIni("Backend" & m_iID.ToString(), "Name", Main.g_oIni.CIIni)

                    'raise error if ini value is missing
                    If Len(xName) = 0 Then
                        xDesc = "Invalid Backend" & m_iID.ToString() & "\Name key in ci.ini."
                        Throw New Exception(xDesc)
                    End If
                    ICIBackend_Name = xName
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat Implements ICIBackend.NumberPromptFormat
        End Function

        Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings Implements ICIBackend.SearchFolder
            Dim oFilter As CFilter

            Try
                oFilter = PromptForFilter()

                If Not oFilter Is Nothing Then
                    ICIBackend_SearchFolder = ICIBackend_GetFolderListings(oFolder, oFilter)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_SearchNative() As CListings Implements ICIBackend.SearchNative
            Throw New System.NotImplementedException("SearchNative is not implemented for Groups backend.")
        End Function

        Private ReadOnly Property ICIBackend_SearchOperators() As ciSearchOperators Implements ICIBackend.SearchOperators
            Get
                ICIBackend_SearchOperators = ciSearchOperators.ciSearchOperator_BeginsWith Or _
                                             ciSearchOperators.ciSearchOperator_Contains Or _
                                             ciSearchOperators.ciSearchOperator_Equals
            End Get
        End Property

        Private Function ICIBackend_SearchStore(oStore As CStore) As CListings Implements ICIBackend.SearchStore
            Throw New System.NotImplementedException("SearchStore is not implemented for Groups backend.")
        End Function

        Private ReadOnly Property ICIBackend_SupportsContactAdd() As Boolean Implements ICIBackend.SupportsContactAdd
            Get
                ICIBackend_SupportsContactAdd = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsContactEdit() As Boolean Implements ICIBackend.SupportsContactEdit
            Get
                ICIBackend_SupportsContactEdit = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsFolders() As Boolean Implements ICIBackend.SupportsFolders
            Get
                ICIBackend_SupportsFolders = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNativeSearch() As Boolean Implements ICIBackend.SupportsNativeSearch
            Get
                ICIBackend_SupportsNativeSearch = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNestedFolders() As Boolean Implements ICIBackend.SupportsNestedFolders
            Get
                ICIBackend_SupportsNestedFolders = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsStoreLoad() As Boolean
            Get
                ICIBackend_SupportsStoreLoad = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsStoreSearch() As Boolean
            Get
                ICIBackend_SupportsStoreSearch = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsMultipleStores() As Boolean Implements ICIBackend.SupportsMultipleStores
            Get
                ICIBackend_SupportsMultipleStores = False
            End Get
        End Property

        Private Sub ICIBackend_AddContact() Implements ICIBackend.AddContact
            Throw New System.NotImplementedException("AddContact is not implemented for CI.CGroups.")
        End Sub

        Private Property ICIBackend_DefaultSortColumn() As ciListingCols Implements ICIBackend.DefaultSortColumn
            Get
                Try
                    If m_iSortCol = 0 Then
                        Try
                            m_iSortCol = Main.g_oIni.GetIni("CIGroups", "SortColumn", Main.g_oIni.CIUserIni())
                        Catch
                        End Try
                    End If
                    If m_iSortCol = 0 Then
                        m_iSortCol = ciListingCols.ciListingCols_DisplayName
                    End If
                    ICIBackend_DefaultSortColumn = m_iSortCol
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(iNew As ciListingCols)
                m_iSortCol = iNew
                Main.g_oIni.SetIni("CIGroups", "SortColumn", CStr(iNew), Main.g_oIni.CIUserIni())
            End Set
        End Property

        Private ReadOnly Property ICIBackend_Col1Name() As String Implements ICIBackend.Col1Name
            Get
                ICIBackend_Col1Name = Main.g_vCols(0, 0)
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col2Name() As String Implements ICIBackend.Col2Name
            Get
                ICIBackend_Col2Name = Main.g_vCols(1, 0)
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col3Name() As String Implements ICIBackend.Col3Name
            Get
                ICIBackend_Col3Name = Main.g_vCols(2, 0)
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col4Name() As String Implements ICIBackend.Col4Name
            Get
                ICIBackend_Col4Name = Main.g_vCols(3, 0)
            End Get
        End Property

        Private ReadOnly Property ICIBackend_DisplayName() As String Implements ICIBackend.DisplayName
            Get
                ICIBackend_DisplayName = Main.g_oIni.GetIni("Backend" & m_iID, "Name", Main.g_oIni.CIIni)
            End Get
        End Property

        Private Sub ICIBackend_EditContact(oListing As CListing) Implements ICIBackend.EditContact
            Throw New System.NotImplementedException("EditContact is not implemented for CI.CGroups.")
        End Sub

        Private Function ICIBackend_Events() As CEventGenerator Implements ICIBackend.Events
            ICIBackend_Events = m_oEvents
        End Function

        Private Function ICIBackend_Filter() As CFilter Implements ICIBackend.Filter
            'returns the filter object for this backend
            Try
                If m_oFilter Is Nothing Then
                    CreateFilter()
                End If

                ICIBackend_Filter = m_oFilter
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_IsLoadableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsLoadableEntity
            Get
                'returns TRUE iff the specified UNID can contain contacts
                Dim oUNID As CUNID
                Dim bIsFolder As Boolean

                Try
                    oUNID = New CUNID

                    'all folders are loadable entities
                    ICIBackend_IsLoadableEntity = (oUNID.GetUNIDType(xUNID) = ciUNIDTypes.ciUNIDType_Folder)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsSearchableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsSearchableEntity
            Get
                'returns TRUE iff the specified UNID can contain contacts
                'all folders are searchable entities
                Dim oUNID As CUNID
                Dim iType As ciUNIDTypes

                Try
                    oUNID = New CUNID
                    iType = oUNID.GetUNIDType(xUNID)
                    ICIBackend_IsSearchableEntity = (iType = ciUNIDTypes.ciUNIDType_Folder)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Function ICIBackend_GetAddresses(oListing As CListing) As CAddresses Implements ICIBackend.GetAddresses
            Dim oAddresses As CAddresses
            Dim oAddress As CAddress
            Dim xSQL As String

            Try
                oAddresses = New CAddresses

                If oListing Is Nothing Then
                    oAddress = New CAddress

                    'specify the word 'various' in the list of available addresses
                    oAddress.Name = "Various"
                    oAddress.UNID = ""
                Else
                    xSQL = "SELECT fldUNID,fldAddressTypeName FROM " & _
                        "tblContactGroupMembers WHERE fldID=" & oListing.ID & " AND fldAddressType <> """""

                    Dim oRS As ADODB.Recordset
                    oRS = New ADODB.Recordset
                    oRS.Open(xSQL, m_oCnn, ADODB.CursorTypeEnum.adOpenForwardOnly)

                    If Not (oRS.EOF And oRS.BOF) Then
                        oAddress = New CAddress
                        With oAddress
                            .Name = oRS.Fields("fldAddressTypeName").Value
                            .UNID = oRS.Fields("fldUNID").Value
                        End With
                    End If

                    oRS.Close()
                    oRS = Nothing
                End If

                'add the address to the collection
                oAddresses.Add(oAddress)

                'return
                ICIBackend_GetAddresses = oAddresses
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetAddresses() As CAddresses Implements ICIBackend.GetAddresses
            Dim oAddresses As CAddresses
            Dim oAddress As CAddress

            Try
                oAddresses = New CAddresses

                oAddress = New CAddress

                'specify the word 'various' in the list of available addresses
                oAddress.Name = "Various"
                oAddress.UNID = ""

                'add the address to the collection
                oAddresses.Add(oAddress)

                'return
                ICIBackend_GetAddresses = oAddresses
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function


        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object, _
                                                ByVal iIncludeData As ciRetrieveData, _
                                                ByVal iAlerts As ciAlerts) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData, iAlerts)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object, _
                                                ByVal iIncludeData As ciRetrieveData) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress) As ICContacts Implements ICIBackend.GetContacts

            Try
                ICIBackend_GetContacts = GetContacts(oListing, oAddress)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields Implements ICIBackend.GetCustomFields
            Throw New System.NotImplementedException("GetCustomFields is not implemented for Groups backend.")
        End Function

        Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Object) As CContactNumber Implements ICIBackend.GetEMailNumber

        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            'calls ICIBackend_GetEMailNumbers in backend of specified listing
            Dim oB As ICIBackend

            Try
                'get backend of listing
                oB = Main.Backends.ItemFromID(oListing.BackendID)

                ICIBackend_GetEMailNumbers = oB.GetEMailNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            'calls ICIBackend_GetEMailNumbers in backend of specified listing
            Dim oB As ICIBackend

            Try
                'get backend of listing
                oB = Main.Backends.ItemFromID(oListing.BackendID)

                ICIBackend_GetEMailNumbers = oB.GetEMailNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Object) As CContactNumber Implements ICIBackend.GetFaxNumber

        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'calls ICIBackend_GetFaxNumbers in backend of specified listing
            Dim oB As ICIBackend

            Try
                ICIBackend_GetFaxNumbers = Main.ListingBackend(oListing) _
                    .GetFaxNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'calls ICIBackend_GetFaxNumbers in backend of specified listing
            Dim oB As ICIBackend

            Try
                ICIBackend_GetFaxNumbers = Main.ListingBackend(oListing) _
                    .GetFaxNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder, oFilter As CFilter) As CListings Implements ICIBackend.GetFolderListings
            'returns the collection of listings that match the filter criteria and
            'are stored in the specified folder
            Dim oRS As ADODB.Recordset
            Dim oListings As CListings
            Dim i As Long
            Dim lRecs As Long
            Dim Cancel As Boolean
            Dim xSQL As String
            Dim xFilterSort As String

            Try
                'get new listings
                oListings = New CListings

                xSQL = "SELECT * FROM tblContactGroupMembers WHERE fldGroup=" & oFolder.ID

                If Not oFilter Is Nothing Then
                    xSQL = xSQL & GetFilterSort(oFilter)
                Else
                    xSQL = xSQL & " ORDER BY fldDisplayName"
                End If

                'get folder records
                oRS = New ADODB.Recordset
                oRS.Open(xSQL, m_oCnn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

                If Not (oRS.BOF And oRS.EOF) Then
                    m_oEvents.RaiseBeforeListingsRetrieved(lRecs, Cancel)

                    'create a new listing for each record returned
                    If Not Cancel Then
                        oRS.MoveFirst()
                        While Not oRS.EOF
                            AddListing(oRS, oListings, oFolder.UNID, True)

                            i = i + 1
                            m_oEvents.RaiseAfterListingAdded(i, lRecs, Cancel)
                            If Cancel Then
                                m_oEvents.RaiseAfterListingsRetrieved(i)
                                ICIBackend_GetFolderListings = oListings
                                Exit Function
                            End If
                            oRS.MoveNext()
                            m_oEvents.RaiseAfterListingsRetrieved(i)
                        End While
                    End If
                Else
                    'return empty listings collection
                    oListings = New CListings
                End If

                ICIBackend_GetFolderListings = oListings
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder) As CListings Implements ICIBackend.GetFolderListings
            'returns the collection of listings that match the filter criteria and
            'are stored in the specified folder
            Dim oRS As ADODB.Recordset
            Dim oListings As CListings
            Dim i As Long
            Dim lRecs As Long
            Dim Cancel As Boolean
            Dim xSQL As String
            Dim xFilterSort As String

            Try
                'get new listings
                oListings = New CListings

                xSQL = "SELECT * FROM tblContactGroupMembers WHERE fldGroup=" & oFolder.ID

                'If Not oFilter Is Nothing Then
                '    xSQL = xSQL & GetFilterSort(oFilter)
                'Else
                xSQL = xSQL & " ORDER BY fldDisplayName"
                'End If

                'get folder records
                oRS = New ADODB.Recordset
                oRS.Open(xSQL, m_oCnn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

                If Not (oRS.BOF And oRS.EOF) Then
                    m_oEvents.RaiseBeforeListingsRetrieved(lRecs, Cancel)

                    '       create a new listing for each record returned
                    If Not Cancel Then
                        oRS.MoveFirst()
                        While Not oRS.EOF
                            AddListing(oRS, oListings, oFolder.UNID, True)

                            i = i + 1
                            m_oEvents.RaiseAfterListingAdded(i, lRecs, Cancel)
                            If Cancel Then
                                m_oEvents.RaiseAfterListingsRetrieved(i)
                                ICIBackend_GetFolderListings = oListings
                                Exit Function
                            End If
                            oRS.MoveNext()
                            m_oEvents.RaiseAfterListingsRetrieved(i)
                        End While
                    End If
                Else
                    'return empty listings collection
                    oListings = New CListings
                End If

                ICIBackend_GetFolderListings = oListings
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

#End Region
    End Class
End Namespace
