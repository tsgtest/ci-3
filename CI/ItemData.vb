﻿Imports LMP

'GLOG : 8849 : ceh
Public Class ItemData
    'class used to support ListBoxes & ComboBoxes
    Public ItemText As String
    Public ItemData As Long

    Public Sub New(ByVal newText As String, ByVal newData As Integer)
        'constructor
        ItemText = newText
        ItemData = newData
    End Sub

    Public Overrides Function ToString() As String
        'Show the text in ListBox and ComboBox displays.
        Return ItemText
    End Function

    Public Overrides Function Equals(ByVal obj As Object) As Boolean

        Try
            'Allow IndexOf() and Contains() searches by ItemData.
            If (TypeOf obj Is Integer) Then
                Return CBool(CInt(obj) = ItemData)
            Else
                Return MyBase.Equals(obj)
            End If
        Catch ex As Exception
            [Error].Show(ex)
        End Try

    End Function
End Class
