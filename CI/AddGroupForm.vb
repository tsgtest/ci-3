﻿Option Explicit On

Imports LMP
Imports LMP.Exceptions

Namespace TSG.CI
    Public Class AddGroupForm
#Region "********************fields*****************"
        Private m_xGroupName As String
        Private m_bCanceled As Boolean
#End Region
#Region "*******************initializer***************"
        Public Sub New()
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.
            m_bCanceled = True
        End Sub
#End Region
#Region "********************properties*****************"
        Public ReadOnly Property Canceled() As Boolean
            Get
                Canceled = m_bCanceled
            End Get
        End Property

        Public Property GroupName() As String
            Set(xNew As String)
                m_xGroupName = xNew
            End Set
            Get
                GroupName = m_xGroupName
            End Get
        End Property
#End Region
#Region "********************events*****************"
        Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
            Try
                Me.GroupName = Me.txtGroupName.Text
                m_bCanceled = False
                Me.Hide()
                Application.DoEvents()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
            Try
                Me.GroupName = String.Empty
                m_bCanceled = True
                Me.Hide()
                Application.DoEvents()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub txtGroupName_TextChanged(sender As Object, e As EventArgs) Handles txtGroupName.TextChanged
            Try
                'enable OK btn only when there
                'is text in the Name txt box
                If Me.txtGroupName.Text = "" Then
                    btnOK.Enabled = False
                Else
                    btnOK.Enabled = True
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub txtGroupName_Enter(sender As Object, e As EventArgs) Handles txtGroupName.Enter
            Try
                With Me.txtGroupName
                    .SelectAll()
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
    End Class
End Namespace
