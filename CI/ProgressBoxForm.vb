﻿Option Explicit On

Imports TSG.CI
Imports LMP

Public Class ProgressBoxForm
#Region "*************declarations****************"
    Private m_bShowCancelled As Boolean
    Private m_bCancelled As Boolean
#End Region
#Region "*************initializer**************"
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Try
            ' Add any initialization after the InitializeComponent() call.
            Me.Title = TSG.CI.Main.g_oSessionType.AppTitle
            Me.pbContacts.Width = Me.Width - (55 * Functions.GetScalingFactor())
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
#End Region
#Region "*************properties*****************"
    Public Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
        Set(bValue As Boolean)
            m_bCancelled = bvalue
        End Set
    End Property

    Public Property ShowCancelled() As Boolean
        Get
            ShowCancelled = m_bShowCancelled
        End Get
        Set(bNew As Boolean)
            With Me
                .btnCancel.Visible = bNew
                .btnCancel.Enabled = bNew
                If bNew Then
                    .lblMsg.Top = 10 * Functions.GetScalingFactor()
                    .pbContacts.Top = 30 * Functions.GetScalingFactor()
                    .btnCancel.Top = .pbContacts.Top + (32 * Functions.GetScalingFactor())
                Else
                    .lblMsg.Top = 25 * Functions.GetScalingFactor()
                    .pbContacts.Top = 50 * Functions.GetScalingFactor()
                End If
            End With
            m_bShowCancelled = bNew
        End Set
    End Property

    Public Property Value() As Single
        Get
            Value = Me.pbContacts.Value
        End Get
        Set(xNew As Single)
            On Error Resume Next
            pbContacts.Value = xNew
            Application.DoEvents()
        End Set
    End Property

    Public Property Message() As String
        Get
            Message = Me.lblMsg.Text
        End Get
        Set(xNew As String)
            Me.lblMsg.Text = xNew
        End Set
    End Property

    Public Property Title() As String
        Get
            Title = Me.Text
        End Get
        Set(xNew As String)
            Me.Text = xNew
        End Set
    End Property
#End Region
#Region "*************events****************"
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Application.DoEvents()
            m_bCancelled = True
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
#End Region

End Class