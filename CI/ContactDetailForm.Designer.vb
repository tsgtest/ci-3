﻿Namespace TSG.CI
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ContactDetailForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ContactDetailForm))
            Me.lstContacts = New System.Windows.Forms.ListBox()
            Me.tbControlDetails = New System.Windows.Forms.TabControl()
            Me.tbPageSummary = New System.Windows.Forms.TabPage()
            Me.pctCopy = New System.Windows.Forms.PictureBox()
            Me.lblAddressType = New System.Windows.Forms.Label()
            Me.txtSummary = New System.Windows.Forms.TextBox()
            Me.tbPageAllFields = New System.Windows.Forms.TabPage()
            Me.grdContactDetail = New System.Windows.Forms.DataGridView()
            Me.btnClose = New System.Windows.Forms.Button()
            Me.tbControlDetails.SuspendLayout()
            Me.tbPageSummary.SuspendLayout()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.tbPageAllFields.SuspendLayout()
            CType(Me.grdContactDetail, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lstContacts
            '
            Me.lstContacts.BackColor = System.Drawing.SystemColors.Control
            Me.lstContacts.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.lstContacts.FormattingEnabled = True
            Me.lstContacts.Location = New System.Drawing.Point(10, 23)
            Me.lstContacts.Margin = New System.Windows.Forms.Padding(2)
            Me.lstContacts.Name = "lstContacts"
            Me.lstContacts.Size = New System.Drawing.Size(186, 312)
            Me.lstContacts.TabIndex = 0
            '
            'tbControlDetails
            '
            Me.tbControlDetails.Alignment = System.Windows.Forms.TabAlignment.Bottom
            Me.tbControlDetails.Controls.Add(Me.tbPageSummary)
            Me.tbControlDetails.Controls.Add(Me.tbPageAllFields)
            Me.tbControlDetails.Location = New System.Drawing.Point(208, 10)
            Me.tbControlDetails.Margin = New System.Windows.Forms.Padding(2)
            Me.tbControlDetails.Multiline = True
            Me.tbControlDetails.Name = "tbControlDetails"
            Me.tbControlDetails.SelectedIndex = 0
            Me.tbControlDetails.Size = New System.Drawing.Size(320, 326)
            Me.tbControlDetails.TabIndex = 3
            '
            'tbPageSummary
            '
            Me.tbPageSummary.Controls.Add(Me.pctCopy)
            Me.tbPageSummary.Controls.Add(Me.lblAddressType)
            Me.tbPageSummary.Controls.Add(Me.txtSummary)
            Me.tbPageSummary.Location = New System.Drawing.Point(4, 4)
            Me.tbPageSummary.Margin = New System.Windows.Forms.Padding(2)
            Me.tbPageSummary.Name = "tbPageSummary"
            Me.tbPageSummary.Padding = New System.Windows.Forms.Padding(2)
            Me.tbPageSummary.Size = New System.Drawing.Size(312, 300)
            Me.tbPageSummary.TabIndex = 0
            Me.tbPageSummary.Text = "Summary"
            Me.tbPageSummary.UseVisualStyleBackColor = True
            '
            'pctCopy
            '
            Me.pctCopy.Image = CType(resources.GetObject("pctCopy.Image"), System.Drawing.Image)
            Me.pctCopy.Location = New System.Drawing.Point(288, 9)
            Me.pctCopy.Name = "pctCopy"
            Me.pctCopy.Size = New System.Drawing.Size(19, 20)
            Me.pctCopy.TabIndex = 47
            Me.pctCopy.TabStop = False
            '
            'lblAddressType
            '
            Me.lblAddressType.AutoSize = True
            Me.lblAddressType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblAddressType.Location = New System.Drawing.Point(10, 49)
            Me.lblAddressType.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
            Me.lblAddressType.Name = "lblAddressType"
            Me.lblAddressType.Size = New System.Drawing.Size(79, 13)
            Me.lblAddressType.TabIndex = 0
            Me.lblAddressType.Text = "lblAddressType"
            '
            'txtSummary
            '
            Me.txtSummary.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.txtSummary.Location = New System.Drawing.Point(13, 76)
            Me.txtSummary.Margin = New System.Windows.Forms.Padding(2)
            Me.txtSummary.Multiline = True
            Me.txtSummary.Name = "txtSummary"
            Me.txtSummary.Size = New System.Drawing.Size(281, 221)
            Me.txtSummary.TabIndex = 1
            '
            'tbPageAllFields
            '
            Me.tbPageAllFields.Controls.Add(Me.grdContactDetail)
            Me.tbPageAllFields.Location = New System.Drawing.Point(4, 4)
            Me.tbPageAllFields.Margin = New System.Windows.Forms.Padding(2)
            Me.tbPageAllFields.Name = "tbPageAllFields"
            Me.tbPageAllFields.Padding = New System.Windows.Forms.Padding(2)
            Me.tbPageAllFields.Size = New System.Drawing.Size(312, 300)
            Me.tbPageAllFields.TabIndex = 1
            Me.tbPageAllFields.Text = "All Fields"
            Me.tbPageAllFields.UseVisualStyleBackColor = True
            '
            'grdContactDetail
            '
            Me.grdContactDetail.AllowUserToAddRows = False
            Me.grdContactDetail.AllowUserToDeleteRows = False
            Me.grdContactDetail.AllowUserToResizeColumns = False
            Me.grdContactDetail.AllowUserToResizeRows = False
            Me.grdContactDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
            Me.grdContactDetail.BackgroundColor = System.Drawing.Color.White
            Me.grdContactDetail.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.grdContactDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
            Me.grdContactDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.grdContactDetail.ColumnHeadersVisible = False
            Me.grdContactDetail.Location = New System.Drawing.Point(2, 10)
            Me.grdContactDetail.Margin = New System.Windows.Forms.Padding(2)
            Me.grdContactDetail.MultiSelect = False
            Me.grdContactDetail.Name = "grdContactDetail"
            Me.grdContactDetail.RowHeadersVisible = False
            Me.grdContactDetail.RowHeadersWidth = 38
            Me.grdContactDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
            Me.grdContactDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
            Me.grdContactDetail.Size = New System.Drawing.Size(308, 290)
            Me.grdContactDetail.TabIndex = 0
            '
            'btnClose
            '
            Me.btnClose.Location = New System.Drawing.Point(446, 317)
            Me.btnClose.Margin = New System.Windows.Forms.Padding(2)
            Me.btnClose.Name = "btnClose"
            Me.btnClose.Size = New System.Drawing.Size(75, 21)
            Me.btnClose.TabIndex = 5
            Me.btnClose.Text = "&Close"
            Me.btnClose.UseVisualStyleBackColor = True
            '
            'ContactDetailForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.AutoSize = True
            Me.ClientSize = New System.Drawing.Size(537, 344)
            Me.Controls.Add(Me.btnClose)
            Me.Controls.Add(Me.tbControlDetails)
            Me.Controls.Add(Me.lstContacts)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Margin = New System.Windows.Forms.Padding(2)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ContactDetailForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Contact Detail"
            Me.tbControlDetails.ResumeLayout(False)
            Me.tbPageSummary.ResumeLayout(False)
            Me.tbPageSummary.PerformLayout()
            CType(Me.pctCopy, System.ComponentModel.ISupportInitialize).EndInit()
            Me.tbPageAllFields.ResumeLayout(False)
            CType(Me.grdContactDetail, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents lstContacts As System.Windows.Forms.ListBox
        Friend WithEvents tbControlDetails As System.Windows.Forms.TabControl
        Friend WithEvents tbPageSummary As System.Windows.Forms.TabPage
        Friend WithEvents txtSummary As System.Windows.Forms.TextBox
        Friend WithEvents tbPageAllFields As System.Windows.Forms.TabPage
        Friend WithEvents lblAddressType As System.Windows.Forms.Label
        Friend WithEvents btnClose As System.Windows.Forms.Button
        Friend WithEvents grdContactDetail As System.Windows.Forms.DataGridView
        Private WithEvents pctCopy As System.Windows.Forms.PictureBox
    End Class
End Namespace