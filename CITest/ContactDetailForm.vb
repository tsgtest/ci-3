﻿Option Explicit On

Imports System.IO
Imports System.Reflection
Imports TSG.CI
Imports LMP

Public Class ContactDetailForm
#Region "**************fields******************"
    Private m_oContacts As ICContacts
    Private m_oSession As ICSession
    Private g_oError As ICError
    'Private g_oIni As CIni
#End Region
#Region "**************initializer***************"
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Try
            ' Add any initialization after the InitializeComponent() call.
            Me.TabControl1.TabPages(0).Select()
            Me.lblAddressType.Text = ""
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
#End Region
#Region "**************properties****************"
    Private Property Contacts As ICContacts
        Get
            Contacts = m_oContacts
        End Get
        Set(oNew As ICContacts)
            m_oContacts = oNew
        End Set
    End Property
#End Region
#Region "**************events*******************"
    Private Sub mnuRetrieve_Entities_Click(sender As Object, e As EventArgs) Handles mnuRetrieve_Entities.Click
        Try
            GetEntities()
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
    Private Sub mnuContacts_Retrieve_Click(sender As Object, e As EventArgs) Handles mnuRetrieve_Contacts.Click
        Try
            GetContacts()
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
    Private Sub lstContacts_Click(sender As Object, e As EventArgs) Handles lstContacts.Click
        Dim oContact As ICContact
        Dim iItem As Integer

        Try
            With Me.lstContacts
                If .Text = "TO" Or .Text = "FROM" Or .Text = "CC" Or .Text = "BCC" Or .Text = "CUSTOM" Then
                    .SelectedIndex = .SelectedIndex + 1
                    Exit Sub
                End If
                iItem = .SelectedValue
                If iItem <> 0 Then
                    oContact = m_oContacts.Item(iItem)
                    If Not oContact Is Nothing Then
                        GetSummary(oContact)
                        GetDetail(oContact)
                    End If
                End If
            End With
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
    Private Sub mnuFile_Exit_Click(sender As Object, e As EventArgs) Handles mnuFile_Exit.Click
        Try
            Me.Close()
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub

#End Region
#Region "**************methods**************"
    Private Sub GetEntities()
        Dim xDesc As String
        Dim dtItems As DataTable
        Dim dtSource As DataTable
        Dim oContacts As ICContacts
        Dim i As Integer

        If m_oSession Is Nothing Then
            m_oSession = CreateCISessionObject()
        End If

        m_oSession.SessionType = ciSessionType.ciSession_Connect

        'Fill DataTable of test entities
        dtSource = New DataTable

        'Add columns
        With dtSource.Columns
            .Add("Entity")
            .Add("Type")
            .Add("")
            .Add("ID")
            .Add("UNID")
        End With

        With dtSource
            .Rows.Add("Attorney 1", 1, String.Empty, 0, String.Empty)
            .Rows.Add("Counsel", 1, String.Empty, 1, String.Empty)
            .Rows.Add("Beneficiary 1", 1, String.Empty, 2, String.Empty)
            .Rows.Add("Attorney 2", 1, String.Empty, 3, String.Empty)
        End With

        oContacts = m_oSession.GetContacts_Custom(oDataTable:=dtSource)

        Me.txtSummary.Text = String.Empty

        If Not oContacts Is Nothing Then
            Me.Contacts = oContacts

            Dim xList As String = ""
            Dim oC As ICContact
            Static iList As ICI.ciSelectionlists

            dtItems = New DataTable

            'Add columns
            With dtItems.Columns
                .Add("Name")
                .Add("ID")
            End With

            For i = Me.Contacts.Count To 1 Step -1
                oC = Me.Contacts.Item(i)
                If i = 1 Then iList = 0
                If iList <> oC.ContactType Then
                    iList = oC.ContactType
                    Select Case iList
                        Case ICI.ciSelectionlists.ciSelectionList_To
                            xList = "TO"
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            xList = "FROM"
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            xList = "CC"
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            xList = "BCC"
                        Case ICI.ciSelectionlists.ciSelectionList_Custom
                            xList = "CUSTOM"
                    End Select
                    dtItems.Rows.Add(xList, "")
                End If
                dtItems.Rows.Add("    " & oC.DisplayName, i)
            Next i

            'load control
            With Me.lstContacts
                .DisplayMember = dtItems.Columns(0).ColumnName
                .ValueMember = dtItems.Columns(1).ColumnName
                .DataSource = dtItems
            End With

            On Error Resume Next
            Me.lstContacts.SelectedIndex = 1
            Me.lstContacts_Click(Me.lstContacts, New EventArgs())
        End If
        m_oSession = Nothing
    End Sub

    Private Sub GetContacts()
        Dim xDesc As String
        Dim oContacts As ICContacts
        Dim dtItems As DataTable

        If m_oSession Is Nothing Then
            m_oSession = CreateCISessionObject()
        End If

        m_oSession.SessionType = ciSessionType.ciSession_CI

        ' ''oContacts = m_oSession.GetContactsEx(CI.ciRetrieveData.ciRetrieveData_All, _
        ' ''                                    CI.ciRetrieveData.ciRetrieveData_None,
        ' ''                                    CI.ciRetrieveData.ciRetrieveData_Names,
        ' ''                                    CI.ciRetrieveData.ciRetrieveData_Names)
        oContacts = m_oSession.GetContactsEx(ciRetrieveData.ciRetrieveData_All, _
                                             ciRetrieveData.ciRetrieveData_All, _
                                             ciRetrieveData.ciRetrieveData_None, _
                                             ciRetrieveData.ciRetrieveData_None)
        ' ''oContacts = m_oSession.GetContactsEx(CI.ciRetrieveData.ciRetrieveData_All,
        ' ''            CI.ciRetrieveData.ciRetrieveData_None, CI.ciRetrieveData.ciRetrieveData_None,
        ' ''            CI.ciRetrieveData.ciRetrieveData_None,
        ' ''             ICI.ciSelectionLists.ciSelectionList_To, 0, CI.ciAlerts.ciAlert_NoAddresses,
        ' ''            "Selected Contacts", "", "", "",
        ' ''            0, 0, 0, 0)

        Me.txtSummary.Text = String.Empty

        If Not oContacts Is Nothing Then
            Me.Contacts = oContacts

            Dim i As Integer
            Dim xList As String = ""
            Dim oC As ICContact
            Static iList As ICI.ciSelectionlists

            dtItems = New DataTable

            'Add columns
            With dtItems.Columns
                .Add("Name")
                .Add("ID")
            End With

            'GLOG : 15841 : ceh
            For i = 1 To Me.Contacts.Count
                oC = Me.Contacts.Item(i)
                'If i = 1 Then iList = 0
                If iList <> oC.ContactType Then
                    iList = oC.ContactType
                    Select Case iList
                        Case ICI.ciSelectionlists.ciSelectionList_To
                            xList = "TO"
                        Case ICI.ciSelectionlists.ciSelectionList_From
                            xList = "FROM"
                        Case ICI.ciSelectionlists.ciSelectionList_CC
                            xList = "CC"
                        Case ICI.ciSelectionlists.ciSelectionList_BCC
                            xList = "BCC"
                        Case ICI.ciSelectionlists.ciSelectionList_Custom
                            xList = "CUSTOM"
                    End Select
                    dtItems.Rows.Add(xList, "")
                End If
                dtItems.Rows.Add("    " & oC.DisplayName, i)
            Next i

            'load control
            With Me.lstContacts
                .DisplayMember = dtItems.Columns(0).ColumnName
                .ValueMember = dtItems.Columns(1).ColumnName
                .DataSource = dtItems
            End With

            On Error Resume Next
            Me.lstContacts.SelectedIndex = 1
            Me.lstContacts_Click(Me.lstContacts, New EventArgs())
        End If
        m_oSession = Nothing
    End Sub
    Public Sub GetSummary(oContact As ICContact)
        'gets a summary string for the specified contact
        Dim xToken As String

        xToken = "{<ADDRESSTYPENAME> Address}"

        Me.lblAddressType.Text = oContact.GetDetail(xToken)

        If Me.lblAddressType.Text = String.Empty Then
            Me.lblAddressType.Text = "No Address Found"
        End If

        'xToken = "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf
        xToken = "{<FULLNAMEWITHPREFIXANDSUFFIX>}" & vbCrLf
        If InStr(UCase(oContact.AddressTypeName), "BUSINESS") Then
            xToken = xToken & "{<TITLE>}" & vbCrLf & _
                "{<COMPANY>}" & vbCrLf
        End If

        xToken = xToken & "{<STREET1>}" & vbCrLf & _
                "{<STREET2>}" & vbCrLf & _
                "{<STREET3>}" & vbCrLf & _
                "{<CITY>{, }<STATE>}{  <ZIPCODE>}"

        Me.txtSummary.Text = oContact.GetDetail(xToken)

        xToken = "Phone: {<PHONE>}" & vbCrLf & _
            "Fax: {<FAX>}" & vbCrLf & _
            "EMail: {<EMAILADDRESS>}"

        Me.txtSummary.Text = Me.txtSummary.Text & vbCrLf & vbCrLf & _
            vbCrLf & oContact.GetDetail(xToken) & vbCrLf & vbCrLf & _
            vbCrLf & "Full Detail:" & vbCrLf & vbCrLf & Replace(oContact.GetDetail("<FULLDETAIL>"), vbCr, vbCrLf)
    End Sub

    Public Sub GetDetail(oContact As ICContact)
        'loads the field values for the specified contact
        Dim xTemp As String
        Dim oCustFld As ICCustomField
        Dim i As Integer
        Dim xVal0 As String
        Dim oDetail As DataTable

        With oContact
            'name/value array not yet built - do it
            oDetail = New DataTable
            ' Add two columns
            oDetail.Columns.Add("Property")
            oDetail.Columns.Add("Value")

            oDetail.Rows.Add("Display Name", .DisplayName)
            oDetail.Rows.Add("Prefix", .Prefix)
            oDetail.Rows.Add("First Name", .FirstName)
            oDetail.Rows.Add("Middle Name", .MiddleName)
            oDetail.Rows.Add("Last Name", .LastName)
            oDetail.Rows.Add("Suffix", .Suffix)
            oDetail.Rows.Add("Full Name", .FullName)
            oDetail.Rows.Add("Initials", .Initials)
            oDetail.Rows.Add("Salutation", .Salutation)
            oDetail.Rows.Add("Title", .Title)
            oDetail.Rows.Add("Department", .Department)
            oDetail.Rows.Add("Company", .Company)
            oDetail.Rows.Add("Address Type", .AddressTypeName)
            oDetail.Rows.Add("Street1", .Street1)
            oDetail.Rows.Add("Street2", .Street2)
            oDetail.Rows.Add("Street3", .Street3)
            oDetail.Rows.Add("Additional", .AdditionalInformation)
            oDetail.Rows.Add("City", .City)
            oDetail.Rows.Add("State", .State)
            oDetail.Rows.Add("Zip Code", .ZipCode)
            oDetail.Rows.Add("Country", .Country)
            oDetail.Rows.Add("Phone", .Phone)
            oDetail.Rows.Add("Phone Extension", .PhoneExtension)
            oDetail.Rows.Add("Fax", .Fax)
            oDetail.Rows.Add("Email Address", .EmailAddress)

            For Each oCustFld In .CustomFields
                i = i + 1
                oDetail.Rows.Add(oCustFld.Name, oCustFld.Value)
            Next oCustFld
        End With

        With Me.grdContactDetail
            .DataSource = oDetail
            .Refresh()
            .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns(0).Width = 128
        End With
    End Sub

    Private Shared Function CreateCISessionObject() As ICSession
        Dim xCIAssembly As String
        Dim oFile As FileInfo
        Dim oAsm As Assembly

        xCIAssembly = Assembly.GetExecutingAssembly().CodeBase
        xCIAssembly = xCIAssembly.Replace("file:///", "")
        'JTS 10/6/16: Directory is part of CodeBase
        oFile = New FileInfo(xCIAssembly)
        xCIAssembly = oFile.Directory.FullName & "\" & "CI.dll"
        oAsm = Assembly.LoadFrom(xCIAssembly)
        CreateCISessionObject = oAsm.CreateInstance("TSG.CI.CSession", True)
    End Function

#End Region
End Class




