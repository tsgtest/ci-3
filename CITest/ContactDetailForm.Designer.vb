﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ContactDetailForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ContactDetailForm))
        Me.lstContacts = New System.Windows.Forms.ListBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.tsMenu_File = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFile_Exit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsMenu_Retrieve = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRetrieve_Contacts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRetrieve_Entities = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblAddressType = New System.Windows.Forms.Label()
        Me.txtSummary = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdContactDetail = New System.Windows.Forms.DataGridView()
        Me.MenuStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdContactDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstContacts
        '
        Me.lstContacts.BackColor = System.Drawing.SystemColors.Control
        Me.lstContacts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lstContacts.FormattingEnabled = True
        Me.lstContacts.ItemHeight = 25
        Me.lstContacts.Location = New System.Drawing.Point(10, 58)
        Me.lstContacts.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lstContacts.Name = "lstContacts"
        Me.lstContacts.Size = New System.Drawing.Size(314, 775)
        Me.lstContacts.TabIndex = 1
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightGray
        Me.MenuStrip1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsMenu_File, Me.tsMenu_Retrieve})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 4, 0, 4)
        Me.MenuStrip1.Size = New System.Drawing.Size(1028, 38)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'tsMenu_File
        '
        Me.tsMenu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile_Exit})
        Me.tsMenu_File.Name = "tsMenu_File"
        Me.tsMenu_File.Size = New System.Drawing.Size(59, 30)
        Me.tsMenu_File.Text = "&File"
        '
        'mnuFile_Exit
        '
        Me.mnuFile_Exit.Name = "mnuFile_Exit"
        Me.mnuFile_Exit.Size = New System.Drawing.Size(121, 30)
        Me.mnuFile_Exit.Text = "E&xit"
        '
        'tsMenu_Retrieve
        '
        Me.tsMenu_Retrieve.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRetrieve_Contacts, Me.mnuRetrieve_Entities})
        Me.tsMenu_Retrieve.Name = "tsMenu_Retrieve"
        Me.tsMenu_Retrieve.Size = New System.Drawing.Size(105, 30)
        Me.tsMenu_Retrieve.Text = "&Retrieve"
        '
        'mnuRetrieve_Contacts
        '
        Me.mnuRetrieve_Contacts.Name = "mnuRetrieve_Contacts"
        Me.mnuRetrieve_Contacts.Size = New System.Drawing.Size(170, 30)
        Me.mnuRetrieve_Contacts.Text = "&Contacts"
        '
        'mnuRetrieve_Entities
        '
        Me.mnuRetrieve_Entities.Name = "mnuRetrieve_Entities"
        Me.mnuRetrieve_Entities.Size = New System.Drawing.Size(170, 30)
        Me.mnuRetrieve_Entities.Text = "&Entities"
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(334, 58)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(690, 792)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.lblAddressType)
        Me.TabPage1.Controls.Add(Me.txtSummary)
        Me.TabPage1.Location = New System.Drawing.Point(4, 37)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage1.Size = New System.Drawing.Size(682, 751)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Summary"
        '
        'lblAddressType
        '
        Me.lblAddressType.AutoSize = True
        Me.lblAddressType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddressType.Location = New System.Drawing.Point(26, 40)
        Me.lblAddressType.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblAddressType.Name = "lblAddressType"
        Me.lblAddressType.Size = New System.Drawing.Size(139, 25)
        Me.lblAddressType.TabIndex = 0
        Me.lblAddressType.Text = "AddressType"
        '
        'txtSummary
        '
        Me.txtSummary.BackColor = System.Drawing.Color.White
        Me.txtSummary.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSummary.Location = New System.Drawing.Point(30, 92)
        Me.txtSummary.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSummary.Multiline = True
        Me.txtSummary.Name = "txtSummary"
        Me.txtSummary.ReadOnly = True
        Me.txtSummary.Size = New System.Drawing.Size(526, 630)
        Me.txtSummary.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdContactDetail)
        Me.TabPage2.Location = New System.Drawing.Point(4, 37)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TabPage2.Size = New System.Drawing.Size(682, 751)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "All Fields"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grdContactDetail
        '
        Me.grdContactDetail.AllowUserToAddRows = False
        Me.grdContactDetail.AllowUserToDeleteRows = False
        Me.grdContactDetail.AllowUserToResizeColumns = False
        Me.grdContactDetail.AllowUserToResizeRows = False
        Me.grdContactDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdContactDetail.BackgroundColor = System.Drawing.Color.White
        Me.grdContactDetail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdContactDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.grdContactDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdContactDetail.ColumnHeadersVisible = False
        Me.grdContactDetail.Location = New System.Drawing.Point(0, 0)
        Me.grdContactDetail.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.grdContactDetail.MultiSelect = False
        Me.grdContactDetail.Name = "grdContactDetail"
        Me.grdContactDetail.ReadOnly = True
        Me.grdContactDetail.RowHeadersVisible = False
        Me.grdContactDetail.RowHeadersWidth = 38
        Me.grdContactDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grdContactDetail.RowTemplate.Height = 18
        Me.grdContactDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdContactDetail.Size = New System.Drawing.Size(560, 734)
        Me.grdContactDetail.TabIndex = 1
        '
        'ContactDetailForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(192.0!, 192.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(1028, 850)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lstContacts)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ContactDetailForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contact Integration 3.0 Client Diagnostic"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdContactDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstContacts As System.Windows.Forms.ListBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents tsMenu_File As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFile_Exit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsMenu_Retrieve As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRetrieve_Contacts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents lblAddressType As System.Windows.Forms.Label
    Friend WithEvents txtSummary As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdContactDetail As System.Windows.Forms.DataGridView
    Friend WithEvents mnuRetrieve_Entities As System.Windows.Forms.ToolStripMenuItem

End Class
