'**********************************************************
'   CustomField Class
'   created 6/30/99 by Daniel Fisherman
'   Contains properties and methods concerning
'   a contact's custom fields - the custom
'   fields collection is a member of the contact class
'**********************************************************
Option Explicit On

Namespace TSG.CI
    Public Class CCustomField
        Implements ICCustomField
#Region "******************fields***********************"
        Private m_xName As String
        Private m_xValue As String
        Private m_vID As Object
#End Region
#Region "******************initializer***********************"
        Private xObjectID As String
        Public Sub New()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property ID As Object Implements ICCustomField.ID
            Get
                Return m_vID
            End Get
            Set(value As Object)
                m_vID = value
            End Set
        End Property

        Public Property Name As String Implements ICCustomField.Name
            Get
                Return m_xName
            End Get
            Set(value As String)
                m_xName = value
            End Set
        End Property

        Public Property Value As String Implements ICCustomField.Value
            Get
                Value = m_xValue
            End Get
            Set(value As String)
                m_xValue = value
            End Set
        End Property
#End Region
    End Class
End Namespace

