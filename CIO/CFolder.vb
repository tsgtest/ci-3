'**********************************************************
'   Folder Class
'   created 12/20/00 by Daniel Fisherman
'   Contains properties and methods defining a
'   logical grouping of listings in a store
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CFolder
#Region "******************fields***********************"
        Private m_vUNID As Object
        Private m_xName As String
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties**************************"
        Public Property UNID As Object
            Get
                Return m_vUNID
            End Get
            Set(value As Object)
                m_vUNID = value
            End Set
        End Property

        Public Property Name As String
            Get
                'Attribute Name.VB_UserMemId = 0
                Name = m_xName
            End Get
            Set(value As String)
                m_xName = value
            End Set
        End Property

        Public ReadOnly Property ID As Object
            Get
                'returns the ID of the folder -parsed from the UNID
                Try
                    ID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Folder)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property StoreID As Object
            Get
                'returns the ID of the store containing
                'this folder- parsed from UNID
                Try
                    StoreID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Store)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property BackendID As Integer
            Get
                Try
                    BackendID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Backend)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
#End Region
    End Class
End Namespace
