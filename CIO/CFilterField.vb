'**********************************************************
'   CFilterField Class
'   created 12/23/00 by Daniel Fisherman
'   Contains properties and methods defining
'   a filter field
'**********************************************************
Option Explicit On

Namespace TSG.CI
    Public Class CFilterField
#Region "******************fields***********************"
        Private m_vID As Object
        Private m_xName As String
        Private m_vValue As Object
        Private m_iOp As ciSearchOperators
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties**************************"
        Public Property ID As Object
            Get
                Return m_vID
            End Get
            Set(value As Object)
                m_vID = value
            End Set
        End Property

        Public Property Value As Object
            Get
                Return m_vValue
            End Get
            Set(value As Object)
                m_vValue = value
            End Set
        End Property

        'ToDo - replace Operator with SearchOperator in calling functions
        Public Property SearchOperator As ciSearchOperators
            Get
                Return m_iOp
            End Get
            Set(value As ciSearchOperators)
                m_iOp = value
            End Set
        End Property

        Public Property Name As String
            Get
                Return m_xName
            End Get
            Set(value As String)
                m_xName = value
            End Set
        End Property
#End Region
    End Class
End Namespace
