Option Explicit On

Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Windows.Forms
Imports LMP


Namespace TSG.CI
    Public Class CIni
#Region "*************fields***********************"
        Private Const CSIDL_LOCAL_APPDATA = &H1C&
        Private Const CSIDL_FLAG_CREATE = &H8000&
        Private Const CSIDL_COMMON_DOCUMENTS = &H2E

        Private Const SHGFP_TYPE_CURRENT = 0
        Private Const SHGFP_TYPE_DEFAULT = 1
        Private Const MAX_PATH = 260
        Private Const MaxIniBuffer As Integer = &H7FFF

        Private Declare Function SHGetFolderPath Lib "shfolder" _
            Alias "SHGetFolderPathA" _
            (ByVal hwndOwner As Long, ByVal nFolder As Long, _
            ByVal hToken As Long, ByVal dwFlags As Long, _
            ByVal pszPath As String) As Long

        ' ''Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
        ' ''    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
        ' ''    ByVal lpKeyName As String, ByVal lpDefault As String, ByVal _
        ' ''    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
        ' ''    As String) As Long

        ' ''Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias _
        ' ''    "GetPrivateProfileSectionA" (ByVal lpAppName As String, _
        ' ''    ByVal lpReturnedString As String, ByVal nSize As Long, _
        ' ''    ByVal lpFileName As String) As Long

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function GetPrivateProfileString(ByVal lpAppName As String, _
                            ByVal lpKeyName As String, _
                            ByVal lpDefault As String, _
                            ByVal lpReturnedString As StringBuilder, _
                            ByVal nSize As Integer, _
                            ByVal lpFileName As String) As Integer
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function GetPrivateProfileSection(ByVal lpAppName As String, _
                          ByVal lpReturnedString As IntPtr, _
                          ByVal nSize As Integer, _
                          ByVal lpFileName As String) As Short
        End Function

        Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
            "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
            ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As _
            String) As Long

        Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
            ByVal lpBuffer As String, nSize As Long) As Long

        Private Const ciAppIni As String = "CI.ini"
#End Region
#Region "*************initializer******************"
        Public Sub New()
        End Sub
#End Region
#Region "*************public methods****************"
        Public Function CIIni() As String
            Dim xAppPath As String
            xAppPath = GetAppPath()
            CIIni = xAppPath & IIf(Right(xAppPath, 1) = "\", "", "\") & ciAppIni
        End Function

        Public Function ApplicationDirectory() As String
            ApplicationDirectory = GetAppPath()
        End Function

        Public Function ApplicationDirectoryMacPac() As String
            ApplicationDirectoryMacPac = GetMacPacAppPath()
        End Function

        Public Function ApplicationDirectoryMacPac10(Optional bIncludeWritable As Boolean = True) As String
            ApplicationDirectoryMacPac10 = GetMacPac10AppPath(bIncludeWritable)
        End Function

        Public Function IsForteLocal() As Boolean
            IsForteLocal = bIsForteLocal()
        End Function

        Public Function GetIni(xSection As String, _
                               xKey As String, _
                               Optional xINIPath As String = "") As String
            Dim xValue As String
            Dim xIni As String
            Dim sbValue As StringBuilder

            If xINIPath = "" Then
                xINIPath = CIIni()
            End If

            xIni = xINIPath
            'xValue = Space(255) & &O0
            xValue = ""

            sbValue = New StringBuilder(500)

            'get requsestor from ini
            GetPrivateProfileString(xSection, _
                                    xKey, _
                                    xValue, _
                                    sbValue, _
                                    sbValue.Capacity, _
                                    xIni)

            GetIni = sbValue.ToString()
        End Function


        Public Function SetIni(xSection As String, _
                               xKey As String, _
                               xValue As String, _
                               Optional xINIPath As String = "") As Boolean
            Dim xIni As String

            If xINIPath = "" Then
                xINIPath = GetAppPath() & "\" & ciAppIni
            End If

            xIni = xINIPath
            WritePrivateProfileString(xSection, _
                                      xKey, _
                                      xValue, _
                                      xIni)

        End Function

        Public Function GetUserIni(ByVal xSection As String, _
                                    ByVal xKey As String)
            'returns the value for the specified key in the User.ini
            Dim xUserIni As String

            'get location of ci ini
            xUserIni = CIUserIni()

            If Len(Dir(xUserIni)) Then
                'user ini exists at specified location
                GetUserIni = GetIni(xSection, xKey, xUserIni)
            End If
        End Function

        Public Sub SetUserIni(ByVal xSection As String, _
                               ByVal xKey As String, _
                               ByVal xValue As String)
            'sets the value of the specified key in User.ini
            Dim xUserIni As String

            'get location of ci ini
            xUserIni = CIUserIni()

            If Len(Dir(xUserIni)) Then
                SetIni(xSection, xKey, xValue, xUserIni)
            End If
        End Sub

        Public Function SectionExists(ByVal xIniSection As String, _
                                      Optional ByVal xIni As String = "") As Boolean

            Dim PtrCh As IntPtr
            Dim iRet As Integer
            If xIni = "" Then
                xIni = GetAppPath() & ciAppIni
            End If

            PtrCh = Marshal.StringToHGlobalAnsi(New String(vbNullChar, 1024))
            iRet = GetPrivateProfileSection(xIniSection, PtrCh, 1024, xIni)

            Return iRet
        End Function

        Public Sub DeleteIniKey(xSection As String, _
                                xKey As String, _
                                xIni As String)
            'deletes the specified key
            'in the specified ini.
            WritePrivateProfileString(xSection, _
                                      xKey, _
                                      vbNullString, _
                                      xIni)
        End Sub

        Public Function GetUserVarPath(ByVal xPath As String) As String
            'substitutes user path for <UserName>
            Dim xBuf As String

            '   return supplied path if no variable is present
            If InStr(UCase(xPath), UCase("<UserName>")) = 0 Then
                GetUserVarPath = xPath
                Exit Function
            End If

            'get logon name
            xBuf = Space(255)
            GetUserName(xBuf, 255)

            If Len(xBuf) = 0 Then
                'alert to no logon name
                Throw New Exception("UserName is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            'trim extraneous buffer chars
            Trim(xBuf)
            xBuf = RTrim(xBuf)
            xBuf = Left(xBuf, Len(xBuf) - 1)

            '   substitute user name at specified point in path
            GetUserVarPath = Replace(xPath, "<UserName>", xBuf, , , vbTextCompare)
        End Function

        Public Function MacPacUserIni() As String
            Dim xPersonalFiles As String

            xPersonalFiles = MacPacUserFilesDir()

            If xPersonalFiles <> String.Empty Then
                MacPacUserIni = xPersonalFiles & "user.ini"
            End If
        End Function

        Public Function MacPacUserFilesDir() As String
            Dim xPath As String
            Dim xReturn As String = ""

            xPath = GetMacPacAppPath()

            Try
                xReturn = GetIni("File Paths", "UserDir", xPath & "\MacPac.ini")
            Catch
            End Try

            'CI2.3.5006 - for compatibility with pre 9.7.1
            If xReturn = "" Then
                Try
                    xReturn = GetIni("General", "UserDir", xPath & "\MacPac.ini")
                Catch
                End Try
            End If
            If Right$(xReturn, 1) <> "\" And xReturn <> String.Empty Then
                xReturn = xReturn & "\"
            End If
            MacPacUserFilesDir = xReturn
        End Function

        Public Function CIUserFilesDir() As String
            Dim xPath As String

            xPath = GetMacPacAppPath()

            Try
                xPath = GetIni("CIApplication", "UserDir", GetAppPath() & "\ci.ini")
            Catch
            End Try

            If Right(xPath, 1).ToString() <> "\" And xPath <> String.Empty Then
                xPath = xPath & "\"
            End If
            CIUserFilesDir = xPath
        End Function

        Public Function CIUserIni() As String
            Dim xCacheFolder As String = ""
            Dim xAppPath As String = ""

            'returns the location of the user ini
            Try
                If General.g_xUserIni = String.Empty Then
                    'CI 2.3.5003
                    General.g_xUserIni = GetIni("CIApplication", "UserDir", GetAppPath() & "\ci.ini")
                    If General.g_xUserIni <> "" Then
                        General.g_xUserIni = IIf(Right(General.g_xUserIni, 1) = "\", General.g_xUserIni & "ciUser.ini", General.g_xUserIni & "\ciUser.ini")
                    Else
                        General.g_xUserIni = GetIni("CIApplication", "UserINI", GetAppPath() & "\ci.ini")   'for back compatibility - pre 2.3.5003
                    End If

                    If General.g_xUserIni = String.Empty Then
                        'use the macpac user.ini if possible
                        General.g_xUserIni = MacPacUserIni()

                        If General.g_xUserIni = String.Empty Then
                            'assume it's in the same folder as this code
                            General.g_xUserIni = GetAppPath() & "\ciUser.ini"
                        End If
                    Else
                        'an ini was specified - deal with any tokens in path
                        If (InStr(UCase(General.g_xUserIni), "<USERNAME>") > 0) Or _
                                (InStr(UCase(General.g_xUserIni), "<USER>") > 0) Then
                            'contains the UserName or User token -
                            'get path from token string
                            General.g_xUserIni = GetUserVarPath(General.g_xUserIni)
                        Else
                            'contains an environment variable other than UserName-
                            'get path from token string
                            General.g_xUserIni = GetEnvironVarPath(General.g_xUserIni)
                        End If
                    End If

                    'v2.4.5
                    Dim xUserDir As String
                    '11 is len(\ciUser.ini)
                    xUserDir = Left(General.g_xUserIni, Len(General.g_xUserIni) - 11)

                    'Test for files and copy if necessary
                    'If (Not File.Exists(xUserDir & "\ci.mdb")) Or _
                    '(Not File.Exists(General.g_xUserIni)) Then
                    If (Not File.Exists(xUserDir & "\ci.mdb")) Then
                        'attempt to copy personal files from cache folder
                        Dim bCacheFound As Boolean
                        xAppPath = GetAppPath()
                        If UCase$(Right$(xAppPath, 5)) = "\APP\" Then
                            xCacheFolder = Left$(xAppPath, Len(xAppPath) - 4) & "UserCache\"
                            bCacheFound = Directory.Exists(xCacheFolder)
                        End If

                        'if not in new location, check in old
                        If Not bCacheFound Then
                            xCacheFolder = xAppPath & "UserCache"
                            bCacheFound = Directory.Exists(xCacheFolder)
                        End If

                        'deal with usercache
                        If bCacheFound Then
                            'cache folder exists - create specified directory if necessary
                            bCreatePath(xUserDir)

                            'if specified folder exists, copy personal files from cache;
                            'don't overwrite existing
                            If Directory.Exists(xUserDir) Then
                                Dim fileEntries As String() = Directory.GetFiles(xCacheFolder)
                                Dim fileName As String
                                'Process the list of files found in the directory.
                                'On Error Resume Next
                                For Each fileName In fileEntries
                                    File.Copy(fileName, _
                                        xUserDir & "\" & Path.GetFileName(fileName), False)
                                Next fileName
                                'On Error GoTo ProcError
                            End If
                        End If
                    End If

                    If Dir(General.g_xUserIni) = String.Empty Then
                        'MsgBox("Invalid user ini file specified in ci.ini: " & vbCrLf & vbCrLf & General.g_xUserIni)
                        Exit Function
                    End If
                End If
                CIUserIni = General.g_xUserIni
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            Dim xToken As String = ""
            Dim iPosStart As Integer
            Dim iPosEnd As Integer
            Dim xValue As String = ""
            Dim xDelimiter As String = ""

            iPosStart = InStr(xPath, "<")
            iPosEnd = InStr(xPath, ">")

            If (iPosStart > 0) And (iPosEnd > 0) Then
                xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
                xValue = Environ(xToken)
            Else
                'GLOG : 4062
                'v2.6.2001
                xDelimiter = "%"
                iPosStart = InStr(xPath, xDelimiter)
                iPosEnd = InStr(Mid(xPath, iPosStart + 1, Len(xPath)), xDelimiter)
                If iPosEnd Then
                    iPosEnd = iPosEnd + iPosStart
                End If
                If (iPosStart > 0) And (iPosEnd > 0) Then
                    xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
                    xValue = Environ(xToken)
                End If
            End If

            If xValue <> "" Then
                If xDelimiter = "" Then
                    GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
                Else
                    GetEnvironVarPath = Replace(xPath, xDelimiter & xToken & xDelimiter, xValue)
                End If
            Else
                GetEnvironVarPath = xPath
            End If

        End Function
#End Region
#Region "*************private methods***************"
        Private Function GetAppPath() As String
            'Subsitute for Functions.AppPath call: first looks in the registry for Public documents location
            'If not found, then uses regular Functions.AppPath command

            Dim oReg As CRegistry
            Dim xPath As String
            Dim xTemp As String
            Static xDir As String

            If xDir = "" Then
                oReg = New CRegistry
                'GLOG : 8909 : ceh
                'get registry key value
                xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\CI", "DataDirectory")
                'If Len(vPath) <= 0 Then
                ''try old registry key value
                'lRet = oReg.ReadValue(oReg.HKeyLocalMachine, "Software\Legal MacPac\9.x", "ContactIntegrationData", vPath)
                'End If

                'exists, so set path to value
                If Len(xPath) > 0 Then
                    'substitute real path for user var if it exists in string
                    If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                        'use API to get Windows user name
                        xTemp = GetUserVarPath(CStr(xPath))
                    ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                        xTemp = GetCommonDocumentsPath(CStr(xPath))
                    Else
                        'use environmental variable
                        xTemp = GetEnvironVarPath(CStr(xPath))
                    End If
                Else
                    'registry value empty, so set path to Functions.AppPath
                    xTemp = Functions.AppPath
                End If

                'raise error if path is invalid
                If Not Functions.DirExists(xTemp) Then
                    MsgBox(IIf(xTemp = "", "", xTemp) & "CI.ini could not be found.  Please contact your administrator.", _
                           vbExclamation, General.g_oSessionType.AppTitle)
                    GetAppPath = String.Empty
                    Exit Function
                End If

                'store for future retrieval
                xDir = xTemp
            End If
            GetAppPath = xDir
        End Function

        Private Function GetMacPacAppPath() As String
            'Looks for MacPac Functions.AppPath

            Dim oReg As CRegistry
            Dim xPath As String = ""
            Dim lRet As Long
            Dim xTemp As String
            Static xDir As String

            oReg = New CRegistry
            'get registry key value
            xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\MacPac 9.0", "DataDirectory")

            If Len(xPath) <= 0 Then
                'try old registry key value
                xPath = oReg.GetLocalMachineValue("Software\Legal MacPac\9.x", "MacPacData")
            End If

            'exists, so set path to value
            If Len(xPath) > 0 Then
                '       substitute real path for user var if it exists in string
                If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                    '       use API to get Windows user name
                    xTemp = GetUserVarPath(CStr(xPath))
                ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                    xTemp = GetCommonDocumentsPath(CStr(xPath))
                Else
                    '       use environmental variable
                    xTemp = GetEnvironVarPath(CStr(xPath))
                End If
            Else
                'registry value empty, so set path to MacPac's Application Path
                'xTemp = oReg.GetDLLOCXPath("MacPac90.Application")
                xTemp = GetAppPath().ToString()
            End If

            '   raise error if path is invalid
            If Not Functions.DirExists(xTemp) Then
                MsgBox(IIf(xTemp = "", "", xTemp & "\") & "CI.ini could not be found.  Please contact your administrator.", _
                       vbExclamation, General.g_oSessionType.AppTitle)
                GetMacPacAppPath = ""
                Exit Function
            End If

            '   store for future retrieval
            xDir = xTemp
            GetMacPacAppPath = xTemp
        End Function

        'GLOG : 8259 : ceh
        Private Function bIsForteLocal() As Boolean
            Dim oReg As CRegistry
            Dim xValue As String = ""
            Dim bIsLocal As Boolean

            oReg = New CRegistry

            'GLOG : 8909 : ceh
            xValue = oReg.GetLocalMachineValue("Software\The Sackett Group\Deca", "FullyLocal")

            'exists, so set path to value
            If xValue = "1" Then
                bIsLocal = True
            End If

            bIsForteLocal = bIsLocal

        End Function

        Private Function GetMacPac10AppPath(Optional IncludeWritable As Boolean = True) As String
            'Looks for MacPac Functions.AppPath

            Dim oReg As CRegistry
            Dim xPath As String = ""
            Dim xTemp As String = ""

            oReg = New CRegistry

            If IncludeWritable Then
                'get registry key value
                'GLOG2 : 6732 : CEH
                'GLOG : 8909 : ceh
                xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\Deca", "WritableDataDirectory")
            End If

            If Len(xPath) <= 0 Then
                'try DataDirectory registry key value
                xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\Deca", "DataDirectory")
                If Len(xPath) <= 0 Then
                    'try RootDirectory registry key value
                    xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\Deca", "RootDirectory")
                    If Len(xPath) > 0 Then
                        xPath = xPath & "\Data"
                    End If
                    MsgBox("xpath1 RootDirectory = " & xPath)
                End If
            End If

            'exists, so set path to value
            If Len(xPath) > 0 Then
                'substitute real path for user var if it exists in string
                If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                    'use API to get Windows user name
                    xTemp = GetUserVarPath(CStr(xPath))
                ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                    xTemp = GetCommonDocumentsPath(CStr(xPath))
                Else
                    'use environmental variable
                    xTemp = GetEnvironVarPath(CStr(xPath))
                End If
            End If

            'raise error if path is invalid
            If Not Functions.DirExists(xTemp) Then
                'GLOG : 7787 : ceh
                MsgBox(IIf(xTemp = "", "", xTemp & "\") & " could not be found.  Please contact your administrator.", _
                       vbExclamation, General.g_oSessionType.AppTitle)
                GetMacPac10AppPath = ""
                Exit Function
            End If

            GetMacPac10AppPath = xTemp
        End Function

        Private Function GetCommonDocumentsPath(ByVal xPath As String) As String
            'substitutes user path for <COMMON_DOCUMENTS>
            Dim xBuf As String

            'get common documents folder
            xBuf = Space(255)

            SHGetFolderPath(0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf)

            If Len(xBuf) = 0 Then
                '       alert to no logon name
                Throw New Exception("Common_Documents is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   trim extraneous buffer chars
            xBuf = RTrim(xBuf)
            xBuf = Left(xBuf, Len(xBuf) - 1)

            '   substitute common documents at specified point in path
            GetCommonDocumentsPath = Replace(xPath, "<COMMON_DOCUMENTS>", xBuf)

        End Function


        Private Function bCreatePath(xFullPath As String) As Boolean
            Dim xParent As String = ""
            Dim oParent As DirectoryInfo

            bCreatePath = False

            If Directory.Exists(xFullPath) Then  ' Folder already created
                bCreatePath = True
                Exit Function
            End If
            On Error Resume Next

            Directory.CreateDirectory(xFullPath)

            If Not Directory.Exists(xFullPath) Then
                ' Couldn't create bottom level folder, call recursively using Parent Folder
                ' Only only the last level may created new
                oParent = Directory.GetParent(xFullPath)
                xParent = oParent.FullName
                If xParent = String.Empty Or Right(xParent, 1) = "\" Then ' Parent folder is Drive Root or nothing
                    bCreatePath = False
                Else
                    If bCreatePath(xParent) Then
                        Directory.CreateDirectory(xFullPath)
                        If Directory.Exists(xFullPath) Then
                            bCreatePath = True
                        End If
                    End If
                End If
            Else
                bCreatePath = True
            End If
        End Function
#End Region
    End Class
End Namespace