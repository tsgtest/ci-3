'**********************************************************
'   CListings Collection Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of listings in a folder or store
'**********************************************************
Option Explicit On

Namespace LMP.CIO
    Public Class CListingsWXarray
#Region "******************declarations***********************"
        Private Enum ciListingCols
            ciListingCols.ciListingCols_UNID = 0
            ciListingCols.ciListingCols_LastName = 1
            ciListingCols.ciListingCols_Company = 2
            ciListingCols.ciListingCols_DisplayName = 3
            ciListingCols.ciListingCols_Type = 4
        End Enum

        Event ListingAdded(oListing As CIO.CListing)
        Event ListingDeleted()
        Private m_oArray As xArray
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oArray = New xArray
        End Sub
#End Region
#Region "*********************fields**************************"
        Public Property xArray As xArray
            Get
                Return m_oArray
            End Get
            Set(value As xArray)
                m_oArray = value
            End Set
        End Property
#End Region
#Region "*********************methods**************************"

        Public Sub Add(oL As CIO.CListing)
            m_oArray.Insert(1, m_oArray.UpperBound(1) + 1)
            With m_oArray
                .Value(.UpperBound(1), ciListingCols.ciListingCols_DisplayName) = oL.DisplayName
                .Value(.UpperBound(1), ciListingCols.ciListingCols_LastName) = oL.LastName
                .Value(.UpperBound(1), ciListingCols.ciListingCols_UNID) = oL.UNID
                .Value(.UpperBound(1), ciListingCols.ciListingCols_Company) = oL.Company
                .Value(.UpperBound(1), ciListingCols.ciListingCols_Type) = oL.ListingType
            End With
            RaiseEvent ListingAdded(oL)
            Exit Sub
ProcError:
            General.g_oError.RaiseError("CListings.Item"
        End Sub

        Public Sub DeleteAll()
            m_oArray.Clear()
        End Sub

        Public Function Count() As Long
            On Error Resume Next
            Count = m_oArray.Count(1)
        End Function

        Public Function Item(Index As Long) As CIO.CListing
            Dim oL As CIO.CListing
            oL = New CIO.CListing

            On Error GoTo ProcError

            Index = ciMax(CDbl(Index), 0)
            With m_oArray
                On Error Resume Next
                oL.DisplayName = .Value(Index, ciListingCols.ciListingCols_DisplayName)
                oL.UNID = .Value(Index, ciListingCols.ciListingCols_ID)
                oL.Company = .Value(Index, ciListingCols.ciListingCols_Company)
                oL.LastName = .Value(Index, ciListingCols.ciListingCols_LastName)
                oL.ListingType = .Value(Index, ciListingCols.ciListingCols_Type)
            End With

            Item = oL
            Exit Function
ProcError:
            General.g_oError.RaiseError("CListings.Item"
        End Function
#End Region
    End Class
End Namespace


