'**********************************************************
'   CAddresses Collection Class
'   created 12/25/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of CAddresses a listing
'**********************************************************
Option Explicit On

Imports System.Collections
Imports System.Data
Imports LMP

Namespace TSG.CI
    Public Class CAddresses
        Implements System.Collections.IEnumerable
#Region "******************fields***********************"
        Private m_oAddresses As Collection
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oAddresses = New Collection
        End Sub
#End Region
#Region "******************properties***********************"
        Default ReadOnly Property Item(vKey As Object) As CAddress
            Get
                Try
                    Return m_oAddresses(vKey)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
#End Region
#Region "******************methods***********************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Try
                GetEnumerator = m_oAddresses.GetEnumerator
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        'Public Function Add(oAddress As CAddress, Optional Before As Integer = 0)
        '    Try
        '        If Before = 0 Then
        '            m_oAddresses.Add(oAddress, oAddress.Name)
        '        Else
        '            m_oAddresses.Add(oAddress, , Before)
        '        End If
        '    Catch
        '    End Try
        'End Function

        Public Function Add(oAddress As CAddress)
            Try
                m_oAddresses.Add(oAddress, oAddress.Name)
            Catch
            End Try
        End Function

        Public Function Add(oAddress As CAddress, Before As Integer)
            Try
                m_oAddresses.Add(oAddress, , Before)
            Catch
            End Try
        End Function

        Public Function Delete(vKey As Object)
            Try
                m_oAddresses.Remove(vKey)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Count() As Integer
            Try
                Count = m_oAddresses.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetArray() As DataTable
            Dim oAddress As CAddress
            Dim xAddress(0, 1) As String
            Dim oDT As New DataTable

            Try
                oDT.Columns.Add("Name")
                oDT.Columns.Add("ID")

                For Each oAddress In m_oAddresses
                    oDT.Rows.Add(oAddress.Name, oAddress.ID)
                Next oAddress

                GetArray = oDT
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

#End Region
    End Class
End Namespace
