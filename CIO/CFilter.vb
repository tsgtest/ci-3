Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Enum ciSearchOperators
        ciSearchOperator_Equals = 1
        ciSearchOperator_BeginsWith = 2
        ciSearchOperator_Contains = 4
    End Enum
    Public Class CFilter
#Region "******************fields***********************"
        'ToDo - original declaration  Private m_oFilterFields(1 To 4) As CFilterField
        Private m_oFilterFields(0 To 3) As CFilterField
        Private m_lSortCol As Long
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            Dim i As Integer

            Try
                For i = 0 To 3
                    m_oFilterFields(i) = New CFilterField
                Next i
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************properties**************************"
        Public Property SortColumn As ciListingCols
            Get
                Return m_lSortCol
            End Get
            Set(value As ciListingCols)
                m_lSortCol = value
            End Set
        End Property
#End Region
#Region "******************methods**************************"
        Public Function IsEmpty() As Boolean
            'returns TRUE if no values have been set in the filter
            Dim i As Integer

            'cycle through filter fields
            For i = 0 To 3
                If FilterFields(i).Value <> String.Empty Then
                    'filter is not empty - exit function
                    IsEmpty = False
                    Exit Function
                End If
            Next i

            'if we got here, no values have been set
            IsEmpty = True
        End Function

        Public Function Reset() As Boolean
            'clears values in the filter
            Dim i As Integer

            'cycle through filter fields
            For i = 0 To 3
                FilterFields(i).Value = String.Empty
            Next i
        End Function

        Public Function FilterFields(iIndex As Integer) As CFilterField
            Try
                If iIndex < 0 Or iIndex > 3 Then
                    Throw New Exception("Subscript out of range.")
                    Return Nothing
                End If

                FilterFields = m_oFilterFields(iIndex)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function CountFields() As Integer
            'returns the number of filter fields specified
            Dim i As Integer
            Dim iCount As Integer = 0

            CountFields = iCount
            Try
                For i = 0 To 3
                    If FilterFields(i).ID <> String.Empty Then
                        iCount = iCount + 1
                    Else
                        Exit For
                    End If
                Next i
                CountFields = iCount
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace
