Option Explicit On

Imports System.Runtime.InteropServices
Imports LMP

Namespace TSG.CI
    Public Class Functions
#Region "******************fields***********************"
        Private Declare Sub OutputDebugString Lib "kernel32" _
          Alias "OutputDebugStringA" _
          (ByVal lpOutputString As String)

        <DllImport("User32.dll")>
        Public Shared Function LockWindowUpdate(ByVal hwndLock As IntPtr) As Boolean
        End Function

        <DllImport("User32.dll")>
        Friend Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
        End Function

#End Region
#Region "******************methods***********************"
        Public Shared Function DirExists(ByVal xDirName As String) As Boolean
            DirExists = False
            Try
                DirExists = (GetAttr(xDirName) And vbDirectory) = vbDirectory
            Catch
            End Try
        End Function

        Public Shared Sub DebugPrint(xOutput As String)
            System.Diagnostics.Debug.Print(xOutput)
            OutputDebugString(xOutput)
        End Sub

        Public Shared Function AppPath() As String
            Return System.AppDomain.CurrentDomain.BaseDirectory()
        End Function

        'returns the screen scaling factor
        Public Shared Function GetScalingFactor() As Single
            Dim g As System.Drawing.Graphics

            g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero)
            Return g.DpiX / 96
        End Function

        Public Shared Sub EchoOff()
            Call LockWindowUpdate(FindWindow("OpusApp", 0&))
        End Sub

        Public Shared Sub EchoOn()
            Call LockWindowUpdate(0&)
        End Sub

#End Region
    End Class
End Namespace
