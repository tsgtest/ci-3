Option Explicit On

Imports System.Runtime.InteropServices
Imports LMP

Namespace TSG.CI
#Region "******************enumerations***********************"
    Public Enum ciErrs
        ciErr_MissingOrInvalidINIKey = 10 + 512 + 1     'vbError + 512 + 1
        ciErr_InvalidUNID
        ciErr_InvalidFilterField
        ciErr_NotImplemented
        ciErr_BackendLogonCancelled
        ciErr_CouldNotSetFilterField
        ciErr_MissingFile
        ciErr_InvalidListing
        ciErr_InvalidUserINI
        ciErr_invalidAddress
        ciErr_CouldNotAddToCollection
        ciErr_InvalidContactNumberType
        ciErr_InvalidContactDetailFormatString
        ciErr_DefaultFolderDoesNotExist
        ciErr_NoConnectedBackends
        ciErr_InvalidColumnParameter
        ciErr_ODBCError
        ciErr_CouldNotConnectToBackend
        ciErr_CouldNotExecuteSQL
        ciErr_CouldNotBootBackend
        ciErr_CouldNotReadRegistry
        ciErr_AccessDenied
        ciErr_InvalidAddressIndex
    End Enum

    Public Enum ciDebugMessageTypes
        ciDebugMessageType_Info
        ciDebugMessageType_Warning
        ciDebugMessageType_Error
    End Enum

#End Region
    Public Class CError
        Implements ICError
#Region "******************fields***********************"
        'Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)
        <DllImport("kernel32.dll")> _
        Shared Sub OutputDebugString(ByVal lpOutputString As String)
        End Sub

        Private m_iDebugMode As Integer
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties**************************"
        Public ReadOnly Property DebugMode As Boolean
            Get
                Try
                    Const DEBUG_ON As Byte = 1
                    Const DEBUG_OFF As Byte = 2

                    Dim oIni As CIni

                    If m_iDebugMode = 0 Then
                        'get whether we're in debug mode or not
                        oIni = New CIni
                        m_iDebugMode = IIf(Trim$(UCase$(oIni.GetIni("CIApplication", "Debug"))) = _
                                           "TRUE", DEBUG_ON, DEBUG_OFF)
                        oIni = Nothing
                    End If

                    DebugMode = (m_iDebugMode = DEBUG_ON)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
#End Region
#Region "******************methods**************************"
        Public Sub SendToDebug(ByVal xMessage As String, ByVal xSource As String, Optional ByVal iMsgType As ciDebugMessageTypes = ciDebugMessageTypes.ciDebugMessageType_Info)
            'sends the supplied message to debug if ci is in debug mode
            Dim oIni As New CIni

            Try
                If DebugMode = True Then
                    'send message to debug log
                    Dim swFile As New System.IO.StreamWriter(oIni.ApplicationDirectory & "\ciDebug.log")

                    If iMsgType = ciDebugMessageTypes.ciDebugMessageType_Warning Then
                        'print warning
                        swFile.WriteLine(Format(Now(), "mm-dd-yy hh:mm:ss") & "  ***Warning***  " & xSource)
                    ElseIf iMsgType = ciDebugMessageTypes.ciDebugMessageType_Error Then
                        'print error
                        swFile.WriteLine(Format(Now(), "mm-dd-yy hh:mm:ss") & "  ***ERROR***  " & xSource)
                    Else
                        'print info
                        swFile.WriteLine(Format(Now(), "mm-dd-yy hh:mm:ss") & "  " & xSource)
                    End If

                    swFile.WriteLine("    " & xMessage)

                    'print blank line
                    swFile.WriteLine("")

                    swFile.Close()
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub RaiseError(ByVal xNewSource As String) Implements ICError.RaiseError
            'raises the current error, appending the source
            With Err()
                If InStr(.Source, ".") Then
                    '           an originating source has already been specified - keep it
                    .Raise(.Number, .Source, .Description)
                Else
                    '           no originating source has been specified - use the one supplied
                    .Raise(.Number, xNewSource, .Description)
                End If
            End With
        End Sub

        Public Sub ShowError() Implements ICError.ShowError
            Dim lNum As Long
            Dim xDesc As String
            Dim xSource As String

            With Err()
                lNum = .Number
                xDesc = .Description
                xSource = .Source
            End With

            SendToDebug("ERROR #" & lNum & " - " & xDesc, _
                xSource, ciDebugMessageTypes.ciDebugMessageType_Error)

            MsgBox("The following unexpected error occurred: " & vbCrLf & vbCrLf & _
                "Number:  " & lNum & vbCr & _
                "Description:  " & xDesc & vbCr & _
                "Source:  " & xSource, vbExclamation, General.g_oSessionType.AppTitle)
        End Sub
#End Region
    End Class
End Namespace


