'**********************************************************
'   CContacts Collection Class
'   created 8/4/98 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of store objects
'**********************************************************
Option Explicit On
Imports System.Collections
Imports System.Data
Imports LMP


Namespace TSG.CI
#Region "******************enumerations***********************"
    Public Enum ciMergeFileFormats
        ciMergeFileFormat_Word = 1
        ciMergeFileFormat_CSV = 2
        ciMergeFileFormat_TabDelim = 3
    End Enum

    'GLOG : 8819 : ceh
    'Public Enum ciContactTypes
    '    ciContactType_To = 1
    '    ciContactType_From = 2
    '    ciContactType_CC = 4
    '    ciContactType_BCC = 8
    '    ciContactType_Other = 16
    'End Enum
#End Region
    Public Class CContacts
        Implements System.Collections.IEnumerable
        Implements ICContacts

        Private Class ContactComparer : Implements IComparer
            Function Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
                Return String.Compare(x, y)
            End Function
        End Class
#Region "******************fields***********************"
        Private m_oContactsCol As Collection
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oContactsCol = New Collection
        End Sub
#End Region
#Region "******************collection methods***********************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Try
                GetEnumerator = m_oContactsCol.GetEnumerator
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub Add(oContact As ICContact, Optional Before As Long = 0) Implements ICContacts.Add
            'key modification allows same contact to be
            'added once per selection list - not truly unique
            'but for all practical purposes, it is so.
            Dim xKey As String
            Dim i As Integer

            Try
                Randomize()
                Dim xRnd As String
                xRnd = Rnd()

                'xKey = oContact.UNID & xAddressID
                xKey = oContact.UNID & "+" & xRnd

                Try
                    'the "before" param is here so that we can
                    'move contacts around in the collection
                    If Before <> 0 Then
                        m_oContactsCol.Add(oContact, xKey, Before)
                    Else
                        m_oContactsCol.Add(oContact, xKey)
                    End If
                Catch
                End Try

                'already added at least once - change key to allow contact to be added again
                While Err.Number = 457
                    Err.Clear()
                    i = i + 1
                    xKey = xKey & i

                    Try
                        If Before <> String.Empty Then
                            m_oContactsCol.Add(oContact, xKey, Before)
                        Else
                            m_oContactsCol.Add(oContact, xKey)
                        End If
                    Catch
                    End Try
                End While

                'GLOG : 15764 : ceh
                'If Err.Number > 0 Then
                '    Throw New Exception(Err.Description)
                'End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function Delete(vKey As Object)
            Try
                m_oContactsCol.Remove(vKey)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub DeleteAll()
            Try
                m_oContactsCol = New Collection
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function Count() As Integer Implements ICContacts.Count
            Count = 0
            Try
                Count = m_oContactsCol.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Item(vKey As Object) As ICContact Implements ICContacts.Item
            Try
                Item = m_oContactsCol.Item(vKey)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
#Region "******************methods***********************"
        Public Sub FillList(ctlP As Object)
            Dim i As Integer

            Try
                For i = 1 To Me.Count
                    ctlP.AddItem(Me.Item(i).DisplayName)
                Next i
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function GetList(Optional ByVal iContactType As ciContactTypes = 0, Optional bSort As Boolean = False) As DataTable
            Dim oDT As New DataTable
            Dim xContact(0, 1) As String
            Dim oContact As ICContact
            Dim iCount As Integer

            Try
                'Add two columns
                oDT.Columns.Add("DisplayName")
                oDT.Columns.Add("Count")

                'cycle through contacts,
                'adding each to array
                For Each oContact In m_oContactsCol
                    iCount += 1
                    If iContactType > 0 Then
                        'get only those contacts of the specified type
                        If oContact.ContactType = iContactType Then
                            'contact is of the specified type
                            oDT.Rows.Add(oContact.DisplayName, iCount)
                        End If
                    Else
                        'get all contacts in collection
                        oDT.Rows.Add(oContact.DisplayName, iCount)
                    End If
                Next oContact

                If bSort Then
                    'sort by display name
                    oDT.DefaultView.Sort = "DisplayName ASC"
                    oDT = oDT.DefaultView.ToTable
                End If

                GetList = oDT
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub CreateDataFile(ByVal xFile As String) Implements ICContacts.CreateDataFile
            'creates a merge data file from contacts in collection
            Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
            Dim xHeader As String
            Dim oCustFlds As CCustomFields
            Dim i As Integer
            Dim j As Integer
            Dim xRecord As String
            Dim oIni As CIni
            Dim oStr As CStrings

            Try
                oIni = New CIni
                oStr = New CStrings

                xHeader = oIni.GetIni("CIApplication", "MergeFileHeader")

                Dim swFile As New System.IO.StreamWriter(xFile)

                'create header; use ini setting only if accurate
                If Not (xHeader <> "" And oStr.lCountChrs(xHeader, "|") = 22) Then
                    xHeader = ciDataFileHeader
                End If

                'get custom fields - we can use the first backend since each backend has the
                'same custom fields (although some fields may not be linked to a data field)
                oCustFlds = Me.Item(1).CustomFields
                For j = 1 To oCustFlds.Count
                    xHeader = xHeader & "|" & oCustFlds(j).Name
                Next j

                'write header
                swFile.WriteLine(xHeader)

                'cycle through contacts, writing to file
                For i = 1 To Me.Count
                    With Me.Item(i)
                        xRecord = """" & .AddressTypeName & """" & _
                                    "|" & .ContactType & _
                                    "|""" & .DisplayName & """" & _
                                    "|""" & .Prefix & """" & _
                                    "|""" & .FirstName & """" & _
                                    "|""" & .MiddleName & """" & _
                                    "|""" & .LastName & """" & _
                                    "|""" & .Suffix & """" & _
                                    "|""" & .Initials & """" & _
                                    "|""" & .Street1 & """" & _
                                    "|""" & .Street2 & """" & _
                                    "|""" & .Street3 & """" & _
                                    "|""" & .City & """" & _
                                    "|""" & .State & """" & _
                                    "|""" & .ZipCode & """" & _
                                    "|""" & .Country & """" & _
                                    "|""" & .Company & """" & _
                                    "|""" & .Department & """" & _
                                    "|""" & .Title & """" & _
                                    "|""" & .EmailAddress & """" & _
                                    "|""" & .Phone & """" & _
                                    "|""" & .PhoneExtension & """" & _
                                    "|""" & .Fax & """"

                        For j = 1 To .CustomFields.Count
                            xRecord = xRecord & "|""" & .CustomFields(j).Value & """"
                        Next j

                        xRecord = Replace(xRecord, vbCrLf, "<1310>")
                        xRecord = Replace(xRecord, vbCr, "<13>")
                        xRecord = Replace(xRecord, Chr(11), "<11>")

                        swFile.WriteLine(xRecord)
                    End With
                Next i

                oIni = Nothing

                'close file
                swFile.Close()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
    End Class
End Namespace
