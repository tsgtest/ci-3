'**********************************************************
'   CBackend Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods of a CI backend-
'   there are a number of unimplemented properties and methods-
'   an actual backend will have to implement these
'**********************************************************
Option Explicit On

Namespace LMP.CIO
    Public Class CBackend
#Region "******************declarations***********************"
        Private m_iID As Integer
        Private m_xName As String
        Private m_xProgID As String
        Private m_bShowRootNode As Boolean
#End Region
#Region "******************fields***********************"
        'CI Backend ID - this corresponds to the ini backend section index
        Public Property ID As Integer
            Get
                Return m_iID
            End Get
            Set(value As Integer)
                m_iID = value
            End Set
        End Property

        Public Property Name As String
            Get
                Return m_xName
            End Get
            Set(value As String)
                m_xName = value
            End Set
        End Property

        Public Property ProgID As String
            Get
                Return m_xProgID
            End Get
            Set(value As String)
                m_xProgID = value
            End Set
        End Property

        Public Property ShowRootNode As Boolean
            Get
                Return m_bShowRootNode
            End Get
            Set(value As Boolean)
                m_bShowRootNode = value
            End Set
        End Property
#End Region
#Region "******************methods***********************"
        Public Function FilterFields() As CIO.CFilterFields
            'returns the collection of filter fields for this backend
            Dim oIni As CIO.CIni
            Dim oFFs As CIO.CFilterFields
            Dim xFFSec As String
            Dim xTemp As String
            Dim iPos As Integer
            Dim iPosEqual As Integer
            Dim xName As String
            Dim xID As String

            On Error GoTo ProcError

            'create new empty filter fields collection
            oFFs = New CIO.CFilterFields

            'get ini section for filter fields for this backend
            oIni = New CIO.CIni
            xFFSec = oIni.GetIniSection("FilterFields" & m_iID, oIni.CIIni)

            While Len(xFFSec)
                iPos = InStr(xFFSec, Chr(0))
                If iPos Then
                    'parse key from section string
                    xTemp = Left(xFFSec, iPos - 1)
                    xFFSec = Mid(xFFSec, iPos + 1)
                Else
                    'no separators left, take last key
                    xTemp = xFFSec
                    xFFSec = ""
                End If

                'add field from ini key
                iPosEqual = InStr(xTemp, "=")
                If iPosEqual Then
                    xName = Left(xTemp, iPosEqual - 1)
                    xID = Mid(xTemp, iPosEqual + 1)
                    If Len(xName) And Len(xID) Then
                        'both name and ID values exist -
                        'add to collection
                        oFFs.Add(xName, xID)
                    Else
                        'name and/or id is missing - alert
                        Err.Raise(ciErrs.ciErr_MissingOrInvalidINIKey, , _
                            "Invalid key in FilterFields" & _
                            m_iID & " section of CI.ini")
                    End If
                Else
                    '=' sign is missing - alert
                    Err.Raise(ciErrs.ciErr_MissingOrInvalidINIKey, , _
                        "Invalid key in FilterFields" & _
                        m_iID & " section of CI.ini")
                End If
            End While
            FilterFields = oFFs
            Exit Function
ProcError:
            General.g_oError.RaiseError("CBackend.FilterFields")
        End Function
#End Region
#Region "******************CIBackend Interface***********************"
        Public ReadOnly Property SupportsMultipleStores() As Boolean
            Get
            End Get
        End Property

        Public ReadOnly Property SupportsFolders() As Boolean
            Get
            End Get
        End Property

        Public ReadOnly Property SupportsNestedFolders() As Boolean
            Get
            End Get
        End Property

        Public Function GetStores() As CIO.CStores

        End Function

        Public Function GetFolders(oStore As CIO.CStore) As CIO.CFolders

        End Function

        Public Function GetSubFolders(oFolder As CIO.CFolder) As CIO.CFolders

        End Function

        Public Function GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter = Nothing) As CIO.CListings

        End Function

        Public Function GetStoreListings(oStore As CIO.CStore, Optional oFilter As CIO.CFilter = Nothing) As CIO.CListings

        End Function

        Public Function GetAddresses(oListing As CIO.CListing) As CIO.CAddresses

        End Function

        Public Function GetContact(oListing As CIO.CListing, oAddress As CIO.CAddress) As CIO.CContact

        End Function

        Public Sub EditContact(oListing As CIO.CListing)

        End Sub
#End Region
    End Class
End Namespace

