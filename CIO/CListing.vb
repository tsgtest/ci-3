'**********************************************************
'   CListing Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that
'   define a CI listing
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Enum ciListingType
        ciListingType_Person = 1
        ciListingType_Group = 2
    End Enum
    Public Class CListing
#Region "******************fields***********************"
        Private m_vUNID As Object
        Private m_xDisplayName As String
        Private m_xCol1 As String
        Private m_xCol2 As String
        Private m_xCol3 As String
        Private m_xCol4 As String
        Private m_iListingType As ciListingType
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties**************************"
        Public ReadOnly Property BackendID As Integer
            Get
                BackendID = 0
                Try
                    BackendID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Backend)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property StoreID As Object
            Get
                StoreID = Nothing
                Try
                    StoreID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Store)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property FolderID As Object
            Get
                FolderID = Nothing
                Try
                    FolderID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Folder)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ID As Object
            Get
                ID = Nothing
                Try
                    ID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Listing)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public Property UNID As Object
            Get
                Return m_vUNID
            End Get
            Set(value As Object)
                m_vUNID = value
            End Set
        End Property

        Public Property DisplayName As String
            Get
                'Attribute DisplayName.VB_UserMemId = 0
                DisplayName = m_xDisplayName
            End Get
            Set(value As String)
                m_xDisplayName = value
            End Set
        End Property

        Public Property AdditionalProperty1 As String
            Get
                Return m_xCol1
            End Get
            Set(value As String)
                m_xCol1 = value
            End Set
        End Property

        Public Property AdditionalProperty2 As String
            Get
                Return m_xCol2
            End Get
            Set(value As String)
                m_xCol2 = value
            End Set
        End Property

        Public Property AdditionalProperty3 As String
            Get
                Return m_xCol3
            End Get
            Set(value As String)
                m_xCol3 = value
            End Set
        End Property

        Public Property FolderPath As String
            Get
                Return m_xCol4
            End Get
            Set(value As String)
                m_xCol4 = value
            End Set
        End Property

        Public Property ListingType As ciListingType
            Get
                Return m_iListingType
            End Get
            Set(value As ciListingType)
                m_iListingType = value
            End Set
        End Property

#End Region
#Region "******************methods**************************"
        Public Function BuildDisplayName(oContact As ICContact, Optional ByVal xDefaultDisplay As String = "") As String
            'constructs and returns a name for display purposes
            Dim xName As String = ""

            BuildDisplayName = ""

            Try
                With oContact
                    If .LastName <> "" And .FirstName <> "" And .MiddleName <> "" Then
                        xName = .LastName & ", " & .FirstName & " " & .MiddleName
                    ElseIf .LastName <> "" And .FirstName <> "" Then
                        xName = .LastName & ", " & .FirstName
                    ElseIf .LastName <> "" Then
                        xName = .LastName
                    ElseIf .FirstName <> "" Then
                        xName = .FirstName
                    ElseIf .Company <> "" Then
                        xName = .Company
                    ElseIf xDefaultDisplay <> String.Empty Then
                        xName = xDefaultDisplay
                    Else
                        xName = ""
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                BuildDisplayName = xName
            End Try
        End Function
#End Region
    End Class
End Namespace