'**********************************************************
'   Store Class
'   created 12/20/00 by Daniel Fisherman
'   Contains properties and methods defining a
'   physical data store of a backend
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CStore
#Region "******************fields***********************"
        Private m_vUNID As Object
        Private m_xName As String
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "*********************properties**************************"
        Public Property UNID As Object
            Get
                Return m_vUNID
            End Get
            Set(value As Object)
                m_vUNID = value
            End Set
        End Property

        Public ReadOnly Property BackendID As Integer
            Get
                BackendID = 0
                Try
                    BackendID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Backend)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ID As Object
            Get
                'returns the ID of the store -
                'parses it from the UNID
                Dim iPos As Integer
                Dim oConst As CConstants

                ID = Nothing
                Try
                    oConst = New CConstants

                    iPos = InStr(CStr(m_vUNID), oConst.UNIDSep)

                    If iPos Then
                        ID = Mid(m_vUNID, iPos + Len(oConst.UNIDSep))
                    Else
                        'something is wrong - all store UNIDs
                        'should have two elements separated by
                        'a separator
                        Dim xDesc As String
                        xDesc = "Invalid Store UNID: " & CStr(m_vUNID)
                        Throw New Exception(xDesc)
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public Property Name As String
            Get
                'Attribute Name.VB_UserMemId = 0
                Name = m_xName
            End Get
            Set(value As String)
                m_xName = value
            End Set
        End Property
#End Region
    End Class
End Namespace