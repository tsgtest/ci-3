Option Explicit On

Namespace TSG.CI
#Region "******************enumerations***********************"
    Public Enum ciContactNumberTypes
        ciContactNumberType_Phone = 1
        ciContactNumberType_Fax = 2
        ciContactNumberType_EMail = 3
    End Enum
#End Region
    Public Class CContactNumber
#Region "******************fields***********************"
        Private m_xDesc As String
        Private m_xNumber As String
        Private m_xExtension As String
        Private m_xCategory As String
        Private m_iType As Integer
        Private m_vID As Object
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property ID As Object
            Get
                Return m_vID
            End Get
            Set(value As Object)
                m_vID = value
            End Set
        End Property

        Public Property NumberType As ciContactNumberTypes
            Get
                Return m_iType
            End Get
            Set(value As ciContactNumberTypes)
                m_iType = value
            End Set
        End Property

        Public Property Category As String
            Get
                Return m_xCategory
            End Get
            Set(value As String)
                m_xCategory = value
            End Set
        End Property

        Public Property Description As String
            Get
                Return m_xDesc
            End Get
            Set(value As String)
                m_xDesc = value
            End Set
        End Property

        Public Property Number As String
            Get
                Return m_xNumber
            End Get
            Set(value As String)
                m_xNumber = value
            End Set
        End Property

        Public Property Extension As String
            Get
                Return m_xExtension
            End Get
            Set(value As String)
                m_xExtension = value
            End Set
        End Property
#End Region
    End Class
End Namespace

