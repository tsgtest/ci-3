﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AlertNoContactNumbersFormNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AlertNoContactNumbersFormNew))
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.chkPrompt = New System.Windows.Forms.CheckBox()
        Me.btnYes = New System.Windows.Forms.Button()
        Me.btnNo = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(27, 18)
        Me.lblMessage.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(21, 13)
        Me.lblMessage.TabIndex = 0
        Me.lblMessage.Text = "##"
        '
        'chkPrompt
        '
        Me.chkPrompt.AutoSize = True
        Me.chkPrompt.Checked = True
        Me.chkPrompt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrompt.Location = New System.Drawing.Point(28, 75)
        Me.chkPrompt.Margin = New System.Windows.Forms.Padding(2)
        Me.chkPrompt.Name = "chkPrompt"
        Me.chkPrompt.Size = New System.Drawing.Size(280, 17)
        Me.chkPrompt.TabIndex = 1
        Me.chkPrompt.Text = "&Continue to alert when no contact numbers are found."
        Me.chkPrompt.UseVisualStyleBackColor = True
        '
        'btnYes
        '
        Me.btnYes.Location = New System.Drawing.Point(92, 104)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(93, 33)
        Me.btnYes.TabIndex = 2
        Me.btnYes.Text = "&Yes"
        Me.btnYes.UseVisualStyleBackColor = True
        '
        'btnNo
        '
        Me.btnNo.Location = New System.Drawing.Point(190, 104)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(93, 33)
        Me.btnNo.TabIndex = 3
        Me.btnNo.Text = "&No"
        Me.btnNo.UseVisualStyleBackColor = True
        '
        'AlertNoContactNumbersFormNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(375, 149)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.btnYes)
        Me.Controls.Add(Me.chkPrompt)
        Me.Controls.Add(Me.lblMessage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AlertNoContactNumbersFormNew"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "##"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents chkPrompt As System.Windows.Forms.CheckBox
    Friend WithEvents btnYes As System.Windows.Forms.Button
    Friend WithEvents btnNo As System.Windows.Forms.Button
End Class
