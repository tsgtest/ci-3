'**********************************************************
'   CContact Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods of a CI contact
'**********************************************************
Option Explicit On

Imports System.Data
Imports TSG.CI
Imports LMP

Namespace TSG.CI
    Public Class CContact
        Implements ICContact
#Region "******************fields***********************"
        Private Const cioTagEmpty As String = "<EMPTY>"
        Private m_vTag As Object
        Private m_iContactType As ciContactTypes
        Private m_vAddressTypeID As Object
        Private m_xAddressTypeName As String
        Private m_vAddressID As Object

        'universal id - unique contact id across all ci backends-
        'describes the source location of the contact
        Private m_vUNID As Object

        Private m_xDisplayName As String
        Private m_xFullName As String
        Private m_xFullNameWithSuffix As String
        Private m_xFullNameWithPrefix As String
        Private m_xFullNameWithPrefixAndSuffix As String
        Private m_xPrefix As String
        Private m_xSuffix As String
        Private m_xFirstName As String
        Private m_xLastName As String
        Private m_xMiddleName As String
        Private m_xInitials As String
        Private m_xSalutation As String

        Private m_xCompany As String
        Private m_xDepartment As String
        Private m_xTitle As String

        Private m_xStreet1 As String
        Private m_xStreet2 As String
        Private m_xStreet3 As String
        Private m_xAddInfo As String
        Private m_xCity As String
        Private m_xState As String
        Private m_xZip As String
        Private m_xCountry As String
        Private m_vPhoneID As String
        Private m_vFaxID As String
        Private m_vGoesBy As String
        Private m_vEAddressID As String
        Private m_xPhone As String
        Private m_xPhoneExt As String
        Private m_xFax As String
        Private m_xEMailAddress As String
        Private m_oCustomFields As CCustomFields
        Private m_xCustomTypeName As String
        Private m_debugID As Long
        Private m_xFullDetail As String
        Private m_xCoreAddress As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oCustomFields = New CCustomFields
        End Sub
#End Region
#Region "******************properties**************************"
        Public Property CoreAddress As String Implements ICContact.CoreAddress
            Get
                Return m_xCoreAddress
            End Get
            Set(value As String)
                If value <> m_xCoreAddress Then
                    m_xCoreAddress = value
                End If
            End Set
        End Property

        Public Property ContactType As ciContactTypes Implements ICContact.ContactType
            Get
                Return m_iContactType
            End Get
            Set(value As ciContactTypes)
                m_iContactType = value
            End Set
        End Property

        Public Property AdditionalInformation As String Implements ICContact.AdditionalInformation
            Get
                Return m_xAddInfo
            End Get
            Set(value As String)
                m_xAddInfo = value
            End Set
        End Property

        Public Property PhoneExtension As String Implements ICContact.PhoneExtension
            Get
                Return m_xPhoneExt
            End Get
            Set(value As String)
                m_xPhoneExt = value
            End Set
        End Property

        Public Property Salutation As String Implements ICContact.Salutation
            Get
                Try
                    If String.IsNullOrEmpty(m_xSalutation) Then
                        'no salutation has been set, get the default salutation
                        m_xSalutation = GetDefaultSalutation()
                    End If

                    Salutation = m_xSalutation
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xPhoneExt = value
            End Set
        End Property

        'Tag property is for internal use only
        Public Property Tag As Object Implements ICContact.Tag
            Get
                'Attribute Tag.VB_MemberFlags = "40"
                Tag = m_vTag
            End Get
            Set(value As Object)
                m_vTag = value
            End Set
        End Property

        Public Property DisplayName As String Implements ICContact.DisplayName
            Get
                'Attribute DisplayName.VB_UserMemId = 0
                DisplayName = m_xDisplayName
            End Get
            Set(value As String)
                m_xDisplayName = value
            End Set
        End Property

        Public Property AddressTypeName As String Implements ICContact.AddressTypeName
            Get
                Return m_xAddressTypeName
            End Get
            Set(value As String)
                m_xAddressTypeName = value
            End Set
        End Property

        Public Property AddressTypeID As Object
            Get
                Return m_vAddressTypeID
            End Get
            Set(value As Object)
                m_vAddressTypeID = value
            End Set
        End Property

        Public Property AddressID As Object
            Get
                Return m_vAddressID
            End Get
            Set(value As Object)
                m_vAddressID = value
            End Set
        End Property

        Public Property PhoneID As Object
            Get
                Return m_vPhoneID
            End Get
            Set(value As Object)
                m_vPhoneID = value
            End Set
        End Property

        Public Property FaxID As Object
            Get
                Return m_vFaxID
            End Get
            Set(value As Object)
                m_vFaxID = value
            End Set
        End Property

        Public Property GoesBy As Object
            Get
                Return m_vGoesBy
            End Get
            Set(value As Object)
                m_vGoesBy = value
            End Set
        End Property

        Public Property EAddressID As Object
            Get
                Return m_vEAddressID
            End Get
            Set(value As Object)
                m_vEAddressID = value
            End Set
        End Property

        Public Property State As String Implements ICContact.State
            Get
                Return m_xState
            End Get
            Set(value As String)
                m_xState = value
            End Set
        End Property

        Public Property Phone As String Implements ICContact.Phone
            Get
                Return m_xPhone
            End Get
            Set(value As String)
                m_xPhone = value
            End Set
        End Property

        Public Property Street1 As String Implements ICContact.Street1
            Get
                Return m_xStreet1
            End Get
            Set(value As String)
                m_xStreet1 = value
            End Set
        End Property

        Public Property Street2 As String Implements ICContact.Street2
            Get
                Return m_xStreet2
            End Get
            Set(value As String)
                m_xStreet2 = value
            End Set
        End Property

        Public Property Street3 As String Implements ICContact.Street3
            Get
                Return m_xStreet3
            End Get
            Set(value As String)
                m_xStreet3 = value
            End Set
        End Property

        Public Property ZipCode As String Implements ICContact.ZipCode
            Get
                Return m_xZip
            End Get
            Set(value As String)
                m_xZip = value
            End Set
        End Property

        Public Property Fax As String Implements ICContact.Fax
            Get
                Return m_xFax
            End Get
            Set(value As String)
                m_xFax = value
            End Set
        End Property

        Public Property Country As String Implements ICContact.Country
            Get
                Return m_xCountry
            End Get
            Set(value As String)
                m_xCountry = value
            End Set
        End Property

        Public Property City As String Implements ICContact.City
            Get
                Return m_xCity
            End Get
            Set(value As String)
                m_xCity = value
            End Set
        End Property

        Public Property Company As String Implements ICContact.Company
            Get
                Return m_xCompany
            End Get
            Set(value As String)
                m_xCompany = value
            End Set
        End Property

        Public Property Department As String Implements ICContact.Department
            Get
                Return m_xDepartment
            End Get
            Set(value As String)
                m_xDepartment = value
            End Set
        End Property

        Public Property EMailAddress As String Implements ICContact.EmailAddress
            Get
                Return m_xEMailAddress
            End Get
            Set(value As String)
                m_xEMailAddress = value
            End Set
        End Property

        Public Property FirstName As String Implements ICContact.FirstName
            Get
                Return m_xFirstName
            End Get
            Set(value As String)
                m_xFirstName = value
            End Set
        End Property

        Public Property Prefix As String Implements ICContact.Prefix
            Get
                Return m_xPrefix
            End Get
            Set(value As String)
                m_xPrefix = value
            End Set
        End Property

        Public Property Suffix As String Implements ICContact.Suffix
            Get
                Return m_xSuffix
            End Get
            Set(value As String)
                m_xSuffix = value
            End Set
        End Property

        Public Property MiddleName As String Implements ICContact.MiddleName
            Get
                Return m_xMiddleName
            End Get
            Set(value As String)
                m_xMiddleName = value
            End Set
        End Property

        Public Property LastName As String Implements ICContact.LastName
            Get
                Return m_xLastName
            End Get
            Set(value As String)
                m_xLastName = value
            End Set
        End Property

        Public Property Title As String Implements ICContact.Title
            Get
                Return m_xTitle
            End Get
            Set(value As String)
                m_xTitle = value
            End Set
        End Property

        Public Property Initials As String Implements ICContact.Initials
            Get
                Return m_xInitials
            End Get
            Set(value As String)
                m_xInitials = value
            End Set
        End Property

        Public Property UNID As Object Implements ICContact.UNID
            Get
                Return m_vUNID
            End Get
            Set(value As Object)
                m_vUNID = value
            End Set
        End Property

        Public Property FullName As String Implements ICContact.FullName
            Get
                Try
                    If String.IsNullOrEmpty(m_xFullName) Then
                        m_xFullName = GetFullName(False, False)
                    End If
                    FullName = m_xFullName
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xFullName = value
            End Set
        End Property

        Public Property FullNameWithSuffix As String
            Get
                Try
                    If String.IsNullOrEmpty(m_xFullNameWithSuffix) Then
                        m_xFullNameWithSuffix = GetFullName(False, True)
                    End If
                    FullNameWithSuffix = m_xFullNameWithSuffix
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xFullNameWithSuffix = value
            End Set
        End Property

        Public Property FullNameWithPrefix As String
            Get
                Try
                    If String.IsNullOrEmpty(m_xFullNameWithPrefix) Then
                        m_xFullNameWithPrefix = GetFullName(True, False)
                    End If
                    FullNameWithPrefix = m_xFullNameWithPrefix
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xFullNameWithPrefix = value
            End Set
        End Property

        Public Property FullNameWithPrefixAndSuffix As String
            Get
                Try
                    If String.IsNullOrEmpty(m_xFullNameWithPrefixAndSuffix) Then
                        m_xFullNameWithPrefixAndSuffix = GetFullName(True, True)
                    End If
                    FullNameWithPrefixAndSuffix = m_xFullNameWithPrefixAndSuffix
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xFullNameWithPrefixAndSuffix = value
            End Set
        End Property

        'GLOG : 7794 : ceh
        Public Property FullDetail As String
            Get
                Try
                    If String.IsNullOrEmpty(m_xFullDetail) Then
                        m_xFullDetail = GetFullDetail()
                    End If
                    FullDetail = m_xFullDetail
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As String)
                m_xFullDetail = value
            End Set
        End Property

        Public Property CustomTypeName As String Implements ICContact.CustomTypeName
            Get
                Return m_xCustomTypeName
            End Get
            Set(value As String)
                m_xCustomTypeName = value
            End Set
        End Property

        Public Property CustomFields As ICCustomFields Implements ICContact.CustomFields
            Get
                Try
                    If m_oCustomFields Is Nothing Then
                        m_oCustomFields = New CCustomFields
                    End If
                    CustomFields = m_oCustomFields
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As ICCustomFields)
                m_oCustomFields = value
            End Set
        End Property
#End Region
#Region "******************methods**************************"
        Private Function FormatDetail(xDetail As String, xDetailItem As String) As String
            Dim oIni As CIni
            Dim xFormat As String = ""
            Dim xTemp As String = ""
            Dim oStr As CStrings
            Dim i As Integer
            Dim j As Integer

            Try
                oStr = New CStrings
                oIni = New CIni

                xFormat = oIni.GetIni("Formats", xDetailItem)

                If xFormat = String.Empty Then
                    'no format has been set, return unformatted detail string
                    xFormat = xDetail
                Else
                    xTemp = oStr.StripNonNumeric(xDetail)

                    'clean up format string
                    'need to deal with phone extension?
                    Select Case xDetailItem
                        Case "Phone", "Fax"
                            'deal with US like numbers with no area code
                            If Len(xTemp) = 7 Then
                                For i = Len(xFormat) To 1 Step -1
                                    If Mid(xFormat, i, 1) = "#" Then
                                        j = j + 1
                                    End If
                                    If j = 7 Then
                                        xFormat = Right(xFormat, (Len(xFormat) - i) + 1)
                                        Exit For
                                    End If
                                Next i
                            ElseIf Len(xTemp) <> 10 Then
                                'add logic for larger & international numbers as well as phone extensions
                                'exit for now
                                FormatDetail = xDetail
                                Exit Function
                            End If
                        Case "Zip"
                            If Len(xTemp) <= 5 Then
                                xFormat = Left(xFormat, Len(xTemp))
                            ElseIf Len(xTemp) <> 9 Then
                                FormatDetail = xDetail
                                Exit Function
                            End If
                        Case Else
                    End Select

                    'replace format tokens
                    For i = 1 To Len(xTemp)
                        xFormat = oStr.xSubstituteFirst(xFormat, "#", Mid(xTemp, i, 1))
                    Next i

                End If

                oIni = Nothing
                oStr = Nothing

                FormatDetail = xFormat
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function


        Private Function GetDefaultSalutation() As String
            'returns the default salutation based on the default salutation
            'token specified in ci.ini.  Returns first name if no token specified
            Static bRetrieved As Boolean
            Dim xToken As String = ""

            If Not bRetrieved Then
                Dim oIni As CIni
                oIni = New CIni

                xToken = oIni.GetIni("CIApplication", "DefaultSalutation")
                bRetrieved = True
            End If

            With Me
                If xToken = String.Empty Then
                    xToken = .FirstName
                Else
                    xToken = Replace(xToken, "<Prefix>", .Prefix)
                    xToken = Replace(xToken, "<FirstName>", .FirstName)
                    xToken = Replace(xToken, "<GoesBy>", .GoesBy)
                    xToken = Replace(xToken, "<MiddleName>", .MiddleName)
                    xToken = Replace(xToken, "<MiddleInitial>", Left$(.MiddleName, 1) & ".")
                    xToken = Replace(xToken, "<LastName>", .LastName)
                    xToken = Replace(xToken, "<Suffix>", .Suffix)
                End If
            End With

            GetDefaultSalutation = Trim$(xToken)
        End Function

        Public Function GetDetail(ByVal xFormatString As String, _
                                  Optional ByVal bRemoveBlankLines As Boolean = True) As String Implements ICContact.GetDetail
            'returns the contact detail in the format specified
            Dim xTemp As String
            Dim oCustFld As CCustomField
            Dim i As Integer
            Dim xVal0 As String = ""
            Static xFlds(,) As String


            Try
                xTemp = xFormatString

                'test for existence of values
                'in the name/value array
                Try
                    xVal0 = xFlds(0, 0)
                Catch
                End Try

                If xVal0 <> String.Empty Then
                    GoTo skip
                End If

                With Me
                    'name/value array not yet built - do it
                    ReDim xFlds(34 + .CustomFields.Count, 1)

                    xFlds(0, 0) = "<ADDITIONALINFORMATION>"
                    xFlds(1, 0) = "<ADDRESSID>"
                    xFlds(2, 0) = "<ADDRESSTYPENAME>"
                    xFlds(3, 0) = "<CITY>"
                    xFlds(4, 0) = "<COMPANY>"
                    xFlds(5, 0) = "<COUNTRY>"
                    xFlds(6, 0) = "<DEPARTMENT>"
                    xFlds(7, 0) = "<DISPLAYNAME>"
                    xFlds(8, 0) = "<EMAILADDRESS>"
                    xFlds(9, 0) = "<FAX>"
                    xFlds(10, 0) = "<FIRSTNAME>"
                    xFlds(11, 0) = "<FULLNAME>"
                    xFlds(12, 0) = "<FULLNAMEWITHPREFIX>"
                    xFlds(13, 0) = "<FULLNAMEWITHSUFFIX>"
                    xFlds(14, 0) = "<FULLNAMEWITHPREFIXANDSUFFIX>"
                    xFlds(15, 0) = "<INITIALS>"
                    xFlds(16, 0) = "<LASTNAME>"
                    xFlds(17, 0) = "<MIDDLENAME>"
                    xFlds(18, 0) = "<PHONE>"
                    xFlds(19, 0) = "<PHONEEXTENSION>"
                    xFlds(20, 0) = "<PREFIX>"
                    xFlds(21, 0) = "<SALUTATION>"
                    xFlds(22, 0) = "<STATE>"
                    xFlds(23, 0) = "<STREET1>"
                    xFlds(24, 0) = "<STREET2>"
                    xFlds(25, 0) = "<STREET3>"
                    xFlds(26, 0) = "<SUFFIX>"
                    xFlds(27, 0) = "<TAG>"
                    xFlds(28, 0) = "<TITLE>"
                    xFlds(29, 0) = "<ZIPCODE>"
                    xFlds(30, 0) = "<ZIP>"
                    xFlds(31, 0) = "<STREET>"
                    'v. 2.4.6002
                    xFlds(32, 0) = "<COREADDRESS>"
                    'v. 2.6.6005
                    'GLOG : 7794 : ceh
                    xFlds(33, 0) = "<FULLDETAIL>"
                    'GLOG : 15855 : ceh
                    xFlds(34, 0) = "<GOESBY>"

                    xFlds(0, 1) = .AdditionalInformation
                    xFlds(1, 1) = .AddressID
                    xFlds(2, 1) = .AddressTypeName
                    xFlds(3, 1) = .City
                    xFlds(4, 1) = .Company
                    xFlds(5, 1) = .Country
                    xFlds(6, 1) = .Department
                    xFlds(7, 1) = .DisplayName
                    xFlds(8, 1) = .EMailAddress
                    xFlds(9, 1) = .Fax
                    xFlds(10, 1) = .FirstName
                    xFlds(11, 1) = .FullName
                    xFlds(12, 1) = .FullNameWithPrefix
                    xFlds(13, 1) = .FullNameWithSuffix
                    xFlds(14, 1) = .FullNameWithPrefixAndSuffix
                    xFlds(15, 1) = .Initials
                    xFlds(16, 1) = .LastName
                    xFlds(17, 1) = .MiddleName
                    xFlds(18, 1) = .Phone
                    xFlds(19, 1) = IIf(String.IsNullOrEmpty(.PhoneExtension), "", "x" & .PhoneExtension)
                    xFlds(20, 1) = .Prefix
                    xFlds(21, 1) = .Salutation
                    xFlds(22, 1) = .State
                    xFlds(23, 1) = .Street1
                    xFlds(24, 1) = .Street2
                    xFlds(25, 1) = .Street3
                    xFlds(26, 1) = .Suffix
                    xFlds(27, 1) = .Tag
                    xFlds(28, 1) = .Title
                    xFlds(29, 1) = .ZipCode
                    xFlds(30, 1) = .ZipCode
                    xFlds(31, 1) = .Street1 & vbCr & .Street2 & vbCr & .Street3
                    xFlds(32, 1) = .CoreAddress
                    xFlds(33, 1) = .FullDetail
                    'GLOG : 15855 : ceh
                    xFlds(34, 1) = .GoesBy

                    For Each oCustFld In Me.CustomFields
                        i = i + 1
                        xFlds(34 + i, 0) = "<" & UCase(oCustFld.Name) & ">"
                        xFlds(34 + i, 1) = oCustFld.Value
                    Next oCustFld
                End With

skip:
                '   replace all detail fields with data
                Dim xReplacement As String
                For i = 0 To UBound(xFlds, 1)
                    'if data is empty, replace with <EMPTY>-
                    'this will be used later to delete blocks
                    'specified to be deleted when the value of a
                    'contact property is empty -
                    'e.g. additional spaces, commas, etc.
                    xReplacement = IIf(xFlds(i, 1) = String.Empty, cioTagEmpty, xFlds(i, 1))
                    xTemp = Replace(xTemp, xFlds(i, 0), xReplacement)
                Next i

                xTemp = DeleteEMPTYBlocks(xTemp)

                'remove blank lines if specified
                If bRemoveBlankLines Then
                    While InStr(xTemp, vbCr & vbCr) > 0
                        xTemp = Replace(xTemp, vbCr & vbCr, vbCr)
                    End While
                    While InStr(xTemp, vbCrLf & vbCrLf) > 0
                        xTemp = Replace(xTemp, vbCrLf & vbCrLf, vbCrLf)
                    End While
                    While InStr(xTemp, Chr(11) & Chr(11)) > 0
                        xTemp = Replace(xTemp, Chr(11) & Chr(11), Chr(11))
                    End While
                    While InStr(xTemp, vbCrLf & vbCr) > 0
                        xTemp = Replace(xTemp, vbCrLf & vbCr, vbCrLf)
                    End While
                    While InStr(xTemp, vbCrLf & Chr(11)) > 0
                        xTemp = Replace(xTemp, vbCrLf & Chr(11), vbCrLf)
                    End While
                    While InStr(xTemp, Chr(11) & vbCr) > 0
                        xTemp = Replace(xTemp, Chr(11) & vbCr, vbCr)
                    End While
                    While InStr(xTemp, Chr(11) & vbCrLf) > 0
                        xTemp = Replace(xTemp, Chr(11) & vbCrLf, vbCrLf)
                    End While
                    While InStr(xTemp, vbCr & Chr(11)) > 0
                        xTemp = Replace(xTemp, vbCr & Chr(11), vbCr)
                    End While
                    While InStr(xTemp, vbCr & vbCrLf) > 0
                        xTemp = Replace(xTemp, vbCr & vbCrLf, vbCrLf)
                    End While
                End If

                GetDetail = xTemp
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function DeleteEMPTYBlocks(ByVal xDetail As String) As String
            'delete blocks defined by {} that contain <EMPTY> tags-
            Dim iPosTag As Integer
            Dim iPosBlockStart As Integer
            Dim iPosBlockEnd As Integer
            Dim iPosNestedStartBrace As Integer
            Dim iPosNestedEndBrace As Integer
            Dim xTemp As String
            Dim xNestedBraces As String
            Dim xBlock As String

            xTemp = xDetail

            Do
                'search for <EMPTY> tag
                iPosTag = InStr(xTemp, cioTagEmpty)

                If iPosTag > 0 Then
                    'empty tag exists - find surrounding
                    'block start and end i.e.{}
                    iPosBlockEnd = InStr(iPosTag, xTemp, "}", vbTextCompare)
                    iPosBlockStart = InStrRev(xTemp, "{", iPosTag, vbTextCompare)

                    If (iPosBlockStart > 0) And (iPosBlockEnd > 0) Then
                        'there exists a block to delete -
                        'get the block defined by the braces
                        xBlock = Mid$(xTemp, iPosBlockStart + 1, iPosBlockEnd - iPosBlockStart - 1)

                        'get positions of any nested braces in the block
                        iPosNestedStartBrace = InStr(xBlock, "{")
                        iPosNestedEndBrace = InStr(xBlock, "}")

                        If iPosNestedStartBrace > 0 Then
                            'there is a nested start brace
                            If iPosNestedEndBrace > 0 Then
                                'there's also a nested end brace -
                                'see which is first
                                If iPosNestedEndBrace < iPosNestedStartBrace Then
                                    'end brace before start brace
                                    xNestedBraces = "}{"
                                Else
                                    'start brace before end brace
                                    xNestedBraces = "{}"
                                End If
                            Else
                                'no end brace, just start brace
                                xNestedBraces = "{"
                            End If
                        ElseIf iPosNestedEndBrace > 0 Then
                            'end brace only
                            xNestedBraces = "}"
                        Else
                            xNestedBraces = String.Empty
                        End If

                        'delete the block, keeping any nested braces
                        xTemp = Left$(xTemp, iPosBlockStart - 1) & _
                            xNestedBraces & Mid$(xTemp, iPosBlockEnd + 1)
                    ElseIf (iPosBlockStart = 0) Or (iPosBlockEnd = 0) Then
                        'no block exists - delete empty tag
                        xTemp = Left$(xTemp, iPosTag - 1) & _
                            Mid$(xTemp, iPosTag + Len(cioTagEmpty))
                    Else
                        Throw New Exception()
                    End If
                End If
                'cycle until there are no more empty tags
            Loop Until (iPosTag = 0)

            'replace remaining braces - they're not needed
            'as they define blocks that don't contain empty data
            xTemp = Replace(xTemp, "{", "")
            xTemp = Replace(xTemp, "}", "")
            xTemp = Trim$(xTemp)
            DeleteEMPTYBlocks = xTemp
        End Function

        'Private Function Evaluate(ByVal xPhrase As String) As String
        ''returns the string to which xPhrase evaluates
        '
        ''   get function name
        '    If Left(xPhrase, 3) = "IF(" Then
        '        Evaluate = EvaluateIF(xPhrase)
        '    ElseIf InStr(xPhrase, "=") Then
        '        Evaluate = EvaluateIdentity(xPhrase)
        '    End If
        'End Function

        'Private Function EvaluateIdentity(ByVal xPhrase As String) As Boolean
        ''returns true if identity phrase evaluates to TRUE, else FALSE
        '    Dim iPos As Integer
        '    Dim xOperand1 As String
        '    Dim xOperand2 As String
        '
        '    On Error GoTo ProcError
        '
        '    iPos = InStr(xPhrase, "=")
        '
        '    If iPos = 0 Then
        ''       not valid syntax - raise error
        '        Err.Raise ciErr_InvalidContactDetailFormatString, , _
        '            "Invalid syntax in phrase '" & xPhrase & "'."
        '    End If
        '
        ''   get operands
        '    xOperand1 = Left(xPhrase, iPos - 1)
        '    xOperand2 = Mid(xPhrase, iPos + 1)
        '
        ''   TRUE is operands are identical strings
        '    EvaluateIdentity = (xOperand1 = xOperand2)
        '    Exit Function
        'ProcError:
        '    General.g_oError.RaiseError("CContact.EvaluateIdentity"
        '    Exit Function
        'End Function
        '
        'Private Function EvaluateIF(ByVal xPhrase As String) As String
        ''returns the string to which the supplied IF function evaluates
        '    Dim xTemp As String
        '    Dim bValid As Boolean
        '    Dim xArgs() As String
        '
        ''   test for proper syntax
        '    On Error GoTo ProcError
        '    bValid = Left(xPhrase, 3) = "IF(" And _
        '        Right(xPhrase, 1) = ")"
        '
        '    If bValid Then
        ''       get args - trim off IF and ()
        '        xTemp = Left(xPhrase, Len(xPhrase) - 1)
        '        xTemp = Mid(xTemp, 4)
        '
        '        xArgs = Split(xTemp, ",")
        '        bValid = UBound(xArgs) = 2
        '    End If
        '
        '    If bValid Then
        ''       check for existence of each argument
        '        bValid = xArgs(0) <> String.Empty And _
        '            xArgs(1) <> String.Empty And _
        '            xArgs(2) <> String.Empty
        '    End If
        '
        '    If Not bValid Then
        ''       not valid syntax - raise error
        '        Err.Raise ciErr_InvalidContactDetailFormatString, , _
        '            "Invalid syntax in phrase '" & xPhrase & "'."
        '    End If
        '
        ''   evaluate arg 1
        '    If Evaluate(xArgs(0)) = True Then
        '        EvaluateIF = xArgs(1)
        '    Else
        '        EvaluateIF = xArgs(2)
        '    End If
        '    Exit Function
        'ProcError:
        '    General.g_oError.RaiseError("CContact.EvaluateIF"
        '    Exit Function
        'End Function

        Private Function GetFullName(Optional bIncludePrefix = False, Optional bIncludeSuffix = False)
            Dim xName As String
            Dim xTemp As String
            Dim oDR As DataRow = Nothing
            Dim bExcludePrefix As Boolean = False

            xName = ""
            xTemp = ""

            If Me.FirstName <> "" Then _
                xName = xName & Me.FirstName & " "
            If Me.MiddleName <> "" Then _
                xName = xName & Me.MiddleName & " "
            If Me.LastName <> "" Then _
                xName = xName & Me.LastName

            xName = Trim(xName)
            If xName = "" Then
                'Use Company if no name included
                GetFullName = Me.Company
                Exit Function
            End If

            'GLOG : 15798 : ceh
            'handle Prefix
            If bIncludePrefix And Me.Prefix <> "" Then
                If bIncludeSuffix And Me.Suffix <> "" Then
                    'find row
                    oDR = General.g_oArrSuffix.Rows.Find(Me.Suffix)
                    If (Not (oDR Is Nothing)) And General.g_oArrPrefixToExclude.Rows.Count() Then
                        oDR = General.g_oArrPrefixToExclude.Rows.Find(Me.Prefix)
                        If Not (oDR Is Nothing) Then 'prefix to exclude was found
                            bExcludePrefix = True
                        End If
                    End If
                End If

                If Not bExcludePrefix Then
                    xName = Me.Prefix & " " & xName
                End If
            End If

            'handle Suffix
            If bIncludeSuffix And Me.Suffix <> "" Then
                If bIncludePrefix And Me.Prefix <> "" Then
                    'find row
                    oDR = General.g_oArrPrefix.Rows.Find(Me.Prefix)

                    If Not (oDR Is Nothing) Then     'prefix found
                        If General.g_oArrSuffixToExclude.Rows.Count() Then
                            oDR = General.g_oArrSuffixToExclude.Rows.Find(Me.Suffix) ' 
                            If oDR Is Nothing Then 'suffix to exclude was not found
                                xTemp = Me.Suffix
                            End If
                        End If
                    Else
                        xTemp = Me.Suffix
                    End If
                Else
                    xTemp = Me.Suffix
                End If

                'Suffix Separator
                If xTemp <> "" Then
                    ' Check if Suffix matches an exclusion, or starts with
                    ' an excluded item followed by a space or comma

                    'set values to find
                    Dim xValues(2) As Object
                    xValues(0) = Me.Suffix
                    xValues(1) = Me.Suffix & " "
                    xValues(2) = Me.Suffix & ","

                    'find row
                    For i = 0 To 2
                        oDR = General.g_oArrSuffixSeparatorExclusions.Rows.Find(xValues(i))
                        If Not IsNothing(oDR) Then
                            Exit For
                        End If
                    Next

                    If Not IsNothing(oDR) Then
                        xName = xName & " " & xTemp
                    Else
                        ' ****9.6.2 - 3847
                        ' Don't duplicate separator if it's already included
                        ' at the start of the suffix
                        If InStr(xTemp, General.g_xSuffixSeparator) = 1 Then
                            xName = xName & xTemp
                        Else
                            xName = xName & General.g_xSuffixSeparator & xTemp
                        End If
                    End If
                End If

            End If
            GetFullName = xName
        End Function

        'GLOG : 7794 : ceh
        Private Function GetFullDetail() As String
            Dim xDetail As String
            Dim xCustomFormat As String

            'logic to find out if contact's country matches ini
            xCustomFormat = GetCustomFormat() 'get format from CI.ini

            If xCustomFormat <> String.Empty Then
                xDetail = Replace(GetDetail(xCustomFormat), "|", vbCr)
            Else
                'get default
                'JTS 1/21/16: Include Title and Company if used
                xDetail = Me.FullNameWithPrefixAndSuffix & vbCr & _
                    IIf(Me.Title <> "", Me.Title & vbCr, "") & _
                    IIf(Me.Company <> "", Me.Company & vbCr, "") & vbCr & Me.CoreAddress
            End If

            GetFullDetail = xDetail
        End Function
        'GLOG : 7794 : ceh
        Private Function GetCustomFormat() As String
            Dim i As Integer
            Dim xFormatCountry As String = ""
            Dim xToken As String = ""
            Dim oIni As CIni

            oIni = New CIni

            'cycle through Custom Formats until
            'country match is found.  If found,
            'return FullDetail token

            Do
                i = i + 1

                xFormatCountry = oIni.GetIni("CIApplication", "Format" & i & "Country")

                If xFormatCountry = "" Then
                    Return ""
                    Exit Function
                Else
                    'JTS 1/21/16: Convert Unicode character tokens
                    Dim iPos As Integer
                    Dim iPos2 As Integer
                    Dim xCode As String
                    Dim lCode As Long
                    Dim xCountryString As String
                    On Error GoTo SkipConversion
                    iPos = InStr(UCase(xFormatCountry), "CHRW(")
                    While iPos > 0
                        iPos2 = InStr(iPos, xFormatCountry, ")")
                        If iPos2 > 0 Then
                            xCode = Mid(xFormatCountry, iPos + 5, iPos2 - (iPos + 5))
                            lCode = CLng(xCode)
                            xCountryString = xCountryString & ChrW(lCode)
                            iPos = InStr(iPos2, UCase(xFormatCountry), "CHRW(")
                        End If
                    End While
                    If xCountryString <> "" Then
                        xFormatCountry = xCountryString
                    End If
SkipConversion:
                    If xFormatCountry = "" Then
                        Exit Function
                    ElseIf (UCase(xFormatCountry) = UCase(Me.Country)) Then
                        xToken = oIni.GetIni("CIApplication", "Format" & i & "FullDetail")
                    End If
                End If

            Loop While (xToken = "")

            GetCustomFormat = xToken

        End Function
#End Region
    End Class
End Namespace
