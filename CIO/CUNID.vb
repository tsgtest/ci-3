'**********************************************************
'   CApplication CollectionClass
'   created 10/11/00 by Daniel Fisherman
'   Contains properties and methods
'   that manipulate the CI objects
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
#Region "******************enumerations***********************"
    Public Enum ciUNIDFields
        ciUNIDFields_Backend = 0
        ciUNIDFields_Store = 1
        ciUNIDFields_Folder = 2
        ciUNIDFields_Listing = 3
        ciUNIDFields_Address = 4
        ciUNIDFields_ContactNumbers = 5
    End Enum

    Public Enum ciUNIDTypes
        ciUNIDType_Backend = 0
        ciUNIDType_Store = 1
        ciUNIDType_Folder = 2
        ciUNIDType_Listing = 3
        ciUNIDType_Address = 4
        ciUNIDType_ContactNumbers = 5
    End Enum
#End Region
    Public Class CUNID
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************methods**************************"
        Public Function GetStore(ByVal xUNID As String, Optional ByVal xName As String = "") As CStore
            'returns the store corresponding to the specified UNID
            Dim oStore As CStore

            Try
                'get the part of the UNID that constitutes
                'the UNID for an address
                xUNID = GetUNIDSubstring(xUNID, ciUNIDTypes.ciUNIDType_Store)

                oStore = New CStore
                oStore.UNID = xUNID
                If Len(xName) Then
                    oStore.Name = xName
                End If
                GetStore = oStore
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetFolder(ByVal xUNID As String, Optional ByVal xName As String = "") As CFolder
            'returns the folder corresponding to the specified UNID
            Dim oFolder As CFolder

            Try
                'get the part of the UNID that constitutes
                'the UNID for a folder
                xUNID = GetUNIDSubstring(xUNID, ciUNIDTypes.ciUNIDType_Folder)

                oFolder = New CFolder
                oFolder.UNID = xUNID
                If Len(xName) Then
                    oFolder.Name = xName
                End If
                GetFolder = oFolder
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetListing(ByVal xUNID As String, Optional ByVal xDisplayName As String = "") As CListing
            'returns the Listing corresponding to the specified UNID
            Dim oListing As CListing

            Try
                'get the part of the UNID that constitutes
                'the UNID for a listing
                xUNID = GetUNIDSubstring(xUNID, ciUNIDTypes.ciUNIDType_Listing)
                Dim oUNID As CUNID

                oListing = New CListing
                oListing.UNID = xUNID
                If Len(xDisplayName) Then
                    oListing.DisplayName = xDisplayName
                End If
                GetListing = oListing
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetAddress(ByVal xUNID As String, Optional ByVal xName As String = "") As CAddress
            'returns the Address corresponding to the specified UNID
            Dim oAddress As CAddress

            Try
                'get the part of the UNID that constitutes
                'the UNID for an address
                xUNID = GetUNIDSubstring(xUNID, ciUNIDTypes.ciUNIDType_Address)

                oAddress = New CAddress
                oAddress.UNID = xUNID
                If Len(xName) Then
                    oAddress.Name = xName
                End If
                GetAddress = oAddress
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        '
        'Public Function GetContactNumbersUNID(ByVal xUNID As String) As String
        ''returns the Phone corresponding to the specified UNID
        '    Dim xPhoneID As String
        '    Dim iPos As Integer
        '
        '    On Error GoTo ProcError
        '
        '    'get the part of the UNID that constitutes
        '    'the UNID for contact numbers
        '    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_ContactNumbers)
        '
        '    'parse up to first semi-colon - that's the phone number
        '    iPos = InStr(xUNID, ";")
        '    xPhoneID = Left$(xUNID, iPos - 1)
        '
        '    'parse number and type from string
        '    iPos = InStr(xPhoneID, "|")
        '    GetPhoneID = Left$(xPhoneID, iPos - 1)
        '    xPhoneType = Mid$(xPhoneID, iPos + 1)
        '    Exit Function
        'ProcError:
        '    General.g_oError.RaiseError("CUNID.GetPhoneID"
        'End Function

        Public Shared Function GetUNIDField(ByVal xUNID As String, ByVal iField As ciUNIDFields)

            Try
                GetUNIDField = General.GetUNIDField(xUNID, iField)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function GetUNIDType(ByVal xUNID As String) As ciUNIDTypes
            'returns the type of UNID represented by xUNID
            Dim bytNumSep As Byte
            Dim oStr As CStrings
            Dim oConst As CConstants

            Try
                oStr = New CStrings
                oConst = New CConstants

                'count the number of separators
                bytNumSep = oStr.lCountChrs(xUNID, oConst.UNIDSep)

                'number of separators matches type id
                GetUNIDType = bytNumSep
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function GetUNIDSubstring(ByVal xUNID As String, ByVal iType As ciUNIDTypes) As String
            'returns the substring of the supplied UNID that
            'constitutes the UNID of the desired type
            Dim iPos1 As Integer
            Dim iPos2 As Integer
            Dim oConst As CConstants
            Dim xDesc As String
            Dim i As Integer
            Dim xClass As String
            Dim iUNIDType As ciUNIDTypes
            Dim iUNIDDifference As Integer

            Try
                oConst = New CConstants

                iUNIDType = GetUNIDType(xUNID)

                If iUNIDType < iType Then
                    'can't get substring- the supplied UNID
                    'does not contain the specified unid type
                    Throw New Exception("Invalid UNID. The UNID " & _
                        "supplied does not have contain the desired sub-UNID.")
                End If

                '   get class name represented by field number
                'remove 'b' if necessary
                If Left(xUNID, 1) = "b" Then
                    xUNID = Mid(xUNID, 2)
                End If

                'get the number of additional separators that the
                'containing UNID has over the desired UNID -
                'we'll trim the unid string below by that number
                'of separators
                iUNIDDifference = iUNIDType - iType

                'get starting position - find appropriate separator
                For i = 1 To iType + 1
                    iPos1 = InStr(IIf(iPos1 = 0, 1, iPos1 + Len(oConst.UNIDSep)), _
                        xUNID, oConst.UNIDSep)
                    If iPos1 = 0 Then
                        'get the whole string - the loop hit
                        'went past the last UNID separator
                        GetUNIDSubstring = xUNID
                        Exit Function
                    End If
                Next i

                GetUNIDSubstring = Left$(xUNID, iPos1 - 1)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace