'**********************************************************
'   CApplication CollectionClass
'   created 10/11/00 by Daniel Fisherman
'   Contains properties and methods
'   that manipulate the CI objects
'**********************************************************
Option Explicit On

Namespace LMP.CIO
    Public Class CApplication

#Region "******************methods***********************"
        Public Function GetStore(ByVal xUNID As String, Optional ByVal xName As String = "") As CIO.CStore
            'returns the store corresponding to the specified UNID
            Dim oStore As CIO.CStore

            On Error GoTo ProcError

            oStore = New CIO.CStore
            oStore.UNID = xUNID
            If Len(xName) Then
                oStore.Name = xName
            End If
            GetStore = oStore
            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetStore")
        End Function

        Public Function GetFolder(xUNID As String, Optional ByVal xName As String = "") As CIO.CFolder
            'returns the folder corresponding to the specified UNID
            Dim oFolder As CIO.CFolder

            On Error GoTo ProcError

            oFolder = New CIO.CFolder
            oFolder.UNID = xUNID
            If Len(xName) Then
                oFolder.Name = xName
            End If
            GetFolder = oFolder
            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetFolder")
        End Function

        Public Function GetListing(xUNID As String, Optional ByVal xDisplayName As String = "") As CIO.CListing
            'returns the Listing corresponding to the specified UNID
            Dim oListing As CIO.CListing

            On Error GoTo ProcError

            oListing = New CIO.CListing
            oListing.UNID = xUNID
            If Len(xDisplayName) Then
                oListing.DisplayName = xDisplayName
            End If
            GetListing = oListing
            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetListing")
        End Function

        Public Function GetAddress(xUNID As String, Optional ByVal xName As String = "") As CIO.CAddress
            'returns the Address corresponding to the specified UNID
            Dim oAddress As CIO.CAddress

            On Error GoTo ProcError

            oAddress = New CIO.CAddress
            oAddress.UNID = xUNID
            If Len(xName) Then
                oAddress.Name = xName
            End If
            GetAddress = oAddress
            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetAddress")
        End Function

        Public Function CUNID.GetUNIDField(ByVal vUNID As Object, ByVal iField As ciUNIDFields)
            On Error GoTo ProcError
            CUNID.GetUNIDField = mdlGeneral.GetUNIDField(vUNID, iField)
            Exit Function
ProcError:
            General.g_oError.RaiseError("CBackends.GetUNIDField"
            Exit Function
        End Function

        Public Function GetBackendName(ByVal iBackendID As Integer) As String
            Dim xName As String
            Dim xDesc As String
            Dim oIni As CIni

            On Error GoTo ProcError
            oIni = New CIni

            'get from ini
            xName = oIni.GetIni("Backend" & iBackendID, "Name")

            'raise error if ini value is missing
            If Len(xName) = 0 Then
                xDesc = "Invalid Backend" & iBackendID & "\Name key in ci.ini."
                Err.Raise(ciErrs.ciErr_MissingOrInvalidINIKey, , xDesc)
                Exit Function
            End If

            GetBackendName = xName
            Exit Function
ProcError:
            General.g_oError.RaiseError("CBackends.Name")
        End Function

        Public Function GetUNIDType(xUNID As String) As ciUNIDTypes
            'returns the type of UNID represented by xUNID
            Dim bytNumSep As Byte
            Dim oStr As CStrings
            Dim oConst As CConstants

            On Error GoTo ProcError

            oStr = New CStrings
            oConst = New CConstants

            'count the number of separators
            bytNumSep = oStr.lCountChrs(xUNID, oConst.UNIDSep)

            'number of separators matches type id
            GetUNIDType = bytNumSep

            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetUNIDType")
        End Function

        Public Function GetFilter(ByVal iBackendID As Integer) As CIO.CFilter
            'returns the collection of filter fields for specified backend
            On Error GoTo ProcError
            GetFilter = g_oSession.Backends(iBackendID).Filter()
            Exit Function
ProcError:
            General.g_oError.RaiseError("CSession.GetFilter")
        End Function
#End Region
    End Class
End Namespace

