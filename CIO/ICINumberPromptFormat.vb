'**********************************************************
'   ICIPromptFormat Class
'   created 10/05/01 by Daniel Fisherman
'   Contains properties and methods that
'   defining characters of a ContactNumber prompt dialog
'**********************************************************
Option Explicit On

Namespace TSG.CI
    Public Interface ICINumberPromptFormat
#Region "******************fields***********************"
        Enum ciNumberPromptTypes
            ciNumberPromptType_Missing = 1
            ciNumberPromptType_Multiple = 2
        End Enum
#End Region
#Region "******************properties**************************"
        Property NumberType As ciContactNumberTypes

        ReadOnly Property AddressTypeHeading As String

        ReadOnly Property CategoryHeading As String

        ReadOnly Property DescriptionHeading As String

        ReadOnly Property NumberHeading As String

        ReadOnly Property ExtensionHeading As String

        ReadOnly Property AddressTypeWidth As Single

        ReadOnly Property CategoryWidth As Single

        ReadOnly Property DescriptionWidth As Single

        ReadOnly Property NumberWidth As Single

        ReadOnly Property ExtensionWidth As Single

        ReadOnly Property MissingNumbersDialogTitle As String

        ReadOnly Property MissingNumberDialogDescriptionText As String

        ReadOnly Property MissingNumbersDialogDescriptionText As String

        ReadOnly Property MultipleNumbersDialogTitle As String

        ReadOnly Property MultipleNumbersDialogDescriptionText As String
        'Property PromptType(iNew As ciNumberPromptTypes) As ciNumberPromptTypes

        Property PromptType As ciNumberPromptTypes

        'Property ContactName(ByVal xNew As String) As String

        Property ContactName As String

#End Region
    End Interface
End Namespace

