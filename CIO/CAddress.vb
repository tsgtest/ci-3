'**********************************************************
'   CAddresses Class
'   created 12/25/00 by Daniel Fisherman
'   Contains properties and methods that
'   define a listing's address - not the
'   actual address but the type, e.g. home address
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CAddress
#Region "******************fields***********************"
        Private m_vUNID As Object
        Private m_xName As String
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property UNID() As Object
            Get
                UNID = m_vUNID
            End Get
            Set(value As Object)
                m_vUNID = value
            End Set
        End Property

        Public ReadOnly Property ID() As Object
            'returns the ID of the address -
            Get
                ID = Nothing
                Try
                    ID = CUNID.GetUNIDField(Me.UNID, ciUNIDFields.ciUNIDFields_Address)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public Property Name() As String
            Get
                Name = m_xName
            End Get
            Set(Value As String)
                m_xName = Value
            End Set
        End Property
#End Region
    End Class
End Namespace
