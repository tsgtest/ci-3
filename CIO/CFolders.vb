'**********************************************************
'   CFolders Collection Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci folders
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CFolders
        Implements System.Collections.IEnumerable
#Region "******************fields***********************"
        Private m_oCol As Collection
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oCol = New Collection
        End Sub

#End Region
#Region "******************collection methods******************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Try
                GetEnumerator = m_oCol.GetEnumerator
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub Add(oFolder As CFolder, Optional ByVal bSkipDuplicates As Boolean = False)
            Dim xDesc As String
            'add to collection
            On Error Resume Next
            Err.Clear()
            m_oCol.Add(oFolder, oFolder.UNID)
            If Err.Number <> 0 And Not bSkipDuplicates Then
                'could not add to collection - alert and exit
                xDesc = "Could not add folder " & oFolder.Name & _
                    " to the collection of Folders. " & Err.Description
                Throw New Exception(xDesc)
                Exit Sub
            End If
        End Sub

        Default ReadOnly Property Item(vKey As Object) As CCustomField
            Get
                Try
                    Return m_oCol(vKey)
                Catch
                End Try
            End Get
        End Property

        Public Function Count() As Integer
            Try
                Count = m_oCol.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace