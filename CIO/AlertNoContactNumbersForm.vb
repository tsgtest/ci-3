﻿Option Explicit On

Imports TSG.CI
Imports System.Windows.Forms
Imports LMP

Public Class AlertNoContactNumbersForm
#Region "******************fields***********************"
    Private m_CancelInsert As Boolean
#End Region
#Region "******************initializer***********************"
    Public Sub New()
        InitializeComponent()
    End Sub
#End Region
#Region "******************properties***********************"
    Public Property CancelInsert() As Boolean
        Get
            CancelInsert = m_CancelInsert
        End Get
        Set(bNew As Boolean)
            m_CancelInsert = bNew
        End Set
    End Property
#End Region
#Region "******************events***********************"
    Private Sub btnYes_Click()
        Try
            Me.Hide()
            Application.DoEvents()
            Me.CancelInsert = False
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
#End Region
End Class