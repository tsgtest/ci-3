﻿Option Explicit On

Imports System.Data
Imports TSG.CI
Imports System.Windows.Forms
Imports LMP

Public Class ContactNumbersForm
#Region "******************enumerations***********************"
    Public Enum PhoneCols
        PhoneCols_AddrType = 0
        PhoneCols_Category = 1
        PhoneCols_Description = 2
        PhoneCols_Number = 3
        PhoneCols_Extension = 4
    End Enum
#End Region
#Region "******************fields***********************"
    Private m_oDT As DataTable
    Private m_bCancelled As Boolean
#End Region
#Region "******************initializer***********************"
    Public Sub New()
        InitializeComponent()

        Try
            'initialize DataTable
            m_oDT = New DataTable

            'add columns
            With m_oDT.Columns
                .Add("Address Type")
                .Add("Phone Type")
                .Add("Description")
                .Add("Number")
                .Add("Ext")
            End With
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub
#End Region
#Region "******************properties***********************"
    Public WriteOnly Property Format() As ICINumberPromptFormat
        Set(oNew As ICINumberPromptFormat)
            With Me
                If oNew.PromptType = ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing Then
                    .Text = oNew.MissingNumbersDialogTitle
                    .lblPhones.Text = Replace(oNew.MissingNumbersDialogDescriptionText, "&", "&&")
                Else
                    .Text = oNew.MultipleNumbersDialogTitle
                    .lblPhones.Text = Replace(oNew.MultipleNumbersDialogDescriptionText, "&", "&&")
                End If
            End With

            With Me.dtgPhones.Columns
                If oNew.AddressTypeWidth Then
                    .Item(PhoneCols.PhoneCols_AddrType).HeaderText = oNew.AddressTypeHeading
                    .Item(PhoneCols.PhoneCols_AddrType).Width = oNew.AddressTypeWidth
                Else
                    .Item(PhoneCols.PhoneCols_AddrType).Visible = False
                End If

                If oNew.CategoryWidth Then
                    .Item(PhoneCols.PhoneCols_Category).HeaderText = oNew.CategoryHeading
                    .Item(PhoneCols.PhoneCols_Category).Width = oNew.CategoryWidth
                Else
                    .Item(PhoneCols.PhoneCols_Category).Visible = False
                End If

                If oNew.DescriptionWidth Then
                    .Item(PhoneCols.PhoneCols_Description).HeaderText = oNew.DescriptionHeading
                    .Item(PhoneCols.PhoneCols_Description).Width = oNew.DescriptionWidth
                Else
                    .Item(PhoneCols.PhoneCols_Description).Visible = False
                End If

                If oNew.NumberWidth Then
                    .Item(PhoneCols.PhoneCols_Number).HeaderText = oNew.NumberHeading
                    .Item(PhoneCols.PhoneCols_Number).Width = oNew.NumberWidth
                Else
                    .Item(PhoneCols.PhoneCols_Number).Visible = False
                End If

                If oNew.ExtensionWidth Then
                    .Item(PhoneCols.PhoneCols_Extension).HeaderText = oNew.ExtensionHeading
                    .Item(PhoneCols.PhoneCols_Extension).Width = oNew.ExtensionWidth
                Else
                    .Item(PhoneCols.PhoneCols_Extension).Visible = False
                End If
            End With
        End Set
    End Property

    Public ReadOnly Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
    End Property

    Public WriteOnly Property Source() As CContactNumbers
        Set(oNew As CContactNumbers)
            Dim oContactNum As CContactNumber
            Dim i As Integer
            Dim xContactNumbers(4) As Object

            'fill element with listing detail
            For Each oContactNum In oNew
                xContactNumbers(PhoneCols.PhoneCols_Category) = oContactNum.Category
                xContactNumbers(PhoneCols.PhoneCols_Description) = oContactNum.Description
                xContactNumbers(PhoneCols.PhoneCols_Number) = oContactNum.Number
                xContactNumbers(PhoneCols.PhoneCols_Extension) = oContactNum.Extension
                'MessageBox.Show("1" & vbCr & oContactNum.Category.ToString() & vbCr & oContactNum.Description & vbCr & oContactNum.Number & vbCr & oContactNum.Extension)
                m_oDT.Rows.Add(xContactNumbers)
            Next oContactNum

            Me.dtgPhones.DataSource = m_oDT
            Me.dtgPhones.Refresh()
        End Set
    End Property

#End Region
#Region "******************methods***********************"
    Public Sub SetMessage(ByVal xContactName As String, _
                          ByVal xAddrType As String, _
                          ByVal xPhoneType As String, _
                          Optional ByVal bPromptForMultiple As Boolean = False, _
                          Optional ByVal ShowAsEAddresses As Boolean = False)
        'set the message that's displayed when form is visible-
        'two types of prompts - missing phones/ multiple phones
        If ShowAsEAddresses Then
            Me.Text = "Choose an E-Mail Address for " & xContactName
            If bPromptForMultiple Then
                Me.lblPhones.Text = "&There exist multiple e-mail" & _
                       " addresses for this address.  " & _
                       "Please select an alternate:"
                Me.chkPromptForMissingPhones.Visible = False
            Else
                Me.lblPhones.Text = "&No e-mail" & _
                       " address is associated with the selected address for " & xContactName & _
                       ".  Please select an alternate:"
                Me.chkPromptForMissingPhones.Visible = True
            End If
        Else
            Me.Text = "Choose a " & xPhoneType & " Number for " & xContactName
            If bPromptForMultiple Then
                Me.lblPhones.Text = "&There exist multiple " & LCase(xPhoneType) & _
                       " numbers for this address.  " & _
                       "Please select an alternate:"
                Me.chkPromptForMissingPhones.Visible = False
            Else
                Me.lblPhones.Text = "&No " & LCase(xPhoneType) & _
                       " number is associated with the selected address for " & xContactName & _
                       ".  Please select an alternate:"
                Me.chkPromptForMissingPhones.Visible = True
            End If
        End If
    End Sub
#End Region
#Region "******************events***********************"
    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click, dtgPhones.DoubleClick
        Try
            Me.Hide()
            Application.DoEvents()
            m_bCancelled = False
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            m_bCancelled = True
            Me.Hide()
            Application.DoEvents()
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub

    Private Sub Form_Activate()
        'select first item in list if possible
        Dim i As Integer

        Try
            If Not (m_oDT Is Nothing) Then
                If m_oDT.Rows.Count > 0 Then
                    'select first row
                    With Me.dtgPhones
                        'select first row
                        .Rows(0).Selected = True
                        .Focus()
                    End With
                End If
            End If

            '   check prompting checkbox
            Me.chkPromptForMissingPhones.Checked = 1
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub

    Private Sub Form_Load()
        Dim oSessionType As New CSessionType

        Try
            m_bCancelled = True
        Catch ex As Exception
            [Error].Show(ex)
        End Try
    End Sub

    'Private Sub Form_Paint()
    '    'Load icon
    '    With g_oSessionType
    '        If (.SessionType = ciSession_Connect) Then
    '            .SetIcon Me.hWnd, "CONNECTICON", False
    '        Else
    '            .SetIcon Me.hWnd, "TSGICON", False
    '        End If
    '    End With
    'End Sub

#End Region
End Class