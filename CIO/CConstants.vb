Option Explicit On

Namespace TSG.CI
#Region "******************enumerations***********************"
    'GLOG : 8819 : ceh
    'Public Enum ciRetrieveData
    '    ciRetrieveData_None = 0
    '    ciRetrieveData_All = 1
    '    ciRetrieveData_Names = 2
    '    ciRetrieveData_Addresses = 4
    '    ciRetrieveData_Phones = 8
    '    ciRetrieveData_Faxes = 16
    '    ciRetrieveData_EAddresses = 32
    '    ciRetrieveData_CustomFields = 64
    '    ciRetrieveData_Salutation = 128
    'End Enum

    Public Enum ciPrompts
        ciPrompt_MissingAddress = 1
        ciPrompt_MissingPhones = 2
        ciPrompt_MissingFax = 4
        ciPrompt_MissingEAddresses = 8
        ciPrompt_MultiplePhones = 16
        ciPrompt_MulitpleFax = 32
        ciPrompt_MulitpleEAddresses = 64
    End Enum
#End Region
    Public Class CConstants
#Region "******************fields***********************"
        Private Const xUNIDSep As String = "||"
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties***********************"
        Public ReadOnly Property UNIDSep As String
            Get
                Return xUNIDSep
            End Get
        End Property

        Public ReadOnly Property EmptyListingName As String
            Get
                Return "No name specified."
            End Get
        End Property

        Public ReadOnly Property MultiplePhones As String
            Get
                Return "zzciMultiplePhoneszz"
            End Get
        End Property
#End Region
    End Class
End Namespace
