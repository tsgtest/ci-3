Option Explicit On

Namespace LMP.CIO
    Public Class ICIOptions
#Region "******************enumerations***********************"
        Enum ciEmptyAddress
            ciEmptyAddress_ReturnEmpty = 0
            ciEmptyAddress_Warn = 1
            ciEmptyAddress_PromptForExisting = 2
        End Enum
#End Region
#Region "******************fields***********************"
        Public Property OnEmptyAddresses As ciEmptyAddress
            Get
            End Get
            Set(value As ciEmptyAddress)
            End Set
        End Property

        Public Property PromptForMissingPhones As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property PromptForMultiplePhones As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property NamesOnlyForTo As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property NamesOnlyForFrom As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property NamesOnlyForCC As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property NamesOnlyForBCC As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property AllowNativeAppEdits As Boolean
            Get
            End Get
            Set(value As Boolean)
            End Set
        End Property
#End Region
    End Class
End Namespace