Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CGlobals
#Region "******************initializer***********************"
        Private xObjectID As String
        Public Sub New()
            ' ''Randomize()
            ' ''xObjectID = "CGlobals" & Format(Now, "hhmmss") & Rnd()
            ' ''General.g_oObjects.Add(xObjectID, xObjectID)
            ' ''Functions.DebugPrint("Added - " & xObjectID & ": " & General.g_oObjects.Count)
            General.g_oSessionType = New CSessionType
        End Sub
#End Region
#Region "******************properties**************************"
        Public ReadOnly Property Objects As Collection
            Get
                Return General.g_oObjects
            End Get
        End Property
#End Region
#Region "******************methods**************************"
        Public Function CIEvents() As CEventGenerator
            'returns the one event generator object -
            'creates it if necessary
            Try
                If General.g_oEvents Is Nothing Then
                    General.g_oEvents = New CEventGenerator
                End If
                CIEvents = General.g_oEvents
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace

