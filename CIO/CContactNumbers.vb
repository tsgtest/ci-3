'**********************************************************
'   CContactNumbers Collection Class
'   created 10/05/01 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of contact numbers (i.e. phones/faxes/emails)
'   of a contact
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CContactNumbers
        Implements System.Collections.IEnumerable
#Region "******************fields***********************"
        Private m_colContactNumbers As Collection
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_colContactNumbers = New Collection
        End Sub
#End Region
#Region "******************collection methods***********************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Try
                GetEnumerator = m_colContactNumbers.GetEnumerator
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Add(ByVal vID As Object, ByVal xDescription As String, ByVal xNumber As String, _
                            ByVal xExtension As String, ByVal iType As ciContactNumberTypes, ByVal xCategory As String) As CContactNumber
            '   add contact number to collection of numbers
            Dim oContactNumber As CContactNumber

            Try
                'create new number
                oContactNumber = New CContactNumber

                'set properties of object
                With oContactNumber
                    .Description = xDescription
                    .Number = xNumber
                    .Extension = xExtension
                    .NumberType = iType
                    .ID = vID
                    .Category = xCategory
                End With

                Try
                    m_colContactNumbers.Add(oContactNumber, CStr(vID))
                Catch
                    Throw New Exception("Could not add ContactNumber with Key Value '" & _
                        iType & xDescription & "'")
                Finally
                    'return
                    Add = oContactNumber
                End Try
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Friend Function Delete(vKey As Object)
            Try
                m_colContactNumbers.Remove(vKey)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Count() As Integer
            Try
                Count = m_colContactNumbers.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Default ReadOnly Property Item(vKey As Object) As CContactNumber
            Get
                Try
                    Return m_colContactNumbers(vKey)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

#End Region
#Region "******************methods***********************"
        Public Function Prompt(oFormat As ICINumberPromptFormat) As CContactNumber
            'prompts user for a contact number - phone, fax, email
            Dim oForm As ContactNumbersForm

            Try
                oForm = New ContactNumbersForm
                With oForm
                    'give contact numbers to form
                    .Source = Me

                    'set contact number type - phone, fax, email
                    oFormat.NumberType = Me.Item(1).NumberType

                    'set the format for the dialog box
                    .Format = oFormat

                    'show '&'
                    oFormat.ContactName = Replace(oFormat.ContactName, "&", "&&")

                    .ShowDialog()

                    If Not .Cancelled Then
                        'get the selected contact number
                        Prompt = Me.Item(.dtgPhones.SelectedRows.Item(0).Index + 1)
                    Else
                        Prompt = Nothing
                    End If
                End With

                'close form
                Try
                    oForm = Nothing
                Catch
                End Try
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Sub AlertNoneIfSpecified(ByVal iType As ciContactNumberTypes, ByVal xContactName As String)
            'alerts user to no numbers if the user hasn't specified not to do so.

            Try
                If General.g_bAlertNoContactNumbers Then
                    Dim oForm As AlertNoContactNumbersForm
                    oForm = New AlertNoContactNumbersForm

                    xContactName = Replace(xContactName, "&", "&&")

                    With oForm
                        'set alert message
                        If iType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            .Text = " No Phone Numbers"
                            .lblMessage.Text = "No phone numbers were found for " & xContactName & "."
                        ElseIf iType = ciContactNumberTypes.ciContactNumberType_Fax Then
                            .Text = " No Fax Numbers"
                            .lblMessage.Text = "No fax numbers were found for " & xContactName & "."
                        Else
                            .Text = " No EMail Addresses"
                            .lblMessage.Text = "No email addresses were found for " & xContactName & "."
                        End If

                        'show dialog
                        .ShowDialog()

                        'cancel alerts if specified by user
                        General.g_bAlertNoContactNumbers = .chkPrompt.Checked = True
                    End With
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Public Function AlertNoneIfSpecifiedNew(ByVal iType As ciContactNumberTypes, _
                                                ByVal xContactName As String, _
                                                ByRef bCancel As Boolean)
            Dim xMessage As String = ""

            Try
                'alerts user to no numbers if the user hasn't specified not to do so.
                If General.g_bAlertNoContactNumbers Then
                    Dim oForm As AlertNoContactNumbersFormNew
                    oForm = New AlertNoContactNumbersFormNew

                    xContactName = Replace(xContactName, "&", "&&")

                    With oForm
                        'set alert message
                        If iType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            .Text = " No Phone Numbers"
                            xMessage = "No phone numbers were found for " & xContactName & "."
                        ElseIf iType = ciContactNumberTypes.ciContactNumberType_Fax Then
                            .Text = "No Fax Numbers"
                            xMessage = "No fax numbers were found for " & xContactName & "."
                        Else
                            .Text = " No EMail Addresses"
                            xMessage = "No email addresses were found for " & xContactName & "."
                        End If

                        .lblMessage.Text = xMessage & vbNewLine & vbNewLine & _
                                              "Do you want to insert the contact with the available information?"

                        'show dialog
                        .ShowDialog()

                        bCancel = .CancelInsert

                        'cancel alerts if specified by user
                        General.g_bAlertNoContactNumbers = .chkPrompt.Checked = True
                    End With
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace

