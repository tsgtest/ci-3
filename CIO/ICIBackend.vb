Option Explicit On

Namespace TSG.CI
#Region "******************enum***********************"
    'GLOG : 8819 : ceh
    'Public Enum ciAlerts
    '    ciAlert_None = 0
    '    ciAlert_All = 1
    '    ciAlert_MultiplePhones = 2
    '    ciAlert_NoPhones = 4
    '    ciAlert_MultipleFaxes = 8
    '    ciAlert_NoFaxes = 16
    '    ciAlert_MultipleEAddresses = 32
    '    ciAlert_NoEAddresses = 64
    '    ciAlert_NoAddresses = 128
    'End Enum
#End Region
    Public Interface ICIBackend
#Region "*********************properties**************************"

        Property DefaultSortColumn As ciListingCols

        ReadOnly Property IsLoadableEntity(ByVal xUNID As String) As Boolean

        ReadOnly Property IsSearchableEntity(ByVal xUNID As String) As Boolean

        ReadOnly Property InternalID As Integer

        Property ID As Integer

        ReadOnly Property Name As String

        ReadOnly Property DisplayName As String

        ReadOnly Property SupportsNativeSearch As Boolean

        ReadOnly Property IsConnected As Boolean

        ReadOnly Property Exists As Boolean

        ReadOnly Property SupportsFolders As Boolean

        ReadOnly Property SupportsNestedFolders As Boolean

        ReadOnly Property SearchOperators As ciSearchOperators

        ReadOnly Property SupportsContactEdit As Boolean

        ReadOnly Property SupportsContactAdd As Boolean

        ReadOnly Property SupportsMultipleStores As Boolean

        ReadOnly Property Col1Name As String

        ReadOnly Property Col2Name As String

        ReadOnly Property Col3Name As String

        ReadOnly Property Col4Name As String
#End Region
#Region "*********************methods**************************"
        Sub Initialize(iID As Integer)
        'ToDo
        'Attribute Initialize.VB_Description = "Allows the backend to execute any initialization procedures"

        Function Events() As CEventGenerator

        Function GetStores() As CStores

        Function Filter() As CFilter
        'ToDo?
        'Attribute Filter.VB_Description = "returns the collection of filter fields for the backend - delegate to CBackends.FilterFields if filter fields are listed in FilterFieldsX section of ci.ini."

        Function GetFolders(oStore As CStore) As CFolders

        Function GetSubFolders(oFolder As CFolder) As CFolders

        Function GetFolderListings(oFolder As CFolder, oFilter As CFilter) As CListings
        Function GetFolderListings(oFolder As CFolder) As CListings

        Function SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings

        Function SearchNative() As CListings

        Function GetStoreListings(oStore As CStore, oFilter As CFilter) As CListings
        Function GetStoreListings(oStore As CStore) As CListings

        Function SearchStore(oStore As CStore) As CListings

        Function GetAddresses(oListing As CListing) As CAddresses
        Function GetAddresses() As CAddresses

        Function GetContacts(oListing As CListing, _
                             oAddress As CAddress, _
                             ByVal vAddressType As Object,
                             ByVal iIncludeData As ciRetrieveData, _
                             ByVal iAlerts As ciAlerts) As ICContacts
        Function GetContacts(oListing As CListing, _
                             oAddress As CAddress, _
                             ByVal vAddressType As Object,
                             ByVal iIncludeData As ciRetrieveData) As ICContacts
        Function GetContacts(oListing As CListing, _
                             oAddress As CAddress, _
                             ByVal vAddressType As Object) As ICContacts
        Function GetContacts(oListing As CListing, _
                             oAddress As CAddress) As ICContacts

        Sub EditContact(oListing As CListing)

        Function GetCustomFields(oListing As CListing) As CCustomFields

        Function GetPhoneNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers

        Function GetPhoneNumbers(oListing As CListing) As CContactNumbers

        Function GetFaxNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers
        Function GetFaxNumbers(oListing As CListing) As CContactNumbers

        Function GetEMailNumbers(oListing As CListing, ByVal vAddressType As Object) As CContactNumbers
        Function GetEMailNumbers(oListing As CListing) As CContactNumbers

        Function HasAddresses(oListing As CListing) As Boolean

        Function NumberPromptFormat() As ICINumberPromptFormat

        Function GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Object) As CContactNumber

        Function GetFaxNumber(oListing As CListing, ByVal vFaxID As Object) As CContactNumber

        Function GetEMailNumber(oListing As CListing, ByVal vEMailID As Object) As CContactNumber

        Sub AddContact()

        Sub CustomProcedure1()

        Sub CustomProcedure2()

        Sub CustomProcedure3()

        Sub CustomProcedure4()

        Sub CustomProcedure5()

        Function CustomMenuItem1() As String

        Function CustomMenuItem2() As String

        Function CustomMenuItem3() As String

        Function CustomMenuItem4() As String

        Function CustomMenuItem5() As String

#End Region
    End Interface
End Namespace



