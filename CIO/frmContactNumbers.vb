﻿Option Explicit On

Imports System.Data

Namespace LMP.CIO
    Public Class frmContactNumbers

        Enum PhoneCols
            PhoneCols_AddrType = 0
            PhoneCols_Category = 1
            PhoneCols_Description = 2
            PhoneCols_Number = 3
            PhoneCols_Extension = 4
        End Enum

        Private m_oArray As DataTable
        Private m_bCancelled As Boolean

#Region "******************initializer***********************"
        Dim xObjectID As String
        Public Sub New()
            Randomize()
            xObjectID = "frmContactNumbers" & Format(Now, "hhmmss") & Rnd()
            General.g_oObjects.Add(xObjectID, xObjectID)
            Functions.DebugPrint("Added - " & xObjectID & ": " & General.g_oObjects.Count)
        End Sub
#End Region

#Region "******************fields***********************"
Property Set Format(oNew As ICINumberPromptFormat)
        With Me
            If oNew.PromptType = ciNumberPromptType_Missing Then
                .Caption = oNew.MissingNumbersDialogTitle
                .lblPhones.Caption = Replace(oNew.MissingNumbersDialogDescriptionText, "&", "&&")
            Else
                .Caption = oNew.MultipleNumbersDialogTitle
                .lblPhones.Caption = Replace(oNew.MultipleNumbersDialogDescriptionText, "&", "&&")
            End If
        End With

        With Me.lstPhones.Columns
            If oNew.AddressTypeWidth Then
                .Item(PhoneCols_AddrType).Caption = oNew.AddressTypeHeading
                .Item(PhoneCols_AddrType).Width = oNew.AddressTypeWidth
            Else
                .Item(PhoneCols_AddrType).Visible = False
            End If

            If oNew.CategoryWidth Then
                .Item(PhoneCols_Category).Caption = oNew.CategoryHeading
                .Item(PhoneCols_Category).Width = oNew.CategoryWidth
            Else
                .Item(PhoneCols_Category).Visible = False
            End If

            If oNew.DescriptionWidth Then
                .Item(PhoneCols_Description).Caption = oNew.DescriptionHeading
                .Item(PhoneCols_Description).Width = oNew.DescriptionWidth
            Else
                .Item(PhoneCols_Description).Visible = False
            End If

            If oNew.NumberWidth Then
                .Item(PhoneCols_Number).Caption = oNew.NumberHeading
                .Item(PhoneCols_Number).Width = oNew.NumberWidth
            Else
                .Item(PhoneCols_Number).Visible = False
            End If

            If oNew.ExtensionWidth Then
                .Item(PhoneCols_Extension).Caption = oNew.ExtensionHeading
                .Item(PhoneCols_Extension).Width = oNew.ExtensionWidth
            Else
                .Item(PhoneCols_Extension).Visible = False
            End If
        End With
    End Property

        Public ReadOnly Property Cancelled() As Boolean
            Get
                Cancelled = m_bCancelled
            End Get
        End Property

    Public Property Set Source(oNew As CIO.CContactNumbers)
        Dim oContactNum As CIO.CContactNumber
        Dim i As Integer

        On Error GoTo ProcError
        Set m_oArray = New XArrayDB

        m_oArray.ReDim 0, oNew.Count - 1, 0, 4

        For Each oContactNum In oNew
            With m_oArray
                .Value(i, PhoneCols_Category) = oContactNum.Category
                .Value(i, PhoneCols_Description) = oContactNum.Description
                .Value(i, PhoneCols_Number) = oContactNum.Number
                .Value(i, PhoneCols_Extension) = oContactNum.Extension
            End With
            i = i + 1
        Next oContactNum

        Me.lstPhones.Array = m_oArray
        Me.lstPhones.ReBind
        Exit Property
    ProcError:
        g_oError.RaiseError "CIO.frmPhones.Source"
        Exit Property
    End Property

#End Region
 
#Region "******************methods***********************"
        Public Sub SetMessage(ByVal xContactName As String, _
                              ByVal xAddrType As String, _
                              ByVal xPhoneType As String, _
                              Optional ByVal bPromptForMultiple As Boolean = False, _
                              Optional ByVal ShowAsEAddresses As Boolean = False)
            'set the message that's displayed when form is visible-
            'two types of prompts - missing phones/ multiple phones
            If ShowAsEAddresses Then
                Me.Caption = "Choose an E-Mail Address for " & xContactName
                If bPromptForMultiple Then
                    Me.lblPhones.Caption = "&There exist multiple e-mail" & _
                           " addresses for this address.  " & _
                           "Please select an alternate:"
                    Me.chkPromptForMissingPhones.Visible = False
                Else
                    Me.lblPhones.Caption = "&No e-mail" & _
                           " address is associated with the selected address for " & xContactName & _
                           ".  Please select an alternate:"
                    Me.chkPromptForMissingPhones.Visible = True
                End If
            Else
                Me.Caption = "Choose a " & xPhoneType & " Number for " & xContactName
                If bPromptForMultiple Then
                    Me.lblPhones.Caption = "&There exist multiple " & LCase(xPhoneType) & _
                           " numbers for this address.  " & _
                           "Please select an alternate:"
                    Me.chkPromptForMissingPhones.Visible = False
                Else
                    Me.lblPhones.Caption = "&No " & LCase(xPhoneType) & _
                           " number is associated with the selected address for " & xContactName & _
                           ".  Please select an alternate:"
                    Me.chkPromptForMissingPhones.Visible = True
                End If
            End If
        End Sub
#End Region


        '**********************************************************
        '   Event Procedures
        '**********************************************************
        Private Sub btnOK_Click()
            Me.Hide()
            DoEvents()
            m_bCancelled = False
        End Sub

        Private Sub btnCancel_Click()
            m_bCancelled = True
            Me.Hide()
            DoEvents()
        End Sub

        Private Sub Form_Activate()
            'select first item in list if possible
            Dim i As Integer

            On Error GoTo ProcError
            If Not (m_oArray Is Nothing) Then
                If m_oArray.Count(1) > 0 Then
                    '           select first row
                    With Me.lstPhones
                        '               select first row
                        .Row = 0
                        .SetFocus()
                    End With
                End If
            End If

            '   check prompting checkbox
            Me.chkPromptForMissingPhones.Value = 1
            Exit Sub
ProcError:
            g_oError.RaiseError "CIO.frmContactNumbers.Form_Activate"
            Exit Sub
        End Sub

        Private Sub Form_Load()
            Dim oSessionType As New CIO.CSessionType


            m_bCancelled = True
        End Sub

        'Private Sub Form_Paint()
        '    'Load icon
        '    With g_oSessionType
        '        If (.SessionType = ciSession_Connect) Then
        '            .SetIcon Me.hWnd, "CONNECTICON", False
        '        Else
        '            .SetIcon Me.hWnd, "TSGICON", False
        '        End If
        '    End With
        'End Sub

        Private Sub lstPhones_DblClick()
            btnOK_Click()
        End Sub
    End Class
End Namespace
