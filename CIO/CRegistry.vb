Option Explicit On

Imports Microsoft.Win32

Namespace TSG.CI
    Public Class CRegistry
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************properties********************"
        'returns the path to the CI root
        Public ReadOnly Property CIRegistryRoot() As String
            Get
                Return "Software\The Sackett Group\CI"
            End Get
        End Property

        Public ReadOnly Property HKeyLocalMachine() As RegistryKey
            Get
                Return Registry.LocalMachine
            End Get
        End Property

        Public ReadOnly Property HkeyClassesRoot() As RegistryKey
            Get
                Return Registry.ClassesRoot
            End Get
        End Property

        Public ReadOnly Property HKeyCurrentUser() As RegistryKey
            Get
                Return Registry.CurrentUser
            End Get
        End Property

#End Region
#Region "******************methods**************************"
        'GLOG : 8909 : ceh
        Public Function GetLocalMachineValue64Bit(xSubKey As String, xName As String) As String
            Dim xValue As String = ""
            Dim oRegKey As RegistryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)

            oRegKey = oRegKey.OpenSubKey(xSubKey)

            Try
                xValue = oRegKey.GetValue(xName).ToString()
            Catch
            End Try

            Return xValue
        End Function

        Public Function GetLocalMachineValue(xSubKey As String, xName As String) As String
            Dim vValue As Object = vbNull

            ReadValue(Registry.LocalMachine, xSubKey, xName, vValue)

            'GLOG : 8500 : jsw
            'exes running in 32-bit process needs to check
            '64-bit reg keys if 32-bit value not found
            Try
                If (String.IsNullOrEmpty(vValue)) Then
                    vValue = GetLocalMachineValue64Bit(xSubKey, xName)
                End If
            Catch
            End Try

            Return vValue
        End Function

        'checks registry for 64-bit install value
        ''will always be null if running in 32-bit process
        Public Function Is64Bit() As Boolean
            Dim vValue As Object = vbNull
            ReadValue(Registry.LocalMachine, Me.CIRegistryRoot, "64bit", vValue)
            Return vValue
        End Function

        Public Function ReadValue(ByVal ParentKey As RegistryKey, _
                                  ByVal SubKey As String, _
                                  ByVal ValueName As String, _
                                  ByRef Value As Object) As Long
            Dim regKey As RegistryKey
            Try
                'open the given subkey
                regKey = ParentKey.OpenSubKey(SubKey, False)
                If regKey Is Nothing Then
                    Value = Nothing
                End If

                'Get the value 
                Value = regKey.GetValue(ValueName)

                Return 0
            Catch e As Exception
                Return -1
            End Try
        End Function

        Public Function WriteValue(ByVal ParentKey As RegistryKey, _
                                   ByVal SubKey As String, _
                                   ByVal ValueName As String, _
                                   ByVal Value As Object) As Long
            Dim regKey As RegistryKey

            Try
                'Open the given subkey 
                regKey = ParentKey.OpenSubKey(SubKey, True)

                If regKey Is Nothing Then 'when subkey doesn't exist create it 
                    regKey = ParentKey.CreateSubKey(SubKey)
                End If

                'Once we have a handle to the key, set the value 
                regKey.SetValue(ValueName, Value)

                Return 0
            Catch e As Exception
                Return -1
            End Try
        End Function

        Public Function GetDLLOCXPath(ByVal xProgID As String) As String
            'returns the location of the dll, ocx, or exe
            'that contains the specified class
            Dim vCLSID As Object
            Dim vPath As Object
            Dim lRet As Long
            Dim iPos As Integer
            Dim iLastPos As Integer

            lRet = Me.ReadValue(Me.HkeyClassesRoot, xProgID & "\Clsid", "", vCLSID)

            lRet = Me.ReadValue(Me.HkeyClassesRoot, "CLSID\" & vCLSID & "\InprocServer32", "", vPath)

            'trim file name
            iPos = 1
            While InStr(iPos, vPath, "\") > 0
                iLastPos = InStr(iPos, vPath, "\")
                iPos = iLastPos + 1
            End While

            GetDLLOCXPath = Left(vPath, iLastPos)
        End Function
#End Region
    End Class
End Namespace