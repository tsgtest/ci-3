'**********************************************************
'   ICIBackends CollectionClass
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci backends
'**********************************************************
Option Explicit On

Namespace LMP.CIO
    Public Class CBackends
#Region "******************declarations***********************"
        Public Enum ciUNIDFields
            ciUNIDFields_Backend = 0
            ciUNIDFields_Store = 1
            ciUNIDFields_Folder = 2
            ciUNIDFields_Listing = 3
            ciUNIDFields_Address = 4
            ciUNIDFields_Contact = 5
        End Enum

        Public Enum ciUNIDTypes
            ciUNIDType_Backend = 0
            ciUNIDType_Store = 1
            ciUNIDType_Folder = 2
            ciUNIDType_Listing = 3
            ciUNIDType_Address = 4
            ciUNIDType_Contact = 5
        End Enum

        Private m_oCol As Collection
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oCol = New Collection
        End Sub
#End Region
#Region "ToDo"
        Public Property Get NewEnum() As IUnknown
                Set NewEnum = m_oCol.[_NewEnum]
        End Property
#End Region
#Region "******************methods***********************"
        Public Function ProgID(ByVal iBackendID As Integer) As String
            Dim xProgID As String
            Dim xDesc As String
            Dim oIni As CIni

            On Error GoTo ProcError
            oIni = New CIni

            'get from ini
            xProgID = oIni.GetIni("Backend" & iBackendID, "ProgID")

            'raise error if ini value is missing
            If Len(xProgID) = 0 Then
                xDesc = "Invalid Backend" & iBackendID & "\ProgID key in ci.ini."
                Err.Raise(ciErrs.ciErr_MissingOrInvalidINIKey, , xDesc)
                Exit Function
            End If

            ProgID = xProgID
            Exit Function
ProcError:
            General.g_oError.RaiseError("CBackends.ProgID")
        End Function

        Friend Function Add(ByVal iID As Integer) As CIO.ICIBackend
            'creates a new backend with id = iID, then
            'adds it to the collection.
            'returns the new backend
            Dim xProgID As String
            Dim xDesc As String
            Dim oIni As CIni
            Dim oB As CIO.ICIBackend

            On Error GoTo ProcError
            oIni = New CIni

            'get ini required ini values
            xProgID = Me.ProgID(iID)

            'create backend object
            oB = CreateObject(xProgID)

            'initialize
            oB.Initialize(iID)

            'add to collection
            On Error Resume Next
            Err.Clear()
            m_oCol.Add(oB, CStr(iID))
            If Err.Number <> 0 Then
                'could not add to collection - alert and exit
                On Error GoTo ProcError
                xDesc = "Could not add backend " & iID & "to collection of backends. " & _
                    Err.Description
                Err.Raise(Err.Number, , xDesc)
                Exit Function
            Else
                On Error GoTo ProcError
                Add = oB
            End If
            Exit Function
ProcError:
            General.g_oError.RaiseError("CIO.ICIBackends.Add")
        End Function

        Public Function Item(ByVal iID As Integer) As CIO.ICIBackend
            'Attribute Item.VB_UserMemId = 0
            'will return Nothing if an error is generated
            On Error Resume Next
            Item = m_oCol.Item(CStr(iID))
        End Function

        Public Function Count() As Integer
            Count = m_oCol.Count
        End Function

#End Region
    End Class
End Namespace
