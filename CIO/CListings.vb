'**********************************************************
'   CListings Collection Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of listings in a folder or store
'**********************************************************

'**********************************************************
Option Explicit On

Imports System.Collections
Imports System.Data
Imports LMP

Namespace TSG.CI
#Region "******************enumerations***********************"
    Public Enum ciListingCols
        ciListingCols_DisplayName = 0
        ciListingCols_AdditionalProperty1 = 1
        ciListingCols_AdditionalProperty2 = 2
        ciListingCols_AdditionalProperty3 = 3
        ciListingCols_UNID = 4
        ciListingCols_Type = 5
        ciListingCols_FolderPath = 6
    End Enum
#End Region
    Public Class CListings
#Region "******************fields***********************"
        Event ListingAdded(ByVal UNID As String, ByVal DisplayName As String, _
                    ByVal Company As String, ByVal LastName As String, _
                    ByVal FirstName As String, ByVal ListingType As ciListingType)
        Private m_oDT As DataTable
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            Try
                m_oDT = New DataTable
                'Add columns
                With m_oDT.Columns
                    .Add("DisplayName")
                    .Add("AdditionalProperty1")
                    .Add("AdditionalProperty2")
                    .Add("AdditionalProperty3")
                    .Add("UNID")
                    .Add("ListingType")
                    .Add("FolderPath")
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************collection methods******************"

        Public Sub Add(ByVal UNID As String, ByVal DisplayName As String, ByVal AdditionalProperty1 As String, _
            ByVal AdditionalProperty2 As String, ByVal AdditionalProperty3 As String, ByVal ListingType As ciListingType, _
            Optional ByVal FolderPath As String = "")

            Dim xListing(6) As Object

            Try
                'fill element with listing detail
                xListing(ciListingCols.ciListingCols_DisplayName) = DisplayName
                xListing(ciListingCols.ciListingCols_AdditionalProperty1) = AdditionalProperty1
                xListing(ciListingCols.ciListingCols_AdditionalProperty2) = AdditionalProperty2
                xListing(ciListingCols.ciListingCols_AdditionalProperty3) = AdditionalProperty3
                xListing(ciListingCols.ciListingCols_UNID) = UNID
                xListing(ciListingCols.ciListingCols_Type) = ListingType
                If FolderPath <> String.Empty Then
                    xListing(ciListingCols.ciListingCols_FolderPath) = FolderPath
                End If

                m_oDT.Rows.Add(xListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub SetCount(NumberOfListings As Long)
            'creates specified number of rows in xarray
            'ToDo - not needed?
            ' ''m_oDT.ReDim(1, NumberOfListings, 0, 6)
        End Sub

        Public Sub DeleteAll()
            Try
                m_oDT.Clear()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function Count() As Long
            Try
                Count = m_oDT.Rows.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Item(ByVal Index As Long) As CListing
            'retrieve listing specified by index
            Dim oL As CListing
            Dim oDR As DataRow

            Try
                If m_oDT.Rows.Count = 0 Or Index < 0 Or Index > m_oDT.Rows.Count Then
                    'there are no items
                    Item = Nothing
                    Exit Function
                Else
                    'create listing
                    oL = New CListing

                    'Get DataRow object
                    oDR = m_oDT.Rows(Index)

                    'fill listing
                    With oL
                        .UNID = oDR.Item(ciListingCols.ciListingCols_UNID)
                        .DisplayName = oDR.Item(ciListingCols.ciListingCols_DisplayName).ToString()
                        .AdditionalProperty1 = oDR.Item(ciListingCols.ciListingCols_AdditionalProperty1).ToString()
                        .AdditionalProperty2 = oDR.Item(ciListingCols.ciListingCols_AdditionalProperty2).ToString()
                        .AdditionalProperty3 = oDR.Item(ciListingCols.ciListingCols_AdditionalProperty3).ToString()
                        .FolderPath = oDR.Item(ciListingCols.ciListingCols_FolderPath).ToString()
                        .ListingType = [Enum].Parse(GetType(ciListingType), oDR.Item(ciListingCols.ciListingCols_Type))
                    End With
                End If

                'return listing
                Item = oL
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function ToArray() As DataTable
            ToArray = m_oDT
        End Function

        Public Sub Sort(Optional ByVal icol As ciListingCols = ciListingCols.ciListingCols_DisplayName)
            m_oDT.DefaultView.Sort = m_oDT.Columns(icol).ColumnName + " ASC"
            m_oDT = m_oDT.DefaultView.ToTable
        End Sub
#End Region
    End Class
End Namespace
