'**********************************************************
'   CSessionType Class
'   created 09/03/2010 by Charlie Homo
'   Contains properties and methods defining a
'   CI session
'**********************************************************
Option Explicit On

Namespace TSG.CI
    'Public Enum ciSessionType
    '    ciSession_CI = 1
    '    ciSession_Connect = 2
    'End Enum
    Public Class CSessionType
#Region "******************fields***********************"
        Private Declare Function GetSystemMetrics Lib "user32" ( _
              ByVal nIndex As Long _
           ) As Long

        Private Const SM_CXICON = 11
        Private Const SM_CYICON = 12

        Private Const SM_CXSMICON = 49
        Private Const SM_CYSMICON = 50

        Private Declare Function LoadImageAsString Lib "user32" Alias "LoadImageA" ( _
              ByVal hInst As Long, _
              ByVal lpsz As String, _
              ByVal uType As Long, _
              ByVal cxDesired As Long, _
              ByVal cyDesired As Long, _
              ByVal fuLoad As Long _
           ) As Long

        Private Const LR_DEFAULTCOLOR = &H0
        Private Const LR_MONOCHROME = &H1
        Private Const LR_COLOR = &H2
        Private Const LR_COPYRETURNORG = &H4
        Private Const LR_COPYDELETEORG = &H8
        Private Const LR_LOADFROMFILE = &H10
        Private Const LR_LOADTRANSPARENT = &H20
        Private Const LR_DEFAULTSIZE = &H40
        Private Const LR_VGACOLOR = &H80
        Private Const LR_LOADMAP3DCOLORS = &H1000
        Private Const LR_CREATEDIBSECTION = &H2000
        Private Const LR_COPYFROMRESOURCE = &H4000
        Private Const LR_SHARED = &H8000&

        Private Const IMAGE_ICON = 1

        Private Declare Function SendMessageLong Lib "user32" Alias "SendMessageA" ( _
              ByVal hWnd As Long, ByVal wMsg As Long, _
              ByVal wParam As Long, ByVal lParam As Long _
           ) As Long

        Private Const WM_SETICON = &H80

        Private Const ICON_SMALL = 0
        Private Const ICON_BIG = 1

        Private Declare Function GetWindow Lib "user32" ( _
           ByVal hWnd As Long, ByVal wCmd As Long) As Long
        Private Const GW_OWNER = 4

        Private m_iSessionType As ciSessionType

        Private Const xIco_CI As String = "TSG.ico"
        Private Const xIco_Connect As String = "CF.ico"
        Private Const xAppTitle_CI As String = "Contact Integration"
        Private Const xAppTitle_Connect As String = "Connect Forms"
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "*********************properties**************************"
        Public Property SessionType As ciSessionType
            Get
                Return m_iSessionType
            End Get
            Set(value As ciSessionType)
                m_iSessionType = value
            End Set
        End Property

        Public ReadOnly Property Ico As String
            Get
                Dim xTemp As String

                If m_iSessionType = ciSessionType.ciSession_Connect Then
                    xTemp = Functions.AppPath & xIco_Connect
                Else
                    xTemp = Functions.AppPath & xIco_CI
                End If

                'check for validity
                If Not System.IO.File.Exists(xTemp) Then
                    xTemp = ""
                End If

                Ico = xTemp
            End Get
        End Property

        Public ReadOnly Property AppTitle As String
            Get
                If m_iSessionType = ciSessionType.ciSession_Connect Then
                    AppTitle = xAppTitle_Connect
                Else
                    AppTitle = xAppTitle_CI
                End If
            End Get
        End Property

        Public ReadOnly Property AppTitleConnect As String
            Get
                Return xAppTitle_Connect
            End Get
        End Property

        Public ReadOnly Property AppTitleCI As String
            Get
                Return xAppTitle_CI
            End Get
        End Property
#End Region
#Region "*********************methods**************************"
        Public Sub SetIcon( _
              ByVal hWnd As Long, _
              ByVal sIconResName As String, _
              Optional ByVal bSetAsAppIcon As Boolean = True _
           )
            'todo
            ' ''Dim lhWndTop As Long
            ' ''Dim lhWnd As Long
            ' ''Dim cx As Long
            ' ''Dim cy As Long
            ' ''Dim hIconLarge As Long
            ' ''Dim hIconSmall As Long

            ' ''If (bSetAsAppIcon) Then
            ' ''    ' Find VB's hidden parent window:
            ' ''    lhWnd = hWnd
            ' ''    lhWndTop = lhWnd
            ' ''    Do While Not (lhWnd = 0)
            ' ''        lhWnd = GetWindow(lhWnd, GW_OWNER)
            ' ''        If Not (lhWnd = 0) Then
            ' ''            lhWndTop = lhWnd
            ' ''        End If
            ' ''    Loop
            ' ''End If

            ' ''cx = GetSystemMetrics(SM_CXICON)
            ' ''cy = GetSystemMetrics(SM_CYICON)
            '' ''ToDo
            '' ''hIconLarge = LoadImageAsString( _
            '' ''      hInstance, sIconResName, _
            '' ''      IMAGE_ICON, _
            '' ''      cx, cy, _
            '' ''      LR_SHARED)
            ' ''If (bSetAsAppIcon) Then
            ' ''    SendMessageLong(lhWndTop, WM_SETICON, ICON_BIG, hIconLarge)
            ' ''End If
            ' ''SendMessageLong(hWnd, WM_SETICON, ICON_BIG, hIconLarge)

            ' ''cx = GetSystemMetrics(SM_CXSMICON)
            ' ''cy = GetSystemMetrics(SM_CYSMICON)
            '' ''ToDo
            '' ''hIconSmall = LoadImageAsString( _
            '' ''      hInstance, sIconResName, _
            '' ''      IMAGE_ICON, _
            '' ''      cx, cy, _
            '' ''      LR_SHARED)
            ' ''If (bSetAsAppIcon) Then
            ' ''    SendMessageLong(lhWndTop, WM_SETICON, ICON_SMALL, hIconSmall)
            ' ''End If
            ' ''SendMessageLong(hWnd, WM_SETICON, ICON_SMALL, hIconSmall)

        End Sub
#End Region
    End Class
End Namespace