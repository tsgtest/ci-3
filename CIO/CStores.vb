'**********************************************************
'   CStores Collection Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci stores
'**********************************************************
Option Explicit On

Imports LMP

Namespace TSG.CI
    Public Class CStores
        Implements System.Collections.IEnumerable
#Region "******************fields***********************"
        Private m_oCol As Collection
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_oCol = New Collection
        End Sub
#End Region
#Region "******************collection methods******************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            GetEnumerator = m_oCol.GetEnumerator
        End Function

        Public Sub Add(oStore As CStore)
            Dim xDesc As String

            'add to collection
            Try
                m_oCol.Add(oStore, oStore.UNID)
            Catch ex As Exception
                'could not add to collection - alert and exit
                xDesc = "Could not add store " & oStore.Name & _
                    " to the collection of stores. " & Err.Description
                Throw New Exception(xDesc)
            End Try
        End Sub

        Default ReadOnly Property Item(vKey As Object) As CStore
            Get
                Try
                    Return m_oCol(vKey)
                Catch
                End Try
            End Get
        End Property

        Public Function Count() As Integer
            Count = m_oCol.Count
        End Function

#End Region
    End Class
End Namespace



