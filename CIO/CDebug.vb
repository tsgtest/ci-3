'**********************************************************
'   CDebug Class
'   created 1/29/03 by Daniel Fisherman
'   Contains properties and methods of a CI contact
'**********************************************************
Option Explicit On

Namespace LMP.CIO
    Public Class CDebug
#Region "******************methods***********************"
        Public Sub StartDebug()

        End Sub

        Public Sub EndDebug()

        End Sub

        Public Sub PrintMessage(ByVal xText As String, ByVal xSource As String)

        End Sub

        Public Sub ClearMessages()

        End Sub
#End Region
    End Class
End Namespace

