Option Explicit On

Namespace TSG.CI
    Public Class CEventGenerator
#Region "******************fields***********************"
        Public Event AfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, ByRef Cancel As Boolean)
        Public Event BeforeStoreSearchFolderSearch(ByVal xFolderName As String, Cancel As Boolean)
        Public Event BeforeListingsRetrieved(ByVal lTotal As Long, Cancel As Boolean)
        Public Event AfterListingsRetrieved(ByVal lTotal As Long)
        Public Event AfterStoreSearched()
        Public Event BeforeExecutingListingsQuery()
        Public Event AfterExecutingListingsQuery()
#End Region
#Region "******************initializer***********************"
        Public Sub New()
        End Sub
#End Region
#Region "******************methods**************************"
        Public Sub RaiseAfterStoreSearched()
            RaiseEvent AfterStoreSearched()
        End Sub

        Public Sub RaiseBeforeStoreSearchFolderSearch(ByVal xFolderName As String, Cancel As Boolean)
            RaiseEvent BeforeStoreSearchFolderSearch(xFolderName, Cancel)
        End Sub

        Public Sub RaiseAfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, ByRef Cancel As Boolean)
            RaiseEvent AfterListingAdded(lIndexAdded, lTotal, Cancel)
        End Sub

        Public Sub RaiseBeforeListingsRetrieved(ByVal lTotal As Long, Cancel As Boolean)
            RaiseEvent BeforeListingsRetrieved(lTotal, Cancel)
        End Sub

        Public Sub RaiseAfterListingsRetrieved(ByVal lTotal As Long)
            RaiseEvent AfterListingsRetrieved(lTotal)
        End Sub

        Public Sub RaiseBeforeExecutingListingsQuery()
            RaiseEvent BeforeExecutingListingsQuery()
        End Sub

        Public Sub RaiseAfterExecutingListingsQuery()
            RaiseEvent AfterExecutingListingsQuery()
        End Sub
#End Region
    End Class
End Namespace
