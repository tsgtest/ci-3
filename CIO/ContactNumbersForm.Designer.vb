﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ContactNumbersForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ContactNumbersForm))
        Me.lblPhones = New System.Windows.Forms.Label()
        Me.chkPromptForMissingPhones = New System.Windows.Forms.CheckBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.dtgPhones = New System.Windows.Forms.DataGridView()
        CType(Me.dtgPhones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPhones
        '
        Me.lblPhones.Location = New System.Drawing.Point(16, 16)
        Me.lblPhones.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPhones.Name = "lblPhones"
        Me.lblPhones.Size = New System.Drawing.Size(639, 70)
        Me.lblPhones.TabIndex = 0
        Me.lblPhones.Text = "##"
        '
        'chkPromptForMissingPhones
        '
        Me.chkPromptForMissingPhones.AutoSize = True
        Me.chkPromptForMissingPhones.Checked = True
        Me.chkPromptForMissingPhones.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPromptForMissingPhones.Location = New System.Drawing.Point(21, 287)
        Me.chkPromptForMissingPhones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.chkPromptForMissingPhones.Name = "chkPromptForMissingPhones"
        Me.chkPromptForMissingPhones.Size = New System.Drawing.Size(526, 29)
        Me.chkPromptForMissingPhones.TabIndex = 2
        Me.chkPromptForMissingPhones.Text = "&Continue to prompt for contact numbers when necessary"
        Me.chkPromptForMissingPhones.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(497, 346)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(163, 58)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(322, 346)
        Me.btnOK.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(163, 58)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "O&K"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'dtgPhones
        '
        Me.dtgPhones.AllowUserToAddRows = False
        Me.dtgPhones.AllowUserToDeleteRows = False
        Me.dtgPhones.AllowUserToResizeRows = False
        Me.dtgPhones.BackgroundColor = System.Drawing.Color.White
        Me.dtgPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgPhones.Location = New System.Drawing.Point(16, 89)
        Me.dtgPhones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtgPhones.Name = "dtgPhones"
        Me.dtgPhones.RowHeadersVisible = False
        Me.dtgPhones.RowTemplate.Height = 24
        Me.dtgPhones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dtgPhones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgPhones.Size = New System.Drawing.Size(639, 189)
        Me.dtgPhones.TabIndex = 1
        '
        'ContactNumbersForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(168.0!, 168.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(668, 420)
        Me.Controls.Add(Me.dtgPhones)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.chkPromptForMissingPhones)
        Me.Controls.Add(Me.lblPhones)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ContactNumbersForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "##"
        CType(Me.dtgPhones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPhones As System.Windows.Forms.Label
    Friend WithEvents chkPromptForMissingPhones As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents dtgPhones As System.Windows.Forms.DataGridView
End Class
