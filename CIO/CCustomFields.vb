'**********************************************************
'   CustomFields Collection Class
'   created 6/30/99 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of CustomFields of a contact
'**********************************************************
Option Explicit On

Imports LMP


Namespace TSG.CI
    Public Class CCustomFields
        Implements System.Collections.IEnumerable
        Implements ICCustomFields
#Region "******************fields***********************"
        Private m_CustomField As ICCustomField
        Private m_colCustomFields As Collection
        Private m_debugID As Long
        Private xObjectID As String
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            m_colCustomFields = New Collection
        End Sub
#End Region
#Region "******************properties***********************"
        Default ReadOnly Property Item(vKey As Object) As ICCustomField Implements ICCustomFields.Item
            Get
                Try
                    Return m_colCustomFields(vKey)
                Catch
                End Try
            End Get
        End Property
#End Region
#Region "******************collection methods***********************"
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Try
                GetEnumerator = m_colCustomFields.GetEnumerator
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Add(ByVal vID As Object, _
                             ByVal xName As String, _
                             Optional ByVal xValue As String = "") As ICCustomField Implements ICCustomFields.Add

            Try
                m_CustomField = General.CreateCustomFieldObject()
                With m_CustomField
                    .ID = vID
                    .Name = xName
                    .Value = xValue
                End With
                m_colCustomFields.Add(m_CustomField, xName)
                Add = m_CustomField
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Friend Function Delete(vKey As Object) Implements ICCustomFields.Delete
            Try
                m_colCustomFields.Remove(vKey)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Function Count() As Integer Implements ICCustomFields.Count
            Count = 0
            Try
                Count = m_colCustomFields.Count
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
    End Class
End Namespace
