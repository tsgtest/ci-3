Option Explicit On

Imports System.Collections
Imports System.Data
Imports System.IO
Imports System.Reflection
Imports LMP


Namespace TSG.CI
    Public Class General
#Region "******************fields***********************"
        Public Shared g_oError As CError
        Public Shared g_oEvents As CEventGenerator
        Public Shared g_xUserIni As String
        Public Shared g_oSessionType As CSessionType
        Public Shared g_bAlertNoContactNumbers As Boolean

        'Contact Prefix/Suffix options
        Public Shared g_oArrSuffixSeparatorExclusions As DataTable
        Public Shared g_oArrPrefix As DataTable
        Public Shared g_oArrSuffix As DataTable
        Public Shared g_oArrSuffixToExclude As DataTable
        Public Shared g_oArrPrefixToExclude As DataTable
        Public Shared g_xSuffixSeparator As String

        Public Shared g_oObjects As Collection
        Public Shared g_xLocalConnectionProvider As String

#End Region
#Region "******************initializer***********************"
        Sub Main()
            'initialize a new error and session
            g_oObjects = New Collection
        End Sub
#End Region
#Region "******************methods***********************"
        Public Shared Function GetUNIDField(ByVal vUNID As Object, ByVal iField As ciUNIDFields)
            'returns the value of the specified UNID field
            Dim iPos1 As Integer
            Dim iPos2 As Integer
            Dim xDesc As String
            Dim i As Integer
            Dim xClass As String
            Dim oConst As CConstants

            Try
                oConst = New CConstants
                xClass = ""

                '   get class name represented by field number
                Select Case iField
                    Case ciUNIDFields.ciUNIDFields_Address
                        xClass = "CAddress"
                    Case ciUNIDFields.ciUNIDFields_Backend
                        xClass = "CBackend"
                    Case ciUNIDFields.ciUNIDFields_ContactNumbers
                        xClass = "CContactNumbers"
                    Case ciUNIDFields.ciUNIDFields_Folder
                        xClass = "CFolder"
                    Case ciUNIDFields.ciUNIDFields_Listing
                        xClass = "CListing"
                    Case ciUNIDFields.ciUNIDFields_Store
                        xClass = "CStore"
                End Select

                'remove 'b' if necessary
                If Left(vUNID, 1) = "b" Then
                    vUNID = Mid(vUNID, 2)
                End If

                'get starting position - find appropriate separator
                For i = 1 To iField
                    iPos1 = InStr(iPos1 + Len(oConst.UNIDSep), CStr(vUNID), oConst.UNIDSep)
                    If iPos1 = 0 Then
                        'something is wrong - all listing elements UNIDs
                        'should have four elements separated by three separators
                        xDesc = "Invalid " & xClass & " UNID: " & CStr(vUNID)
                        Throw New Exception(xDesc)
                    End If
                Next i

                If iPos1 Then
                    'starting position is after separator
                    iPos1 = iPos1 + Len(oConst.UNIDSep)

                    'get ending position
                    Try
                        iPos2 = InStr(iPos1, CStr(vUNID), oConst.UNIDSep)
                    Catch
                    End Try
                Else
                    'starting position is the first character
                    iPos1 = 1

                    'get ending position
                    Try
                        iPos2 = InStr(CStr(vUNID), oConst.UNIDSep)
                    Catch
                    End Try
                End If

                'if ipos2 is 0, this is the last field -
                'set position as the last char
                If iPos2 = 0 Then
                    GetUNIDField = Mid(CStr(vUNID), iPos1)
                Else
                    'get position before last separator
                    GetUNIDField = Mid(CStr(vUNID), iPos1, iPos2 - iPos1)
                End If

                If GetUNIDField = String.Empty Then
                    'something is wrong - this
                    'should not be empty
                    xDesc = "Invalid " & xClass & " UNID: " & CStr(vUNID)
                    Throw New Exception(xDesc)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Shared Sub LoadPrefixSuffixOptions()
            Dim oIni As New CIni
            Dim i As Integer
            Dim aSuffixSeparatorExclusions() As String
            Dim aSuffixExclusions() As String
            Dim aSuffixExclusionPrefixes() As String
            Dim aPrefixExclusions() As String
            Dim aPrefixExclusionSuffixes() As String
            Dim oPrimaryKeys(1) As DataColumn

            'Contact Prefix/Suffix logic
            General.g_xSuffixSeparator = oIni.GetIni("Contact Suffix Formats", "SuffixSeparator")
            If General.g_xSuffixSeparator = "" Then
                General.g_xSuffixSeparator = ", "
            Else
                'Strip Pipe delimiters - allows specifying empty separator using ||
                General.g_xSuffixSeparator = Replace(g_xSuffixSeparator, "|", "")
            End If

            'Initialize global objects
            g_oArrSuffixSeparatorExclusions = New DataTable
            g_oArrPrefix = New DataTable
            g_oArrSuffix = New DataTable
            g_oArrSuffixToExclude = New DataTable
            g_oArrPrefixToExclude = New DataTable

            'Add Column and set Primary Key
            oPrimaryKeys(0) = g_oArrSuffixSeparatorExclusions.Columns.Add("Item")
            g_oArrSuffixSeparatorExclusions.PrimaryKey = oPrimaryKeys

            oPrimaryKeys(0) = g_oArrPrefix.Columns.Add("Item")
            g_oArrPrefix.PrimaryKey = oPrimaryKeys

            oPrimaryKeys(0) = g_oArrSuffix.Columns.Add("Item")
            g_oArrSuffix.PrimaryKey = oPrimaryKeys

            oPrimaryKeys(0) = g_oArrSuffixToExclude.Columns.Add("Item")
            g_oArrSuffixToExclude.PrimaryKey = oPrimaryKeys

            oPrimaryKeys(0) = g_oArrPrefixToExclude.Columns.Add("Item")
            g_oArrPrefixToExclude.PrimaryKey = oPrimaryKeys

            On Error GoTo LeaveSub

            'Suffixes that don't use separators
            aSuffixSeparatorExclusions = Split(oIni.GetIni("Contact Suffix Formats", "SeparatorExclusions"), ",")
            For i = 0 To UBound(aSuffixSeparatorExclusions)
                g_oArrSuffixSeparatorExclusions.Rows.Add(aSuffixSeparatorExclusions(i))
            Next i

            'Exclude these suffixes when Prefix is in corresponding list
            aSuffixExclusionPrefixes = Split(oIni.GetIni("Contact Suffix Exclusions", "Prefix"), ",")
            For i = 0 To UBound(aSuffixExclusionPrefixes)
                g_oArrPrefix.Rows.Add(aSuffixExclusionPrefixes(i))
            Next i

            aSuffixExclusions = Split(oIni.GetIni("Contact Suffix Exclusions", "SuffixToExclude"), ",")
            For i = 0 To UBound(aSuffixExclusions)
                g_oArrSuffixToExclude.Rows.Add(aSuffixExclusions(i))
            Next i

            'Exclude these prefixes when Suffix is in corresponding list
            aPrefixExclusionSuffixes = Split(oIni.GetIni("Contact Prefix Exclusions", "Suffix"), ",")
            For i = 0 To UBound(aPrefixExclusionSuffixes)
                g_oArrSuffix.Rows.Add(aPrefixExclusionSuffixes(i))
            Next i

            aPrefixExclusions = Split(oIni.GetIni("Contact Prefix Exclusions", "PrefixToExclude"), ",")
            For i = 0 To UBound(aPrefixExclusions)
                g_oArrPrefixToExclude.Rows.Add(aPrefixExclusions(i))
            Next i
LeaveSub:
        End Sub

        Public Shared Sub SetConnectionProvider()
            Dim oReg As New CRegistry

            If oReg.Is64Bit() Then
                g_xLocalConnectionProvider = "Microsoft.ACE.OLEDB.12.0"
            Else
                g_xLocalConnectionProvider = "Microsoft.Jet.OLEDB.4.0"
            End If
        End Sub

        Public Shared Function CreateCustomFieldObject() As ICCustomField
            Dim xCIOAssembly As String
            Dim oFile As FileInfo
            Dim oAsm As Assembly

            xCIOAssembly = Assembly.GetExecutingAssembly().CodeBase
            xCIOAssembly = xCIOAssembly.Replace("file:///", "")
            'JTS 10/6/16: Directory is part of CodeBase
            oFile = New FileInfo(xCIOAssembly)
            'xCIOAssembly = oFile.Directory.FullName & "\" & "CIO.dll"
            oAsm = Assembly.LoadFrom(xCIOAssembly)
            CreateCustomFieldObject = oAsm.CreateInstance("TSG.CI.CCustomField", True)
        End Function

#End Region
    End Class
End Namespace