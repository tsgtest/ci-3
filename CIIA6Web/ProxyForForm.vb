﻿Option Explicit On

Imports LexisNexis.InterAction.API.DotNet.Domain
Imports System.Windows.Forms
Imports LMP

Namespace TSG.CI.IA6Web
    Public Class ProxyForForm
#Region "******************fields***********************"
        Private m_bCancelled As Boolean
        Private m_bEndProxy As Boolean
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property Cancelled As Boolean
            Set(value As Boolean)

                m_bCancelled = value
            End Set
            Get
                Cancelled = m_bCancelled
            End Get
        End Property

        Public Property EndProxy As Boolean
            Set(value As Boolean)

                m_bEndProxy = value
            End Set
            Get
                EndProxy = m_bEndProxy
            End Get
        End Property

#End Region
#Region "******************events***********************"
        Private Sub ProxyForForm_Activated(sender As Object, e As EventArgs) Handles Me.Activated
            Try
                If Me.lstUsers.Items.Count > 0 Then
                    If GlobalMethods.g_oCnn.CurrentProxyUser Is Nothing Then
                        Me.lstUsers.ClearSelected()
                        Me.btnEndProxy.Enabled = False
                        Me.btnOK.Enabled = False
                    Else
                        Me.lstUsers.Text = GlobalMethods.g_oCnn.CurrentProxyUser.DisplayName
                    End If
                Else
                    Me.lstUsers.Enabled = False
                    Me.btnEndProxy.Enabled = False
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ProxyForForm_Load(sender As Object, e As EventArgs) Handles Me.Load
            Try
                LoadUsers()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click,
                                                                          lstUsers.DoubleClick
            Try
                m_bCancelled = False
                m_bEndProxy = False
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click,
                                                                              Me.FormClosed
            Try
                m_bCancelled = True
                m_bEndProxy = False
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnEndProxy_Click(sender As Object, e As EventArgs) Handles btnEndProxy.Click
            Try
                m_bCancelled = False
                m_bEndProxy = True
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstUsers_SelectedValueChanged(sender As Object, e As EventArgs) Handles lstUsers.SelectedValueChanged
            Try
                Me.btnOK.Enabled = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************methods***********************"
        Private Sub LoadUsers()
            Dim oDT As DataTable
            Dim ilProxyUsers As IList
            Dim iNum As Integer
            Dim i As Integer
            Dim oProxy As User

            oDT = New DataTable

            ' Add two columns
            oDT.Columns.Add("Name")
            oDT.Columns.Add("ID")

            ilProxyUsers = GlobalMethods.g_oCnn.Users.GetUsersWhoICanProxyFor()

            iNum = ilProxyUsers.Count

            For i = 1 To iNum
                oProxy = ilProxyUsers(i - 1)
                'get name & id
                oDT.Rows.Add(oProxy.DisplayName,
                             oProxy.Id.SingleId.ToString())
            Next i

            'load control
            With Me.lstUsers
                .DisplayMember = oDT.Columns(0).ColumnName
                .ValueMember = oDT.Columns(1).ColumnName
                .DataSource = oDT
            End With
        End Sub
#End Region
    End Class
End Namespace