﻿Option Explicit On

Imports LMP

Namespace TSG.CI.IA6Web
    Public Class AddressesForm
#Region "******************fields***********************"
        Private m_oAddresses As CAddresses
        Private m_oListing As CListing
        Private m_bCancelled As Boolean
        Private m_bPromptForMissingPhones As Boolean
        Private m_bPromptForMultiplePhones As Boolean
        Private m_bPromptForMultiple As Boolean
        Private m_oB As ICIBackend
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()
        End Sub
#End Region
#Region "******************properties***********************"
        Public Property Listing As CListing
            Set(value As CListing)
                m_oListing = value
            End Set
            Get
                Listing = m_oListing
            End Get
        End Property

        Public Property Cancelled As Boolean
            Set(value As Boolean)

                m_bCancelled = value
            End Set
            Get
                Cancelled = m_bCancelled
            End Get
        End Property

        Public WriteOnly Property Backend As ICIBackend
            Set(value As ICIBackend)
                m_oB = value
            End Set
        End Property

        Public Property Addresses As CAddresses
            Set(value As CAddresses)
                m_oAddresses = value
            End Set
            Get
                Addresses = m_oAddresses
            End Get
        End Property
#End Region
#Region "******************events***********************"
        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
            Try
                Me.Cancelled = True
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
            Try
                Me.Hide()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub AddressesForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
            Try
                If Me.lstAddressTypes.Items.Count > 1 Then
                    Me.lstAddressTypes.SetSelected(0, True)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstAddressTypes_Click(sender As Object, e As EventArgs) Handles lstAddressTypes.Click
            Try
                ShowListingDetail()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub AddressesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            '   turn off prompt for existing addresses and phones-
            '   will get reset in btnOK_Click, btnCancel_Click
            Dim oAddress As CAddress

            Try
                Me.Cancelled = False

                With Me.lstAddressTypes
                    'load address types into list
                    For Each oAddress In m_oAddresses
                        .Items.Add(oAddress.Name)
                    Next oAddress


                    Try
                        .SelectedItem = 0
                    Catch
                    End Try

                    'load message caption
                    Me.lblAddresses.Text = "The requested address for '" & _
                        Me.Listing.DisplayName & "' does not exist.  Please select " & _
                        "from one of the available addresses listed below."
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub lstAddressTypes_DoubleClick(sender As Object, e As EventArgs) Handles lstAddressTypes.DoubleClick
            Try
                Me.btnOK_Click(sender, e)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************methods***********************"
        Private Sub ShowListingDetail()
            'displays detail form with detail for
            'specified listing with specified address

            Dim oAddress As CAddress
            Dim oUNID As CUNID
            Dim oContact As CContact
            Dim xToken As String

            'get address type from dlg
            oUNID = New CUNID

            With Me.lstAddressTypes
                If .SelectedIndex > -1 Then
                    oAddress = m_oAddresses.Item(.SelectedIndex + 1)

                    oContact = m_oB.GetContacts(oUNID.GetListing(oAddress.UNID), _
                        oAddress, vbNull, ciRetrieveData.ciRetrieveData_Addresses, ciAlerts.ciAlert_None).Item(1)

                    xToken = "<STREET1>" & vbCrLf & _
                            "<STREET2>" & vbCrLf & _
                            "<STREET3>" & vbCrLf & _
                            "<CITY>" & vbCrLf & _
                            "<STATE>" & vbCrLf & _
                            "<ZIPCODE>" & vbCrLf & _
                            "<COUNTRY>" & vbCrLf

                    Me.lblDetail.Text = oContact.GetDetail(xToken, False)
                End If
            End With

            'enable OK if there is detail
            Me.btnOK.Enabled = (Me.lblDetail.Text <> "")
        End Sub

#End Region
    End Class
End Namespace