Option Explicit On

Imports [Interface].Foundation.Common
Imports LexisNexis.InterAction.API.Common
Imports LexisNexis.InterAction.API.Common.Enumeration
Imports LexisNexis.InterAction.API.Common.Enumeration.Data
Imports LexisNexis.InterAction.API.DotNet.Domain.ContactLists
Imports LexisNexis.InterAction.API.DotNet.Domain.Contacts
Imports LexisNexis.InterAction.API.DotNet.Domain.PhonesAndAddresses
Imports LexisNexis.InterAction.API.DotNet.Exceptions
Imports LexisNexis.InterAction.API.DotNet.Manager.ReturnOptions
Imports LexisNexis.InterAction.API.DotNet.Manager.SearchCriteria
Imports LexisNexis.InterAction.API.DotNet
Imports LexisNexis.InterAction.API.DotNet.Domain
Imports LMP

Namespace TSG.CI.IA6Web
    Friend Class GlobalMethods
#Region "******************fields***********************"
        Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
            "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal _
            lpString As String, ByVal lpFileName As String) As Long

        Public Const ciIA6InternalID As Integer = 101

        Public Shared g_vCols(3, 1) As Object
        Public Shared g_iSortCol As ciListingCols
        Public Shared g_bDisplayByContactType As Boolean
        Public Shared g_oCnn As Connection

        Public Shared g_oProxiedUser As User

        Public Shared g_oError As CError
        Public Shared g_oGlobals As CGlobals
        Public Shared g_oIni As CIni
        Public Shared g_oReg As CRegistry
        Public Shared g_oSession As CSessionType
        Public Shared g_oConstants As CConstants
        Public Shared g_iID As Integer

        Private Const ciIA6IDConversion As Long = 4294967296
#End Region
#Region "******************methods***********************"
        Public Shared Function GetLongID(oDualID As DualId) As Long
            'returns the Long ID using DualID object
            GetLongID = oDualID.Aux + (oDualID.Source * ciIA6IDConversion)
        End Function

        Public Shared Function GetShortID(oDualID As DualId, lLongID As Long) As Long
            'returns the Long ID using DualID object
            GetShortID = lLongID - (oDualID.Source * ciIA6IDConversion)
        End Function

        Public Shared Function GetMaxResults() As Long
            'returns the maximum number of contacts that can be retrieved
            Dim xMaxresults As String = ""
            Dim lMaxResults As Long = 0

            Try
                xMaxresults = g_oIni.GetUserIni("CIIAWeb", "MaxResults")
                If xMaxresults = "" Or Not IsNumeric(xMaxresults) Then
                    lMaxResults = 500
                Else
                    lMaxResults = CLng(xMaxresults)
                End If
                GetMaxResults = lMaxResults
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Public Shared Function Max(i As Double, j As Double) As Double
            If i > j Then
                Max = i
            Else
                Max = j
            End If
        End Function

        Public Shared Function Min(i As Double, j As Double) As Double
            If i > j Then
                Min = j
            Else
                Min = i
            End If
        End Function

        Public Shared Function IsEven(dNum As Double) As Boolean
            If dNum / 2 = CLng(dNum / 2) Then
                IsEven = True
            Else
                IsEven = False

            End If
        End Function

        Public Shared Function xSubstitute(ByVal xString As String, _
                             ByVal xSearch As String, _
                             ByVal xReplace As String) As String
            'replaces xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer
            Dim xNewString As String

            xNewString = Nothing

            'get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            'remove switch all chars
            While iSeachPos
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
                iSeachPos = InStr(UCase(xString), _
                                  UCase(xSearch))

            End While

            xNewString = xNewString & xString
            xSubstitute = xNewString
        End Function

        Public Shared Function CurrentTick() As Long
            CurrentTick = Environment.TickCount ' GetTickCount()
        End Function

        Function ElapsedTime(lStartTick As Long) As Single
            'returns the time elapsed from lStartTick-
            'precision in milliseconds
            ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
        End Function

        Public Sub DebugPrint(xOutput As String)
            Debug.Print(xOutput)
            CError.OutputDebugString(xOutput)
        End Sub

#End Region
    End Class
End Namespace