﻿Namespace TSG.CI.IA6Web
    Partial Class LoginForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.btnOK = New System.Windows.Forms.Button()
            Me.txtPassword = New System.Windows.Forms.TextBox()
            Me.txtName = New System.Windows.Forms.TextBox()
            Me.lblPassword = New System.Windows.Forms.Label()
            Me.lblName = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'btnCancel
            '
            Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.btnCancel.Location = New System.Drawing.Point(169, 81)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(74, 25)
            Me.btnCancel.TabIndex = 5
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'btnOK
            '
            Me.btnOK.Location = New System.Drawing.Point(87, 81)
            Me.btnOK.Name = "btnOK"
            Me.btnOK.Size = New System.Drawing.Size(74, 25)
            Me.btnOK.TabIndex = 4
            Me.btnOK.Text = "O&K"
            Me.btnOK.UseVisualStyleBackColor = True
            '
            'txtPassword
            '
            Me.txtPassword.Location = New System.Drawing.Point(87, 41)
            Me.txtPassword.Name = "txtPassword"
            Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
            Me.txtPassword.Size = New System.Drawing.Size(156, 20)
            Me.txtPassword.TabIndex = 3
            '
            'txtName
            '
            Me.txtName.Location = New System.Drawing.Point(87, 12)
            Me.txtName.Name = "txtName"
            Me.txtName.Size = New System.Drawing.Size(156, 20)
            Me.txtName.TabIndex = 1
            '
            'lblPassword
            '
            Me.lblPassword.AutoSize = True
            Me.lblPassword.Location = New System.Drawing.Point(18, 44)
            Me.lblPassword.Name = "lblPassword"
            Me.lblPassword.Size = New System.Drawing.Size(56, 13)
            Me.lblPassword.TabIndex = 2
            Me.lblPassword.Text = "Password:"
            '
            'lblName
            '
            Me.lblName.AutoSize = True
            Me.lblName.Location = New System.Drawing.Point(18, 15)
            Me.lblName.Name = "lblName"
            Me.lblName.Size = New System.Drawing.Size(63, 13)
            Me.lblName.TabIndex = 0
            Me.lblName.Text = "User Name:"
            '
            'LoginForm
            '
            Me.AcceptButton = Me.btnOK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(264, 116)
            Me.Controls.Add(Me.txtPassword)
            Me.Controls.Add(Me.txtName)
            Me.Controls.Add(Me.lblPassword)
            Me.Controls.Add(Me.lblName)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnOK)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "LoginForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "InterAction Login"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents btnOK As System.Windows.Forms.Button
        Private WithEvents txtPassword As System.Windows.Forms.TextBox
        Private WithEvents txtName As System.Windows.Forms.TextBox
        Private WithEvents lblPassword As System.Windows.Forms.Label
        Private WithEvents lblName As System.Windows.Forms.Label
    End Class
End Namespace