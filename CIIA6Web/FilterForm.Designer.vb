﻿Namespace TSG.CI.IA6Web
    Partial Class FilterForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FilterForm))
            Me.fraFilter = New System.Windows.Forms.GroupBox()
            Me.lblFilterField1 = New System.Windows.Forms.Label()
            Me.lblFilterField2 = New System.Windows.Forms.Label()
            Me.lblFilterField3 = New System.Windows.Forms.Label()
            Me.lblFilterField0 = New System.Windows.Forms.Label()
            Me.txtFilterField3 = New System.Windows.Forms.TextBox()
            Me.txtFilterField2 = New System.Windows.Forms.TextBox()
            Me.txtFilterField1 = New System.Windows.Forms.TextBox()
            Me.txtFilterField0 = New System.Windows.Forms.TextBox()
            Me.btnRemove = New System.Windows.Forms.Button()
            Me.lblType = New System.Windows.Forms.Label()
            Me.lblSortBy = New System.Windows.Forms.Label()
            Me.btnSearch = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.cmbSearchTypes = New CIControls.ComboBox()
            Me.cbxSortBy = New CIControls.ComboBox()
            Me.fraFilter.SuspendLayout()
            Me.SuspendLayout()
            '
            'fraFilter
            '
            Me.fraFilter.Controls.Add(Me.lblFilterField1)
            Me.fraFilter.Controls.Add(Me.lblFilterField2)
            Me.fraFilter.Controls.Add(Me.lblFilterField3)
            Me.fraFilter.Controls.Add(Me.lblFilterField0)
            Me.fraFilter.Controls.Add(Me.txtFilterField3)
            Me.fraFilter.Controls.Add(Me.txtFilterField2)
            Me.fraFilter.Controls.Add(Me.txtFilterField1)
            Me.fraFilter.Controls.Add(Me.txtFilterField0)
            Me.fraFilter.Location = New System.Drawing.Point(21, 20)
            Me.fraFilter.Name = "fraFilter"
            Me.fraFilter.Size = New System.Drawing.Size(297, 201)
            Me.fraFilter.TabIndex = 0
            Me.fraFilter.TabStop = False
            Me.fraFilter.Text = "Display Contacts Where:"
            '
            'lblFilterField1
            '
            Me.lblFilterField1.AutoSize = True
            Me.lblFilterField1.Location = New System.Drawing.Point(16, 60)
            Me.lblFilterField1.Name = "lblFilterField1"
            Me.lblFilterField1.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField1.TabIndex = 2
            Me.lblFilterField1.Text = "##"
            '
            'lblFilterField2
            '
            Me.lblFilterField2.AutoSize = True
            Me.lblFilterField2.Location = New System.Drawing.Point(16, 103)
            Me.lblFilterField2.Name = "lblFilterField2"
            Me.lblFilterField2.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField2.TabIndex = 4
            Me.lblFilterField2.Text = "##"
            '
            'lblFilterField3
            '
            Me.lblFilterField3.AutoSize = True
            Me.lblFilterField3.Location = New System.Drawing.Point(16, 145)
            Me.lblFilterField3.Name = "lblFilterField3"
            Me.lblFilterField3.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField3.TabIndex = 6
            Me.lblFilterField3.Text = "##"
            '
            'lblFilterField0
            '
            Me.lblFilterField0.AutoSize = True
            Me.lblFilterField0.Location = New System.Drawing.Point(16, 21)
            Me.lblFilterField0.Name = "lblFilterField0"
            Me.lblFilterField0.Size = New System.Drawing.Size(21, 13)
            Me.lblFilterField0.TabIndex = 0
            Me.lblFilterField0.Text = "##"
            '
            'txtFilterField3
            '
            Me.txtFilterField3.Location = New System.Drawing.Point(14, 162)
            Me.txtFilterField3.Name = "txtFilterField3"
            Me.txtFilterField3.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField3.TabIndex = 7
            '
            'txtFilterField2
            '
            Me.txtFilterField2.Location = New System.Drawing.Point(14, 120)
            Me.txtFilterField2.Name = "txtFilterField2"
            Me.txtFilterField2.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField2.TabIndex = 5
            '
            'txtFilterField1
            '
            Me.txtFilterField1.Location = New System.Drawing.Point(14, 77)
            Me.txtFilterField1.Name = "txtFilterField1"
            Me.txtFilterField1.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField1.TabIndex = 3
            '
            'txtFilterField0
            '
            Me.txtFilterField0.Location = New System.Drawing.Point(14, 38)
            Me.txtFilterField0.Name = "txtFilterField0"
            Me.txtFilterField0.Size = New System.Drawing.Size(264, 20)
            Me.txtFilterField0.TabIndex = 1
            '
            'btnRemove
            '
            Me.btnRemove.Location = New System.Drawing.Point(337, 92)
            Me.btnRemove.Name = "btnRemove"
            Me.btnRemove.Size = New System.Drawing.Size(82, 25)
            Me.btnRemove.TabIndex = 1
            Me.btnRemove.Text = "Cl&ear Search"
            Me.btnRemove.UseVisualStyleBackColor = True
            '
            'lblType
            '
            Me.lblType.AutoSize = True
            Me.lblType.Location = New System.Drawing.Point(37, 238)
            Me.lblType.Name = "lblType"
            Me.lblType.Size = New System.Drawing.Size(75, 13)
            Me.lblType.TabIndex = 2
            Me.lblType.Text = "F&ind Contacts:"
            '
            'lblSortBy
            '
            Me.lblSortBy.AutoSize = True
            Me.lblSortBy.Location = New System.Drawing.Point(37, 286)
            Me.lblSortBy.Name = "lblSortBy"
            Me.lblSortBy.Size = New System.Drawing.Size(44, 13)
            Me.lblSortBy.TabIndex = 4
            Me.lblSortBy.Text = "&Sort By:"
            '
            'btnSearch
            '
            Me.btnSearch.Location = New System.Drawing.Point(337, 266)
            Me.btnSearch.Name = "btnSearch"
            Me.btnSearch.Size = New System.Drawing.Size(82, 25)
            Me.btnSearch.TabIndex = 6
            Me.btnSearch.Text = "O&K"
            Me.btnSearch.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.btnCancel.Location = New System.Drawing.Point(337, 298)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(82, 25)
            Me.btnCancel.TabIndex = 7
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'cmbSearchTypes
            '
            Me.cmbSearchTypes.AllowEmptyValue = False
            Me.cmbSearchTypes.AutoSize = True
            Me.cmbSearchTypes.Borderless = False
            Me.cmbSearchTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cmbSearchTypes.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cmbSearchTypes.IsDirty = False
            Me.cmbSearchTypes.LimitToList = True
            Me.cmbSearchTypes.Location = New System.Drawing.Point(35, 254)
            Me.cmbSearchTypes.Margin = New System.Windows.Forms.Padding(2)
            Me.cmbSearchTypes.MaxDropDownItems = 8
            Me.cmbSearchTypes.Name = "cmbSearchTypes"
            Me.cmbSearchTypes.SelectedIndex = -1
            Me.cmbSearchTypes.SelectedValue = Nothing
            Me.cmbSearchTypes.SelectionLength = 0
            Me.cmbSearchTypes.SelectionStart = 0
            Me.cmbSearchTypes.Size = New System.Drawing.Size(262, 23)
            Me.cmbSearchTypes.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cmbSearchTypes.SupportingValues = ""
            Me.cmbSearchTypes.TabIndex = 3
            Me.cmbSearchTypes.Tag2 = Nothing
            Me.cmbSearchTypes.Value = ""
            '
            'cbxSortBy
            '
            Me.cbxSortBy.AllowEmptyValue = False
            Me.cbxSortBy.AutoSize = True
            Me.cbxSortBy.Borderless = False
            Me.cbxSortBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
            Me.cbxSortBy.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.cbxSortBy.IsDirty = False
            Me.cbxSortBy.LimitToList = True
            Me.cbxSortBy.Location = New System.Drawing.Point(35, 302)
            Me.cbxSortBy.Margin = New System.Windows.Forms.Padding(2)
            Me.cbxSortBy.MaxDropDownItems = 8
            Me.cbxSortBy.Name = "cbxSortBy"
            Me.cbxSortBy.SelectedIndex = -1
            Me.cbxSortBy.SelectedValue = Nothing
            Me.cbxSortBy.SelectionLength = 0
            Me.cbxSortBy.SelectionStart = 0
            Me.cbxSortBy.Size = New System.Drawing.Size(262, 23)
            Me.cbxSortBy.SortOrder = System.Windows.Forms.SortOrder.None
            Me.cbxSortBy.SupportingValues = ""
            Me.cbxSortBy.TabIndex = 5
            Me.cbxSortBy.Tag2 = Nothing
            Me.cbxSortBy.Value = ""
            '
            'FilterForm
            '
            Me.AcceptButton = Me.btnSearch
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.btnCancel
            Me.ClientSize = New System.Drawing.Size(435, 347)
            Me.Controls.Add(Me.cbxSortBy)
            Me.Controls.Add(Me.cmbSearchTypes)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnSearch)
            Me.Controls.Add(Me.lblSortBy)
            Me.Controls.Add(Me.lblType)
            Me.Controls.Add(Me.btnRemove)
            Me.Controls.Add(Me.fraFilter)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "FilterForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Find"
            Me.fraFilter.ResumeLayout(False)
            Me.fraFilter.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents fraFilter As System.Windows.Forms.GroupBox
        Friend WithEvents txtFilterField2 As System.Windows.Forms.TextBox
        Friend WithEvents txtFilterField1 As System.Windows.Forms.TextBox
        Friend WithEvents txtFilterField0 As System.Windows.Forms.TextBox
        Friend WithEvents lblFilterField1 As System.Windows.Forms.Label
        Friend WithEvents lblFilterField2 As System.Windows.Forms.Label
        Friend WithEvents lblFilterField3 As System.Windows.Forms.Label
        Friend WithEvents lblFilterField0 As System.Windows.Forms.Label
        Friend WithEvents btnRemove As System.Windows.Forms.Button
        Friend WithEvents lblType As System.Windows.Forms.Label
        Friend WithEvents lblSortBy As System.Windows.Forms.Label
        Friend WithEvents btnSearch As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents txtFilterField3 As System.Windows.Forms.TextBox
        Friend WithEvents cmbSearchTypes As CIControls.ComboBox
        Friend WithEvents cbxSortBy As CIControls.ComboBox
    End Class
End Namespace