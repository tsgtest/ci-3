﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Contact Integration Support for InterAction 6")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("The Sackett Group, Inc.")> 
<Assembly: AssemblyProduct("Contact Integration")> 
<Assembly: AssemblyCopyright("©1990 - 2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("71b73d41-57ba-4682-a527-28288f33d4eb")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.0023")> 
<Assembly: AssemblyFileVersion("3.0.0.0023")> 
