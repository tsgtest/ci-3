﻿Option Explicit On

Imports LMP

Namespace TSG.CI.IA6Web
    Public Class MaxResultsForm
#Region "******************fields***********************"
        Private m_bCancelled As Boolean
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()
        End Sub
#End Region
#Region "******************properties***********************"
        Public ReadOnly Property Cancelled() As Boolean
            Get
                Cancelled = m_bCancelled
            End Get
        End Property
#End Region
#Region "******************events***********************"
        Private Sub BtnOk_Click(sender As Object, e As EventArgs) Handles BtnOk.Click, lstSetMax.DoubleClick
            Try
                Application.DoEvents()
                Me.Hide()
                m_bCancelled = False
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
            Try
                Application.DoEvents()
                Me.Hide()
                m_bCancelled = True
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub MaxResultsForm_Activated(sender As Object, e As EventArgs) Handles Me.Activated
            Dim xMax As String

            Try
                xMax = GlobalMethods.g_oIni.GetUserIni("CIIAWeb", "MaxResults")
                If xMax = String.Empty Then
                    xMax = "500"
                End If
                Me.lstSetMax.SelectedValue = xMax
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub MaxResultsForm_Load(sender As Object, e As EventArgs) Handles Me.Load
            Dim oDT As DataTable

            Try
                oDT = New DataTable

                ' Add two columns
                oDT.Columns.Add("Value")

                With oDT
                    oDT.Rows.Add("50")
                    oDT.Rows.Add("100")
                    oDT.Rows.Add("500")
                    oDT.Rows.Add("1,000")
                    oDT.Rows.Add("2,000")
                    oDT.Rows.Add("3,000")
                    oDT.Rows.Add("5,000")
                    oDT.Rows.Add("10,000")
                End With

                'load control
                With Me.lstSetMax
                    .DisplayMember = oDT.Columns(0).ColumnName
                    .ValueMember = oDT.Columns(0).ColumnName
                    .DataSource = oDT
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

#End Region
#Region "******************methods***********************"
#End Region
    End Class
End Namespace