﻿Namespace TSG.CI.IA6Web
    Partial Class AddressesForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.lblAddresses = New System.Windows.Forms.Label()
            Me.lstAddressTypes = New System.Windows.Forms.ListBox()
            Me.chkPromptForMissingAddresses = New System.Windows.Forms.CheckBox()
            Me.btnOK = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.lblDetail = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'lblAddresses
            '
            Me.lblAddresses.Location = New System.Drawing.Point(21, 16)
            Me.lblAddresses.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
            Me.lblAddresses.Name = "lblAddresses"
            Me.lblAddresses.Size = New System.Drawing.Size(705, 86)
            Me.lblAddresses.TabIndex = 0
            Me.lblAddresses.Text = "The requested address for this contact does not exist.  Please select from one of" & _
        " the available addresses listed below."
            '
            'lstAddressTypes
            '
            Me.lstAddressTypes.FormattingEnabled = True
            Me.lstAddressTypes.ItemHeight = 24
            Me.lstAddressTypes.Location = New System.Drawing.Point(26, 84)
            Me.lstAddressTypes.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
            Me.lstAddressTypes.Name = "lstAddressTypes"
            Me.lstAddressTypes.Size = New System.Drawing.Size(244, 172)
            Me.lstAddressTypes.TabIndex = 1
            '
            'chkPromptForMissingAddresses
            '
            Me.chkPromptForMissingAddresses.AutoSize = True
            Me.chkPromptForMissingAddresses.Location = New System.Drawing.Point(26, 312)
            Me.chkPromptForMissingAddresses.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
            Me.chkPromptForMissingAddresses.Name = "chkPromptForMissingAddresses"
            Me.chkPromptForMissingAddresses.Size = New System.Drawing.Size(473, 29)
            Me.chkPromptForMissingAddresses.TabIndex = 3
            Me.chkPromptForMissingAddresses.Text = "&Continue to prompt for addresses when necessary"
            Me.chkPromptForMissingAddresses.UseVisualStyleBackColor = True
            '
            'btnOK
            '
            Me.btnOK.Location = New System.Drawing.Point(486, 299)
            Me.btnOK.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
            Me.btnOK.Name = "btnOK"
            Me.btnOK.Size = New System.Drawing.Size(130, 44)
            Me.btnOK.TabIndex = 4
            Me.btnOK.Text = "O&K"
            Me.btnOK.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.Location = New System.Drawing.Point(630, 299)
            Me.btnCancel.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(130, 44)
            Me.btnCancel.TabIndex = 5
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'lblDetail
            '
            Me.lblDetail.AutoSize = True
            Me.lblDetail.Location = New System.Drawing.Point(368, 89)
            Me.lblDetail.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
            Me.lblDetail.Name = "lblDetail"
            Me.lblDetail.Size = New System.Drawing.Size(0, 25)
            Me.lblDetail.TabIndex = 2
            '
            'AddressesForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(168.0!, 168.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
            Me.ClientSize = New System.Drawing.Size(774, 362)
            Me.Controls.Add(Me.lblDetail)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnOK)
            Me.Controls.Add(Me.chkPromptForMissingAddresses)
            Me.Controls.Add(Me.lstAddressTypes)
            Me.Controls.Add(Me.lblAddresses)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
            Me.Name = "AddressesForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Select an Address"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblAddresses As System.Windows.Forms.Label
        Friend WithEvents lstAddressTypes As System.Windows.Forms.ListBox
        Friend WithEvents chkPromptForMissingAddresses As System.Windows.Forms.CheckBox
        Friend WithEvents btnOK As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents lblDetail As System.Windows.Forms.Label
    End Class
End Namespace
