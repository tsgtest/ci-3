﻿Namespace TSG.CI.IA6Web
    Partial Class ProxyForForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProxyForForm))
            Me.lblUsers = New System.Windows.Forms.Label()
            Me.lstUsers = New System.Windows.Forms.ListBox()
            Me.btnOK = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.btnEndProxy = New System.Windows.Forms.Button()
            Me.SuspendLayout()
            '
            'lblUsers
            '
            Me.lblUsers.Location = New System.Drawing.Point(14, 11)
            Me.lblUsers.Name = "lblUsers"
            Me.lblUsers.Size = New System.Drawing.Size(78, 31)
            Me.lblUsers.TabIndex = 0
            Me.lblUsers.Text = "&Users:"
            '
            'lstUsers
            '
            Me.lstUsers.FormattingEnabled = True
            Me.lstUsers.Location = New System.Drawing.Point(17, 27)
            Me.lstUsers.Name = "lstUsers"
            Me.lstUsers.Size = New System.Drawing.Size(305, 212)
            Me.lstUsers.TabIndex = 1
            '
            'btnOK
            '
            Me.btnOK.Location = New System.Drawing.Point(168, 252)
            Me.btnOK.Name = "btnOK"
            Me.btnOK.Size = New System.Drawing.Size(74, 25)
            Me.btnOK.TabIndex = 2
            Me.btnOK.Text = "O&K"
            Me.btnOK.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.btnCancel.Location = New System.Drawing.Point(248, 252)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(74, 25)
            Me.btnCancel.TabIndex = 3
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'btnEndProxy
            '
            Me.btnEndProxy.Location = New System.Drawing.Point(17, 252)
            Me.btnEndProxy.Name = "btnEndProxy"
            Me.btnEndProxy.Size = New System.Drawing.Size(105, 25)
            Me.btnEndProxy.TabIndex = 4
            Me.btnEndProxy.Text = "&End Proxy"
            Me.btnEndProxy.UseVisualStyleBackColor = True
            '
            'ProxyForForm
            '
            Me.AcceptButton = Me.btnOK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.btnCancel
            Me.ClientSize = New System.Drawing.Size(337, 289)
            Me.Controls.Add(Me.btnEndProxy)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnOK)
            Me.Controls.Add(Me.lstUsers)
            Me.Controls.Add(Me.lblUsers)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "ProxyForForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Act On Behalf Of"
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents lblUsers As System.Windows.Forms.Label
        Friend WithEvents lstUsers As System.Windows.Forms.ListBox
        Friend WithEvents btnOK As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents btnEndProxy As System.Windows.Forms.Button
    End Class
End Namespace