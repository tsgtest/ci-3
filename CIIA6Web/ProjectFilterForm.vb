﻿Option Explicit On

Namespace TSG.CI.IA6Web
    Public Class ProjectFilterForm
        ' ''        Private g_oCnn As Interaction.Connection
        ' ''        Private m_oArr As XArrayDB
        ' ''        Private m_bCanceled As Boolean

        ' ''Public Property Get Canceled() As Boolean
        ' ''    Canceled = m_bCanceled
        ' ''End Property

        ' ''Public Property Set IAConnection(oCnn As Interaction.Connection)
        ' ''    Set g_oCnn = oCnn
        ' ''End Property

        ' ''Private Sub btnCancel_Click()
        ' ''            Me.Hide()
        ' ''            m_bCanceled = True
        ' ''        End Sub

        ' ''        Private Sub btnFind_Click()
        ' ''            Dim oIAProj As Project
        ' ''            On Error GoTo ProcError
        ' ''            Me.MousePointer = vbHourglass
        ' ''            oIAProj = g_oCnn.FindProjectDlg("Find Project")
        ' ''            If Not oIAProj Is Nothing Then
        ' ''                'check if project is in list
        ' ''                With Me.cmbProjects
        ' ''                    .BoundText = oIAProj.ProjectId
        ' ''                    If .BoundText = Empty Then
        ' ''                        'add project to array
        ' ''                        With .Array
        ' ''                            .Insert(1, .UpperBound(1) + 1)
        ' ''                            .Value(.UpperBound(1), 0) = oIAProj.DisplayName
        ' ''                            .Value(.UpperBound(1), 1) = oIAProj.ProjectId
        ' ''                        End With
        ' ''                        .ReBind()
        ' ''                        ResizeTDBCombo(Me.cmbProjects, 8)

        ' ''                        'select the item
        ' ''                        Me.cmbProjects.BoundText = oIAProj.ProjectId
        ' ''                    End If
        ' ''                End With
        ' ''            End If
        ' ''            Me.MousePointer = vbDefault
        ' ''            Exit Sub
        ' ''ProcError:
        ' ''            Dim lErr As Long
        ' ''            Dim xDesc As String

        ' ''            lErr = Err.Number
        ' ''            xDesc = Err.Description

        ' ''            Me.MousePointer = vbDefault

        ' ''            Err.Number = lErr
        ' ''            Err.Description = xDesc
        ' ''            g_oError.RaiseError "CIIAWeb.frmProjectFilter.btnFind_Click"
        ' ''            Exit Sub
        ' ''        End Sub

        ' ''        Private Sub btnSearch_Click()
        ' ''            If IsNull(Me.cmbProjects.SelectedItem) Then
        ' ''                MsgBox("Please select a project.", vbExclamation)
        ' ''            Else
        ' ''                Me.Hide()
        ' ''                m_bCanceled = False
        ' ''            End If
        ' ''        End Sub

        ' ''        Private Sub Form_Activate()
        ' ''            On Error GoTo ProcError
        ' ''            With Me.cmbProjects
        ' ''                If .Array.Count(1) > 0 Then
        ' ''                    .SelectedItem = 0
        ' ''                End If
        ' ''            End With
        ' ''            Exit Sub
        ' ''ProcError:
        ' ''            g_oError.ShowError()
        ' ''            Exit Sub
        ' ''        End Sub

        ' ''        Private Sub Form_Load()
        ' ''            Dim xSec As String

        ' ''            GetMyProjects()

        ' ''            m_bCanceled = True
        ' ''        End Sub

        ' ''        Private Sub GetMyProjects()
        ' ''            Dim oProjSearch As IAProjectSearch

        ' ''            On Error GoTo ProcError

        ' ''            oProjSearch = g_oCnn.NewProjectSearch
        ' ''            With oProjSearch
        ' ''                Dim oIAModule As IAProjectModule
        ' ''                oIAModule = g_oCnn.ProjectModules.FindById("200")
        ' ''                .Module = oIAModule
        ' ''                .InMyProjects = True
        ' ''                .Execute()
        ' ''                Dim oProjs As Projects
        ' ''                Dim oProj As Project
        ' ''                oProjs = .Results
        ' ''            End With

        ' ''            Dim oArray As XArrayDB
        ' ''            oArray = New XArrayDB
        ' ''            oArray.ReDim(0, -1, 0, 1)

        ' ''            For Each oProj In oProjs
        ' ''                oArray.Insert(1, oArray.UpperBound(1) + 1)
        ' ''                oArray.Value(oArray.UpperBound(1), 0) = oProj.DisplayName
        ' ''                oArray.Value(oArray.UpperBound(1), 1) = oProj.ProjectId
        ' ''            Next oProj
        ' ''            Me.cmbProjects.Array = oArray
        ' ''            ResizeTDBCombo(Me.cmbProjects, 8)
        ' ''            Exit Sub
        ' ''ProcError:
        ' ''            g_oError.RaiseError "CIIAWeb.frmProjectFilter.GetMyProjects"
        ' ''            Exit Sub
        ' ''        End Sub

    End Class
End Namespace