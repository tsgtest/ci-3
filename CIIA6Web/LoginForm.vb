﻿Option Explicit On

Imports System.Windows.Forms
Imports LexisNexis.InterAction.API.DotNet
Imports LMP

Namespace TSG.CI.IA6Web
    Public Class LoginForm
#Region "******************fields***********************"
        Private m_Connection As Connection
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()
        End Sub
#End Region
#Region "******************properties***********************"
        Public ReadOnly Property Connection() As Connection
            Get
                Connection = m_Connection
            End Get
        End Property
#End Region
#Region "******************methods***********************"
        Private Function Authenticate() As Boolean
            Dim xAccountName As String
            Dim xPassword As String
            Dim bIsConnected As Boolean = False

            m_Connection = New Connection()
            xAccountName = Me.txtName.Text
            xPassword = Me.txtPassword.Text

            Try
                m_Connection.Authenticate(xAccountName, xPassword)
                bIsConnected = True
            Catch ex As Exception
                m_Connection = Nothing
                Throw
            End Try

            Return bIsConnected
        End Function
#End Region
#Region "******************events***********************"
        Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
            Try
                Authenticate()
            Catch ex As Exception
                MessageBox.Show("An error occurred during login:" + _
                                vbCr + vbCr & ex.Message + _
                                vbCr + vbCr & "Please try again.", "Invalid login", _
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End Try
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
            Try
                Me.DialogResult = DialogResult.Cancel
                Me.Close()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
    End Class
End Namespace