﻿Namespace TSG.CI.IA6Web
    Partial Class MaxResultsForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MaxResultsForm))
            Me.Label1 = New System.Windows.Forms.Label()
            Me.BtnOk = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.lstSetMax = New System.Windows.Forms.ListBox()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(139, 18)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(170, 60)
            Me.Label1.TabIndex = 1
            Me.Label1.Text = "Specify the maximum number of contacts to retrieve from an InterAction search or " & _
        "folder."
            '
            'BtnOk
            '
            Me.BtnOk.Location = New System.Drawing.Point(142, 109)
            Me.BtnOk.Name = "BtnOk"
            Me.BtnOk.Size = New System.Drawing.Size(74, 25)
            Me.BtnOk.TabIndex = 2
            Me.BtnOk.Text = "O&K"
            Me.BtnOk.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.btnCancel.Location = New System.Drawing.Point(222, 109)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(74, 25)
            Me.btnCancel.TabIndex = 3
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'lstSetMax
            '
            Me.lstSetMax.FormattingEnabled = True
            Me.lstSetMax.Location = New System.Drawing.Point(18, 15)
            Me.lstSetMax.Name = "lstSetMax"
            Me.lstSetMax.Size = New System.Drawing.Size(107, 121)
            Me.lstSetMax.TabIndex = 0
            '
            'MaxResultsForm
            '
            Me.AcceptButton = Me.BtnOk
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.btnCancel
            Me.ClientSize = New System.Drawing.Size(315, 153)
            Me.Controls.Add(Me.lstSetMax)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.BtnOk)
            Me.Controls.Add(Me.Label1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "MaxResultsForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Set Maximum Results"
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents BtnOk As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents lstSetMax As System.Windows.Forms.ListBox
    End Class
End Namespace