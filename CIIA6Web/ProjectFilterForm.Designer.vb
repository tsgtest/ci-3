﻿Namespace TSG.CI.IA6Web
    Partial Class ProjectFilterForm
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.lblProject = New System.Windows.Forms.Label()
            Me.chkDistributionList = New System.Windows.Forms.CheckBox()
            Me.chkPersonContacts = New System.Windows.Forms.CheckBox()
            Me.btnFind = New System.Windows.Forms.Button()
            Me.btnSearch = New System.Windows.Forms.Button()
            Me.btnCancel = New System.Windows.Forms.Button()
            Me.cmbProjects = New System.Windows.Forms.ComboBox()
            Me.SuspendLayout()
            '
            'lblProject
            '
            Me.lblProject.Location = New System.Drawing.Point(12, 9)
            Me.lblProject.Name = "lblProject"
            Me.lblProject.Size = New System.Drawing.Size(150, 38)
            Me.lblProject.TabIndex = 0
            Me.lblProject.Text = "&Project:"
            '
            'chkDistributionList
            '
            Me.chkDistributionList.AutoSize = True
            Me.chkDistributionList.Location = New System.Drawing.Point(16, 63)
            Me.chkDistributionList.Name = "chkDistributionList"
            Me.chkDistributionList.Size = New System.Drawing.Size(183, 17)
            Me.chkDistributionList.TabIndex = 3
            Me.chkDistributionList.Text = "Only Contacts On &Distribution List"
            Me.chkDistributionList.UseVisualStyleBackColor = True
            '
            'chkPersonContacts
            '
            Me.chkPersonContacts.AutoSize = True
            Me.chkPersonContacts.Location = New System.Drawing.Point(214, 63)
            Me.chkPersonContacts.Name = "chkPersonContacts"
            Me.chkPersonContacts.Size = New System.Drawing.Size(128, 17)
            Me.chkPersonContacts.TabIndex = 4
            Me.chkPersonContacts.Text = "&Only Person Contacts"
            Me.chkPersonContacts.UseVisualStyleBackColor = True
            '
            'btnFind
            '
            Me.btnFind.Location = New System.Drawing.Point(329, 28)
            Me.btnFind.Name = "btnFind"
            Me.btnFind.Size = New System.Drawing.Size(74, 25)
            Me.btnFind.TabIndex = 2
            Me.btnFind.Text = "&Find Project..."
            Me.btnFind.UseVisualStyleBackColor = True
            '
            'btnSearch
            '
            Me.btnSearch.Location = New System.Drawing.Point(249, 95)
            Me.btnSearch.Name = "btnSearch"
            Me.btnSearch.Size = New System.Drawing.Size(74, 25)
            Me.btnSearch.TabIndex = 5
            Me.btnSearch.Text = "O&K"
            Me.btnSearch.UseVisualStyleBackColor = True
            '
            'btnCancel
            '
            Me.btnCancel.Location = New System.Drawing.Point(329, 95)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New System.Drawing.Size(74, 25)
            Me.btnCancel.TabIndex = 6
            Me.btnCancel.Text = "Cancel"
            Me.btnCancel.UseVisualStyleBackColor = True
            '
            'cmbProjects
            '
            Me.cmbProjects.FormattingEnabled = True
            Me.cmbProjects.Location = New System.Drawing.Point(16, 28)
            Me.cmbProjects.Name = "cmbProjects"
            Me.cmbProjects.Size = New System.Drawing.Size(305, 21)
            Me.cmbProjects.TabIndex = 1
            '
            'ProjectFilterForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(422, 134)
            Me.Controls.Add(Me.cmbProjects)
            Me.Controls.Add(Me.btnCancel)
            Me.Controls.Add(Me.btnSearch)
            Me.Controls.Add(Me.btnFind)
            Me.Controls.Add(Me.chkPersonContacts)
            Me.Controls.Add(Me.chkDistributionList)
            Me.Controls.Add(Me.lblProject)
            Me.Name = "ProjectFilterForm"
            Me.Text = "Find Contacts in Project"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents lblProject As System.Windows.Forms.Label
        Friend WithEvents chkDistributionList As System.Windows.Forms.CheckBox
        Friend WithEvents chkPersonContacts As System.Windows.Forms.CheckBox
        Friend WithEvents btnFind As System.Windows.Forms.Button
        Friend WithEvents btnSearch As System.Windows.Forms.Button
        Friend WithEvents btnCancel As System.Windows.Forms.Button
        Friend WithEvents cmbProjects As System.Windows.Forms.ComboBox
    End Class
End Namespace