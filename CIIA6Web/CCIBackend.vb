Option Explicit On
#Region "******************Imports*******************"
Imports [Interface].Foundation.Common
Imports LexisNexis.InterAction.API.Common
Imports LexisNexis.InterAction.API.Common.Enumeration
Imports LexisNexis.InterAction.API.Common.Enumeration.Data
Imports LexisNexis.InterAction.API.DotNet.Domain.ContactLists
Imports LexisNexis.InterAction.API.DotNet.Domain.Contacts
Imports LexisNexis.InterAction.API.DotNet.Domain.PhonesAndAddresses
Imports LexisNexis.InterAction.API.DotNet.Exceptions
Imports LexisNexis.InterAction.API.DotNet.Manager.ReturnOptions
Imports LexisNexis.InterAction.API.DotNet.Manager.SearchCriteria
Imports LexisNexis.InterAction.API.DotNet
Imports LexisNexis.InterAction.API.DotNet.Domain
Imports LexisNexis.InterAction.API.DotNet.Domain.Projects
Imports LexisNexis.InterAction.API.DotNet.Domain.Relationships
Imports LexisNexis.InterAction.API.DotNet.Manager
Imports LexisNexis.InterAction.API.DotNet.Enumeration
Imports System.Linq
Imports TSG.CI
Imports LMP
#End Region
Namespace TSG.CI.IA6Web
    Friend Class CCIBackend
        Implements ICIBackend
        Implements ICINumberPromptFormat
#Region "******************fields*******************"
        Private Enum ciCompanyCommonNameUse
            ciCompanyCommonNameUse_None = 0
            ciCompanyCommonNameUse_Companies = 1
            ciCompanyCommonNameUse_People = 2
            ciCompanyCommonNameUse_All = 3
        End Enum

        Private Enum IAObjectsConstants
            IAObjectsConstant_Private = 1
            IAObjectsConstant_Firm = 2
            IAObjectsConstant_MarketingList = 3
            IAObjectsConstant_WorkingList = 6
        End Enum

        Private m_xDisplayName As String
        Private m_bIsConnected As Boolean
        Private m_oFilter As CFilter
        Private m_oUNID As CUNID
        Private m_oEvents As CEventGenerator
        Private m_iPromptType As ICINumberPromptFormat.ciNumberPromptTypes
        Private m_iSortCol As ciListingCols
        Private m_iNumberType As ciContactNumberTypes
        Private m_oIAContacts As Collection
        Private m_xName As String
        Private m_xUserIni As String
        Private m_bSkipConnectionWarning As Boolean
        Private m_ilAllFolderClasses As IList
        Private m_ilAllFolderTypes As IList
#End Region
#Region "******************initializer****************"
        Public Sub New()
            GlobalMethods.g_oGlobals = New CGlobals
            'initialize errors and events
            m_oEvents = GlobalMethods.g_oGlobals.CIEvents
            GlobalMethods.g_oConstants = New CConstants
            GlobalMethods.g_oSession = New CSessionType
            GlobalMethods.g_oIni = New CIni
            GlobalMethods.g_oError = New CError
            GlobalMethods.g_oReg = New CRegistry
            m_oUNID = New CUNID
        End Sub
#End Region
#Region "******************methods***********************"
        Private Function GetAddresses(Optional oListing As CListing = Nothing) As CAddresses
            'todo
            'get addresses for specified listing -
            'NOTE: the tag "v2-" has been prepended to the address id - this is necessary
            'because we've changed the address UNID to contain the address' AddressID.  Previously, the
            'UNID contained the Address' relationship ID.  We need to distinguish whether the
            'UNID is a UNID with the old address info (i.e. the relationship ID) or the new info (i.e. the address id)
            Dim oCIAddresses As CAddresses
            Dim oCIAddress As CAddress
            Dim oIAAddress As Address
            'Dim oIAAddresses As Addresses
            Dim oIAAddressObj As Object
            Dim oIAContact As Contact
            Dim oUNID As CUNID
            Dim iStoreID As Integer
            Dim xContextMailingID As String = ""
            Dim bHasContextMailing As Boolean
            Dim i As Integer
            Dim ilFolderSpecific As IList
            Dim xAddressID As String
            Dim oAdd As Address

            Connect()
            oUNID = New CUNID
            oCIAddresses = New CAddresses

            If Not oListing Is Nothing Then
                'get address types available for listing
                oIAContact = IAContactFromListing(oListing)

                If Not oIAContact Is Nothing Then

                    'GLOG : 7793 : ceh
                    'deal with context mailing address first
                    'todo
                    ' ''For i = 0 To oIAContact.Addresses.Count - 1
                    ' ''    oIAAddressObj = oIAContact.Addresses(i)
                    ' ''    With oIAAddressObj
                    ' ''        ilFolderSpecific = .FolderSpecificMailingAddresses
                    ' ''        If ilFolderSpecific.Count Then
                    ' ''            oCIAddress = oUNID.GetAddress(oListing.UNID & _
                    ' ''                GlobalMethods.g_oConstants.UNIDSep & "v2-" & .LocationType.Id.SingleId & "-" & oIAAddressObj.AddressID, _
                    ' ''                .LocationType.Name & " (Mailing)")
                    ' ''            'add context address
                    ' ''            oCIAddresses.Add(oCIAddress)
                    ' ''            'set var
                    ' ''            bHasContextMailing = True
                    ' ''            xContextMailingID = oIAAddressObj.AddressID
                    ' ''            Exit For
                    ' ''        End If
                    ' ''    End With
                    ' ''Next i

                    'GLOG : 7793 : ceh
                    'add non-context addresses
                    For Each oIAAddress In oIAContact.Addresses
                        'add address types
                        With oIAAddress

                            If .Id Is Nothing Then
                                xAddressID = 0
                            Else
                                xAddressID = .Id.SingleId
                            End If

                            If Not bHasContextMailing Then
                                If oIAAddress.IsGlobalMailingAddress Then
                                    oCIAddress = oUNID.GetAddress(oListing.UNID &
                                        GlobalMethods.g_oConstants.UNIDSep & "v2-" & .LocationType.Id.SingleId & "-" & xAddressID,
                                        .LocationType.Name & " (Mailing)")
                                Else
                                    oCIAddress = oUNID.GetAddress(oListing.UNID &
                                        GlobalMethods.g_oConstants.UNIDSep & "v2-" & .LocationType.Id.SingleId & "-" & xAddressID,
                                        .LocationType.Name & IIf(oIAAddress.Description <> String.Empty, " - ", "") &
                                        oIAAddress.Description)
                                End If
                                'add address
                                oCIAddresses.Add(oCIAddress)
                            Else
                                If xContextMailingID <> oIAAddress.Id.SingleId Then
                                    oCIAddress = oUNID.GetAddress(oListing.UNID &
                                        GlobalMethods.g_oConstants.UNIDSep & "v2-" & .LocationType.Id.SingleId & "-" & xAddressID,
                                        .LocationType.Name & IIf(oIAAddress.Description <> String.Empty, " - ", "") &
                                        oIAAddress.Description)
                                    'add address
                                    oCIAddresses.Add(oCIAddress)
                                End If
                            End If
                        End With
                    Next oIAAddress
                End If
            Else
                'get all available location types
                Dim ilLocationTypes As IList
                Dim asc As New LocationTypeSearchCriteria
                Dim j As Integer

                'set search criteria
                'ilLocationTypes = GlobalMethods.g_oCnn.LocationTypes.FindLocationTypes(LocationTypeSearchCriteria.EasyCriteria.Active)
                'For j = 1 To ilLocationTypes.Count
                '    oLocType = ilLocationTypes(j - 1)
                '    'add address types
                '    oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & _
                '         GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & oLocType.Id.SingleId, oLocType.Name)
                '    oCIAddresses.Add(oCIAddress)
                'Next j

                'could not find a way to sort by Display Order, so static for now

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.Business.Id.SingleId, "Business")
                oCIAddresses.Add(oCIAddress)

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.Business2.Id.SingleId, "Business2")
                oCIAddresses.Add(oCIAddress)

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.AltBusiness.Id.SingleId, "Alternate Business")
                oCIAddresses.Add(oCIAddress)

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.Home.Id.SingleId, "Home")
                oCIAddresses.Add(oCIAddress)

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.AltHome.Id.SingleId, "Alternate Home")
                oCIAddresses.Add(oCIAddress)

                oCIAddress = oUNID.GetAddress(GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep &
                                              GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & LocationType.Other.Id.SingleId, "Other")
                oCIAddresses.Add(oCIAddress)

            End If
            GetAddresses = oCIAddresses
        End Function

        Private Function GetContacts(oListing As CListing,
                                     oAddress As CAddress,
                                     Optional ByVal vAddressType As Object = Nothing,
                                     Optional ByVal iIncludeData As ciRetrieveData = 1&,
                                     Optional ByVal iAlerts As ciAlerts = 1&) As CContacts

            'returns a ci contact filled with IA data of the specified listing and address
            Static bIniRetrieved As Boolean
            Static iUseCompanyCommonName As ciCompanyCommonNameUse
            Dim oIAContact As Contact
            Dim oContacts As CContacts
            Dim bAlertAll As Boolean
            Dim oFormat As ICINumberPromptFormat
            Dim oNum As CContactNumber
            Dim xUse As String
            Dim xMessage As String
            Dim bCancel As Boolean
            Dim oCIContact As CContact
            Dim oNums As CContactNumbers
            Dim oPerson As Person = Nothing
            Dim oCompany As Company = Nothing

            bAlertAll = iAlerts And ciAlerts.ciAlert_All

            Connect()
            oContacts = New CContacts

            'create a new ci contact to hold ia contact info
            oCIContact = New CContact

            'get ia contact from listing
            oIAContact = IAContactFromListing(oListing)

            If oIAContact Is Nothing Then
                'GLOG : 15851 : ceh - need to show error
                'Exit Function
                'something is wrong
                Throw New Exception("Invalid contact listing with UNID '" & oListing.UNID & "'.")
            End If

            If oIAContact.IsPerson Then
                oPerson = oIAContact
            Else
                oCompany = oIAContact
            End If

            With oIAContact
                'fill ci contact with ia data
                If oAddress Is Nothing Then
                    oCIContact.UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & ""
                Else
                    oCIContact.UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & oAddress.ID
                End If

                oCIContact.DisplayName = .DisplayName

                If Not bIniRetrieved Then
                    'get whether or not to use common name for company
                    xUse = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "UseCompanyCommonName")

                    If xUse = String.Empty Then
                        iUseCompanyCommonName = ciCompanyCommonNameUse.ciCompanyCommonNameUse_None
                    ElseIf IsNumeric(xUse) Then
                        'valid values are from 0 to 2 - see below
                        If CInt(xUse) <= ciCompanyCommonNameUse.ciCompanyCommonNameUse_All And
                            CInt(xUse) >= ciCompanyCommonNameUse.ciCompanyCommonNameUse_None Then
                            iUseCompanyCommonName = CInt(xUse)
                        Else
                            Throw New Exception("Invalid key in ci.ini: " & "UseCompanyCommonName=" & xUse)

                        End If
                    Else
                        Throw New Exception("Invalid key in ci.ini: " & "UseCompanyCommonName=" & xUse)

                    End If

                    bIniRetrieved = True
                End If

                If (iIncludeData And ciRetrieveData.ciRetrieveData_Names) Or
                    (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                    'get name fields
                    'todo
                    If oIAContact.IsPerson Then
                        oCIContact.MiddleName = oPerson.MiddleName
                        oCIContact.Prefix = oPerson.Title
                        oCIContact.Suffix = oPerson.Suffix
                        oCIContact.LastName = oPerson.LastName
                        oCIContact.FirstName = oPerson.FirstName
                    Else
                        oCIContact.LastName = oCompany.DisplayName
                    End If

                    'todo
                    '' ''get company name based on configuration
                    ' ''Select Case iUseCompanyCommonName
                    ' ''    Case ciCompanyCommonNameUse.ciCompanyCommonNameUse_None
                    ' ''        oCIContact.Company = .CompanyName
                    ' ''    Case ciCompanyCommonNameUse.ciCompanyCommonNameUse_Companies
                    ' ''        If .EntityType = IAEntityTypeCompany Then
                    ' ''            oCIContact.Company = .CompanyAKAName
                    ' ''        Else
                    ' ''            oCIContact.Company = .CompanyName
                    ' ''        End If
                    ' ''    Case ciCompanyCommonNameUse.ciCompanyCommonNameUse_People
                    ' ''        If .EntityType = IAEntityTypePerson Then
                    ' ''            oCIContact.Company = .CompanyAKAName
                    ' ''        Else
                    ' ''            oCIContact.Company = .CompanyName
                    ' ''        End If
                    ' ''    Case Else
                    ' ''        oCIContact.Company = .CompanyAKAName
                    ' ''End Select

                    oCIContact.Company = .CompanyName

                    If .IsPerson Then
                        oCIContact.GoesBy = oPerson.GoesBy
                        oCIContact.Department = oPerson.Department
                        oCIContact.Title = oPerson.JobTitle
                    End If
                End If

                If (iIncludeData And ciRetrieveData.ciRetrieveData_Addresses) Or
                    (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                    'attempt to retrieve address info
                    If Not (oAddress Is Nothing) Then
                        'get address info
                        GetAddressNew(oIAContact, oCIContact, oAddress, iAlerts, bCancel)
                        If (oCIContact Is Nothing) Or bCancel Then
                            'don't add contact
                            GetContacts = Nothing
                            Exit Function
                        End If
                    Else
                        If iAlerts <> ciAlerts.ciAlert_None Then
                            xMessage = "No addresses were found for " & oCIContact.FullName & "." & vbCr & vbCr &
                                       "Do you want to insert the contact with the available information?"
                            bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
                            If bCancel Then
                                '       don't add contact
                                GetContacts = Nothing
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End With

            Dim xRelID As String
            Dim iPos As Integer

            If Not (oAddress Is Nothing) Then
                If Left$(oAddress.ID, 3) = "v2-" Then
                    'parse releationship id from address id -
                    'get relationship id/address id separator position
                    iPos = InStr(4, oAddress.ID, "-")
                    xRelID = Left$(oAddress.ID, iPos - 1)

                    'parse prefix
                    xRelID = Mid$(xRelID, 4)
                Else
                    xRelID = oAddress.ID
                End If
            End If

            '   get phones if specified
            If (iIncludeData And ciRetrieveData.ciRetrieveData_Phones) Or
               (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                '       get all phones for the supplied address
                If xRelID = String.Empty Then
                    'there is no address relationship specified - no address
                    oNums = ICIBackend_GetPhoneNumbers(oListing)
                Else
                    'there is an address relationship specified
                    oNums = ICIBackend_GetPhoneNumbers(oListing, xRelID)
                End If

                If Not (oNums Is Nothing) Then
                    With oNums
                        If .Count = 1 Then
                            'assign phone to contact
                            oCIContact.Phone = .Item(1).Number
                            oCIContact.PhoneExtension = .Item(1).Extension
                            oCIContact.PhoneID = .Item(1).ID
                        ElseIf .Count > 1 And (bAlertAll Or
                            (iAlerts And ciAlerts.ciAlert_MultiplePhones)) Then
                            'there are multiple phones - prompts are requested
                            'prompt to select from all phones
                            If oAddress Is Nothing Then
                                PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple,
                                    ciContactNumberTypes.ciContactNumberType_Phone, , bCancel)
                            Else
                                PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple,
                                    ciContactNumberTypes.ciContactNumberType_Phone, xRelID, bCancel)
                            End If
                            If bCancel Then
                                'don't add contact
                                GetContacts = Nothing
                                Exit Function
                            End If
                        ElseIf .Count > 1 Then
                            'no prompt requested - take the first one
                            oCIContact.Phone = .Item(1).Number
                            oCIContact.PhoneExtension = .Item(1).Extension
                            oCIContact.PhoneID = .Item(1).ID
                        ElseIf bAlertAll Or (iAlerts And ciAlerts.ciAlert_NoPhones) Then
                            'alert to missing numbers
                            PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing,
                                                    ciContactNumberTypes.ciContactNumberType_Phone, , bCancel)
                            If bCancel Then
                                'don't add contact
                                GetContacts = Nothing
                                Exit Function
                            End If
                        End If
                    End With
                Else
                    If (iAlerts And ciAlerts.ciAlert_NoPhones) Or (iAlerts And ciAlerts.ciAlert_All) Then
                        xMessage = "No phone numbers exist for " & oCIContact.FullName & "." & vbCr & vbCr &
                                   "Do you want to insert the contact with the available information?"
                        bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Phones")
                        If bCancel Then
                            '       don't add contact
                            GetContacts = Nothing
                            Exit Function
                        End If
                    End If
                End If
            End If

            'get faxes - prompt if multiple or missing-
            If (iIncludeData And ciRetrieveData.ciRetrieveData_Faxes) Or
               (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                'get all faxes for the supplied address
                If oAddress Is Nothing Then
                    oNums = ICIBackend_GetFaxNumbers(oListing)
                Else
                    If oAddress Is Nothing Then
                        oNums = ICIBackend_GetFaxNumbers(oListing)
                    Else
                        oNums = ICIBackend_GetFaxNumbers(oListing, xRelID)
                    End If
                End If

                If Not (oNums Is Nothing) Then
                    With oNums
                        If .Count = 1 Then
                            'assign fax to contact
                            oCIContact.Fax = .Item(1).Number
                            oCIContact.FaxID = .Item(1).ID
                        ElseIf .Count > 1 And (bAlertAll Or
                            (iAlerts And ciAlerts.ciAlert_MultipleFaxes)) Then
                            'prompt to select from all phones
                            If oAddress Is Nothing Then
                                PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple,
                                    ciContactNumberTypes.ciContactNumberType_Fax, , bCancel)
                            Else
                                PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple,
                                    ciContactNumberTypes.ciContactNumberType_Fax, xRelID, bCancel)
                            End If
                            If bCancel Then
                                '       don't add contact
                                GetContacts = Nothing
                                Exit Function
                            End If
                        ElseIf .Count > 1 Then
                            'no prompt requested - take first fax
                            oCIContact.Fax = .Item(1).Number
                            oCIContact.FaxID = .Item(1).ID
                        ElseIf (bAlertAll Or
                            (iAlerts And ciAlerts.ciAlert_NoFaxes)) Then
                            '                   alert that there are no faxes
                            PromptForPhonesFaxesNew(oIAContact, oCIContact,
                                  ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing,
                                  ciContactNumberTypes.ciContactNumberType_Fax, , bCancel)
                            If bCancel Then
                                'don't add contact
                                GetContacts = Nothing
                                Exit Function
                            End If
                        End If
                    End With
                Else
                    If (iAlerts And ciAlerts.ciAlert_NoFaxes) Or (iAlerts And ciAlerts.ciAlert_All) Then
                        xMessage = "No fax numbers exist for " & oCIContact.FullName & "." & vbCr & vbCr &
                                   "Do you want to insert the contact with the available information?"
                        bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Faxes")
                        If bCancel Then
                            'don't add contact
                            GetContacts = Nothing
                            Exit Function
                        End If
                    End If
                End If
            End If

            If (iIncludeData And ciRetrieveData.ciRetrieveData_EAddresses) Or
               (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                'get email addresses
                If oAddress Is Nothing Then
                    oNums = ICIBackend_GetEMailNumbers(oListing)
                Else
                    oNums = ICIBackend_GetEMailNumbers(oListing, xRelID)
                End If

                If oNums.Count = 1 Then
                    oCIContact.EMailAddress = oNums.Item(1).Number
                    oCIContact.EAddressID = oNums.Item(1).ID
                ElseIf oNums.Count > 1 And (bAlertAll Or
                    (iAlerts And ciAlerts.ciAlert_MultipleEAddresses)) Then
                    'there are multiple numbers - get list of numbers
                    oFormat = Me
                    oFormat.PromptType = ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple

                    'prompt to select one
                    oNum = oNums.Prompt(oFormat)
                    If Not oNum Is Nothing Then
                        oCIContact.EMailAddress = oNum.Number
                        oCIContact.EAddressID = oNum.ID
                    End If
                ElseIf oNums.Count > 1 Then
                    'no prompt requested - take first
                    oCIContact.EMailAddress = oNums.Item(1).Number
                    oCIContact.EAddressID = oNums.Item(1).ID
                ElseIf (bAlertAll Or
                    (iAlerts And ciAlerts.ciAlert_NoEAddresses)) Then
                    'if there are no numbers available, alert
                    oNums = ICIBackend_GetEMailNumbers(oListing)
                    If oNums.Count = 0 Then
                        oNums.AlertNoneIfSpecified(ciContactNumberTypes.ciContactNumberType_EMail, oCIContact.FullName)
                    Else
                        'there are multiple numbers - get list of numbers
                        oFormat = Me
                        oFormat.PromptType = ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing

                        'prompt to select one
                        oNum = oNums.Prompt(oFormat)
                        If Not oNum Is Nothing Then
                            oCIContact.EMailAddress = oNum.Number
                            oCIContact.EAddressID = oNum.ID
                        End If
                    End If
                End If
            End If

            '    modify UNID to include phone, fax, email ID
            With oCIContact
                .UNID = .UNID & GlobalMethods.g_oConstants.UNIDSep & .PhoneID &
                    ";" & .FaxID & ";" & .EAddressID
            End With

            '   get custom fields for contact if specified
            Dim oCustFlds As CCustomFields
            If (iIncludeData And ciRetrieveData.ciRetrieveData_CustomFields) Or
               (iIncludeData = ciRetrieveData.ciRetrieveData_All) Then
                oCIContact.CustomFields = ICIBackend_GetCustomFields(oListing)
            End If

            oContacts.Add(oCIContact)
            GetContacts = oContacts
        End Function

        Private Function GetEMailNumbers(oListing As CListing, Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'returns the EMail numbers for the specified listing and address relationship
            Dim oIAContact As Contact

            Try
                Connect()
                oIAContact = IAContactFromListing(oListing)
                GetEMailNumbers = GetEMailNumbers(oIAContact, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function GetFolderListings(oFolder As CFolder, Optional oFilter As CFilter = Nothing) As CListings
            'todo
            'returns a listings collection of contacts
            'in the specified IA folder
            Dim oIAContacts As IList
            Dim oSearch As ContactSearchCriteria
            Dim oReturnOptions As New ContactReturnOptions
            Dim oCIFolder As CFolder
            Dim oProjSearch As ProjectSearchCriteria
            Dim oProj As Project

            Try
                Connect()

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oFolder.StoreID=" & oFolder.StoreID,
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                'todo

                ' ''If oFolder.StoreID = "100" Or oFolder.StoreID = "200" Or oFolder.StoreID = "300" Then
                ' ''    oProjSearch = GlobalMethods.g_oCnn.NewProjectSearch

                ' ''    'store is a matter, engagement, or opportunity
                ' ''    If oFolder.ID = 0 Then
                ' ''        'folder is the 'click here to find a project' folder
                ' ''        Dim xDlgTitle As String

                ' ''        'prompt user to select a project using find project dialog
                ' ''        If oFolder.StoreID = "100" Then
                ' ''            xDlgTitle = "Find a Matter"
                ' ''        ElseIf oFolder.StoreID = "200" Then
                ' ''            xDlgTitle = "Find an Engagement"
                ' ''        Else
                ' ''            xDlgTitle = "Find an Opportunity"
                ' ''        End If

                ' ''        'todo
                ' ''        ' ''If g_oProxiedUser Is Nothing Then
                ' ''        oProj = GetProject(oFolder.StoreID, , xDlgTitle)
                ' ''        ' ''Else
                ' ''        ' ''    oProj = GetProject(oFolder.StoreID, g_oProxiedUser, xDlgTitle)
                ' ''        ' ''End If

                ' ''        If oProj Is Nothing Then
                ' ''            Exit Function
                ' ''        Else
                ' ''            oProjSearch.ProjectId = oProj.Id.SingleId
                ' ''            oFolder.Name = oProj.Name
                ' ''        End If
                ' ''    Else
                ' ''        oProjSearch.ProjectId = oFolder.ID
                ' ''    End If

                ' ''    'get contacts in project
                ' ''    oProjSearch.Execute()

                ' ''    m_oEvents.RaiseBeforeExecutingListingsQuery()

                ' ''    If oProjSearch.Results.Count > 0 Then
                ' ''        'get all contacts or distribution list and return
                ' ''        oIAContacts = oProjSearch.Results.Item(1).GetContactsDlg()
                ' ''        m_oEvents.RaiseAfterExecutingListingsQuery()

                ' ''        If Not oIAContacts Is Nothing Then
                ' ''            'get listings from iaContacts collection
                ' ''            GetFolderListings = GetListings( _
                ' ''                oIAContacts, oFolder.UNID, ICIBackend_DefaultSortColumn())
                ' ''        End If

                ' ''        m_oEvents.RaiseAfterExecutingListingsQuery()
                ' ''        Exit Function
                ' ''    Else
                ' ''        MsgBox("Could not find project '" & oFolder.Name & "'.  " & _
                ' ''            "It is possible that the project no longer exists.", vbExclamation)

                ' ''        m_oEvents.RaiseAfterExecutingListingsQuery()
                ' ''        Exit Function
                ' ''    End If
                ' ''End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("Before Set oSearch = GetIASearchObj",
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                'get search criteria from filter
                oSearch = GetIASearchObj(oFilter, oFolder)

                If oSearch Is Nothing Then
                    GetFolderListings = Nothing
                    Exit Function
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("Before With oSearch",
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                'limit to UserContacts if appropriate
                'If oStore.ID = IAObjectsConstants.IAObjectsConstant_Private Then
                '    oSearch.UserFirmScope = UserFirmScopeIndicator.User
                'Else
                '    oSearch.UserFirmScope = UserFirmScopeIndicator.FirmAndUser
                'End If

                'Construct return options to indicate how much information we want back
                oReturnOptions = ContactReturnOptions.EasyOptions.AllPhonesAndAddresses
                oReturnOptions.AddressFlags = AddressFlags.All

                'GLOG : 15854 : ceh
                oReturnOptions.IncludeAdditionalFieldsWithoutValues = True
                If oFolder.StoreID = IAObjectsConstants.IAObjectsConstant_Private Then
                    oReturnOptions.IncludeAdditionalFieldDefinitions = AdditionalFieldTypeIndicator.User
                    oReturnOptions.IncludeAdditionalFields = AdditionalFieldTypeIndicator.User
                    oReturnOptions.UserFirmPreference = UserFirmPreferenceIndicator.User
                Else
                    oReturnOptions.IncludeAdditionalFieldDefinitions = AdditionalFieldTypeIndicator.Global
                    oReturnOptions.IncludeAdditionalFields = AdditionalFieldTypeIndicator.Global
                    oReturnOptions.UserFirmPreference = UserFirmPreferenceIndicator.Firm
                End If

                m_oEvents.RaiseBeforeExecutingListingsQuery()

                'execute search

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("Before .Execute",
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                'Do the contact search based on our criteria and return options.
                'The contacts are returned as an IList<Contact>.
                oIAContacts = GlobalMethods.g_oCnn.Contacts.FindContacts(oSearch, oReturnOptions, New PageContext(PagingMode.NoPaging))

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".Results.Count=" & oIAContacts.Count,
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oIAContacts is nothing=" & (oIAContacts Is Nothing),
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If

                'alert user if max results were returned
                'todo
                ' ''If oIAContacts.Count >= .MaxResults Then
                ' ''    Dim xMax As String
                ' ''    xMax = Format(.MaxResults, "#,##0")
                ' ''    MsgBox("This folder or search contains more than " & xMax & " contacts." & vbCr & _
                ' ''        "Only the first " & xMax & " will be displayed." & vbCr & vbCr & _
                ' ''        "To change the maximum number of contacts that can be displayed," & vbCr & _
                ' ''        "right click on any Interaction folder or filing cabinet in " & GlobalMethods.g_oSession.AppTitle & ".", _
                ' ''        vbInformation, GlobalMethods.g_oSession.AppTitle)
                ' ''End If

                m_oEvents.RaiseAfterExecutingListingsQuery()

                'get listings from iaContacts collection
                GetFolderListings = GetListings(
                    oIAContacts, oFolder.UNID, ICIBackend_DefaultSortColumn())

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("after GetListings",
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If


                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("after .Reset",
                    "CIIAWeb.CCIBackend.GetFolderListings")
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function GetPhoneNumbers(oListing As CListing, Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'returns the phone numbers for the specified listing and address relationship
            Dim oIAContact As Contact

            Try
                Connect()
                oIAContact = IAContactFromListing(oListing)
                GetPhoneNumbers = GetPhoneFaxNumbers(oIAContact,
                                                     ciContactNumberTypes.ciContactNumberType_Phone,
                                                     vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function GetStoreListings(oStore As CStore, Optional oFilter As CFilter = Nothing) As CListings
            'todo
            Dim oListings As CListings
            Dim oSearch As ContactSearchCriteria
            Dim returnOptions As New ContactReturnOptions
            Dim ilFoundContacts As IList


            Try
                'allow for User Contacts or stores where a filter has been set
                If (oStore.ID <> IAObjectsConstants.IAObjectsConstant_Private) And (oFilter.IsEmpty) Then
                    MsgBox("Please specify filter criteria before showing these contacts.", vbExclamation, GlobalMethods.g_oSession.AppTitle)
                    GetStoreListings = Nothing
                    Exit Function
                End If

                Connect()
                oListings = New CListings

                m_oEvents.RaiseBeforeExecutingListingsQuery()

                ' ''If oStore.ID = Interaction.IAContactSearchListProject Then
                ' ''    'get contacts in project
                ' ''    Dim oIAProjs As Projects
                ' ''    Dim oIAProj As Project
                ' ''    Dim oIAProjSearch As IAProjectSearch

                ' ''    'search for a project with ID specified in filter field
                ' ''    oIAProjSearch = g_oCnn.NewProjectSearch
                ' ''    With oIAProjSearch
                ' ''        .ProjectId = oFilter.FilterFields(1).Value
                ' ''        .Execute()
                ' ''        oIAProj = .Results.Item(1)

                ' ''        If oFilter.FilterFields(2).Value = True Then
                ' ''            'get distribution list contacts only
                ' ''            oIAContacts = oIAProj.DistributionList
                ' ''        Else
                ' ''            If oFilter.FilterFields(3).Value = True Then
                ' ''                'get people contacts only
                ' ''                Dim l As Long
                ' ''                oIAContacts = oIAProj.Contacts
                ' ''                For l = oIAContacts.Count To 1 Step -1
                ' ''                    'cycle through contacts
                ' ''                    If oIAContacts.Item(l).EntityType <> IAEntityTypePerson Then
                ' ''                        'not a person contact - remove
                ' ''                        oIAContacts.Remove(l)
                ' ''                    End If
                ' ''                Next l
                ' ''            Else
                ' ''                'get all contacts in project
                ' ''                oIAContacts = oIAProj.Contacts
                ' ''            End If
                ' ''        End If

                ' ''        'get listings
                ' ''        GetStoreListings = GetListings( _
                ' ''            oIAContacts, oStore.UNID & g_oConstants.UNIDSep & "", ICIBackend_DefaultSortColumn())
                ' ''    End With
                ' ''Else
                'create an IA search
                oSearch = GetIASearchObj(oFilter)

                'Construct return options to indicate how much information we want back
                returnOptions = ContactReturnOptions.EasyOptions.AllPhonesAndAddresses
                returnOptions.AddressFlags = AddressFlags.All

                'GLOG : 15854 : ceh
                returnOptions.IncludeAdditionalFieldsWithoutValues = True
                'limit to UserContacts if appropriate
                If oStore.ID = IAObjectsConstants.IAObjectsConstant_Private Then
                    oSearch.UserFirmScope = UserFirmScopeIndicator.User
                    returnOptions.IncludeAdditionalFieldDefinitions = AdditionalFieldTypeIndicator.User
                    returnOptions.IncludeAdditionalFields = AdditionalFieldTypeIndicator.User
                    returnOptions.UserFirmPreference = UserFirmPreferenceIndicator.User
                Else
                    'GLOG : 15851 : ceh
                    oSearch.UserFirmScope = UserFirmScopeIndicator.Firm
                    returnOptions.IncludeAdditionalFieldDefinitions = AdditionalFieldTypeIndicator.Global
                    returnOptions.IncludeAdditionalFields = AdditionalFieldTypeIndicator.Global
                    returnOptions.UserFirmPreference = UserFirmPreferenceIndicator.Firm
                End If


                'Do the actual contact search based on our criteria and return options.
                'The contacts are returned as an IList<Contact>.
                ilFoundContacts = GlobalMethods.g_oCnn.Contacts.FindContacts(oSearch, returnOptions, New PageContext(PagingMode.NoPaging))

                '' ''alert user if max results were returned
                ' ''If ilFoundContacts.Count > PageContext.MaximumPageSize Then
                ' ''    Dim xMax As String
                ' ''    xMax = Format(PageContext.MaximumPageSize, "#,##0")
                ' ''    MsgBox("This folder or search contains more than " & xMax & " contacts." & vbCr & _
                ' ''        "Only the first " & xMax & " will be displayed." & vbCr & vbCr & _
                ' ''        "To change the maximum number of contacts that can be displayed," & vbCr & _
                ' ''        "right click on any Interaction folder or filing cabinet in " & GlobalMethods.g_oSession.AppTitle & ".", _
                ' ''        vbInformation, GlobalMethods.g_oSession.AppTitle)
                ' ''End If

                m_oEvents.RaiseAfterExecutingListingsQuery()

                'get listings from iaContacts collection
                GetStoreListings = GetListings(
                    ilFoundContacts, oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & "", ICIBackend_DefaultSortColumn())

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                m_oEvents.RaiseAfterExecutingListingsQuery()
            End Try

        End Function

        Private Function Connect() As Boolean
            Dim xDesc As String
            Dim xApiURL As String


            Try
                If GlobalMethods.g_oCnn Is Nothing Then
                    xApiURL = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID,
                                                          "RestAPIServer",
                                                          GlobalMethods.g_oIni.CIIni)

                    If Not String.IsNullOrEmpty(xApiURL) Then
                        GlobalMethods.g_oCnn = New Connection(xApiURL)
                    Else
                        xDesc = "Invalid Backend" & GlobalMethods.g_iID & "\RestAPIServer key in ci.ini."
                        Throw New Exception(xDesc)
                    End If

                End If

                If Not GlobalMethods.g_oCnn.HasAuthenticated Then

                    'try Windows Integrated Authentication
                    If AuthenticateUsingWindowsIntegratedMode() Then
                        'GLOG : 15833 : ceh
                        m_bIsConnected = True
                        Exit Function
                    End If

                    'prompt for Name & Password
                    Dim oForm As LoginForm
                    oForm = New LoginForm
                    If (oForm.ShowDialog() = DialogResult.OK) Then
                        GlobalMethods.g_oCnn = oForm.Connection
                    End If
                End If

                If Err.Number <> 0 Then
                    'err occurred during log in - alert

                    'CI2.3.5008 - 0608
                    m_bSkipConnectionWarning = UCase(GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID,
                                                                                 "SkipConnectionWarning",
                                                                                 GlobalMethods.g_oIni.CIIni) = "TRUE")

                    If Not m_bSkipConnectionWarning Then
                        Throw New Exception("Could not connect to " &
                            ICIBackend_DisplayName & ".  " & Err.Description)
                    End If
                End If

                m_bIsConnected = GlobalMethods.g_oCnn.HasAuthenticated

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                If Not m_bIsConnected Then
                    GlobalMethods.g_oCnn = Nothing
                End If
                Connect = m_bIsConnected
            End Try
        End Function

        Private Function AuthenticateUsingWindowsIntegratedMode()
            Dim bIsConnected As Boolean = False

            Try
                GlobalMethods.g_oCnn.Authenticate()
                bIsConnected = True
            Catch
            End Try

            Return bIsConnected

        End Function

        Public Sub SetMaxResults()
            'sets the maximum number of contacts to return as listings
            Dim oForm As MaxResultsForm

            Try
                oForm = New MaxResultsForm

                With oForm
                    .ShowDialog()
                    If Not .Cancelled Then
                        GlobalMethods.g_oIni.SetUserIni("CIIAWeb", "MaxResults", .lstSetMax.SelectedValue)
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Sub ProxyFor()
            'sets the user for which the current user is proxying
            Dim bRet As Boolean
            Dim oForm As ProxyForForm

            Try
                If GlobalMethods.g_oCnn Is Nothing Then
                    bRet = Connect()
                    If bRet = False Then
                        Exit Sub
                    End If
                End If

                oForm = New ProxyForForm
                With oForm
                    .ShowDialog()
                    If Not .Cancelled Then
                        If Not .EndProxy Then
                            'set proxy user as selected proxy
                            GlobalMethods.g_oCnn.ProxyFor(.lstUsers.SelectedValue)
                        Else
                            'end proxy
                            GlobalMethods.g_oCnn.EndProxyFor()
                        End If
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Private Function AddressIsEmpty(oIAAddress As Address)
            If oIAAddress Is Nothing Then
                AddressIsEmpty = True
            Else
                With oIAAddress
                    AddressIsEmpty = Trim$(.Street & .City & .State & .PostalCode) = String.Empty
                End With
            End If
        End Function

        Private Function PromptForAddress(ByVal xListingUNID As String, oIAAddresses As IList(Of Address), ByVal xContactName As String) As CAddress
            'prompts user to select an address, returns
            'the selected one as a GlobalMethods.g_oIni.CAddress
            Dim oForm As AddressesForm
            Dim oCIAddresses As CAddresses
            Dim oCIAddress As CAddress
            Dim oIAAddress As Address
            Dim iAddressID As Integer

            Connect()
            oForm = New AddressesForm

            With oForm
                .Backend = Me

                'create a new addresses collection
                oCIAddresses = New CAddresses

                'cycle through existing address collection
                For Each oIAAddress In oIAAddresses
                    If Not AddressIsEmpty(oIAAddress) Then
                        'create new address
                        oCIAddress = New CAddress

                        'fill address properties
                        With oCIAddress
                            If oIAAddress.Description <> String.Empty Then
                                .Name = oIAAddress.Description
                            Else
                                .Name = oIAAddress.LocationType.Name
                            End If


                            Try
                                iAddressID = oIAAddress.Id.SingleId
                            Catch
                            End Try

                            .UNID = xListingUNID & GlobalMethods.g_oConstants.UNIDSep &
                                "v2-" & oIAAddress.LocationType.Id.ToString() & "-" & iAddressID.ToString()
                        End With

                        'add to collection
                        oCIAddresses.Add(oCIAddress)
                    End If
                Next oIAAddress

                .Addresses = oCIAddresses
                Dim oUNID As CUNID
                oUNID = New CUNID

                .Listing = oUNID.GetListing(xListingUNID, xContactName)

                .ShowDialog()
                If Not .Cancelled Then
                    'get the selected contact number

                    Try
                        PromptForAddress = oForm.Addresses.Item(.lstAddressTypes.SelectedIndex + 1)
                    Catch
                    End Try
                End If
            End With

            oForm.Close()
            oForm.Dispose()
        End Function

        Private Function PromptForMultipleAddresses(ByVal xListingUNID As String, oCIAddresses As CAddresses,
            ByVal xContactName As String) As CAddress
            'prompts user to select an address, returns
            'the selected one as a GlobalMethods.g_oIni.CAddress
            Dim oForm As AddressesForm

            Connect()
            oForm = New AddressesForm

            With oForm
                .Backend = Me

                .Addresses = oCIAddresses
                Dim oUNID As CUNID
                oUNID = New CUNID

                .Listing = oUNID.GetListing(xListingUNID, xContactName)

                .lblAddresses.Text = "There are multiple addresses of this type for '" &
                    xContactName & "'.  " & "Please select one from the list below."

                .ShowDialog()
                If Not .Cancelled Then
                    'get the selected contact number
                    Try
                        PromptForMultipleAddresses = oForm.Addresses.Item(.lstAddressTypes.SelectedIndex + 1)
                    Catch
                    End Try
                End If
            End With

            oForm.Close()
            oForm.Dispose()
        End Function

        Private Function GetProject(ByVal xModuleID As String, Optional ByVal oProxy As User = Nothing, Optional ByVal xFindProjectDlgTitle As String = Nothing) As Project
            'todo
            '' ''returns a filter for projects
            ' ''Dim oIAProj As Project
            ' ''Dim oOptions As IAFindProjectOptions
            ' ''Dim oCIFolder As CFolder

            ' ''Connect()
            ' ''oOptions = New IAFindProjectOptions
            ' ''With oOptions
            ' ''    .ModuleId = xModuleID

            ' ''    If xFindProjectDlgTitle <> String.Empty Then
            ' ''        .DialogTitle = xFindProjectDlgTitle
            ' ''    End If

            ' ''    If Not oProxy Is Nothing Then
            ' ''        .proxy = oProxy
            ' ''    Else
            ' ''        .proxy = GlobalMethods.g_oCnn.CurrentUser
            ' ''    End If
            ' ''End With

            ' ''oIAProj = GlobalMethods.g_oCnn.FindProjectDlg2(oOptions)

            ' ''GetProject = oIAProj
        End Function

        Private Function GetProjectFilterOLD() As CFilter
            ''returns a filter for projects
            'Dim oFilter As CFilter
            'Connect()
            'oFilter = New CFilter
            'With oFilter
            '    .FilterFields(1).Name = "Project"
            '    .FilterFields(2).Name = "DistributionListsOnly"
            '    .FilterFields(3).Name = "PeopleOnly"
            'End With
            'Dim oForm As ProjectFilterForm
            'oForm = New ProjectFilterForm
            'With oForm
            '    .IAConnection = GlobalMethods.g_oCnn
            '    .ShowDialog()
            '    If Not .Canceled Then
            '        oFilter.FilterFields(1).Value = .cmbProjects.BoundText
            '        oFilter.FilterFields(2).Value = .chkDistributionList = vbChecked
            '        oFilter.FilterFields(3).Value = .chkPersonContacts = vbChecked
            '        GetProjectFilterOLD = oFilter
            '    End If
            '    oForm = Nothing
            'End With
        End Function

        Private Function GetFilter() As CFilter
            'prompts user for filter/sort conditions
            Dim oForm As FilterForm
            Dim oBackend As ICIBackend
            Dim i As Integer

            oForm = New FilterForm

            With oForm
                .Backend = Me

                'add sort fields
                With .cbxSortBy
                    For i = 0 To 3
                        If GlobalMethods.g_vCols(i, 0) <> Nothing Then
                            .Items.Add(GlobalMethods.g_vCols(i, 0))
                        End If
                    Next i
                End With

                'get default sort field
                .SortColumn = ICIBackend_DefaultSortColumn

                'show filter form
                .ShowDialog()

                'branch on type of action requested
                If .Action = FilterForm.ciFilterActions.ciFilterAction_ClearSearch _
                    Or .Action = FilterForm.ciFilterActions.ciFilterAction_Search Then

                    '           set filter based on current settings in filter dlg
                    oBackend = Me
                    With oBackend.Filter
                        For i = 0 To .CountFields - 1
                            'set filter field values
                            .FilterFields(i).Value = oForm.txtFilterField(i).Text
                            .FilterFields(i).SearchOperator = oForm.cmbSearchTypes.SelectedValue
                        Next i
                    End With

                    'return filter
                    GetFilter = oBackend.Filter
                Else
                    GetFilter = Nothing
                End If

                oForm.Close()
                oForm.Dispose()
            End With
        End Function

        Public ReadOnly Property UniqueID() As Integer
            Get
                UniqueID = 2
            End Get
        End Property

        Private Function GetPhoneFaxNumbers(oIAContact As Contact,
                                            ByVal iPhoneType As ciContactNumberTypes,
                                            Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'retrieves phone/fax numbers for the specified ia contact and addresss type
            Dim oNums As CContactNumbers
            Dim oIAPhone As Phone
            Dim bIsCorrectType As Boolean

            Connect()
            oNums = New CContactNumbers

            For Each oIAPhone In oIAContact.Phones
                With oIAPhone
                    'get whether the phone type is the type requested
                    If iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone Then
                        bIsCorrectType = InStr(UCase(.PhoneType.Name), "FAX") = 0
                    Else
                        bIsCorrectType = InStr(UCase(.PhoneType.Name), "FAX") > 0
                    End If

                    If bIsCorrectType Then
                        'number is the type we want
                        If vAddressType IsNot Nothing Then
                            'GLOG 8854 - Description is always empty for User Contacts
                            oNums.Add(.PhoneType.Name & "|" & .PhoneNumber,
                                .PhoneType.Name & " - " & IIf(.Description <> "", .Description, .LocationType.Name),
                                .PhoneNumber, .Extension, iPhoneType, .PhoneType.Name)

                        Else
                            Dim ilRelTypes As IList
                            Dim criteria As New RelationshipTypeSearchCriteria
                            If TypeOf vAddressType Is String Then
                                criteria.Id = New DomainId(CLng(vAddressType), 2)
                            Else
                                criteria.Id = New DomainId(vAddressType, 2)
                            End If
                            ilRelTypes = GlobalMethods.g_oCnn.Relationships.FindRelationshipTypes(criteria)
                            If ilRelTypes.Count Then
                                If InStr(UCase(ilRelTypes(0).Name), "BUSINESS") > 0 Then
                                    'address type is business - get phones for all business types
                                    'GLOG 8854
                                    If InStr(UCase(.LocationType.Name), "BUSINESS") > 0 Then
                                        oNums.Add(.PhoneType.Name & "|" & .PhoneNumber,
                                        .PhoneType.Name & " - " & IIf(.Description <> "", .Description, .LocationType.Name),
                                        .PhoneNumber, .Extension, iPhoneType, .PhoneType.Name)
                                    End If
                                Else
                                    'address is not business - get only phones with same relationship
                                    If ilRelTypes(0).ID.SingleID = .PhoneType.Id.SingleId Then    '  .APERelationshipType.RelationshipId Then
                                        'GLOG 8854
                                        oNums.Add(.PhoneType.Name & "|" & .PhoneNumber,
                                        .PhoneType.Name & " - " & IIf(.Description <> "", .Description, .LocationType.Name),
                                        .PhoneNumber, .Extension, iPhoneType, .PhoneType.Name)
                                    End If
                                End If
                            End If
                        End If
                    End If
                End With
            Next oIAPhone

            'return collection
            GetPhoneFaxNumbers = oNums
        End Function

        'Private Function GetPhoneFaxNumbers(oIAContact As Contact, _
        '        ByVal iPhoneType As ciContactNumberTypes, _
        '        Optional ByVal vAddressType As Variant) As CContactNumbers
        ''retrieves phone/fax numbers for the specified ia contact and addresss type
        '    Dim oNums As CContactNumbers
        '    Dim oIAPhones As IAPhones
        '    Dim oIAPhone As Phone
        '    Dim oRel As IAAPERelationshipType
        '    Dim bIsCorrectType As Boolean
        '
        '    On Error GoTo ProcError
        '    Connect
        '    Set oNums = New CContactNumbers
        ''    If oIAContact.SourceFolderID = String.Empty Then
        ''        'get user contact numbers
        ''        If iPhoneType = ciContactNumberType_Phone Then
        ''            'get phones
        ''            With oIAContact.ContactDetail
        ''                If IsMissing(vAddressType) Then
        ''                    If .BusPhone <> Empty Then
        ''                        oNums.Add "Direct Phone|" & .BusPhone, "Direct Phone", .BusPhone, "", _
        ''                            ciContactNumberType_Phone, "Business"
        ''                    End If
        ''                    If .BusPhone2 <> Empty Then
        ''                        oNums.Add "Company Phone|" & .BusPhone2, "Company Phone", .BusPhone2, "", _
        ''                            ciContactNumberType_Phone, "Business"
        ''                    End If
        ''                    If .MobilePhone <> Empty Then
        ''                        oNums.Add "Mobile Phone|" & .MobilePhone, "Mobile Phone", .MobilePhone, "", _
        ''                            ciContactNumberType_Phone, "Business"
        ''                    End If
        ''                    If .Pager <> Empty Then
        ''                        oNums.Add "Pager|" & .Pager, "Pager", .Pager, "", ciContactNumberType_Phone, "Business"
        ''                    End If
        ''                    If .HomePhone <> Empty Then
        ''                        oNums.Add "Home Phone|" & .HomePhone, "Home Phone", .HomePhone, "", _
        ''                            ciContactNumberType_Phone, "Home"
        ''                    End If
        ''                    If .OtherPhone <> Empty Then
        ''                        oNums.Add "Other Phone|" & .OtherPhone, "Other Phone", .OtherPhone, "", _
        ''                            ciContactNumberType_Phone, "Business"
        ''                    End If
        ''                End If
        ''            End With
        ''        End If
        ''    End If
        '
        '    For Each oIAPhone In oIAContact.Phones
        '        With oIAPhone
        '            'get whether the phone type is the type requested
        '            If iPhoneType = ciContactNumberType_Phone Then
        '                bIsCorrectType = InStr(.PhoneType.DisplayName, "fax") = 0
        '            Else
        '                bIsCorrectType = InStr(.PhoneType.DisplayName, "fax") > 0
        '            End If
        '
        '            If bIsCorrectType Then
        '                'number is the type we want
        '                If IsMissing(vAddressType) Then
        '                    oNums.Add .APERelationshipType.DisplayName & "|" & .Phone & .PhoneID, _
        '                        .APERelationshipType.DisplayName & "" & _
        '                         IIf(.Description <> Empty, "-" & .Description, ""), _
        '                        .FormattedPhone, .Extension, iPhoneType, .APERelationshipType.DisplayName
        '                Else
        '                    Set oRel = g_oCnn.APERelationshipTypes.FindById(CLng(vAddressType))
        '                    If InStr(oRel.DisplayName, "business") > 0 Then
        '                        'address type is business - get phones for all business types
        '                        If InStr(.APERelationshipType.DisplayName, "business") > 0 Then
        '                            oNums.Add .APERelationshipType.DisplayName & "|" & .Phone & .PhoneID, _
        '                                .APERelationshipType.DisplayName & "" & _
        '                                 IIf(.Description <> Empty, "-" & .Description, ""), _
        '                                .FormattedPhone, .Extension, iPhoneType, .APERelationshipType.DisplayName
        '                        End If
        '                    Else
        '                        'address is not business - get only phones with same relationship
        '                        If oRel.RelationshipId = .APERelationshipType.RelationshipId Then
        '                            oNums.Add .APERelationshipType.DisplayName & "|" & .Phone & .PhoneID, _
        '                                .APERelationshipType.DisplayName & "" & _
        '                                 IIf(.Description <> Empty, "-" & .Description, ""), _
        '                                .FormattedPhone, .Extension, iPhoneType, .APERelationshipType.DisplayName
        '                        End If
        '                    End If
        '                End If
        '            End If
        '        End With
        '    Next oIAPhone
        '
        '    'return collection
        '    Set GetPhoneFaxNumbers = oNums
        '    Exit Function
        'ProcError:
        '    g_oError.RaiseError "CIIAWeb.CCIBackend.GetPhoneFaxNumbers"
        '    Exit Function
        'End Function

        Private Function GetProjectFolders(ByVal xModuleID As String, ByRef oCIFolders As CFolders, xStoreUNID As String) As CFolders
            'todo
            ' ''Dim oProjs As Projects
            ' ''Dim oProj As Project
            ' ''Dim oIAModule As IAProjectModule

            ' ''oIAModule = GlobalMethods.g_oCnn.ProjectModules.FindById(xModuleID)

            ' ''If Not g_oProxiedUser Is Nothing Then
            ' ''    oProjs = oIAModule.MyFavorites(g_oProxiedUser)
            ' ''Else
            ' ''    oProjs = oIAModule.MyFavorites()
            ' ''End If

            ' ''Dim oFolder As CFolder

            ' ''For Each oProj In oProjs
            ' ''    oFolder = New CFolder
            ' ''    oFolder.UNID = xStoreUNID & g_oConstants.UNIDSep & oProj.ProjectId
            ' ''    oFolder.Name = oProj.DisplayName
            ' ''    oCIFolders.Add(oFolder)
            ' ''Next oProj

            ' ''GetProjectFolders = oCIFolders
        End Function

        'todo
        Private Function GetIASearchObj(oFilter As CFilter, Optional oFolder As CFolder = Nothing) As ContactSearchCriteria
            'gets an iaContactSearch object
            'that corresponds to the supplied filter
            Dim iPos As Integer
            Dim i As Integer
            Dim xFld As String
            Dim xVal As String
            Dim iOp As ciSearchOperators
            Dim oIAFolder As Folder
            Dim oSearch As New ContactSearchCriteria
            Dim oFolderSearch As New FolderSearchCriteria
            Dim iFolderID As Integer
            Dim ilFoundFolders As IList

            Connect()

            With oSearch
                'GLOG : 15851 : ceh
                .SearchAllFolders = False

                If Not oFolder Is Nothing Then
                    'parse folder id
                    iPos = InStr(oFolder.ID, ".")
                    If iPos > 0 Then
                        iFolderID = Mid(oFolder.ID, iPos + 1)
                    Else
                        iFolderID = oFolder.ID
                    End If

                    oFolderSearch.Id = New DomainId(iFolderID, 2)

                    ilFoundFolders = GlobalMethods.g_oCnn.Folders.FindFolders(oFolderSearch, New PageContext(PagingMode.NoPaging))

                    If ilFoundFolders.Count Then
                        'don't allow empty searches on large firm folders
                        'applies to specific types containing a large amount of sub-folders, not contacts
                        'this property is set by adminitstrators
                        If ilFoundFolders(0).FolderType.IsLargeFolder And oFilter.IsEmpty Then
                            MsgBox("Please specify filter criteria before retrieving contacts " &
                                "from this folder.  The folder is " & vbCr & "designated as a ""large folder"" " &
                                "and may have a very large number of contacts.", vbExclamation)
                            GetIASearchObj = Nothing
                            Exit Function
                        Else
                            .Folder = ilFoundFolders(0)
                        End If
                    End If
                Else
                    'GLOG : 15851 : ceh
                    .SearchAllFolders = True
                End If

                'todo:not needed
                ''If GlobalMethods.g_oCnn.CurrentProxyUser IsNot Nothing Then
                ''    'user is proxying
                ''    .ProxyForUser = g_oProxiedUser
                ''End If

                'cycle through filter fields
                For i = 0 To 3
                    'get name of field
                    xFld = oFilter.FilterFields(i).Name

                    If xFld <> String.Empty Then
                        'get value of field
                        xVal = oFilter.FilterFields(i).Value

                        If xVal <> String.Empty Then
                            'add appropriate wildcards for specified operator
                            iOp = oFilter.FilterFields(i).SearchOperator
                            If iOp = ciSearchOperators.ciSearchOperator_Contains Then
                                xVal = "*" & xVal & "*"
                            ElseIf iOp = ciSearchOperators.ciSearchOperator_BeginsWith Then
                                xVal = xVal & "*"
                            End If

                            'set value to appropriate ia search property
                            Select Case xFld
                                ' ''Case "Display Name"
                                ' ''    .ContactName = xVal
                                Case "First Name"
                                    .FirstName = xVal
                                Case "Last Name"
                                    .LastName = xVal
                                Case "Company"
                                    .CompanyName = xVal
                                    ' ''Case "Country"
                                    ' ''    .Country = xVal
                                    ' ''Case "Department"
                                    ' ''    .Department = xVal
                                Case "EMail"
                                    .Email = xVal
                                Case "Job Title"
                                    .JobTitle = xVal
                                    ' ''Case "Postal Code"
                                    ' ''    .PostalCode = xVal
                                    ' ''Case "Sounds Like"
                                    ' ''    .SoundsLike = xVal
                                    ' ''Case "State"
                                    ' ''    .State = xVal
                            End Select
                        End If
                    End If
                Next i
            End With

            'return
            GetIASearchObj = oSearch
        End Function

        Private Function GetListings(oIAContacts As IList, ByVal xFolderUNID As String, iSortColumn As ciListingCols) As CListings
            'returns a CListings collection with contacts
            'in thespecified IAContacts collection
            Dim oListings As CListings
            Dim oIAContact As Contact
            Dim xUNID As String
            Dim bCancelled As Boolean
            Dim i As Integer
            Dim oUNID As CUNID
            Dim oPerson As Person
            Dim oCompany As Company

            Connect()
            oListings = New CListings
            oUNID = New CUNID

            m_oIAContacts = New Collection

            'cycle through collection,
            'adding to listings collection
            'todo
            For Each oIAContact In oIAContacts
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oIAContact.DisplayName - oIAContact.Class=" & oIAContact.DisplayName & " - " & oIAContact.UserFirmScope.ToString(),
                    "CIIAWeb.CCIBackend.GetListings")
                End If

                With oIAContact
                    'create contact UNID
                    xUNID = xFolderUNID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.GetLongID(.Id.DualId)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("xUNID=" & xUNID,
                        "CIIAWeb.CCIBackend.GetListings")
                    End If

                    If (.IsPerson) Then
                        oPerson = oIAContact
                        oListings.Add(xUNID, oPerson.DisplayName, oPerson.CompanyName,
                            oPerson.LastName, oPerson.FirstName, ciListingType.ciListingType_Person)
                    ElseIf .IsCompany Then
                        oCompany = oIAContact
                        oListings.Add(xUNID, oCompany.DisplayName, oCompany.CompanyName,
                            "", "", ciListingType.ciListingType_Person)
                    End If

                    'add to contacts collection - we use this so that
                    'we can reference individual contacts without having to
                    'call the API for the contact each time - much faster -
                    'see function IAContactFromListing
                    m_oIAContacts.Add(oIAContact, xUNID)
                End With

                'increment index that denotes the
                'number of listings added
                i = i + 1

                'notify the client that the
                'listing was added
                m_oEvents.RaiseAfterListingAdded(i,
                    oIAContacts.Count, bCancelled)

                If bCancelled Then
                    Exit For
                End If
            Next oIAContact

            'sort if necessary
            If iSortColumn <> ciListingCols.ciListingCols_DisplayName Then
                'sort by specified column - default sort is by display name
                oListings.Sort(iSortColumn)
            End If

            'cycle through returned contacts -
            'add each to listings collection
            m_oEvents.RaiseAfterListingsRetrieved(i)
            GetListings = oListings
        End Function

        Private Function GetEMailNumbers(oIAContact As Contact,
                Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'retrieves phone/fax numbers for the specified ia contact and addresss type
            Dim oNums As CContactNumbers
            Dim oIAEAddress As ElectronicAddress

            Connect()
            oNums = New CContactNumbers
            For Each oIAEAddress In oIAContact.ElectronicAddresses
                With oIAEAddress
                    If vAddressType IsNot Nothing Then
                        If InStr(UCase(oIAEAddress.ElectronicAddressType.Name), "E-MAIL") Or
                            InStr(UCase(oIAEAddress.ElectronicAddressType.Name), "EMAIL") Then
                            'eaddress type is EMail
                            oNums.Add(.AddressText,
                                 .LocationType.Name & "" &
                                 IIf(.Description <> String.Empty, "-" & .Description, " EMail"),
                                .AddressText, "", ciContactNumberTypes.ciContactNumberType_EMail, "EMail")
                        End If
                    Else
                        If InStr(UCase(.LocationType.Name), "E-MAIL") Or
                            InStr(UCase(.LocationType.Name), "EMAIL") Then

                            Dim ilRelTypes As IList
                            Dim criteria As New RelationshipTypeSearchCriteria
                            criteria.Id = CLng(vAddressType.SingleID.ToString())
                            ilRelTypes = GlobalMethods.g_oCnn.Relationships.FindRelationshipTypes(criteria)

                            'eaddress type is EMail
                            If InStr(UCase(ilRelTypes(0).Name), "BUSINESS") > 0 Then
                                'address type is business - get eaddresses for all business types
                                If InStr(UCase(.LocationType.Name), "BUSINESS") > 0 Then
                                    oNums.Add(.AddressText,
                                         .LocationType.Name & "" &
                                         IIf(.Description <> String.Empty, "-" & .Description, " EMail"),
                                        .AddressText, "", ciContactNumberTypes.ciContactNumberType_EMail, "EMail")
                                End If
                            Else
                                'address is not business - get only phones with same relationship
                                If ilRelTypes(0).ID.SingleID = .LocationType.Id.SingleId Then
                                    'If oRel.RelationshipId = .APERelationshipType.RelationshipId Then
                                    oNums.Add(.AddressText,
                                        .LocationType.Name & "" &
                                         IIf(.Description <> String.Empty, "-" & .Description, " EMail"),
                                        .AddressText, "", ciContactNumberTypes.ciContactNumberType_EMail, "EMail")
                                End If
                            End If
                        End If
                    End If
                End With
            Next oIAEAddress

            'return collection
            GetEMailNumbers = oNums
        End Function

        Private Function GetCustomFieldsCollection() As CCustomFields
            Dim oCustFlds As CCustomFields
            Dim iIndex As Integer
            Dim i As Integer
            Dim xCustomFlds As String
            Dim xName As String
            Dim xID As String
            Dim iPos As Integer
            Dim oIni As CIni

            If oCustFlds Is Nothing Then
                'create collection of custom fields with empty values
                oCustFlds = New CCustomFields

                oIni = New CIni

                i = 1

                'get custom fields from ini
                xCustomFlds = oIni.GetIni("Backend" & GlobalMethods.g_iID, "Custom" & i, oIni.CIIni)

                While xCustomFlds <> String.Empty
                    'get position of sep in key
                    iPos = InStr(xCustomFlds, ",")

                    If iPos = 0 Then
                        'invalid ini key setup
                        Throw New Exception("Invalid key '" & "Backend" & GlobalMethods.g_iID &
                            "\CustomField" & i & "' in ci.ini.")
                    Else
                        '               get name and id of custom field
                        xName = Left(xCustomFlds, iPos - 1)
                        xID = Mid(xCustomFlds, iPos + 1)

                        If xName = String.Empty Or xID = String.Empty Then
                            'invalid ini setup
                            Throw New Exception("Invalid key '" & "Backend" & GlobalMethods.g_iID &
                                "\CustomField" & i & "' in ci.ini.")
                        End If

                        'add to collection
                        oCustFlds.Add(xID, xName)
                    End If

                    'get next custom field from ini
                    i = i + 1
                    xCustomFlds = oIni.GetIni(
                        "Backend" & GlobalMethods.g_iID, "Custom" & i, GlobalMethods.g_oIni.CIIni)
                End While
            End If
            GetCustomFieldsCollection = oCustFlds
        End Function

        Private Function PromptForPhonesFaxesNew(oIAContact As Contact, oContact As CContact,
                        ByVal iPromptType As ICINumberPromptFormat.ciNumberPromptTypes,
                        ByVal iPhoneType As ciContactNumberTypes,
                        Optional ByVal vAddressID As Object = Nothing,
                        Optional bCancel As Boolean = False)
            'prompt for phone
            Dim oFormat As ICINumberPromptFormat
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber
            Dim oUNID As CUNID
            Dim xMessage As String

            Connect()

            oFormat = Me

            With oFormat
                'specify type of number supplied
                .NumberType = iPhoneType
                'set contact name
                .ContactName = oContact.FullName
                'specify whether we're prompting for a missing number
                'or multiple numbers
                .PromptType = iPromptType
            End With

            oUNID = New CUNID

            'get phone numbers as collection of ContactNumbers
            oNums = GetPhoneFaxNumbers(oIAContact,
                iPhoneType, vAddressID)

            If oNums.Count > 0 Then
                'prompt user for one number
                oNum = oNums.Prompt(oFormat)

                If Not oNum Is Nothing Then
                    With oContact
                        If iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            'set phone number and extension to selected item
                            .Phone = oNum.Number
                            .PhoneExtension = oNum.Extension
                            .PhoneID = oNum.ID
                        Else
                            'set fax number and extension to selected item
                            .Fax = oNum.Number
                            .FaxID = oNum.ID
                        End If
                    End With
                Else
                    'specify empty numbers
                    With oContact
                        If iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            .Phone = String.Empty
                            .PhoneExtension = String.Empty
                            .PhoneID = String.Empty
                        Else
                            .Fax = String.Empty
                            .FaxID = String.Empty
                        End If
                    End With
                    xMessage = "Do you want to insert the contact with the available information?"
                    bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No " & IIf(iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone, "Phones", "Faxes"))
                End If
            Else
                oNums.AlertNoneIfSpecifiedNew(iPhoneType, oContact.FullName, bCancel)
            End If
        End Function

        Private Function PromptForPhonesFaxes(oIAContact As Contact, oContact As CContact,
                                ByVal iPromptType As ICINumberPromptFormat.ciNumberPromptTypes,
                                ByVal iPhoneType As ciContactNumberTypes,
                                Optional ByVal vAddressID As Object = Nothing)
            'prompt for phone
            Dim oFormat As ICINumberPromptFormat
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber
            Dim oUNID As CUNID

            'this object implements ICINumberPromptFormat
            Connect()
            oFormat = Me
            With oFormat
                'specify type of number supplied
                .NumberType = iPhoneType
                'set contact name
                .ContactName = oContact.FullName
                'specify whether we're prompting for a missing number
                'or multiple numbers
                .PromptType = iPromptType
            End With

            oUNID = New CUNID

            '   get phone numbers as collection of ContactNumbers
            oNums = GetPhoneFaxNumbers(oIAContact,
                iPhoneType, vAddressID)

            If oNums.Count > 0 Then
                '       prompt user for one number
                oNum = oNums.Prompt(oFormat)

                If Not oNum Is Nothing Then
                    With oContact
                        If iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            'set phone number and extension to selected item
                            .Phone = oNum.Number
                            .PhoneExtension = oNum.Extension
                            .PhoneID = oNum.ID
                        Else
                            'set fax number and extension to selected item
                            .Fax = oNum.Number
                            .FaxID = oNum.ID
                        End If
                    End With
                Else
                    'specify empty numbers
                    With oContact
                        If iPhoneType = ciContactNumberTypes.ciContactNumberType_Phone Then
                            .Phone = String.Empty
                            .PhoneExtension = String.Empty
                            .PhoneID = String.Empty
                        Else
                            .Fax = String.Empty
                            .FaxID = String.Empty
                        End If
                    End With
                End If
            Else
                oNums.AlertNoneIfSpecified(iPhoneType, oContact.FullName)
            End If
        End Function

        Private Function IAContactFromListing(oListing As CListing) As Contact
            'returns an IA Contact for the specified listing
            Dim iStoreID As Integer
            Dim oUNID As CUNID
            Dim oIAContact As Contact = Nothing
            Dim oContactID As DomainId
            Dim options As New ContactReturnOptions

            If GlobalMethods.g_oCnn Is Nothing Then
                Connect()
            End If
            If GlobalMethods.g_oCnn.HasAuthenticated Then
                oUNID = New CUNID

                If Not m_oIAContacts Is Nothing Then
                    'attempt to get ia contact from stored collection of
                    'contacts - this collection is filled every time
                    'GetListings is called - this is fastest, as additional
                    'call to the API is required
                    'GLOG : 15851 : ceh
                    Try
                        If TypeOf m_oIAContacts.Item(oListing.UNID) Is Contact Then
                            oIAContact = m_oIAContacts.Item(oListing.UNID)
                        End If
                    Catch
                    End Try
                End If

                If oIAContact Is Nothing Then

                    'could not get ia contact from collection - get ia contact from listing
                    iStoreID = CUNID.GetUNIDField(oListing.UNID, ciUNIDFields.ciUNIDFields_Store)

                    'set return criteria
                    options = ContactReturnOptions.EasyOptions.AllPhonesAndAddresses

                    'get domainID from UNID
                    oContactID = DomainId.ParseLongUnsignedId(oListing.ID)

                    'GLOG : 15851 : ceh
                    'If iStoreID = IAObjectsConstants.IAObjectsConstant_Private Then
                    'try user contact first
                    Try
                        oIAContact = GlobalMethods.g_oCnn.Contacts.GetUserContact(New DomainId(DomainId.DomainIdClassType.UserContact,
                                                                                               oContactID.DualId.Aux,
                                                                                               oContactID.DualId.Source), options)
                    Catch
                    End Try

                    If oIAContact Is Nothing Then
                        Try
                            oIAContact = GlobalMethods.g_oCnn.Contacts.GetFirmContact(New DomainId(DomainId.DomainIdClassType.FirmContact,
                                                                                                   oContactID.DualId.Aux,
                                                                                                   oContactID.DualId.Source), options)
                        Catch
                        End Try
                    End If
                End If
            End If
            IAContactFromListing = oIAContact
        End Function

        Private Function GetAddress(ByVal oIAContact As Contact, ByRef oContact As CContact,
                                ByRef oAddress As CAddress, ByVal iAlerts As ciAlerts)
            'gets address info for supplied contact-
            'prompts user if necessary
            Dim iRelationID As Integer
            Dim oIAAddress As Address
            Dim i As Integer
            Dim j As Integer
            Dim iIndex As Integer
            Dim xListingUNID As String
            Dim oListing As CListing
            Dim xAddressID As String
            Dim xRelID As String

            Connect()
            With oIAContact

                'get address from relationship ID if we're not working with a v2 address
                'or if the contact is a user contact - contact is a user contact
                'if source folder id is empty
                If Left$(oAddress.ID, 3) <> "v2-" Or oIAContact.UserFirmScope = UserFirmScopeIndicator.User Then
                    'ensure address belongs to a user contact.  Should be recoded as a CContact Property.
                    If Len(oAddress.ID) <= 7 Then
                        GetAddressFromRelID(oIAContact, oContact, oAddress, iAlerts)
                        Exit Function
                    End If
                End If

                'v2 oAddress.id contains both address id and
                'address relationship id - parse address id
                Dim iPos As Integer

                'first parse out 'v2-' prefix
                xAddressID = Mid$(oAddress.ID, 4)

                'parse address
                iPos = InStr(xAddressID, "-")
                xAddressID = Mid$(xAddressID, iPos + 1)

                If oIAContact.Addresses.Count Then
                    For Each oAddr In oIAContact.Addresses
                        If oAddr.Id.SingleId = xAddressID Then
                            With oAddr
                                oContact.AddressTypeName = .LocationType.Name
                                oContact.AddressID = .Id.SingleId
                                oContact.AddressTypeID = .LocationType.Id.SingleId
                                oContact.Street1 = .Street
                                oContact.Street2 = ""
                                oContact.Street3 = ""
                                oContact.AdditionalInformation = .Additional
                                oContact.City = .City
                                oContact.State = .State
                                oContact.ZipCode = .PostalCode
                                oContact.Country = .CountryName
                                'GLOG : 8835 : ceh - should be temporary
                                oContact.CoreAddress = Replace(.FormattedAddress, .City & " " & .State, .City & ", " & .State)
                            End With
                            Exit Function
                        End If
                    Next
                End If

                If .Addresses.Count > 0 Then
                    'there are other addresses prompt
                    'get listing unid - required by PromptForAddress
                    xListingUNID = m_oUNID.GetListing(oContact.UNID).UNID

                    'prompt for address
                    oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses,
                        IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

                    If Not oAddress Is Nothing Then
                        'fill contact with newly selected address
                        GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
                    End If
                ElseIf ((iAlerts And ciAlerts.ciAlert_NoAddresses = ciAlerts.ciAlert_NoAddresses) Or iAlerts = ciAlerts.ciAlert_All) Then
                    'alert that there are no addresses
                    MsgBox("No addresses were found for " & .DisplayName & ".", vbExclamation, "No Addresses")
                End If
            End With
        End Function


        Private Function GetAddressNew(ByVal oIAContact As Contact,
                                       ByRef oContact As CContact,
                                       ByRef oAddress As CAddress,
                                       ByVal iAlerts As ciAlerts,
                                       bCancel As Boolean)
            'gets address info for supplied contact-
            'prompts user if necessary
            Dim iRelationID As Integer
            Dim ilFoundAddress As IList
            Dim i As Integer
            Dim j As Integer
            Dim iIndex As Integer
            Dim xListingUNID As String
            Dim oListing As CListing
            Dim xAddressID As String
            Dim xRelID As String
            Dim xMessage As String

            Connect()

            With oIAContact
                'get address from relationship ID if we're not working with a v2 address
                'or if the contact is a user contact - contact is a user contact
                'if source folder id is empty

                'todo
                If Left$(oAddress.ID, 3) <> "v2-" Or oIAContact.UserFirmScope = UserFirmScopeIndicator.User Then
                    'ensure address belongs to a user contact.  Should be recoded as a CContact Property.
                    If Len(oAddress.ID) <= 7 Then
                        GetAddressFromRelID(oIAContact, oContact, oAddress, iAlerts)
                        Exit Function
                    End If
                End If

                'v2 oAddress.id contains both address id and
                'address relationship id - parse address id
                Dim iPos As Integer

                'first parse out 'v2-' prefix
                xAddressID = Mid$(oAddress.ID, 4)

                'parse address
                iPos = InStr(xAddressID, "-")
                xAddressID = Mid$(xAddressID, iPos + 1)

                If oIAContact.Addresses.Count Then
                    For Each oAddr In oIAContact.Addresses
                        If oAddr.Id.SingleId = xAddressID Then
                            With oAddr
                                oContact.AddressTypeName = .LocationType.Name
                                oContact.AddressID = .Id.SingleId
                                oContact.AddressTypeID = .LocationType.Id.SingleId
                                oContact.Street1 = .Street
                                oContact.Street2 = ""
                                oContact.Street3 = ""
                                oContact.AdditionalInformation = .Additional
                                oContact.City = .City
                                oContact.State = .State
                                oContact.ZipCode = .PostalCode
                                oContact.Country = .CountryName
                                'GLOG : 8835 : ceh - should be temporary
                                oContact.CoreAddress = Replace(.FormattedAddress, .City & " " & .State, .City & ", " & .State)
                            End With
                            Exit Function
                        End If
                    Next
                End If

                If .Addresses.Count > 0 Then
                    'there are other addresses so prompt user
                    'get listing unid - required by PromptForAddress
                    xListingUNID = m_oUNID.GetListing(oContact.UNID).UNID

                    'prompt for address
                    oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses,
                        IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

                    If Not oAddress Is Nothing Then
                        'fill contact with newly selected address
                        GetAddressNew(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses, bCancel)
                    Else
                        xMessage = "Do you want to insert the contact with the available information?"
                        bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Phones")
                    End If
                ElseIf ((iAlerts And ciAlerts.ciAlert_NoAddresses = ciAlerts.ciAlert_NoAddresses) Or iAlerts = ciAlerts.ciAlert_All) Then
                    'alert that there are no addresses
                    xMessage = "No addresses were found for " & .DisplayName & "." & vbCr & vbCr &
                               "Do you want to insert the contact with the available information?"
                    bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
                End If
                '(iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All)
            End With
        End Function


        Private Function GetAddressFromRelIDNew(oIAContact As Contact,
                                                oContact As CContact,
                                                oAddress As CAddress,
                                                ByVal iAlerts As ciAlerts,
                                                bCancel As Boolean)

            'gets address info for supplied contact-
            'prompts user if necessary
            'Dim iRelationID As Integer
            'Dim oIAAddress As Address
            'Dim i As Integer
            'Dim j As Integer
            'Dim iIndex As Integer
            'Dim xListingUNID As String
            'Dim oListing As CListing
            'Dim oCIAddresses As CAddresses
            'Dim oCIAddress As CAddress
            'Dim xAddressID As String
            'Dim bAddressesExist As Boolean
            'Dim xMessage As String

            'todo? doesn't seem to be used
            ' ''On Error GoTo ProcError

            ' ''Connect()

            ' ''If Left$(oAddress.ID, 3) = "v2-" Then
            ' ''    'v2 oAddress.id contains both address id and
            ' ''    'address relationship id - parse relationship id
            ' ''    Dim iPos As Integer

            ' ''    'first parse out 'v2-' prefix
            ' ''    xAddressID = Mid$(oAddress.ID, 4)

            ' ''    'parse address
            ' ''    iPos = InStr(xAddressID, "-")
            ' ''    iRelationID = Left$(xAddressID, iPos - 1)
            ' ''Else
            ' ''    iRelationID = oAddress.ID
            ' ''End If


            ' ''oCIAddresses = New CAddresses

            '' ''get listing unid - required below
            ' ''xListingUNID = m_oUNID.GetListing(oContact.UNID).UNID

            ' ''i = 1

            ' ''With oIAContact
            ' ''    'count the number of addresses with the specified relationship
            ' ''    For Each oIAAddress In .Addresses
            ' ''        If oIAAddress.APERelationshipType.RelationshipId = iRelationID Then
            ' ''            If Not AddressIsEmpty(oIAAddress) Then
            ' ''                'create new address
            ' ''                oCIAddress = New CAddress

            ' ''                'fill address properties
            ' ''                With oCIAddress
            ' ''                    If oIAAddress.Description <> Empty Then
            ' ''                        .Name = oIAAddress.Description
            ' ''                    Else
            ' ''                        .Name = oIAAddress.APERelationshipType.DisplayName
            ' ''                    End If

            ' ''                    .UNID = xListingUNID & g_oConstants.UNIDSep & _
            ' ''                        "v2-" & oIAAddress.APERelationshipType.RelationshipId & "-" & oIAAddress.AddressID
            ' ''                End With

            ' ''                'add to collection
            ' ''                oCIAddresses.Add(oCIAddress)

            ' ''                'mark last index
            ' ''                iIndex = i
            ' ''            End If
            ' ''        End If
            ' ''        i = i + 1
            ' ''    Next oIAAddress

            ' ''    If oCIAddresses.Count = 1 Then
            ' ''        'exactly one address with specified relationship id
            ' ''        If AddressIsEmpty(.Addresses(iIndex)) Then
            ' ''            'that address is empty
            ' ''            oListing = m_oUNID.GetListing(oContact.UNID)

            ' ''            If ICIBackend_GetAddresses(oListing).Count > 0 Then
            ' ''                'prompt user for address-
            ' ''                oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses, _
            ' ''                    IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

            ' ''                If Not oAddress Is Nothing Then
            ' ''                    'fill contact with newly selected address
            ' ''                    GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
            ' ''                Else
            ' ''                    xMessage = "Do you want to insert the contact with the available information?"
            ' ''                    bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''                End If
            ' ''            Else
            ' ''                xMessage = "No addresses were found for " & .FullName & "." & vbCr & vbCr & _
            ' ''                           "Do you want to insert the contact with the available information?"
            ' ''                bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''            End If
            ' ''        Else
            ' ''            With .Addresses(iIndex)
            ' ''                On Error Resume Next
            ' ''                oContact.AddressTypeName = .APERelationshipType.DisplayName
            ' ''                oContact.AddressID = .APERelationshipType.RelationshipId
            ' ''                oContact.AddressTypeID = .APERelationshipType.RelationshipId
            ' ''                'todo
            ' ''                ' ''oContact.Street1 = .Line1
            ' ''                ' ''oContact.Street2 = .Line2
            ' ''                ' ''oContact.Street3 = .Line3
            ' ''                oContact.AdditionalInformation = .Additional
            ' ''                oContact.City = .City
            ' ''                oContact.State = .State
            ' ''                oContact.ZipCode = .PostalCode
            ' ''                oContact.Country = .CountryName
            ' ''                oContact.CoreAddress = .FormattedAddress
            ' ''            End With
            ' ''        End If
            ' ''    ElseIf oCIAddresses.Count > 1 Then
            ' ''        'prompt user for address-
            ' ''        oAddress = PromptForMultipleAddresses(xListingUNID, oCIAddresses, _
            ' ''            IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

            ' ''        If Not oAddress Is Nothing Then
            ' ''            'fill contact with newly selected address
            ' ''            GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
            ' ''        Else
            ' ''            xMessage = "Do you want to insert the contact with the available information?"
            ' ''            bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''        End If
            ' ''    Else
            ' ''        'there are no addresses with the specified type
            ' ''        If oIAContact.Addresses.Count > 0 Then
            ' ''            For Each oIAAddress In oIAContact.Addresses
            ' ''                If Not AddressIsEmpty(oIAAddress) Then
            ' ''                    bAddressesExist = True
            ' ''                    Exit For
            ' ''                End If
            ' ''            Next oIAAddress

            ' ''            If bAddressesExist Then
            ' ''                'there are other addresses - prompt user for address
            ' ''                oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses, _
            ' ''                    IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

            ' ''                If Not oAddress Is Nothing Then
            ' ''                    'fill contact with newly selected address
            ' ''                    GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
            ' ''                Else
            ' ''                    xMessage = "Do you want to insert the contact with the available information?"
            ' ''                    bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''                End If
            ' ''            ElseIf (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
            ' ''                xMessage = "No addresses were found for " & .FullName & "." & vbCr & vbCr & _
            ' ''                           "Do you want to insert the contact with the available information?"
            ' ''                bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''            End If
            ' ''        ElseIf (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
            ' ''            xMessage = "No addresses were found for " & .FullName & "." & vbCr & vbCr & _
            ' ''                       "Do you want to insert the contact with the available information?"
            ' ''            bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
            ' ''        End If
            ' ''    End If
            ' ''End With
        End Function


        Private Function GetAddressFromRelID(oIAContact As Contact, oContact As CContact,
                                             oAddress As CAddress, ByVal iAlerts As ciAlerts)
            'gets address info for supplied contact-
            'prompts user if necessary
            Dim iRelationID As Integer
            Dim oIAAddress As Address
            Dim i As Integer
            Dim j As Integer
            Dim iIndex As Integer
            Dim xListingUNID As String
            Dim oListing As CListing
            Dim oCIAddresses As CAddresses
            Dim oCIAddress As CAddress
            Dim xAddressID As String
            Dim bAddressesExist As Boolean
            Dim iAddressID As Integer

            Connect()

            If Left$(oAddress.ID, 3) = "v2-" Then
                'v2 oAddress.id contains both address id and
                'address relationship id - parse relationship id
                Dim iPos As Integer

                'first parse out 'v2-' prefix
                xAddressID = Mid$(oAddress.ID, 4)

                'parse address
                iPos = InStr(xAddressID, "-")
                iRelationID = Left$(xAddressID, iPos - 1)
            Else
                iRelationID = oAddress.ID
            End If

            oCIAddresses = New CAddresses

            'get listing unid - required below
            xListingUNID = m_oUNID.GetListing(oContact.UNID).UNID

            i = 0

            With oIAContact
                'count the number of addresses with the specified relationship
                For i = 0 To .Addresses.Count - 1
                    oIAAddress = .Addresses(i)
                    If oIAAddress.LocationType.Id = iRelationID Then
                        If Not AddressIsEmpty(oIAAddress) Then
                            'create new address
                            oCIAddress = New CAddress

                            'fill address properties
                            With oCIAddress
                                If oIAAddress.Description <> String.Empty Then
                                    .Name = oIAAddress.Description
                                Else
                                    .Name = oIAAddress.LocationType.Name
                                End If

                                Try
                                    iAddressID = oIAAddress.Id.SingleId
                                Catch
                                End Try

                                .UNID = xListingUNID & GlobalMethods.g_oConstants.UNIDSep &
                                    "v2-" & oIAAddress.LocationType.Id.ToString() & "-" & iAddressID
                            End With

                            'add to collection
                            oCIAddresses.Add(oCIAddress)

                            'mark last index
                            iIndex = i
                        End If
                    End If
                Next i

                If oCIAddresses.Count = 1 Then
                    'exactly one address with specified relationship id
                    If AddressIsEmpty(.Addresses(iIndex)) Then
                        'that address is empty
                        oListing = m_oUNID.GetListing(oContact.UNID)

                        If ICIBackend_GetAddresses(oListing).Count > 0 Then
                            'prompt user for address-
                            oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses,
                                IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

                            If Not oAddress Is Nothing Then
                                'fill contact with newly selected address
                                GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
                            End If
                        Else
                            MsgBox("No addresses were found for " & .DisplayName & ".", vbExclamation, "No Addresses")
                        End If
                    Else
                        With .Addresses(iIndex)
                            oContact.AddressTypeName = .LocationType.Name
                            oContact.AddressID = .LocationType.Id.SingleId    '''.APERelationshipType.RelationshipId
                            oContact.AddressTypeID = .LocationType.Id.SingleId     '''.APERelationshipType.RelationshipId
                            oContact.Street1 = .Street
                            oContact.Street2 = ""
                            oContact.Street3 = ""
                            oContact.AdditionalInformation = .Additional
                            oContact.City = .City
                            oContact.State = .State
                            oContact.ZipCode = .PostalCode
                            oContact.Country = .CountryName
                            'GLOG : 8835 : ceh - should be temporary
                            oContact.CoreAddress = Replace(.FormattedAddress, .City & " " & .State, .City & ", " & .State)
                        End With
                    End If
                ElseIf oCIAddresses.Count > 1 Then
                    'prompt user for address-
                    oAddress = PromptForMultipleAddresses(xListingUNID, oCIAddresses,
                        IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

                    If Not oAddress Is Nothing Then
                        'fill contact with newly selected address
                        GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
                    End If
                Else
                    'there are no addresses with the specified type
                    If oIAContact.Addresses.Count > 0 And
                    (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
                        For Each oIAAddress In oIAContact.Addresses
                            If Not AddressIsEmpty(oIAAddress) Then
                                bAddressesExist = True
                                Exit For
                            End If
                        Next oIAAddress

                        If bAddressesExist Then
                            'there are other addresses - prompt user for address
                            oAddress = PromptForAddress(xListingUNID, oIAContact.Addresses,
                                IIf(oContact.FullName = "", oContact.FullName, oContact.DisplayName))

                            If Not oAddress Is Nothing Then
                                'fill contact with newly selected address
                                GetAddress(oIAContact, oContact, oAddress, ciAlerts.ciAlert_NoAddresses)
                            End If
                        ElseIf (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
                            MsgBox("No addresses were found for " & oContact.FullName & ".", vbExclamation, "No Addresses")
                        End If
                    ElseIf (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
                        MsgBox("No addresses were found for " & oContact.FullName & ".", vbExclamation, "No Addresses")
                    End If
                End If
            End With
        End Function

        Private Sub CreateFilter()
            'creates an empty filter for backend based on ini definition
            Dim xKey As String
            Dim i As Integer
            Dim lCol As ciListingCols
            Dim oIni As New CIni

            'create empty filter
            m_oFilter = New CFilter

            For i = 1 To 4
                'get ini key for filter field of this backend
                xKey = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Filter" & i, oIni.CIIni)

                If xKey = String.Empty Then
                    Exit For
                End If

                With m_oFilter.FilterFields(i - 1)
                    'get name, id, operator of filter field
                    .Name = xKey
                    .ID = xKey
                    .SearchOperator = ciSearchOperators.ciSearchOperator_Equals +
                                      ciSearchOperators.ciSearchOperator_BeginsWith +
                                      ciSearchOperators.ciSearchOperator_Contains
                End With
            Next

            'get sort column

            Try
                lCol = CLng(GlobalMethods.g_oIni.GetUserIni("CIApplication", "Sort"))
            Catch ex As Exception
                lCol = ciListingCols.ciListingCols_DisplayName
            End Try

            m_oFilter.SortColumn = lCol
        End Sub

#End Region
#Region "******************ICIBackend***********************"
        Private Function ICIBackend_GetStoreListings(oStore As CStore, oFilter As CFilter) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore, oFilter)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing,
                                            ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers
            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers
            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder,
                                                      oFilter As CFilter) As CListings Implements ICIBackend.GetFolderListings
            'returns a collection of listings that match the filter criteria and are stored in the specified folder
            Try
                ICIBackend_GetFolderListings = GetFolderListings(oFolder, oFilter)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder) As CListings Implements ICIBackend.GetFolderListings
            'returns a collection of listings that match the filter criteria and are stored in the specified folder
            Try
                ICIBackend_GetFolderListings = GetFolderListings(oFolder)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing, vAddressType As Object) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing,
                                oAddress As CAddress,
                                ByVal vAddressType As Object,
                                ByVal iIncludeData As ciRetrieveData,
                                ByVal iAlerts As ciAlerts) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData, iAlerts)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing,
                                                oAddress As CAddress,
                                                ByVal vAddressType As Object,
                                                ByVal iIncludeData As ciRetrieveData) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing,
                                                oAddress As CAddress,
                                                ByVal vAddressType As Object) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing,
                                                oAddress As CAddress) As ICContacts Implements ICIBackend.GetContacts
            Try
                ICIBackend_GetContacts = GetContacts(oListing, oAddress)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetAddresses(oListing As CListing) As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Try
                ICIBackend_GetAddresses = GetAddresses(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetAddresses() As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Try
                ICIBackend_GetAddresses = GetAddresses()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_SupportsMultipleStores() As Boolean Implements ICIBackend.SupportsMultipleStores
            Get
                ICIBackend_SupportsMultipleStores = True
            End Get
        End Property

        Friend Property ICIBackend_DefaultSortColumn As ciListingCols Implements ICIBackend.DefaultSortColumn
            Get
                If m_iSortCol = vbNull Then

                    Try
                        m_iSortCol = GlobalMethods.g_oIni.GetIni("CIOutlookOM", "SortColumn", GlobalMethods.g_oIni.CIUserIni())
                    Catch
                    End Try

                    If m_iSortCol = String.Empty Then
                        m_iSortCol = ciListingCols.ciListingCols_DisplayName
                    End If
                End If

                ICIBackend_DefaultSortColumn = m_iSortCol

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_DefaultSortColumn= " & m_iSortCol,
                    "CIOutlookOM.CCIBackend.ICIBackend_DefaultSortColumn")
                End If
            End Get
            Set(value As ciListingCols)
                m_iSortCol = value
                GlobalMethods.g_oIni.SetIni("CIOutlookOM", "SortColumn", CStr(value), GlobalMethods.g_oIni.CIUserIni())
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("Set ci.ini.[CIOutlookOM]\SortColumn= " & value,
                    "CIIAWeb.CCIBackend.ICIBackend_DefaultSortColumn")
                End If
            End Set

        End Property

        Private Function ICIBackend_CustomMenuItem1() As String Implements ICIBackend.CustomMenuItem1
            Dim xCurProxied As String = ""

            Try
                If GlobalMethods.g_oCnn.IsActingAsProxy Then
                    xCurProxied = "'" & GlobalMethods.g_oCnn.CurrentProxyUser.DisplayName & "'"
                Else
                    xCurProxied = "myself"
                End If
                ICIBackend_CustomMenuItem1 = "&Act On Behalf Of... (currently " &
                    xCurProxied & ")"
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function

        Private Function ICIBackend_CustomMenuItem2() As String Implements ICIBackend.CustomMenuItem2
            'not available as a search parameter
            'ICIBackend_CustomMenuItem2 = "Set &Maximum Contacts to Display..."
            ICIBackend_CustomMenuItem2 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem3() As String Implements ICIBackend.CustomMenuItem3
            ICIBackend_CustomMenuItem3 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem4() As String Implements ICIBackend.CustomMenuItem4
            ICIBackend_CustomMenuItem4 = String.Empty
        End Function

        Private Function ICIBackend_CustomMenuItem5() As String Implements ICIBackend.CustomMenuItem5
            ICIBackend_CustomMenuItem5 = String.Empty
        End Function

        Private Sub ICIBackend_CustomProcedure1() Implements ICIBackend.CustomProcedure1
            Try
                ProxyFor()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub ICIBackend_CustomProcedure2() Implements ICIBackend.CustomProcedure2
            Try
                SetMaxResults()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub


        Private Sub ICIBackend_CustomProcedure3() Implements ICIBackend.CustomProcedure3
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure4() Implements ICIBackend.CustomProcedure4
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure5() Implements ICIBackend.CustomProcedure5
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private ReadOnly Property ICIBackend_SupportsContactAdd() As Boolean Implements ICIBackend.SupportsContactAdd
            Get
                'todo:API does not support
                'Dim xAllow As String

                'xAllow = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "AllowNewContact")

                'If UCase$(xAllow) = "FALSE" Then
                ICIBackend_SupportsContactAdd = False
                'Else
                '    ICIBackend_SupportsContactAdd = True
                'End If
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsContactEdit() As Boolean Implements ICIBackend.SupportsContactEdit
            Get
                ICIBackend_SupportsContactEdit = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsFolders() As Boolean Implements ICIBackend.SupportsFolders
            Get
                ICIBackend_SupportsFolders = True
            End Get

        End Property

        Private ReadOnly Property ICIBackend_SupportsNativeSearch() As Boolean Implements ICIBackend.SupportsNativeSearch
            Get
                ICIBackend_SupportsNativeSearch = False
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNestedFolders() As Boolean Implements ICIBackend.SupportsNestedFolders
            Get
                ICIBackend_SupportsNestedFolders = True
            End Get

        End Property

        'Private Property Get ICIBackend_SupportsStoreSearch() As Boolean
        '    ICIBackend_SupportsStoreSearch = True
        'End Property

        'Public Property Get ICIBackend_SupportsStoreLoad() As Boolean
        '    ICIBackend_SupportsStoreLoad = True
        'End Property
        '

        Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings Implements ICIBackend.SearchFolder
            'todo
            Dim oFilter As CFilter
            'Dim oIAProj As Project

            Try
                'If oFolder.StoreID = Interaction.IAContactSearchListProject Then
                '    oFilter = New CFilter
                '    With oFilter.FilterFields(1)
                '        .Name = "Project"
                '        .Value = oIAProj.ProjectId
                '    End With
                'Else
                oFilter = GetFilter()
                'End If

                If Not oFilter Is Nothing Then
                    ICIBackend_SearchFolder = ICIBackend_GetFolderListings(oFolder, oFilter)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_SearchOperators() As ciSearchOperators Implements ICIBackend.SearchOperators
            Get
                ICIBackend_SearchOperators = ciSearchOperators.ciSearchOperator_BeginsWith + ciSearchOperators.ciSearchOperator_Contains
            End Get
        End Property

        Private Function ICIBackend_SearchNative() As CListings Implements ICIBackend.SearchNative
            'todo: not available through API any longer
            ' ''            Dim oIAContacts As IAContacts
            ' ''            Dim oDlgOptions As IAFindDlgOptions

            ' ''            Connect()
            ' ''            On Error GoTo ProcError
            ' ''            oDlgOptions = New IAFindDlgOptions

            ' ''            With oDlgOptions
            ' ''                .DialogTitle = "Find Contacts"
            ' ''                .ShowAPEType = IAAPETypeNone
            ' ''                .ShowIncludeCompany = True
            ' ''            End With

            ' ''            oIAContacts = GlobalMethods.g_oCnn.FindMultContactDlg(oDlgOptions)

            ' ''            If Not oIAContacts Is Nothing Then
            ' ''                If oIAContacts.Item(1).Class = IAContactClassIAC Then
            ' ''                    ICIBackend_SearchNative = GetListings(oIAContacts, GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & _
            ' ''                        Interaction.IAContactSearchListPrivate & GlobalMethods.g_oConstants.UNIDSep & "", ICIBackend_DefaultSortColumn())
            ' ''                Else
            ' ''                    ICIBackend_SearchNative = GetListings(oIAContacts, GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & _
            ' ''                        Interaction.IAContactSearchListFirm & GlobalMethods.g_oConstants.UNIDSep & "", ICIBackend_DefaultSortColumn())
            ' ''                End If
            ' ''            End If
            ' ''            Exit Function
            ' ''ProcError:
            ' ''            GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICIBackend_SearchNative")
            ' ''            Exit Function
        End Function

        Private Function ICIBackend_SearchStore(oStore As CStore) As CListings Implements ICIBackend.SearchStore
            Dim oFilter As CFilter


            Try
                Connect()

                If oStore.ID = "100" Or oStore.ID = "200" Or oStore.ID = "300" Then
                    Exit Function
                End If

                oFilter = GetFilter()

                If Not oFilter Is Nothing Then
                    ICIBackend_SearchStore = ICIBackend_GetStoreListings(oStore, oFilter)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
        '
        'Private Function ICIBackend_SearchStore(oStore as CStore) as CListings
        '    Dim oFilter as CFilter
        '
        '    Connect
        '    On Error GoTo ProcError
        '    If oStore.id = "100" Then
        '        If g_oProxiedUser Is Nothing Then
        '            Set oFilter = GetProjectFilter("100")
        '        Else
        '            Set oFilter = GetProjectFilter("100", g_oProxiedUser)
        '        End If
        '    ElseIf oStore.id = "200" Then
        '        If g_oProxiedUser Is Nothing Then
        '            Set oFilter = GetProjectFilter("200")
        '        Else
        '            Set oFilter = GetProjectFilter("200", g_oProxiedUser)
        '        End If
        '    ElseIf oStore.id = "300" Then
        '        If g_oProxiedUser Is Nothing Then
        '            Set oFilter = GetProjectFilter("300")
        '        Else
        '            Set oFilter = GetProjectFilter("300", g_oProxiedUser)
        '        End If
        '    Else
        '        Set oFilter = GetFilter()
        '    End If
        '
        '    If Not oFilter Is Nothing Then
        '        Set ICIBackend_SearchStore = ICIBackend_GetStoreListings(oStore, oFilter)
        '    End If
        '    Exit Function
        'ProcError:
        '    GlobalMethods.g_oError.RaiseError "CIIAWeb.CCIBackend.ICIBackend_SearchStore"
        '    Exit Function
        'End Function

        Private Sub ICIBackend_Initialize(iID As Integer) Implements ICIBackend.Initialize
            Dim xName As String
            Dim xKey As String
            Dim lID As Long
            Dim i As Integer
            Dim iPos As Integer


            Try
                GlobalMethods.g_iID = iID

                'get column names/ids
                For i = 1 To 4
                    xKey = GlobalMethods.g_oIni.GetIni("Backend" & iID, "Col" & i, GlobalMethods.g_oIni.CIIni)
                    If xKey <> String.Empty Then
                        'assign to columns array
                        GlobalMethods.g_vCols(i - 1, 0) = xKey
                        GlobalMethods.g_vCols(i - 1, 1) = xKey
                    End If
                Next i

                'get how to display the firm contacts store - by folder or by contact type
                Try
                    GlobalMethods.g_bDisplayByContactType = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "DisplayByContactType")
                Catch
                End Try
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
        Private ReadOnly Property ICIBackend_InternalID() As Integer Implements ICIBackend.InternalID
            Get
                ICIBackend_InternalID = GlobalMethods.ciIA6InternalID
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsConnected() As Boolean Implements ICIBackend.IsConnected
            Get
                ICIBackend_IsConnected = m_bIsConnected
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsLoadableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsLoadableEntity
            'returns TRUE iff the specified UNID can contain contacts
            Get
                'todo
                Dim oUNID As CUNID
                Dim iUNIDType As ciUNIDTypes
                Dim iStoreID As Integer

                ICIBackend_IsLoadableEntity = False

                Try

                    oUNID = New CUNID
                    iUNIDType = oUNID.GetUNIDType(xUNID)

                    'entity can contain contacts if it's a folder whose unid
                    'folder field has a '.' or ' is the My Contacts store or
                    'is the 'double-click to search for project' node in Projects store
                    If iUNIDType <> ciUNIDTypes.ciUNIDType_Backend Then
                        iStoreID = CUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Store)

                        'todo: add Project....
                        ' ''ICIBackend_IsLoadableEntity = ((iUNIDType = ciUNIDTypes.ciUNIDType_Folder) And _
                        ' ''    (InStr(xUNID, ".") > 0 Or iStoreID <> IAObjectsConstants.IAObjectsConstant_Firm)) Or _
                        ' ''    (iUNIDType = ciUNIDTypes.ciUNIDType_Folder And GlobalMethods.g_bDisplayByContactType) Or _
                        ' ''    xUNID = GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & Interaction.IAContactSearchListProject & GlobalMethods.g_oConstants.UNIDSep & "0" Or _
                        ' ''    ((iUNIDType = ciUNIDTypes.ciUNIDType_Store) And iStoreID = IAObjectsConstants.IAObjectsConstant_Private)
                        ICIBackend_IsLoadableEntity = ((iUNIDType = ciUNIDTypes.ciUNIDType_Folder) And
                            (InStr(xUNID, ".") > 0 Or iStoreID <> IAObjectsConstants.IAObjectsConstant_Firm)) Or
                            (iUNIDType = ciUNIDTypes.ciUNIDType_Folder And GlobalMethods.g_bDisplayByContactType) Or
                            ((iUNIDType = ciUNIDTypes.ciUNIDType_Store) And iStoreID = IAObjectsConstants.IAObjectsConstant_Private)
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsSearchableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsSearchableEntity
            'returns TRUE iff the specified UNID can contain contacts
            Get
                'all folders and stores are searchable entities in MAPI
                Dim oUNID As CUNID
                Dim iType As ciUNIDTypes
                Dim iStoreID As Integer

                Try
                    oUNID = New CUNID
                    iType = oUNID.GetUNIDType(xUNID)

                    If iType <> ciUNIDTypes.ciUNIDType_Backend Then
                        iStoreID = CUNID.GetUNIDField(xUNID, ciUNIDFields.ciUNIDFields_Store)

                        ICIBackend_IsSearchableEntity = (iType = ciUNIDTypes.ciUNIDType_Folder And
                            iStoreID <> "100" And iStoreID <> "200" And iStoreID <> "300") Or
                            (iType = ciUNIDTypes.ciUNIDType_Store And (iStoreID = IAObjectsConstants.IAObjectsConstant_Firm Or
                            iStoreID = IAObjectsConstants.IAObjectsConstant_Private))
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Name() As String Implements ICIBackend.Name
            Get
                Dim xName As String
                Dim xDesc As String
                Dim oIni As CIni

                ICIBackend_Name = ""

                Try
                    oIni = New CIni

                    'get from ini
                    xName = oIni.GetIni("Backend" & GlobalMethods.g_iID, "Name", GlobalMethods.g_oIni.CIIni)

                    'raise error if ini value is missing
                    If Len(xName) = 0 Then
                        xDesc = "Invalid Backend" & GlobalMethods.g_iID & "\Name key in ci.ini."
                        Throw New Exception(xDesc)
                        Exit Property
                    End If

                    ICIBackend_Name = xName
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat Implements ICIBackend.NumberPromptFormat
            ICIBackend_NumberPromptFormat = Me
        End Function

        'Private Function ICIBackend_PromptForAddress(oListing As CListing) As CAddress Implements ICIBackend.PromptForAddress

        'End Function

        'Private Function ICIBackend_PromptForPhones(oContact As CContact, ByVal iPromptType As ICINumberPromptFormat.ciNumberPromptTypes, Optional ByVal vAddressID As Object = Nothing) As Object Implements ICIBackend.PromptForPhones

        'End Function


        Private Function ICIBackend_GetStores() As CStores Implements ICIBackend.GetStores
            'todo
            Dim bRet As Boolean

            Try
                If GlobalMethods.g_oCnn Is Nothing Then
                    bRet = Connect()
                    If bRet = False Then
                        ICIBackend_GetStores = Nothing
                        Exit Function
                    End If
                End If

                Static oStores As CStores
                Dim oS As CStore

                If oStores Is Nothing Then

                    'get folder classes & types
                    m_ilAllFolderClasses = GlobalMethods.g_oCnn.Folders.FindFolderClasses(FolderClassSearchCriteria.EasyCriteria.Active)
                    m_ilAllFolderTypes = GlobalMethods.g_oCnn.Folders.FindFolderTypes(FolderTypeSearchCriteria.EasyCriteria.Active)

                    Dim xExclusionList As String = ""

                    'get exclusion list
                    xExclusionList = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "StoreExclusions")

                    'get stores
                    oStores = New CStores

                    If InStr(xExclusionList, "My Contacts") = 0 Then
                        oS = New CStore
                        oS.UNID = GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & IAObjectsConstants.IAObjectsConstant_Private
                        oS.Name = "My Contacts"
                        oStores.Add(oS)
                    End If
                    If InStr(xExclusionList, "Firm Contacts") = 0 Then
                        oS = New CStore
                        oS.UNID = GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & IAObjectsConstants.IAObjectsConstant_Firm
                        oS.Name = "Firm Contacts"
                        oStores.Add(oS)
                    End If
                    If InStr(xExclusionList, "Marketing Lists") = 0 Then
                        oS = New CStore
                        oS.UNID = GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & IAObjectsConstants.IAObjectsConstant_MarketingList
                        oS.Name = "Marketing Lists"
                        oStores.Add(oS)
                    End If

                    If InStr(xExclusionList, "Working Lists") = 0 Then
                        oS = New CStore
                        oS.UNID = GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & IAObjectsConstants.IAObjectsConstant_WorkingList
                        oS.Name = "Working Lists"
                        oStores.Add(oS)
                    End If

                    'todo?  Modules...
                    ''Matters
                    'Dim oMatter As Projects.Matter
                    'bCanView = GlobalMethods.g_oCnn.SecurityManager.Projects.CanViewMatter

                    ''don't show if store is in exclusion list
                    'bCanView = bCanView And InStr(xExclusionList, GlobalMethods.g_oCnn.Nomenclature.Matter) = 0

                    'If bCanView Then
                    '    'add a store
                    '    oS = New CStore
                    '    oS.UNID = GlobalMethods.CIIA6WebInternalID & GlobalMethods.g_oConstants.UNIDSep & ""
                    '    oS.Name = GlobalMethods.g_oCnn.Nomenclature.Matters
                    '    oStores.Add(oS)
                    'End If

                    ''Opportunites
                    'Dim oOpport As Projects.Opportunity
                    'bCanView = GlobalMethods.g_oCnn.SecurityManager.Projects.CanViewOpportunity

                    ''don't show if store is in exclusion list
                    'bCanView = bCanView And InStr(xExclusionList, GlobalMethods.g_oCnn.Nomenclature.Opportunity) = 0

                    'If bCanView Then
                    '    'add a store
                    '    oS = New CStore
                    '    oS.UNID = GlobalMethods.CIIA6WebInternalID & GlobalMethods.g_oConstants.UNIDSep & oOpport.ModuleId.ToString()
                    '    oS.Name = GlobalMethods.g_oCnn.Nomenclature.Opportunities
                    '    oStores.Add(oS)
                    'End If

                    ''Engagements
                    'Dim oEngagement As Projects.Matter
                    'bCanView = GlobalMethods.g_oCnn.SecurityManager.Projects.CanViewEngagement

                    ''don't show if store is in exclusion list
                    'bCanView = bCanView And InStr(xExclusionList, GlobalMethods.g_oCnn.Nomenclature.Engagement) = 0

                    'If bCanView Then
                    '    'add a store
                    '    oS = New CStore
                    '    oS.UNID = GlobalMethods.CIIA6WebInternalID & GlobalMethods.g_oConstants.UNIDSep & oEngagement.ModuleId.ToString()
                    '    oS.Name = GlobalMethods.g_oCnn.Nomenclature.Engagements
                    '    oStores.Add(oS)
                    'End If

                    '' ''cycle through modules, adding a store for each one that the user can view
                    ' ''For Each oIAModule In oIAModules
                    ' ''    If g_oProxiedUser Is Nothing Then
                    ' ''        bCanView = oIAModule.UserCanView(g_oCnn.CurrentUser)
                    ' ''    Else
                    ' ''        bCanView = oIAModule.UserCanView(g_oProxiedUser)
                    ' ''    End If

                    ' ''    'don't show if store is in exclusion list
                    ' ''    bCanView = bCanView And InStr(xExclusionList, oIAModule.ModuleName) = 0

                    ' ''    If bCanView Then
                    ' ''        'add a store
                    ' ''        oS = New CStore
                    ' ''        oS.UNID = CIIA6WebInternalID & g_oConstants.UNIDSep & oIAModule.ModuleId
                    ' ''        oS.Name = oIAModule.PluralName
                    ' ''        oStores.Add(oS)
                    ' ''    End If
                    ' ''Next oIAModule
                End If


                ICIBackend_GetStores = oStores
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders Implements ICIBackend.GetSubFolders
            'todo
            'Dim oIAFolders As Folders
            Dim oIAFolder As Folder
            Dim oCIFolder As CFolder
            Dim oCIFolders As CFolders
            Dim iIAFolderType As Integer
            Dim iStoreID As Integer
            Dim ilFolderResults As IList


            Try
                If GlobalMethods.g_bDisplayByContactType Then
                    'no sub folders - sub folders exist
                    'only for firm contact folders
                    ICIBackend_GetSubFolders = Nothing
                    Exit Function
                End If

                Connect()

                If InStr(oFolder.ID, ".") > 0 Or
                    oFolder.StoreID = IAObjectsConstants.IAObjectsConstant_MarketingList Then
                    'the folder is not a folder type or
                    'the folder is a marketing folder-
                    'no more nested folders
                    ICIBackend_GetSubFolders = Nothing
                    Exit Function
                End If

                'the only root folders are IA folder types
                'get the type id of the root folder
                iIAFolderType = oFolder.ID

                oCIFolders = New CFolders

                'get store ID of specified root folder (oFolder)
                iStoreID = CInt(oFolder.StoreID)

                Select Case iStoreID
                    Case IAObjectsConstants.IAObjectsConstant_Firm
                        Dim oCriteria = New FolderSearchCriteria

                        'set criteria
                        oCriteria.FolderTypeId = iIAFolderType

                        'get all non-mailing list IA folders
                        ilFolderResults = GlobalMethods.g_oCnn.Folders.FindFolders(oCriteria, New PageContext(PagingMode.NoPaging))
                        For Each IAFolder As Folder In ilFolderResults
                            'take only those folders that are of the
                            'type designated by the parent folder (oFolder)
                            With IAFolder
                                'folder is not a mailing list folder - ID
                                'of IA subfolders is of the form x.y where
                                'x=foldertypeid and y=folderid - this allows
                                'us to distinguish between folder types and folders-
                                'folder types don't have the '.' - see above
                                oCIFolder = m_oUNID.GetFolder(
                                    GlobalMethods.ciIA6InternalID & GlobalMethods.g_oConstants.UNIDSep & iStoreID & GlobalMethods.g_oConstants.UNIDSep &
                                    .FolderType.Id.ToString() & "." & IAFolder.Id.SingleId, .Name)

                                'add folder to collection of folders
                                oCIFolders.Add(oCIFolder, True)
                            End With
                        Next IAFolder
                    Case IAObjectsConstants.IAObjectsConstant_Private
                        'don't do anything - there are no private folders
                    Case IAObjectsConstants.IAObjectsConstant_MarketingList
                        'do nothing - there are no marketing subfolders
                        'Case Interaction.IAContactSearchListProject
                        '    'add when this functionality becomes available
                End Select

                ICIBackend_GetSubFolders = oCIFolders
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean Implements ICIBackend.HasAddresses

        End Function

        'used to refer back to the ci.ini in the functions of this class
        Private Property ICIBackend_ID() As Integer Implements ICIBackend.ID
            Set(value As Integer)
                GlobalMethods.g_iID = value
            End Set
            Get
                ICIBackend_ID = GlobalMethods.g_iID
            End Get
        End Property

        Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Object) As CContactNumber Implements ICIBackend.GetPhoneNumber

        End Function


        Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders Implements ICIBackend.GetFolders
            'todo
            'Dim oIAFolders As IAFolders
            Dim oIAFolder As Folder
            Dim oCIFolder As CFolder
            Dim oCIFolders As CFolders
            ' ''Dim oIATypes As Interaction.IAContactTypes
            ' ''Dim oIAType As IAContactType
            Dim oDT As DataTable
            Dim i As Integer
            Dim j As Long
            'Dim oProjSearch As IAProjectSearch
            'Dim oProjs As Projects
            Dim oProj As Project

            Try
                Connect()
                oCIFolders = New CFolders

                Select Case oStore.ID
                    Case IAObjectsConstants.IAObjectsConstant_Firm
                        If GlobalMethods.g_bDisplayByContactType Then
                            'get all contact type exclusions
                            Dim xExclusions As String = String.Empty
                            Dim xExclusion As String = String.Empty

                            i = 1

                            Do
                                xExclusion = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID,
                                    "ContactTypeExclusion" & i, GlobalMethods.g_oIni.CIIni)

                                If xExclusion <> String.Empty Then
                                    xExclusions = xExclusions & "|" & xExclusion
                                End If
                                i = i + 1
                            Loop While xExclusion <> String.Empty

                            'append trailing pipe
                            xExclusions = xExclusions & "|"

                            'get all contact folders
                            Dim iResults As IList
                            Dim criteria As New FolderSearchCriteria

                            'set search criteria
                            criteria = FolderSearchCriteria.EasyCriteria.ContactTypes

                            Application.DoEvents()

                            iResults = GlobalMethods.g_oCnn.Folders.FindFolders(criteria,
                                                                                New PageContext(PagingMode.NoPaging))

                            For Each oFolder As Folder In iResults
                                If Not UCase$(xExclusions) Like "*|" & UCase$(oFolder.Name) & "|*" Then
                                    oCIFolder = m_oUNID.GetFolder(
                                        oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & oFolder.Id.SingleId, oFolder.Name)

                                    'add folder to collection of folders
                                    oCIFolders.Add(oCIFolder, True)
                                End If
                            Next
                        Else
                            'get all Administrative & Contact IA folder types
                            Dim ilAllFolderTypes As IList
                            Dim ilAllFolderClasses As IList

                            oCIFolder = Nothing

                            For Each oFolderClass As FolderClass In m_ilAllFolderClasses

                                If (oFolderClass.Id = InterActionConstants.FolderClassId.AdministrativeFolder Or
                                oFolderClass.Id = InterActionConstants.FolderClassId.ContactType) Then
                                    For Each oFolderType As FolderType In m_ilAllFolderTypes
                                        If oFolderType.FolderClass.Equals(oFolderClass) Then
                                            oCIFolder = m_oUNID.GetFolder(
                                                    oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & oFolderType.Id.SingleId, oFolderType.Name)
                                            'add folder to collection of folders
                                            oCIFolders.Add(oCIFolder, True)
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    Case IAObjectsConstants.IAObjectsConstant_Private
                        'don't do anything - there are no private folders
                    Case IAObjectsConstants.IAObjectsConstant_MarketingList
                        Dim iResults As IList
                        iResults = GlobalMethods.g_oCnn.Folders.FindFolders(FolderSearchCriteria.EasyCriteria.MarketingLists,
                                                                            New PageContext(PagingMode.NoPaging))

                        Application.DoEvents()

                        For Each oFolder As Folder In iResults
                            oCIFolder = m_oUNID.GetFolder(
                                    oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & oFolder.Id.SingleId, oFolder.Name)

                            'add folder to collection of folders
                            oCIFolders.Add(oCIFolder, True)
                        Next
                    Case IAObjectsConstants.IAObjectsConstant_WorkingList
                        Dim iResults As IList
                        iResults = GlobalMethods.g_oCnn.Folders.FindFolders(FolderSearchCriteria.EasyCriteria.WorkingLists,
                                                                            New PageContext(PagingMode.NoPaging))

                        Application.DoEvents()

                        For Each oFolder As Folder In iResults
                            oCIFolder = m_oUNID.GetFolder(
                                    oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & oFolder.Id.SingleId, oFolder.Name)

                            'add folder to collection of folders
                            oCIFolders.Add(oCIFolder, True)
                        Next

                        'todo
                        ' ''Case Interaction.IAContactSearchListProject
                        ' ''    oCIFolders.Add(m_oUNID.GetFolder(oStore.UNID & g_oConstants.UNIDSep & "0", _
                        ' ''                "Click 'Find' or Double-click here to search..."))

                        ' ''    oProjSearch = g_oCnn.NewProjectSearch

                        ' ''    With oProjSearch
                        ' ''        If Not g_oProxiedUser Is Nothing Then
                        ' ''            .ProxyForUser = g_oProxiedUser
                        ' ''        End If

                        ' ''        'we need to code for new project modules
                        ' ''        .InMyProjects = True
                        ' ''        .Execute()
                        ' ''        oProjs = .Results
                        ' ''    End With

                        ' ''    Dim oFolder As CFolder
                        ' ''    For Each oProj In oProjs
                        ' ''        oFolder = New CFolder
                        ' ''        oFolder.UNID = oStore.UNID & g_oConstants.UNIDSep & oProj.ProjectId
                        ' ''        oFolder.Name = oProj.DisplayName
                        ' ''        oCIFolders.Add(oFolder)
                        ' ''    Next oProj
                        ' ''Case 100 'Matters
                        ' ''    oCIFolders.Add(m_oUNID.GetFolder(oStore.UNID & g_oConstants.UNIDSep & "0", _
                        ' ''                "Double-click here to search..."))

                        ' ''    GetProjectFolders(oStore.ID, oCIFolders, oStore.UNID)
                        ' ''Case 200 'Engagement
                        ' ''    oCIFolders.Add(m_oUNID.GetFolder(oStore.UNID & g_oConstants.UNIDSep & "0", _
                        ' ''                "Double-click here to search..."))

                        ' ''    GetProjectFolders(oStore.ID, oCIFolders, oStore.UNID)
                        ' ''Case 300 'Opportunity
                        ' ''    oCIFolders.Add(m_oUNID.GetFolder(oStore.UNID & g_oConstants.UNIDSep & "0", _
                        ' ''                "Double-click here to search..."))

                        ' ''    GetProjectFolders(oStore.ID, oCIFolders, oStore.UNID)
                End Select

                ICIBackend_GetFolders = oCIFolders
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Object) As CContactNumber Implements ICIBackend.GetFaxNumber

        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing, vAddressType As Object) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'returns all phone numbers as a collection of contact numbers
            'returns the fax numbers for the specified listing and address relationship
            Dim oIAContact As Contact

            Try
                Connect()
                oIAContact = IAContactFromListing(oListing)
                ICIBackend_GetFaxNumbers = GetPhoneFaxNumbers(oIAContact,
                                                              ciContactNumberTypes.ciContactNumberType_Fax,
                                                              vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'returns all phone numbers as a collection of contact numbers
            'returns the fax numbers for the specified listing and address relationship
            Dim oIAContact As Contact

            Try
                Connect()
                oIAContact = IAContactFromListing(oListing)
                ICIBackend_GetFaxNumbers = GetPhoneFaxNumbers(oIAContact,
                                                              ciContactNumberTypes.ciContactNumberType_Fax)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function


        Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Object) As CContactNumber Implements ICIBackend.GetEMailNumber

        End Function


        Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields Implements ICIBackend.GetCustomFields
            'GLOG : 15854 : ceh
            Dim oCustFld As CCustomField
            Dim oCustFlds As CCustomFields
            Dim oIAContact As Contact
            Dim oIAValue As Fields.FieldValue = Nothing
            Dim xID As String

            Try
                Connect()
                oIAContact = IAContactFromListing(oListing)
                oCustFlds = GetCustomFieldsCollection()

                'get custom fields
                For Each oCustFld In oCustFlds
                    oCustFld.Value = String.Empty

                    'get id of custom field
                    xID = Trim(oCustFld.ID)


                    With oIAContact.AdditionalFields
                        'get value of field
                        Select Case UCase$(xID)
                            Case "CUSTOM1"
                                If Not String.IsNullOrEmpty(.Item(0).SingleValue.StringValue) Then
                                    oCustFld.Value = .Item(0).SingleValue.StringValue
                                End If
                            Case "CUSTOM2"
                                If Not String.IsNullOrEmpty(.Item(1).SingleValue.StringValue) Then
                                    oCustFld.Value = .Item(1).SingleValue.StringValue
                                End If
                            Case "CUSTOM3"
                                If Not String.IsNullOrEmpty(.Item(2).SingleValue.StringValue) Then
                                    oCustFld.Value = .Item(2).SingleValue.StringValue
                                End If
                            Case "CUSTOM4"
                                If Not String.IsNullOrEmpty(.Item(3).SingleValue.StringValue) Then
                                    oCustFld.Value = .Item(3).SingleValue.StringValue
                                End If
                            Case Else
                                'field id is not a user contact custom field
                                'todo? would need to return value by name
                        End Select
                    End With

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oCustFld.Value=" & oCustFld.Value &
                        "; oCustFld.ID=" & oCustFld.ID,
                        "CIIAWeb.CCIBackend.GetCustomFields")
                    End If
                Next oCustFld

                'return
                ICIBackend_GetCustomFields = oCustFlds
            Catch ex As Exception
                [Error].Show(ex)
            End Try
            ' ''            Exit Function
        End Function


        Private Sub ICIBackend_AddContact() Implements ICIBackend.AddContact
            'todo: not available through API
            'Dim oIAContact As Contact
            'On Error GoTo ProcError
            'If GlobalMethods.g_oCnn Is Nothing Then
            '    Connect()
            'End If
            'oIAContact = GlobalMethods.g_oCnn.NewContactDlg
            '            Exit Sub
            'ProcError:
            '            GlobalMethods.g_oError.ShowError()
            '            Exit Sub
        End Sub

        Private ReadOnly Property ICIBackend_Col1Name() As String Implements ICIBackend.Col1Name
            Get
                ICIBackend_Col1Name = ""
                Try
                    ICIBackend_Col1Name = GlobalMethods.g_vCols(0, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col1Name=" & ICIBackend_Col1Name,
                            "CIIA6Web.CCIBackend.ICIBackend_Col1Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col2Name() As String Implements ICIBackend.Col2Name
            Get
                ICIBackend_Col2Name = ""
                Try
                    ICIBackend_Col2Name = GlobalMethods.g_vCols(1, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col2Name=" & ICIBackend_Col2Name,
                            "CIIA6Web.CCIBackend.ICIBackend_Col2Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
        Private ReadOnly Property ICIBackend_Col3Name() As String Implements ICIBackend.Col3Name
            Get
                ICIBackend_Col3Name = ""
                Try
                    ICIBackend_Col3Name = GlobalMethods.g_vCols(2, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col3Name=" & ICIBackend_Col3Name,
                            "CIIA6Web.CCIBackend.ICIBackend_Col3Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col4Name() As String Implements ICIBackend.Col4Name
            Get
                ICIBackend_Col4Name = ""
                Try
                    ICIBackend_Col4Name = GlobalMethods.g_vCols(3, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col4Name=" & ICIBackend_Col4Name,
                            "CIIA6Web.CCIBackend.ICIBackend_Col4Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_DisplayName() As String Implements ICIBackend.DisplayName
            Get
                Try
                    If m_xDisplayName = "" Then
                        'get from ini
                        m_xDisplayName = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Name")

                        If m_xDisplayName = "" Then
                            'no value in ini - use default name
                            m_xDisplayName = ICIBackend_Name()
                        End If
                    End If
                    ICIBackend_DisplayName = m_xDisplayName

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_DisplayName=" & m_xDisplayName,
                        "CIIA6Web.CCIBackend.ICIBackend_DisplayName")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Sub ICIBackend_EditContact(oListing As CListing) Implements ICIBackend.EditContact
            'todo
            ' ''            Dim oIAContact As Contact
            ' ''            On Error GoTo ProcError
            ' ''            With g_oCnn
            ' ''                'get the contact from the listing
            ' ''                oIAContact = IAContactFromListing(oListing)

            ' ''                If oIAContact.Class = IAContactClassIAC Then
            ' ''                    'show ia edit contact dlg
            ' ''                    oIAContact.EditContactDlg()
            ' ''                ElseIf oIAContact.Class = IAContactClassIAL Then
            ' ''                    MsgBox("You can't edit InterAction firm contacts.", vbExclamation)
            ' ''                End If
            ' ''            End With
            ' ''            Exit Sub
            ' ''ProcError:
            ' ''            GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICIBackend_EditContact")
            ' ''            Exit Sub
        End Sub

        Private Function ICIBackend_Events() As CEventGenerator Implements ICIBackend.Events
            ICIBackend_Events = m_oEvents
        End Function

        Private ReadOnly Property ICIBackend_Exists() As Boolean Implements ICIBackend.Exists
            Get
                Dim bExists As Boolean
                Dim oApp As Object = Nothing

                Try
                    bExists = True

                    Try
                        oApp = New Connection
                    Catch
                    End Try

                    If oApp Is Nothing Then
                        bExists = False
                    End If

                    ICIBackend_Exists = bExists

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Exists=" & bExists & " (if false, then Interaction.Connection object is nothing)",
                        "CIIAWeb.CCIBackend.ICIBackend_Exists")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try


            End Get
        End Property

        Private Function ICIBackend_Filter() As CFilter Implements ICIBackend.Filter
            'returns the filter object for this backend
            ICIBackend_Filter = Nothing
            Try
                If m_oFilter Is Nothing Then
                    CreateFilter()
                End If

                ICIBackend_Filter = m_oFilter
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
#Region "******************ICINumberPromptFormat***********************"
        Public ReadOnly Property CategoryHeading As String Implements ICINumberPromptFormat.CategoryHeading
            Get
            End Get
        End Property

        Public ReadOnly Property CategoryWidth As Single Implements ICINumberPromptFormat.CategoryWidth
            Get
            End Get
        End Property

        Public Property ICINumberPromptFormat_ContactName() As String Implements ICINumberPromptFormat.ContactName
            Set(value As String)
                m_xName = value
            End Set
            Get
                ICINumberPromptFormat_ContactName = m_xName
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_AddressTypeHeading() As String Implements ICINumberPromptFormat.AddressTypeHeading
            Get
                ICINumberPromptFormat_AddressTypeHeading = "Type"
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumberDialogDescriptionText() As String Implements ICINumberPromptFormat.MissingNumberDialogDescriptionText
            Get
                Const ciText As String = "No <text> exist for this address type.  " &
                                         "Please select an alternate:"

                Dim xText As String = ""

                ICINumberPromptFormat_MissingNumberDialogDescriptionText = ""

                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                    ICINumberPromptFormat_MissingNumberDialogDescriptionText =
                        Replace(ciText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumbersDialogDescriptionText() As String Implements ICINumberPromptFormat.MissingNumbersDialogDescriptionText
            Get
                Dim xBaseText As String
                Dim xText As String = ""

                xBaseText = "No <text> exist for this address type for '" & ICINumberPromptFormat_ContactName() &
                            "'.  Please select an alternate:"
                ICINumberPromptFormat_MissingNumbersDialogDescriptionText = ""

                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select

                    ICINumberPromptFormat_MissingNumbersDialogDescriptionText =
                        Replace(xBaseText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property
        Private ReadOnly Property ICINumberPromptFormat_MultipleNumbersDialogDescriptionText() As String Implements ICINumberPromptFormat.MultipleNumbersDialogDescriptionText
            Get
                Dim xBaseText As String
                Dim xText As String = ""

                xBaseText = "Multiple <text> exist for '" & ICINumberPromptFormat_ContactName() &
                            "'.  Please select an alternate:"
                ICINumberPromptFormat_MultipleNumbersDialogDescriptionText = ""

                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select

                    ICINumberPromptFormat_MultipleNumbersDialogDescriptionText =
                        Replace(xBaseText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumbersDialogTitle() As String Implements ICINumberPromptFormat.MissingNumbersDialogTitle
            Get
                ICINumberPromptFormat_MissingNumbersDialogTitle = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select a Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select a Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select an EMail Address"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MultipleNumbersDialogTitle() As String Implements ICINumberPromptFormat.MultipleNumbersDialogTitle
            Get
                ICINumberPromptFormat_MultipleNumbersDialogTitle = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select a Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select a Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select an EMail Address"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Property ICINumberPromptFormat_NumberType As ciContactNumberTypes Implements ICINumberPromptFormat.NumberType
            Set(value As ciContactNumberTypes)
                m_iNumberType = value
            End Set
            Get
                ICINumberPromptFormat_NumberType = m_iNumberType
            End Get
        End Property

        '        Public ReadOnly Property ICINumberPromptFormat_PhoneTypeHeading() As String Implements ICINumberPromptFormat.PhoneTypeHeading
        '            Get
        '                On Error GoTo ProcError
        '                ICINumberPromptFormat_PhoneTypeHeading = "Type"
        '                Exit Property
        'ProcError:
        '                GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICINumberPromptFormat_PhoneTypeHeading")
        '                Exit Property
        '            End Get
        '        End Property

        Public ReadOnly Property ICINumberPromptFormat_DescriptionHeading() As String Implements ICINumberPromptFormat.DescriptionHeading
            Get
                ICINumberPromptFormat_DescriptionHeading = "Description"
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_NumberHeading() As String Implements ICINumberPromptFormat.NumberHeading
            Get
                ICINumberPromptFormat_NumberHeading = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_NumberHeading = "Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_NumberHeading = "Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_NumberHeading = "EMail Address"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " &
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_ExtensionHeading() As String Implements ICINumberPromptFormat.ExtensionHeading
            Get
                ICINumberPromptFormat_ExtensionHeading = ""
                Try
                    ICINumberPromptFormat_ExtensionHeading = "Ext."
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_AddressTypeWidth() As Single Implements ICINumberPromptFormat.AddressTypeWidth
            Get
                ICINumberPromptFormat_AddressTypeWidth = 0
                Try
                    ICINumberPromptFormat_AddressTypeWidth = 0
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        'todo?
        ' ''        Public ReadOnly Property ICINumberPromptFormat_PhoneTypeWidth() As Single Implements ICINumberPromptFormat.PhoneTypeWidth
        ' ''            Get
        ' ''                On Error GoTo ProcError
        ' ''                ICINumberPromptFormat_PhoneTypeWidth = 0
        ' ''                Exit Property
        ' ''ProcError:
        ' ''                GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICINumberPromptFormat_PhoneTypeWidth")
        ' ''                Exit Property
        ' ''            End Get
        ' ''        End Property

        Public ReadOnly Property ICINumberPromptFormat_DescriptionWidth() As Single Implements ICINumberPromptFormat.DescriptionWidth
            Get
                ICINumberPromptFormat_DescriptionWidth = 0
                Try
                    ICINumberPromptFormat_DescriptionWidth = 160
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_NumberWidth() As Single Implements ICINumberPromptFormat.NumberWidth
            Get
                ICINumberPromptFormat_NumberWidth = 0
                Try
                    ICINumberPromptFormat_NumberWidth = 160
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_ExtensionWidth() As Single Implements ICINumberPromptFormat.ExtensionWidth
            Get
                ICINumberPromptFormat_ExtensionWidth = 0
                Try
                    If m_iNumberType = ciContactNumberTypes.ciContactNumberType_Phone Then
                        ICINumberPromptFormat_ExtensionWidth = 50
                    Else
                        ICINumberPromptFormat_ExtensionWidth = 0
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Property ICINumberPromptFormat_PromptType As ICINumberPromptFormat.ciNumberPromptTypes Implements ICINumberPromptFormat.PromptType
            Get
                ICINumberPromptFormat_PromptType = m_iPromptType
            End Get
            Set(value As ICINumberPromptFormat.ciNumberPromptTypes)
                m_iPromptType = value
            End Set
        End Property

        '        Public ReadOnly Property ICINumberPromptFormat_CategoryHeading() As String Implements ICINumberPromptFormat.CategoryHeading
        '            Get
        '                On Error GoTo ProcError
        '                ICINumberPromptFormat_CategoryHeading = "Type"
        '                Exit Property
        'ProcError:
        '                GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICINumberPromptFormat_CategoryHeading")
        '                On Error GoTo ProcError
        '                Exit Property
        '            End Get
        '        End Property


        '        Public ReadOnly Property ICINumberPromptFormat_CategoryWidth() As Single Implements ICINumberPromptFormat.CategoryWidth
        '            Get
        '                On Error GoTo ProcError
        '                ICINumberPromptFormat_CategoryWidth = 0
        '                Exit Property
        'ProcError:
        '                GlobalMethods.g_oError.RaiseError("CIIAWeb.CCIBackend.ICINumberPromptFormat_CategoryWidth")
        '                Exit Property
        '            End Get

        '        End Property

#End Region
    End Class
End Namespace