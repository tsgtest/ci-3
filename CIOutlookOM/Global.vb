Option Explicit On

Imports [Outlook] = Microsoft.Office.Interop.Outlook
Imports Microsoft.Office.Interop.Outlook
Imports System.Windows.Forms
Imports LMP

Namespace TSG.CI.OutlookOM
    Friend Class GlobalMethods
        Private Declare Sub OutputDebugString Lib "kernel32" _
          Alias "OutputDebugStringA" _
          (ByVal lpOutputString As String)

        Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
          ByVal lpBuffer As String, nSize As Long) As Long

        Public Const MAPI_E_USER_CANCEL = &H80040113
        Public Const MAPI_E_NOT_FOUND = &H8004010F

        'Public Shared g_oCIFav As CIOutlook.CCIFavorites
        Public Shared g_oOutlook As Outlook.Application

        Public Shared g_iID As Integer
        Public Shared g_oError As TSG.CI.CError
        Public Shared g_oSession As CSessionType
        Public Shared g_oGlobals As CGlobals
        Public Shared g_oIni As CIni
        Public Shared g_oReg As CRegistry
        Public Shared g_oConstants As CConstants

        Public Sub DebugPrint(xOutput As String)
            Debug.Print(xOutput)
            OutputDebugString(xOutput)
        End Sub

        Public Shared Function GetMaxResults() As Long
            'returns the maximum number of contacts that can be retrieved
            Dim xMaxresults As String = ""
            Dim lMaxResults As Long

            If g_oIni Is Nothing Then
                g_oIni = New CIni
            End If


            Try
                xMaxresults = g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "MaxResults", g_oIni.CIIni)
            Catch
            End Try

            If xMaxresults = "" Or Not IsNumeric(xMaxresults) Then
                lMaxResults = 500
            Else
                lMaxResults = CLng(xMaxresults)
            End If
            GetMaxResults = lMaxResults
        End Function


        Public Shared Sub ResizeCombo(cmbBox As ComboBox, Optional iMaxRows As Integer = 0)
            'resizes the dropdown of the true db combo
            'to display the specified number of rows.
            Dim dtTable As DataTable
            Dim iRows As Integer

            With cmbBox
                dtTable = .DataSource

                If dtTable.Rows.Count() < iMaxRows Then
                    iRows = dtTable.Rows.Count()
                Else
                    iRows = iMaxRows
                End If

                Try
                    .MaxDropDownItems = iRows
                Catch
                End Try

            End With
        End Sub

        Public Shared Function bValidateBoundCombo(cmbBox As ComboBox) As Boolean
            'returns FALSE if invalid author has been specified, else TRUE
            With cmbBox
                If .SelectedText = "" Then 'BoundText
                    MsgBox("Invalid entry.  Please select an item " & _
                           "from the list.", vbExclamation, GlobalMethods.g_oSession.AppTitle)
                Else
                    'set text of control equal to the
                    'display text of the selected row
                    '.Text = .Columns(.ListField)
                    .Text = .SelectedText
                    bValidateBoundCombo = True
                End If
            End With
        End Function


        Public Shared Sub CorrectComboMismatch(cmbBox As ComboBox, iReposition As Integer)
            'resets the combo value to the previous match -
            'this procedure should be called in the
            'Mismatch even procedure only
            'Dim bytStart As Byte
            'todo

            'iReposition = False

            'With cmbBox
            '    '       get current selection start
            '    bytStart = .SelectionStart

            '       reset the text to the current list text
            'If .ListField = Nothing Then
            '    .Text = .Columns(0)
            'Else
            '    .Text = .Columns(.ListField)
            'End If

            '       return selection to original selection
            '.SelectionStart = GlobalMethods.Max(CDbl(bytStart - 1), 0)
            '.SelectionLength = Len(.Text)
            'End With
        End Sub

        Private Shared Function Max(i As Double, j As Double) As Double
            If i > j Then
                Max = i
            Else
                Max = j
            End If
        End Function

        Function Min(i As Double, j As Double) As Double
            If i > j Then
                Min = j
            Else
                Min = i
            End If
        End Function
    End Class
End Namespace