Option Explicit On

Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.Windows.Forms
Imports LMP

Namespace TSG.CI.OutlookOM
    Friend Class CCIBackend
        Implements ICIBackend
        Implements ICINumberPromptFormat
#Region "******************fields***********************"
        Public Enum ciMAPIAddresses
            ciMAPIAddress_Mailing = 1
            ciMAPIAddress_Business = 2
            ciMAPIAddress_Home = 3
            ciMAPIAddress_Other = 4
        End Enum

        Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, _
                 ByVal lpWindowName As String) As IntPtr
        Private Declare Function GetForegroundWindow Lib "user32" () As System.IntPtr
        Public Declare Function BringWindowToTop Lib "user32" (ByVal HWnd As IntPtr) As Boolean

        Private Const ciOutlookInternalID As Integer = 100
        Private m_iPromptType As ICINumberPromptFormat.ciNumberPromptTypes
        Private m_oEvents As CEventGenerator
        Private m_oOutApp As Outlook.Application
        Private m_iNumberType As ciContactNumberTypes
        Private m_vCols(3, 1) As Object
        Private m_xDisplayName As String
        Private m_bIsConnected As Boolean
        Private m_xName As String
        Private m_iSortCol As ciListingCols
        Private m_iShowContactFoldersOnly As Integer
        Private g_oIni As CIni
        Private SecurityManager As AddinExpress.Outlook.SecurityManager
        Private m_oInspector As Outlook.Inspector
        Private m_xExclusionArray() As String
        Private m_xExclusionList As String

#End Region
#Region "******************initializer***********************"
        Public Sub New()
            GlobalMethods.g_oGlobals = New CGlobals
            'initialize errors and events
            m_oEvents = GlobalMethods.g_oGlobals.CIEvents
            GlobalMethods.g_oConstants = New CConstants
            GlobalMethods.g_oIni = New CIni
            GlobalMethods.g_oError = New TSG.CI.CError
            GlobalMethods.g_oSession = New CSessionType
            GlobalMethods.g_oReg = New CRegistry

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("Initialized class CIOutlookOM.CCIBackend", _
                    "CIOutlookOM.CCIBackend.Class_Initialize")
            End If
        End Sub
#End Region
#Region "******************methods***********************"
        Protected Overrides Sub Finalize()
            'GLOG : 8797 : ceh
            If SecurityManager IsNot Nothing Then
                Try
                    If SecurityManager.DisableOOMWarnings = True Then
                        SecurityManager.DisableOOMWarnings = False
                    End If
                Catch
                    'do nothing
                Finally
                    SecurityManager.Dispose()
                    If m_oInspector IsNot Nothing Then
                        Try
                            m_oInspector.Close(Outlook.OlInspectorClose.olDiscard)
                        Catch
                        End Try
                    End If
                End Try
            End If
            MyBase.Finalize()
        End Sub

        Private Function GetStoreListings(oStore As CStore, _
                                          Optional oFilter As CFilter = Nothing) As CListings
            Dim oFolders As CFolders
            Dim oFolder As CFolder
            Dim oListings As CListings
            Dim oB As ICIBackend
            Dim l As Long
            Dim bCancel As Boolean

            If oFilter.IsEmpty Then
                'prevent listings from being retrieved - it could take ages
                MsgBox("Please specify search criteria before searching for contacts in your mailbox.", vbExclamation)
                GetStoreListings = Nothing
                Exit Function
            End If

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("oStore.UNID=" & oStore.UNID, _
                "CIOutlookOM.CCIBackend.ICIBackend_GetStoreListings")
            End If

            'if filter sort column is empty, use default sort column
            If oFilter.SortColumn = Nothing Then
                oFilter.SortColumn = ICIBackend_DefaultSortColumn()
            End If

            oB = Me

            'create new listings collection
            oListings = New CListings

            'get root folders in store
            oFolders = ICIBackend_GetFolders(oStore)

            'cycle through each root folder
            For Each oFolder In oFolders
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oStore.Name\oFolder.Name=" & _
                    oStore.Name & "\" & oFolder.Name, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetStoreListings")
                End If

                'get listings in folder and all subfolders
                GetFolderWithSubFolderListings(oFolder, oListings, _
                    oStore.Name & "\" & oFolder.Name, bCancel, oFilter)

                'client has Cancelled
                If bCancel Then
                    'return listings found so far
                    m_oEvents.RaiseAfterListingsRetrieved(oListings.Count)
                    GetStoreListings = oListings
                    Exit Function
                End If
            Next oFolder

            If oListings.Count Then
                'sort listings
                oListings.Sort(oFilter.SortColumn)
            End If

            'return listings
            GetStoreListings = oListings
            m_oEvents.RaiseAfterListingsRetrieved(oListings.Count)
        End Function

        Private Function PromptForAddress(oListing As CListing) As CAddress
            'prompts for addresses, returns the selected one
            Dim oForm As AddressesForm

            oForm = New AddressesForm

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID, _
                "CIOutlookOM.CCIBackend.PromptForAddress")
            End If

            With oForm
                .Backend = Me
                .Addresses = ICIBackend_GetAddresses(oListing)

                If .Addresses Is Nothing Then
                    MsgBox("Contact '" & oListing.DisplayName & "' has no addresses.")
                    PromptForAddress = Nothing
                    Exit Function
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".Addresses.count=" & .Addresses.Count, _
                    "CIOutlookOM.CCIBackend.PromptForAddress")
                End If

                .Listing = oListing

                .ShowDialog()

                If Not .Cancelled Then
                    '           get the selected contact number
                    PromptForAddress = oForm.Addresses.Item(.lstAddressTypes.SelectedIndex + 1)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("PromptForAddress=" & PromptForAddress.Name, _
                        "CIOutlookOM.CCIBackend.PromptForAddress")
                    End If
                Else
                    PromptForAddress = Nothing
                End If
            End With

            oForm.Close()
            oForm.Dispose()
        End Function

        Private Sub GetFolderListings(oFolder As CFolder, _
                                      oListings As CListings, _
                                      Optional oFilter As CFilter = Nothing, _
                                      Optional ByVal xFolderPath As String = vbNullString)
            'fills the supplied collection of listings that match the filter criteria and are stored in the specified folder
            'Const MAPI_ACCESS_MODIFY = &H1 'folder access modify permission bit

            Dim i As Long
            Dim lNumMsgs As Long
            Dim Cancel As Boolean
            Dim bIsOwner As Boolean
            Dim oItems As Outlook.Items = Nothing
            Dim oOutlookFolder As Outlook.Folder
            Dim oFilteredItems As Outlook.Items
            Dim l As Long
            Dim lMaxResults As Long

            Try
                If m_oOutApp Is Nothing Then
                    ConnectToOutlook()
                End If

                'get folder
                oOutlookFolder = m_oOutApp.Session.GetFolderFromID(oFolder.ID, oFolder.StoreID)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oFolder.ID=" & oFolder.ID & _
                    "; oFolder.StoreID=" & oFolder.StoreID & _
                    "; oOutlookFolder Is Nothing=" & CStr(oOutlookFolder Is Nothing), _
                     "CIOutlookOM.CCIBackend.GetFolderListings")
                End If

                If oOutlookFolder Is Nothing Then
                    Exit Sub
                End If

                If oOutlookFolder.DefaultItemType <> 2 Then
                    'there are no contacts in this folder - exit
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oOutlookFolder.DefaultItemType <> olOutlook.ContactItem", _
                         "CIOutlookOM.CCIBackend.GetFolderListings")
                    End If
                    Exit Sub
                End If

                'get folder messages
                Try
                    oItems = oOutlookFolder.Items
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oOutlookFolder.Items.Count = " & oOutlookFolder.Items.Count, _
                     "CIOutlookOM.CCIBackend.GetFolderListings")
                End If

                Dim lErr As Long
                Dim xDesc As String
                Dim iChoice As MsgBoxResult

                lErr = Err.Number

                While lErr = -2147024891
                    iChoice = MsgBox("To display your Outlook contacts, you must click 'Yes' " & _
                        "to the Outlook security dialog that just appeared.", vbExclamation + vbOKCancel)
                    If iChoice = vbOK Then
                        Try
                            oItems = oOutlookFolder.Items
                        Catch
                        End Try
                        lErr = Err.Number
                    Else
                        Exit Sub
                    End If
                End While

                If lErr <> 0 Then
                    'raise error
                    xDesc = Err.Description
                    Throw New Exception(xDesc)
                End If

                If oItems.Count = 0 Then
                    Exit Sub
                End If

                oFilter.SortColumn = ICIBackend_DefaultSortColumn()

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oFilter.SortColumn=" & oFilter.SortColumn, _
                    "CIOutlookOM.CCIBackend.GetFolderListings")
                End If

                m_oEvents.RaiseBeforeExecutingListingsQuery()

                'set filter - exclude private contacts
                'if user is not the owner of the folder
                oFilteredItems = SetFilterSort(oItems, oFilter, Not bIsOwner)

                Try
                    lNumMsgs = oFilteredItems.Count
                Catch
                End Try

                m_oEvents.RaiseAfterExecutingListingsQuery()

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("lNumMsgs=" & lNumMsgs, _
                    "CIOutlookOM.CCIBackend.GetFolderListings")
                End If

                'restrict to Max Results
                lMaxResults = GlobalMethods.GetMaxResults()

                If lNumMsgs > lMaxResults Then
                    Dim xMax As String
                    Dim vbMess As MsgBoxResult

                    xMax = Format(lMaxResults, "#,##0")
                    vbMess = MsgBox("This folder or search contains more than " & xMax & " contacts." & vbCr & vbCr & _
                        "Unless you load a different folder or specify new filter criteria, the first " & _
                        xMax & " contacts will be displayed.", vbOKCancel + vbInformation, GlobalMethods.g_oSession.AppTitle)

                    If vbMess = vbCancel Then
                        oListings = Nothing
                        Exit Sub
                    End If

                    lNumMsgs = lMaxResults
                End If

                If lNumMsgs Then
                    m_oEvents.RaiseBeforeListingsRetrieved(lNumMsgs, Cancel)

                    If Not Cancel Then
                        For i = 1 To lNumMsgs
                            If oFilteredItems.Item(i).Class = 40 Then   'olContact
                                AddListing(oFilteredItems.Item(i), oListings, oFolder.UNID, xFolderPath)
                                Application.DoEvents()
                                m_oEvents.RaiseAfterListingAdded(i, lNumMsgs, Cancel)
                                If Cancel Then
                                    m_oEvents.RaiseAfterListingsRetrieved(i)
                                    Exit Sub
                                End If
                            End If
                        Next i
                        m_oEvents.RaiseAfterListingsRetrieved(i - 1)
                    End If

                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("GetFolderListings is complete", _
                    "CIOutlookOM.CCIBackend.GetFolderListings")
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Public Function GetCIContactFromOutlookContact(oItem As Outlook.ContactItem, _
                                                        ByVal iDataRetrieved As ciRetrieveData) As CContact
            '   returns a CI contact from the specified Outlook contact item

            Dim oContact As CContact
            Dim xAddress As String
            Dim bHasMultPhones As Boolean
            Dim i As Integer
            Dim xTempPhone As String
            Dim xRetPhone As String
            Dim xRetExt As String
            Dim xRetFax As String
            Dim xRetFaxExt As String
            Dim xRetEAddress As String
            Dim xDefDisplayName As String
            Dim iMailingAddressSource As Integer
            Dim iChoice As MsgBoxResult
            Dim xOutlookFieldName As String
            Dim xSubNum As String
            Dim xSubNumType As String
            Dim xPhoneUNIDField As String
            Dim xFaxUNIDField As String
            Dim xEMailUNIDField As String

            oContact = New CContact

            xPhoneUNIDField = Nothing
            xFaxUNIDField = Nothing
            xEMailUNIDField = Nothing

            'fill Contact fields with DB field values
            With oItem
                On Error Resume Next
                oContact.AddressTypeName = "Mailing"
                oContact.AddressID = ciMAPIAddresses.ciMAPIAddress_Mailing
                oContact.AddressTypeID = ciMAPIAddresses.ciMAPIAddress_Mailing

                oContact.MiddleName = .MiddleName
                oContact.Prefix = .Title
                oContact.Suffix = .Suffix
                oContact.LastName = .LastName
                oContact.FirstName = .FirstName
                oContact.Company = .CompanyName
                oContact.Department = .Department
                oContact.Title = .JobTitle
                oContact.Initials = .Initials
                oContact.DisplayName = .FileAs
                'GLOG : 15855 : ceh
                oContact.GoesBy = .NickName

                If iDataRetrieved = ciRetrieveData.ciRetrieveData_All Or _
                    (iDataRetrieved And ciRetrieveData.ciRetrieveData_Addresses) Then
                    oContact.Street1 = .MailingAddressStreet
                    oContact.City = .MailingAddressCity
                    oContact.State = .MailingAddressState
                    oContact.ZipCode = .MailingAddressPostalCode
                    oContact.Country = .MailingAddressCountry

                    With oContact
                        'test for existence of address-related info
                        xAddress = .Street1 & .City & _
                            .State & .Country & .ZipCode
                    End With

                    If xAddress = "" Then
                        MsgBox("No mailing address found for '" & _
                            .FullName & "'.", vbExclamation)
                    End If
                End If

                If iDataRetrieved = ciRetrieveData.ciRetrieveData_All Or _
                    (iDataRetrieved And ciRetrieveData.ciRetrieveData_Phones) Then
                    'get phone/fax only if specified

                    'choose phone based on mailing address
                    If .SelectedMailingAddress = Outlook.OlMailingAddress.olBusiness Or .SelectedMailingAddress = Outlook.OlMailingAddress.olNone Then
                        oContact.Phone = .BusinessTelephoneNumber

                        If oContact.Phone = "" Then
                            xSubNum = .Business2TelephoneNumber
                            xSubNumType = "Business2"

                            If xSubNum = "" Then
                                xSubNum = .HomeTelephoneNumber
                                xSubNumType = "Home"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .Home2TelephoneNumber
                                xSubNumType = "Home2"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .OtherTelephoneNumber
                                xSubNumType = "Other"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No business telephone number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' phone number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Phone = xSubNum
                                End If
                            Else
                                MsgBox("No phone numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xPhoneUNIDField = "1|" & xSubNumType
                        Else
                            xPhoneUNIDField = "1|Business2"
                        End If
                    ElseIf .SelectedMailingAddress = Outlook.OlMailingAddress.olHome Then
                        oContact.Phone = .HomeTelephoneNumber

                        If oContact.Phone = "" Then
                            xSubNum = .Home2TelephoneNumber
                            xSubNumType = "Home2"

                            If xSubNum = "" Then
                                xSubNum = .BusinessTelephoneNumber
                                xSubNumType = "Business"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .Business2TelephoneNumber
                                xSubNumType = "Business2"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .OtherTelephoneNumber
                                xSubNumType = "Other"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No home telephone number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' phone number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Phone = xSubNum
                                End If
                            Else
                                MsgBox("No phone numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xPhoneUNIDField = "1|" & xSubNumType
                        Else
                            xPhoneUNIDField = "1|Home"
                        End If
                    ElseIf .SelectedMailingAddress = Outlook.OlMailingAddress.olOther Then
                        oContact.Phone = .OtherTelephoneNumber

                        If oContact.Phone = "" Then
                            xSubNum = .BusinessTelephoneNumber
                            xSubNumType = "Business"

                            If xSubNum = "" Then
                                xSubNum = .Business2TelephoneNumber
                                xSubNumType = "Business2"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .HomeTelephoneNumber
                                xSubNumType = "Home"
                            End If

                            If xSubNum = "" Then
                                xSubNum = .Home2TelephoneNumber
                                xSubNumType = "Home2"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No 'Other' telephone number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' phone number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Phone = xSubNum
                                End If
                            Else
                                MsgBox("No phone numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xPhoneUNIDField = "1|" & xSubNumType
                        Else
                            xPhoneUNIDField = "1|Other"
                        End If
                    End If
                End If

                If iDataRetrieved = ciRetrieveData.ciRetrieveData_All Or _
                    (iDataRetrieved And ciRetrieveData.ciRetrieveData_Faxes) Then
                    'choose fax based on mailing address
                    If .SelectedMailingAddress = Outlook.OlMailingAddress.olBusiness Or .SelectedMailingAddress = Outlook.OlMailingAddress.olNone Then
                        oContact.Fax = .BusinessFaxNumber

                        If oContact.Fax = "" Then
                            xSubNum = .HomeFaxNumber
                            xSubNumType = "Home"

                            If xSubNum = "" Then
                                xSubNum = .OtherFaxNumber
                                xSubNumType = "Other"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No business fax number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' fax number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Fax = xSubNum
                                End If
                            Else
                                MsgBox("No fax numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xFaxUNIDField = "2|" & xSubNumType
                        Else
                            xFaxUNIDField = "2|Business"
                        End If
                    ElseIf .SelectedMailingAddress = Outlook.OlMailingAddress.olHome Then
                        oContact.Fax = .HomeFaxNumber

                        If oContact.Fax = "" Then
                            xSubNum = .BusinessFaxNumber
                            xSubNumType = "Business"

                            If xSubNum = "" Then
                                xSubNum = .OtherFaxNumber
                                xSubNumType = "Other"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No home fax number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' fax number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Fax = xSubNum
                                End If
                            Else
                                MsgBox("No fax numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xFaxUNIDField = "2|" & xSubNumType
                        Else
                            xFaxUNIDField = "2|Home"
                        End If
                    ElseIf .SelectedMailingAddress = Outlook.OlMailingAddress.olOther Then
                        oContact.Fax = .OtherFaxNumber

                        If oContact.Fax = "" Then
                            xSubNum = .BusinessFaxNumber
                            xSubNumType = "Business"

                            If xSubNum = "" Then
                                xSubNum = .HomeFaxNumber
                                xSubNumType = "Home"
                            End If

                            If xSubNum <> "" Then
                                iChoice = MsgBox("No 'Other' fax number was found for " & .FullName & _
                                    " .  Do you want to use the '" & xSubNumType & "' fax number?", vbQuestion + vbYesNo)
                                If iChoice = vbYes Then
                                    oContact.Fax = xSubNum
                                End If
                            Else
                                MsgBox("No phone numbers found for '" & .FullName & "'.", vbExclamation)
                            End If
                            xFaxUNIDField = "2|" & xSubNumType
                        Else
                            xFaxUNIDField = "2|" & "Other"
                        End If
                    End If
                End If

                If iDataRetrieved = ciRetrieveData.ciRetrieveData_All Or _
                    (iDataRetrieved And ciRetrieveData.ciRetrieveData_EAddresses) Then
                    oContact.EMailAddress = .Email1Address
                    If oContact.EMailAddress = "" Then
                        oContact.EMailAddress = .Email2Address

                        If oContact.EMailAddress = "" Then
                            MsgBox("No email address found for '" & _
                                .FullName & "'.", vbExclamation)
                        End If
                        xEMailUNIDField = "3|Email 2"
                    Else
                        xEMailUNIDField = "3|Email 1"
                    End If
                End If

                'cehrecode
                If iDataRetrieved = ciRetrieveData.ciRetrieveData_All Or _
                    (iDataRetrieved And ciRetrieveData.ciRetrieveData_CustomFields) Then
                    oContact.CustomFields = GetCustomFieldsCollection()
                    For i = 1 To oContact.CustomFields.Count
                        'get the Outlook name for the specified field
                        xOutlookFieldName = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, _
                            "Custom" & i & "OutlookFieldName")

                        If xOutlookFieldName <> "" Then
                            'retrieve the value from the Outlook contact
                            oContact.CustomFields.Item(i).Value = ""
                            oContact.CustomFields.Item(i).Value = _
                               .ItemProperties.Item(xOutlookFieldName).Value
                        End If
                    Next i
                End If

                Dim oFolder As Outlook.MAPIFolder
                oFolder = .Parent

                oContact.UNID = ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & oFolder.StoreID & _
                    GlobalMethods.g_oConstants.UNIDSep & oFolder.EntryID & GlobalMethods.g_oConstants.UNIDSep & .EntryID & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Mailing & _
                    GlobalMethods.g_oConstants.UNIDSep & xPhoneUNIDField & ";" & xFaxUNIDField & ";" & xEMailUNIDField
                ParseStreetAddress(oContact)
            End With
            Err.Clear()
            GetCIContactFromOutlookContact = oContact
        End Function

        Private Sub ParseStreetAddress(ByRef oContact As CContact, Optional xSep As String = vbCrLf)
            '   parse street address into lines 1,2,3
            Dim iPos As Integer
            Dim iPos2 As Integer
            Dim xStreetAddr As String

            With oContact
                'get entire address
                xStreetAddr = .Street1

                'get first separator
                iPos = InStr(xStreetAddr, xSep)

                If iPos = 0 Then
                    'try soft return as separator
                    xSep = Chr(11)
                    iPos = InStr(xStreetAddr, xSep)
                End If

                If iPos > 1 Then
                    'there are multiple lines - get first line
                    .Street1 = Trim$(Left$(xStreetAddr, iPos - 1))

                    'check for a second separator
                    iPos2 = InStr(iPos + Len(xSep), xStreetAddr, xSep)
                    If iPos2 Then
                        '               second separator exists, parse lines 2 and 3
                        .Street2 = Trim$(Mid$(xStreetAddr, iPos + Len(xSep), (iPos2 - 1) - (iPos + Len(xSep)) + 1))
                        .Street3 = Trim$(Mid$(xStreetAddr, iPos2 + Len(xSep)))
                    Else
                        '               only line 2 exists
                        .Street2 = Trim$(Mid$(xStreetAddr, iPos + Len(xSep)))
                    End If
                End If
            End With
        End Sub

        'SMTP email address from Exchange address code
        Private Function xGetSMTPAddress(ExchangeMailAddress As String) As String
            Dim objMailItem As Outlook.MailItem

            objMailItem = m_oOutApp.CreateItem(0)

            objMailItem.To = ExchangeMailAddress

            objMailItem.Recipients.ResolveAll()

            On Error Resume Next
            If objMailItem.Recipients.Item(1).Resolved Then
                xGetSMTPAddress = objMailItem.Recipients.Item(1).AddressEntry.GetExchangeUser.PrimarySmtpAddress
                If Err.Number <> 0 Then xGetSMTPAddress = ExchangeMailAddress
            Else
                xGetSMTPAddress = ExchangeMailAddress
            End If
            Err.Clear()

            objMailItem = Nothing
        End Function

        Private Function GetCustomFields(oItem As Outlook.ContactItem) As CCustomFields
            'returns a filled collection of custom fields
            Dim oCustFlds As CCustomFields
            Dim iIndex As Integer
            Dim i As Integer
            Dim xCustomFlds As String
            Dim xFlds() As String
            Dim xName As String
            Dim xID As String
            Dim iPos As Integer
            Dim oIni As CIni
            Dim oCustFld As CCustomField

            oCustFlds = GetCustomFieldsCollection()

            '   get custom fields
            For Each oCustFld In oCustFlds
                On Error Resume Next
                oCustFld.Value = ""
                oCustFld.Value = oItem.ItemProperties(oCustFld.ID).Value
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oCustFld.Value=" & oCustFld.Value & _
                    "; oCustFld.ID=" & oCustFld.ID, _
                    "CIOutlookOM.CCIBackend.GetCustomFields")
                End If
            Next oCustFld
            Err.Clear()
            'return
            GetCustomFields = oCustFlds
        End Function

        Private Function GetCustomFieldsCollection() As CCustomFields
            Dim oCustFlds As CCustomFields
            Dim i As Integer
            Dim xCustomFlds As String
            Dim xName As String
            Dim xID As String
            Dim iPos As Integer

            'If oCustFlds Is Nothing Then
            'create collection of custom fields with empty values
            oCustFlds = New CCustomFields

            i = 1

            'get custom fields from ini
            xCustomFlds = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Custom" & i, GlobalMethods.g_oIni.CIIni)

            While xCustomFlds <> ""
                'get position of sep in key
                iPos = InStr(xCustomFlds, ",")

                If iPos = 0 Then
                    'invalid ini key setup
                    Throw New Exception("Invalid key '" & "Backend" & GlobalMethods.g_iID & _
                        "\CustomField" & i & "' in ci.ini.")
                Else
                    'get name and id of custom field
                    xName = Left(xCustomFlds, iPos - 1)
                    xID = Mid(xCustomFlds, iPos + 1)

                    If xName = "" Or xID = "" Then
                        'invalid ini setup
                        Throw New Exception("Invalid key '" & "Backend" & GlobalMethods.g_iID & _
                            "\CustomField" & i & "' in ci.ini.")
                    End If

                    'add to collection
                    oCustFlds.Add(xID, xName)
                End If

                'get next custom field from ini
                i = i + 1
                xCustomFlds = GlobalMethods.g_oIni.GetIni( _
                    "Backend" & GlobalMethods.g_iID, "Custom" & i, GlobalMethods.g_oIni.CIIni)
            End While
            GetCustomFieldsCollection = oCustFlds
        End Function

        Private Function SearchWithOutlook() As CListings
            Dim oListings As CListings
            Dim i As Long
            Dim lNumMsgs As Long
            Dim Cancel As Boolean
            Dim oIni As CIni
            Dim oItem As Object
            Dim oContactItem As Outlook.ContactItem
            Dim oConst As CConstants
            Dim xView As String
            Dim xCurView As String
            Dim oFolder As Outlook.Folder
            Dim lErr As Long
            Dim xDesc As String
            Dim oExplorer As Outlook.Explorer


            Try
                oIni = New CIni
                oConst = New CConstants
                xCurView = Nothing

                'clear out listings
                oListings = New CListings

                m_oEvents.RaiseBeforeListingsRetrieved(lNumMsgs, Cancel)

                'create a new listing for each record returned
                If Not Cancel Then
                    Dim hwnd As IntPtr
                    Dim hwndOutlook As IntPtr
                    Dim iExCount As Integer

                    'get handle of contact dlg
                    hwnd = GetForegroundWindow()

                    If m_oOutApp Is Nothing Then
                        ConnectToOutlook()
                    End If

                    'get existing Explorers count
                    iExCount = m_oOutApp.Explorers.Count

                    'exit if Outlook not started/visible
                    If iExCount = 0 Then
                        MsgBox("You need to start Outlook to use this feature.", vbInformation, My.Application.Info.Title)
                        SearchWithOutlook = Nothing
                        Exit Function
                    End If

                    'show contacts in Outlook
                    m_oOutApp.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts).Display()

                    oExplorer = m_oOutApp.ActiveExplorer

                    'get view for native search
                    xView = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "NativeSearchView")

                    If xView <> "" Then
                        'get current view
                        xCurView = oExplorer.CurrentView
                        'view was specified - set view
                        oExplorer.CurrentView = xView
                    End If

                    hwndOutlook = FindWindow(vbNullString, oExplorer.Caption)
                    If hwndOutlook <> 0 Then
                        BringWindowToTop(hwndOutlook)
                    End If

                    'cycle until we return to contact dlg
                    While GetForegroundWindow() <> hwnd
                        Application.DoEvents()
                    End While

                    'get num items selected
                    Try
                        lNumMsgs = oExplorer.Selection.Count
                    Catch
                    End Try

                    lErr = Err.Number
                    xDesc = Err.Description

                    If InStr(UCase$(xDesc), "THE EXPLORER HAS BEEN CLOSED") > 0 Then
                        'Outlook explorer was closed - alert
                        MsgBox("The contacts you may have selected could not be retrieved.  To insert contacts," & vbCr & "use ALT+TAB, or select Word in the taskbar " & _
                            "to return to " & GlobalMethods.g_oSession.AppTitle & ".", vbInformation, GlobalMethods.g_oSession.AppTitle)
                        SearchWithOutlook = Nothing
                        Exit Function
                    ElseIf lNumMsgs Then
                        If m_oOutApp Is Nothing Then
                            ConnectToOutlook()
                        End If
                    Else
                        If xCurView <> "" Then
                            oExplorer.CurrentView = xCurView
                        End If

                        Try
                            oExplorer.Close()
                        Catch
                        End Try

                        SearchWithOutlook = Nothing
                        Exit Function
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("lNumMsgs=" & lNumMsgs, _
                        "CIOutlookOM.CCIBackend.SearchWithOutlook")
                    End If

                    For Each oItem In oExplorer.Selection
                        'get only contact items
                        Try
                            oContactItem = oItem
                        Catch
                        End Try

                        If Not oContactItem Is Nothing Then

                            oFolder = oContactItem.Parent

                            If GlobalMethods.g_oError.DebugMode Then
                                GlobalMethods.g_oError.SendToDebug("oContactItem.FullName=" & oContactItem.FullName & _
                                "; oFolder.StoreID=" & oFolder.StoreID & "; oFolder.EntryID=" & oFolder.EntryID, "CIOutlookOM.CCIBackend.SearchWithOutlook")
                            End If

                            'add to collection of listings
                            AddListing(oContactItem, oListings, ciOutlookInternalID & oConst.UNIDSep & _
                                oFolder.StoreID & oConst.UNIDSep & oFolder.EntryID)
                        End If

                        'increment count of items retrieved
                        i = i + 1

                        m_oEvents.RaiseAfterListingAdded(i + 1, lNumMsgs, Cancel)

                        If Cancel Then
                            SearchWithOutlook = Nothing
                            Exit Function
                        End If
                    Next oItem

                    If xCurView <> "" Then
                        oExplorer.CurrentView = xCurView
                    End If

                    oExplorer.Close()

                    m_oEvents.RaiseAfterListingsRetrieved(oListings.Count)

                End If

                SearchWithOutlook = oListings
                'GLOG : 15765 : ceh
            Catch ex As Exception
                [Error].Show(ex)
                'Finally
                '    If Not (oExplorer Is Nothing) Then
                '        oExplorer.Close()
                '    End If
            End Try
        End Function

        'cehrecode if necessary
        ''Private Sub RefreshMessageFieldIDs(oMsg As MAPI.Message)
        ''    Dim i As Integer
        ''    Dim xFieldID As String
        ''    Static vCols(3, 1) As Variant
        ''
        ''    On Error GoTo ProcError
        ''
        ''    If vCols(0, 0) = Empty Then
        ''        'get the field IDs from ini - we do this so that we can
        ''        'ensure that we get the correct ID for each Exchange server -
        ''        'the user might be requesting a contact from a different server,
        ''        'and the ID would likely be different
        ''        GetColumnInfoArray vCols
        ''    End If
        ''
        ''    'cycles through the columns, changing those
        ''    'MAPI IDs specified in hex/guid form to longs-
        ''    'some CDO function only accept the longs
        ''    For i = 0 To 3
        ''        If Not IsNumeric(vCols(i, 1)) Then
        ''            'id is in hex form with prop set GUID
        ''            On Error Resume Next
        ''            m_vCols(i, 1) = oMsg.Fields(vCols(i, 1)).ID
        ''            On Error GoTo ProcError
        ''        End If
        ''    Next i
        ''    Exit Sub
        ''ProcError:
        ''    g_oError.RaiseError "CIOutlookOM.CCIBackend.RefreshMessageFieldIDs"
        ''    Exit Sub
        ''End Sub

        'cehrecode
        Private Function AddListing(oContact As Outlook.ContactItem, _
                                    oListings As CListings, _
                                    ByVal xFolderUNID As String, _
                                    Optional ByVal xFolderPath As String = vbNullString)
            Dim oConst As CConstants
            Dim xID As String
            Dim xDisplayName As String
            Dim xProp1 As String
            Dim xProp2 As String
            Dim xProp3 As String
            Dim xListingType As String
            Dim xMsgType As String
            Dim t0 As Long
            Dim xCompany As String
            Dim xLast As String
            Dim xFirst As String

            Try
                AddListing = Nothing

                With oContact
                    'clear out vars
                    xID = ""
                    xDisplayName = ""
                    xProp1 = ""
                    xProp2 = ""
                    xProp3 = ""
                    xMsgType = ""

                    xDisplayName = .ItemProperties.Item(m_vCols(0, 1)).Value       '.FileAs      '2.6.3016 to match MAPI

                    If xDisplayName = "" Then
                        If xDisplayName = "" Then
                            If GlobalMethods.g_oError.DebugMode Then
                                GlobalMethods.g_oError.SendToDebug("DisplayName is empty.  Build from Company/Last/First", _
                                "CIOutlookOM.CCIBackend.AddListing")
                            End If
                            'something is wrong, build DisplayName from Company/Last/First
                            xCompany = .CompanyName
                            xLast = .LastName
                            xFirst = .FirstName
                            If xLast <> "" Then
                                If xFirst <> "" Then
                                    xDisplayName = xLast & ", " & xFirst
                                Else
                                    xDisplayName = xLast
                                End If
                            ElseIf xFirst <> "" Then
                                xDisplayName = xFirst
                            Else
                                xDisplayName = xCompany
                            End If

                        End If
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("xDisplayName=" & xDisplayName, _
                        "CIOutlookOM.CCIBackend.AddListing")
                    End If

                    xProp1 = .ItemProperties.Item(m_vCols(1, 1)).Value
                    xProp2 = .ItemProperties.Item(m_vCols(2, 1)).Value
                    xProp3 = .ItemProperties.Item(m_vCols(3, 1)).Value

                    oConst = New CConstants
                    xID = xFolderUNID & oConst.UNIDSep & .EntryID

                    xListingType = ciListingType.ciListingType_Person

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("xID=" & xID & "; xDisplayName=" & xDisplayName & _
                        "; xProp1=" & xProp1 & "; xProp2=" & xProp2 & "; xProp3=" & xProp3 & _
                        "; xListingType=" & xListingType & "; xFolderPath=" & xFolderPath, _
                        "CIOutlookOM.CCIBackend.AddListing")
                    End If

                    oListings.Add(xID, xDisplayName, xProp1, xProp2, xProp3, xListingType, xFolderPath)
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function CreateFilter() As CFilter
            'creates an empty filter for backend based on ini definition
            Dim xKey As String
            Dim iPos As Integer
            Dim i As Integer
            Dim lCol As ciListingCols
            Dim oFilter As CFilter

            'create empty filter
            oFilter = New CFilter

            For i = 1 To 4
                'get ini key for filter field of this backend
                xKey = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Filter" & i, GlobalMethods.g_oIni.CIIni)

                If xKey = "" Then
                    Exit For
                End If

                'search for ',' - this delimits name from id in key
                iPos = InStr(xKey, ",")
                If iPos = 0 Or iPos = 1 Or iPos = Len(xKey) Then
                    'no delimiter present, no name specified, or no id specified
                    Throw New Exception("Key 'FilterFields" & GlobalMethods.g_iID & "\ " & _
                            CStr(i) & "' in CI.ini has an invalid value.")
                End If

                With oFilter.FilterFields(i - 1)
                    'get name, id, operator of filter field
                    .Name = Left$(xKey, iPos - 1)
                    .ID = Mid$(xKey, iPos + 1)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oFilter.FilterFields(" & i & ")  .Name=" & .Name & "; .ID=" & .ID & _
                        "; .Operator=" & .SearchOperator, "CIOutlookOM.CCIBackend.CreateFilter")
                    End If
                End With
            Next

            'get sort column

            Try
                lCol = CLng(GlobalMethods.g_oIni.GetIni("CIApplication", "Sort", GlobalMethods.g_oIni.CIUserIni))
            Catch
                lCol = ciListingCols.ciListingCols_DisplayName
            End Try

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("SortColumn (lCol)=" & lCol, _
                "CIOutlookOM.CCIBackend.CreateFilter")
            End If

            oFilter.SortColumn = lCol
            CreateFilter = oFilter
        End Function

        Private Function ShowContactFoldersOnly() As Boolean
            'returns False only if ini explicitly
            'says not to show contact folders only
            Dim xShow As String

            If m_iShowContactFoldersOnly = 0 Then
                'value has not been read from ini
                xShow = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "ShowContactFoldersOnly")
                If UCase$(xShow) = "FALSE" Then
                    'equivalent of False
                    m_iShowContactFoldersOnly = 1
                Else
                    'equivalent of True
                    m_iShowContactFoldersOnly = 2
                End If
            End If

            'return
            ShowContactFoldersOnly = m_iShowContactFoldersOnly - 1
        End Function

        Private Function GetFilter() As CFilter
            'prompts user for filter/sort conditions
            Dim oForm As FilterForm
            Dim oBackend As ICIBackend
            Dim i As Integer
            Dim oFilter As CFilter

            oForm = New FilterForm

            oFilter = CreateFilter()

            With oForm
                .Backend = Me

                'add sort fields
                With .cbxSortBy
                    For i = 0 To 3
                        If m_vCols(i, 0) <> Nothing Then
                            .Items.Add(m_vCols(i, 0))
                        End If
                    Next i
                End With

                'get default sort field
                .SortColumn = ICIBackend_DefaultSortColumn

                'show filter form
                .ShowDialog()

                'branch on type of action requested
                If .Action = FilterForm.ciMAPIFilterActions.ciMAPIFilterAction_ClearSearch _
                    Or .Action = FilterForm.ciMAPIFilterActions.ciMAPIFilterAction_Search Then

                    'set filter based on current settings in filter dlg
                    oBackend = Me
                    With oFilter
                        For i = 0 To .CountFields - 1
                            'set filter field values
                            .FilterFields(i).Value = oForm.txtFilterField(i).Text
                            .FilterFields(i).SearchOperator = oForm.cmbSearchTypes.SelectedValue


                            If GlobalMethods.g_oError.DebugMode Then
                                GlobalMethods.g_oError.SendToDebug(".FilterFields(i).Value=" & oForm.Controls("txtFilterField" & (i - 1).ToString).ToString & _
                                "; .FilterFields(i).Name=" & .FilterFields(i).Name, _
                                "CIOutlookOM.CCIBackend.GetFilter")
                            End If
                        Next i

                        'set sort field
                        .SortColumn = oForm.cbxSortBy.SelectedIndex

                        'set default sort column for future use
                        ICIBackend_DefaultSortColumn = .SortColumn

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("ICIBackend_DefaultSortColumn=" & _
                            ICIBackend_DefaultSortColumn, "CIOutlookOM.CCIBackend.GetFilter")
                        End If
                    End With

                    'return filter
                    GetFilter = oFilter
                Else
                    GetFilter = Nothing
                End If

                oForm.Close()
                oForm.Dispose()

            End With
        End Function

        Private Sub GetFolderWithSubFolderListings(oFolder As CFolder, _
                                           oListings As CListings, _
                                           ByVal xPath As String, _
                                           bCancel As Boolean, _
                                           Optional oFilter As CFilter = Nothing)
            Static oTopFolder As CFolder
            Dim oSubFolders As CFolders
            Dim oSubFolder As CFolder
            Dim xClass As String
            Dim oOutlookFolder As Outlook.Folder

            If oTopFolder Is Nothing Then
                'we're in the first iteration of the procedure-
                'mark, and create a new empty collection of listings
                oTopFolder = oFolder
            End If

            'get the sub folders of the current folder
            oSubFolders = ICIBackend_GetSubFolders(oFolder)

            'folder has sub folders - iterate
            For Each oSubFolder In oSubFolders
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oTopFolder=" & oTopFolder.Name & _
                    "; xPath & '\' & oSubFolder.Name=" & xPath & "\" & oSubFolder.Name, _
                    "CIOutlookOM.CCIBackend.GetFolderWithSubFolderListings")
                End If

                GetFolderWithSubFolderListings(oSubFolder, oListings, _
                    xPath & "\" & oSubFolder.Name, bCancel, oFilter)

                If bCancel Then
                    Exit For
                End If
            Next oSubFolder

            'folder has no sub folders - get listings in folder
            oOutlookFolder = m_oOutApp.Session.GetFolderFromID(oFolder.ID, oFolder.StoreID)

            If oOutlookFolder Is Nothing Then
                Exit Sub
            End If

            If oOutlookFolder.DefaultItemType <> 2 Then
                'there are no contacts in this folder - exit
                Exit Sub
            End If

            'folder contains listings -
            'alert client that we're searching the specified folder
            m_oEvents.RaiseBeforeStoreSearchFolderSearch(oFolder.Name, bCancel)

            'get listings
            GetFolderListings(oFolder, oListings, oFilter, xPath)

            If bCancel Then
                Exit Sub
            End If

            If oFolder Is oTopFolder Then
                'we're at the top folder, reset for next top folder
                oTopFolder = Nothing
            End If
        End Sub

        Private Function SetFilterSort(oItems As Outlook.Items, _
                                       oFilter As CFilter, _
                                       ByVal bExcludePrivate As Boolean) As Outlook.Items

            Dim i As Integer
            Dim xFilter As String
            Dim oFilteredItems As Outlook.Items
            Dim xLimitToIPMContacts As String
            Dim xValue As String
            Dim xFieldID As String
            Dim xSchema As String

            xFilter = ""

            If oItems.Count = 0 Then
                SetFilterSort = Nothing
                Exit Function
            End If

            xSchema = "urn:schemas:contacts:"

            'DoEvents()
            For i = 0 To 3
                With oFilter.FilterFields(i)
                    If .ID <> Nothing And .Value <> Nothing Then

                        xValue = .Value
                        If xValue <> "" Then
                            xFieldID = .ID

                            'double up any apostrophes
                            xValue = Replace(xValue, "'", "''")

                            'add wildcards if specified by operator
                            If .SearchOperator = ciSearchOperators.ciSearchOperator_BeginsWith Then
                                If xValue = "^123^" Then
                                    xValue = " LIKE '0%' OR " & _
                                             xSchema & xFieldID & " LIKE '1%' Or " & _
                                             xSchema & xFieldID & " LIKE '2%' Or " & _
                                             xSchema & xFieldID & " LIKE '3%' Or " & _
                                             xSchema & xFieldID & " LIKE '4%' Or " & _
                                             xSchema & xFieldID & " LIKE '5%' Or " & _
                                             xSchema & xFieldID & " LIKE '6%' Or " & _
                                             xSchema & xFieldID & " LIKE '7%' Or " & _
                                             xSchema & xFieldID & " LIKE '8%' Or " & _
                                             xSchema & xFieldID & " LIKE '9%'"
                                Else
                                    xValue = " LIKE '" & xValue & "%'"
                                End If
                            ElseIf .SearchOperator = ciSearchOperators.ciSearchOperator_Contains Then
                                xValue = " LIKE '%" & xValue & "%'"
                            Else
                                xValue = " LIKE '" & xValue & "'"
                            End If


                            If xFilter <> "" Then
                                xFilter = xFilter & " And "
                            End If

                            xFilter = xFilter & xSchema & xFieldID & xValue
                        End If

                    End If
                End With
            Next i

            '    If bExcludePrivate Then
            '        'include only those contacts not marked as private
            '        oMAPIFilter.Fields(ciMAPISensitivityFieldID) = 0
            '    End If

            If xFilter <> "" Then
                oFilteredItems = oItems.Restrict("@SQL=" & xFilter)
            Else
                oFilteredItems = oItems
            End If

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("xFilter=" & xFilter, _
                "CIOutlookOM.CCIBackend.SetFilterSort")
                GlobalMethods.g_oError.SendToDebug("oFilteredItems.Count=" & oFilteredItems.Count, _
                "CIOutlookOM.CCIBackend.SetFilterSort")
            End If

            'Sort items
            oFilteredItems.Sort(m_vCols(oFilter.SortColumn, 1))

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("m_vCols(oFilter.SortColumn, 1)=" & m_vCols(oFilter.SortColumn, 1), _
                "CIOutlookOM.CCIBackend.SetFilterSort")
            End If

            SetFilterSort = oFilteredItems

        End Function

        Private Sub GetColumnInfoArray(ByRef vCols(,) As Object)
            Dim xName As String
            Dim xKey As String
            Dim vID As Object
            Dim i As Integer
            Dim iPos As Integer

            Dim oIni As New CIni
            'get column names/ids
            For i = 1 To 4
                xKey = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Col" & i, GlobalMethods.g_oIni.CIIni)
                If xKey <> "" Then
                    'parse value
                    iPos = InStr(xKey, ",")
                    If iPos = 0 Then
                        'no comma found - should be in form 'Name,ID'
                        Throw New Exception("Invalid value in ci.ini for Backend" & GlobalMethods.g_iID & _
                            "\Col" & i & ".")
                    Else
                        'get name and id from string
                        xName = Left$(xKey, iPos - 1)
                        vID = Mid$(xKey, iPos + 1)

                        If xName = "" Or vID = Nothing Then
                            'missing some value
                            Throw New Exception("Invalid value in ci.ini for Backend" & GlobalMethods.g_iID & _
                                "\Col" & i & ".")
                        Else
                            'assign to columns array
                            vCols(i - 1, 0) = xName
                            vCols(i - 1, 1) = vID

                            'todo
                            'If GlobalMethods.g_oError.DebugMode Then
                            '    GlobalMethods.g_oError.SendToDebug("vCols(i - 1, 0)=" & xName & _
                            '    "; vCols(i - 1, 1)=" & vID, _
                            '    "CIOutlookOM.CCIBackend.GetColumnInfoArray")
                            'End If
                        End If
                    End If
                End If
            Next i
            oIni = Nothing
        End Sub

        Function GetContact(oListing As CListing, _
                    oAddress As CAddress, Optional iData As ciRetrieveData = ciRetrieveData.ciRetrieveData_All, _
                    Optional ByVal iAlerts As ciAlerts = ciAlerts.ciAlert_All) As CContact
            'creates a contact from a listing and address
            Dim oContact As CContact
            Dim oItem As Outlook.ContactItem = Nothing
            Dim oConst As CConstants
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber
            Dim oFormat As ICINumberPromptFormat
            Dim bCancel As Boolean

            Try
                oConst = New CConstants

                If (m_oOutApp Is Nothing) Then
                    ConnectToOutlook()
                ElseIf (m_oOutApp.Session Is Nothing) Then
                    ConnectToOutlook()
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oListing is Nothing=" & CStr(oListing Is Nothing), _
                    "CIOutlookOM.CCIBackend.GetContact")
                End If

                'get mapi message from the ci listing
                With oListing
                    Try
                        oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)
                    Catch
                    End Try
                End With

                If oItem Is Nothing Then
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oItem Is Nothing=" & CStr(oItem Is Nothing), _
                        "CIOutlookOM.CCIBackend.GetContact")
                    End If
                    GetContact = Nothing
                    Exit Function
                Else
                    oContact = New CContact
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oAddress is nothing =" & CStr(oAddress Is Nothing) & _
                    "; iData= " & iData & "; iAlerts= " & iAlerts, _
                    "CIOutlookOM.CCIBackend.GetContact")
                End If

                'fill Contact fields with DB field values
                With oItem
                    If oAddress Is Nothing Then
                        oContact.UNID = oListing.UNID & oConst.UNIDSep
                    Else
                        oContact.UNID = oListing.UNID & oConst.UNIDSep & oAddress.ID
                    End If

                    If oListing.DisplayName = Nothing Then
                        oContact.DisplayName = .FullName
                    Else
                        'use listing display name
                        oContact.DisplayName = oListing.DisplayName
                    End If

                    If (iData And ciRetrieveData.ciRetrieveData_Names) Or _
                        (iData = ciRetrieveData.ciRetrieveData_All) Then
                        'get name fields
                        oContact.MiddleName = .MiddleName
                        oContact.Prefix = .Title
                        oContact.Suffix = .Suffix
                        oContact.LastName = .LastName
                        oContact.FirstName = .FirstName
                        oContact.Company = .CompanyName
                        oContact.Department = .Department
                        oContact.Title = .JobTitle
                        oContact.Initials = .Initials
                        'GLOG : 15855 : ceh
                        oContact.GoesBy = .NickName
                    End If

                    If ((iData And ciRetrieveData.ciRetrieveData_Addresses) Or _
                        (iData = ciRetrieveData.ciRetrieveData_All)) And Not (oAddress Is Nothing) Then
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oAddress.UNID =" & oAddress.UNID, _
                            "CIOutlookOM.CCIBackend.GetContact")
                        End If
                        'get address info
                        GetAddressNew(oItem, oContact, oAddress, iAlerts, bCancel)
                        'user cancelled adding contact
                        If bCancel Then
                            oContact = Nothing
                            GetContact = Nothing
                            Exit Function
                        End If
                    End If
                End With

                Dim bAlertAll As Boolean
                bAlertAll = iAlerts And ciAlerts.ciAlert_All

                '   get phones if specified
                If (iData And ciRetrieveData.ciRetrieveData_Phones) Or _
                   (iData = ciRetrieveData.ciRetrieveData_All) Then
                    '       get all phones for the supplied address
                    If oAddress Is Nothing Then
                        oNums = ICIBackend_GetPhoneNumbers(oListing)
                    Else
                        oNums = ICIBackend_GetPhoneNumbers(oListing, oAddress.ID)
                    End If

                    If Not (oNums Is Nothing) Then
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oNums.count (Phones) =" & oNums.Count, _
                            "CIOutlookOM.CCIBackend.GetContact")
                        End If

                        With oNums
                            If .Count = 1 Then
                                'assign phone to contact
                                oContact.Phone = .Item(1).Number
                                oContact.PhoneExtension = .Item(1).Extension
                                oContact.PhoneID = .Item(1).ID
                            ElseIf .Count > 1 And (bAlertAll Or _
                                (iAlerts And ciAlerts.ciAlert_MultiplePhones)) Then
                                'there are multiple phones - prompts are requested
                                'prompt to select from all phones
                                PromptForPhones(oContact, _
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple, oAddress.ID)
                            ElseIf .Count > 1 Then
                                'no prompt requested - take the first one
                                oContact.Phone = .Item(1).Number
                                oContact.PhoneExtension = .Item(1).Extension
                                oContact.PhoneID = .Item(1).ID
                            ElseIf bAlertAll Or (iAlerts And ciAlerts.ciAlert_NoPhones) Then
                                'alert to missing numbers
                                PromptForPhones(oContact, _
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing)
                                'user cancelled adding contact
                                If oContact Is Nothing Then
                                    GetContact = Nothing
                                    Exit Function
                                End If
                            End If
                        End With
                    End If
                End If

                'get faxes - prompt if multiple or missing-
                If (iData And ciRetrieveData.ciRetrieveData_Faxes) Or _
                   (iData = ciRetrieveData.ciRetrieveData_All) Then
                    'get all faxes for the supplied address
                    If oAddress Is Nothing Then
                        oNums = ICIBackend_GetFaxNumbers(oListing)
                    Else
                        oNums = ICIBackend_GetFaxNumbers(oListing, oAddress.ID)
                    End If

                    If Not (oNums Is Nothing) Then
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oNums.count (Faxes) =" & oNums.Count, _
                            "CIOutlookOM.CCIBackend.GetContact")
                        End If
                        With oNums
                            If .Count = 1 Then
                                'assign fax to contact
                                oContact.Fax = .Item(1).Number
                                oContact.FaxID = .Item(1).ID
                            ElseIf .Count > 1 And (bAlertAll Or _
                                (iAlerts And ciAlerts.ciAlert_MultipleFaxes)) Then
                                'prompt to select from all phones
                                PromptForFaxes(oContact, _
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple, _
                                    oAddress.ID)
                            ElseIf .Count > 1 Then
                                'no prompt requested - take first fax
                                oContact.Fax = .Item(1).Number
                                oContact.FaxID = .Item(1).ID
                            ElseIf (bAlertAll Or _
                                (iAlerts And ciAlerts.ciAlert_NoFaxes)) Then
                                'alert that there are no faxes
                                PromptForFaxes(oContact, _
                                    ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Missing)
                                'user cancelled adding contact
                                If oContact Is Nothing Then
                                    GetContact = Nothing
                                    Exit Function
                                End If
                            End If
                        End With
                    End If
                End If

                If (iData And ciRetrieveData.ciRetrieveData_EAddresses) Or _
                   (iData = ciRetrieveData.ciRetrieveData_All) Then
                    'get email addresses
                    oNums = ICIBackend_GetEMailNumbers(oListing)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oNums.count (EMail) =" & oNums.Count, _
                        "CIOutlookOM.CCIBackend.GetContact")
                    End If

                    If oNums.Count = 1 Then
                        oContact.EMailAddress = oNums.Item(1).Number
                        oContact.EAddressID = oNums.Item(1).ID
                    ElseIf oNums.Count > 1 And (bAlertAll Or _
                        (iAlerts And ciAlerts.ciAlert_MultipleEAddresses)) Then
                        'there are multiple numbers - get list of numbers
                        oFormat = Me
                        oFormat.PromptType = ICINumberPromptFormat.ciNumberPromptTypes.ciNumberPromptType_Multiple

                        'prompt to select one
                        oNum = oNums.Prompt(oFormat)
                        If Not oNum Is Nothing Then
                            oContact.EMailAddress = oNum.Number
                            oContact.EAddressID = oNum.ID
                        End If
                    ElseIf oNums.Count > 1 Then
                        'no prompt requested - take first
                        oContact.EMailAddress = oNums.Item(1).Number
                        oContact.EAddressID = oNums.Item(1).ID
                    ElseIf (bAlertAll Or _
                        (iAlerts And ciAlerts.ciAlert_NoEAddresses)) Then
                        'if there are no numbers available, alert
                        oNums.AlertNoneIfSpecifiedNew( _
                            ciContactNumberTypes.ciContactNumberType_EMail, oContact.FullName, bCancel)
                        'user cancelled adding contact
                        If bCancel Then
                            oContact = Nothing
                            GetContact = Nothing
                            Exit Function
                        End If
                    End If
                End If

                'get custom fields for contact if specified
                If (iData And ciRetrieveData.ciRetrieveData_CustomFields) Or _
                   (iData = ciRetrieveData.ciRetrieveData_All) Then
                    oContact.CustomFields = ICIBackend_GetCustomFields(oListing)
                End If

                With oContact
                    'update UNID with contact number IDs
                    If oAddress Is Nothing Then
                        .UNID = oListing.UNID & oConst.UNIDSep & _
                             oConst.UNIDSep & _
                            .PhoneID & ";" & .FaxID & ";" & .EAddressID
                    Else
                        .UNID = oListing.UNID & oConst.UNIDSep & _
                            oAddress.ID & oConst.UNIDSep & _
                            .PhoneID & ";" & .FaxID & ";" & .EAddressID
                    End If
                End With
                GetContact = oContact
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function GetAddressNew(oItem As Outlook.ContactItem, oContact As CContact, _
            oAddress As CAddress, ByVal iAlerts As ciAlerts, ByRef bCancel As Boolean)
            'gets address info for supplied contact-
            'prompts user if necessary

            Dim iAddrID As ciMAPIAddresses
            Dim iMailingAddressSource As Integer
            Dim xMessage As String

            Try
                GetAddressNew = Nothing

                With oItem
                    If oAddress.ID = ciMAPIAddresses.ciMAPIAddress_Mailing Then
                        'get source address of mailing address

                        Try
                            iMailingAddressSource = oItem.SelectedMailingAddress
                        Catch
                        End Try

                        Select Case iMailingAddressSource
                            Case 1 'home
                                iAddrID = ciMAPIAddresses.ciMAPIAddress_Home
                            Case 2 'business
                                iAddrID = ciMAPIAddresses.ciMAPIAddress_Business
                            Case 3 'other
                                iAddrID = ciMAPIAddresses.ciMAPIAddress_Other
                        End Select
                    Else
                        iAddrID = oAddress.ID
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oAddress.ID=" & oAddress.ID & "; iAddrID= " & iAddrID, _
                        "CIOutlookOM.CCIBackend.GetAddressNew")
                    End If

                    Select Case iAddrID
                        Case ciMAPIAddresses.ciMAPIAddress_Business
                            oContact.AddressTypeName = "Business"
                            oContact.AddressID = ciMAPIAddresses.ciMAPIAddress_Business
                            oContact.AddressTypeID = ciMAPIAddresses.ciMAPIAddress_Business
                            oContact.Street1 = .BusinessAddressStreet
                            oContact.City = .BusinessAddressCity
                            oContact.State = .BusinessAddressState
                            oContact.ZipCode = .BusinessAddressPostalCode
                            oContact.Country = .BusinessAddressCountry
                            oContact.CoreAddress = .BusinessAddress

                        Case ciMAPIAddresses.ciMAPIAddress_Home
                            oContact.AddressTypeName = "Home"
                            oContact.AddressID = ciMAPIAddresses.ciMAPIAddress_Home
                            oContact.AddressTypeID = ciMAPIAddresses.ciMAPIAddress_Home
                            oContact.Street1 = .HomeAddressStreet
                            oContact.City = .HomeAddressCity
                            oContact.State = .HomeAddressState
                            oContact.Country = .HomeAddressCountry
                            oContact.ZipCode = .HomeAddressPostalCode
                            oContact.CoreAddress = .HomeAddress

                        Case ciMAPIAddresses.ciMAPIAddress_Other
                            oContact.AddressTypeName = "Other"
                            oContact.AddressID = ciMAPIAddresses.ciMAPIAddress_Other
                            oContact.AddressTypeID = ciMAPIAddresses.ciMAPIAddress_Other
                            oContact.Street1 = oItem.OtherAddressStreet
                            oContact.City = oItem.OtherAddressCity
                            oContact.State = oItem.OtherAddressState
                            oContact.Country = oItem.OtherAddressCountry
                            oContact.ZipCode = oItem.OtherAddressPostalCode
                            oContact.CoreAddress = oItem.OtherAddress
                    End Select
                End With

                With oContact
                    If (.Street1 = "" And .City = "" And _
                        .State = "" And .Country = "") And _
                        (iAlerts = ciAlerts.ciAlert_NoAddresses Or iAlerts = ciAlerts.ciAlert_All) Then
                        If HasAddresses(oItem) Then
                            Dim oUNID As CUNID
                            Dim oListing As CListing
                            oUNID = New CUNID

                            'prompt user for address
                            oListing = oUNID.GetListing(oContact.UNID)
                            oListing.DisplayName = .DisplayName
                            oAddress = PromptForAddress(oListing)

                            If Not oAddress Is Nothing Then
                                'fill contact with newly selected address
                                GetAddressNew(oItem, oContact, oAddress, ciAlerts.ciAlert_NoAddresses, bCancel)
                            End If
                        Else
                            xMessage = "No addresses were found for " & .FullName & "." & vbCr & vbCr & _
                                       "Do you want to insert the contact with the available information?"
                            bCancel = vbNo = MsgBox(xMessage, vbYesNo + vbExclamation, "No Addresses")
                        End If
                    Else
                        'parse street1 address into street2 and street3 if necessary
                        Dim iPos As Integer
                        Dim xStreet As String
                        xStreet = .Street1
                        iPos = InStr(xStreet, vbCrLf)

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("xStreet= " & xStreet & "; iPos= " & iPos, _
                            "CIOutlookOM.CCIBackend.GetAddressNew")
                        End If

                        If iPos > 0 Then
                            'there are multiple lines in street address-
                            'put 1st line in street1
                            .Street1 = Left$(xStreet, iPos - 1)
                            xStreet = Mid$(xStreet, iPos + Len(vbCrLf))

                            'check for additional lines
                            iPos = InStr(xStreet, vbCrLf)

                            If GlobalMethods.g_oError.DebugMode Then
                                GlobalMethods.g_oError.SendToDebug("xStreet= " & xStreet & "; iPos= " & iPos, _
                                 "CIOutlookOM.CCIBackend.GetAddressNew")
                            End If

                            If iPos > 0 Then
                                'there are at least three lines-
                                'parse into street2 and street3
                                .Street2 = Left$(xStreet, iPos - 1)
                                .Street3 = Mid$(xStreet, iPos + Len(vbCrLf))
                            Else
                                'no more additional lines - rest is street2
                                .Street2 = xStreet
                            End If
                        End If
                    End If
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Function

        Private Function PromptForPhones(ByRef oContact As CContact, _
                                ByVal iPromptType As ICINumberPromptFormat.ciNumberPromptTypes, _
                                Optional ByVal vAddressID As Object = vbNull)
            'prompt for phone
            Dim oFormat As ICINumberPromptFormat
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber
            Dim oUNID As CUNID
            Dim bCancel As Boolean

            PromptForPhones = Nothing

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("iPromptType= " & iPromptType & "; vAddressID= " & _
                    CStr(vAddressID), "CIOutlookOM.CCIBackend.PromptForPhones")
            End If

            'this object implements ICINumberPromptFormat
            oFormat = Me
            With oFormat
                'specify type of number supplied
                .NumberType = ciContactNumberTypes.ciContactNumberType_Phone
                'set contact name
                .ContactName = oContact.FullName
                'specify whether we're prompting for a missing number
                'or multiple numbers
                .PromptType = iPromptType
            End With

            oUNID = New CUNID

            'get phone numbers as collection of ContactNumbers
            oNums = ICIBackend_GetPhoneNumbers( _
                oUNID.GetListing(oContact.UNID), vAddressID)

            If oNums.Count > 0 Then
                'prompt user for one number
                oNum = oNums.Prompt(oFormat)

                If Not oNum Is Nothing Then
                    'set phone number and extension to selected item
                    With oContact
                        .Phone = oNum.Number
                        .PhoneExtension = oNum.Extension
                        .PhoneID = oNum.ID

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oNum.Number= " & oNum.Number & "; oNum.Extension= " & _
                            oNum.Extension & ";oNum.ID= " & oNum.ID, _
                            "CIOutlookOM.CCIBackend.PromptForPhones")
                        End If
                    End With
                Else
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oNum Is Nothing= TRUE", _
                        "CIOutlookOM.CCIBackend.PromptForPhones")
                    End If
                    With oContact
                        .Phone = ""
                        .PhoneExtension = ""
                        .PhoneID = ""
                    End With
                End If
            Else
                oNums.AlertNoneIfSpecifiedNew( _
                    ciContactNumberTypes.ciContactNumberType_Phone, oContact.FullName, bCancel)
                If bCancel Then
                    'user cancel adding contact with no number
                    oContact = Nothing
                End If
            End If
        End Function

        Private Function PromptForFaxes(ByRef oContact As CContact, iPromptType As ICINumberPromptFormat.ciNumberPromptTypes, _
                                        Optional vAddressID As Object = vbNull)
            'prompt for fax number
            Dim oFormat As ICINumberPromptFormat
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber
            Dim oUNID As CUNID
            Dim bCancel As Boolean

            PromptForFaxes = Nothing

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("iPromptType= " & iPromptType & "; vAddressID= " & _
                    CStr(vAddressID), "CIOutlookOM.CCIBackend.PromptForFaxes")
            End If

            '   this object implements ICINumberPromptFormat
            oFormat = Me

            With oFormat
                'specify type of number supplied
                .NumberType = ciContactNumberTypes.ciContactNumberType_Fax
                'set contact name
                .ContactName = oContact.FullName
                'specify whether we're prompting for a missing number
                'or multiple numbers
                .PromptType = iPromptType
            End With

            oUNID = New CUNID

            '   get fax numbers as collection of ContactNumbers
            oNums = ICIBackend_GetFaxNumbers( _
                oUNID.GetListing(oContact.UNID), vAddressID)

            If oNums.Count > 0 Then
                '       prompt user for one number
                oNum = oNums.Prompt(oFormat)

                If Not oNum Is Nothing Then
                    '           set fax number to selected item
                    oContact.Fax = oNum.Number
                    oContact.FaxID = oNum.ID

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oNum.Number= " & oNum.Number & "; oNum.ID= " & _
                        oNum.ID, "CIOutlookOM.CCIBackend.PromptForFaxes")
                    End If
                Else
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oNum Is Nothing=TRUE", _
                        "CIOutlookOM.CCIBackend.PromptForFaxes")
                    End If
                End If
            Else
                oNums.AlertNoneIfSpecifiedNew(ciContactNumberTypes.ciContactNumberType_Fax, _
                    oContact.FullName, bCancel)
                If bCancel Then
                    '           user cancel adding contact with no number
                    oContact = Nothing
                End If
            End If
        End Function

        Private Function IsContactFolder(oFolder As Outlook.MAPIFolder) As Boolean
            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("oFolder.DefaultMessageClass= " & oFolder.DefaultMessageClass, _
                "CIOutlookOM.CCIBackend.IsContactFolder")
            End If
            IsContactFolder = InStr(UCase(oFolder.DefaultMessageClass), "IPM.CONTACT") > 0
        End Function

        Private Function FolderContainsContactFolder(ByVal oOutlookFolder As Outlook.Folder) As Boolean
            'returns True if the specified folder is a contact folder,
            'or if it contains a contact folder
            Dim oSubFolder As Outlook.Folder
            Dim bRet As Boolean
            Dim oContactItem As Outlook.OlItemType

            FolderContainsContactFolder = Nothing

            Try
                oContactItem = oOutlookFolder.DefaultItemType
            Catch
            End Try

            If oContactItem = Outlook.OlItemType.olContactItem Then
                FolderContainsContactFolder = True
                If GlobalMethods.g_oError.DebugMode = True Then
                    GlobalMethods.g_oError.SendToDebug(oOutlookFolder.Name & " contains contacts", _
                        "CIOutlookOM.CCIBackend.FolderContainsContactFolder")
                End If
            Else
                If GlobalMethods.g_oError.DebugMode = True Then
                    GlobalMethods.g_oError.SendToDebug(oOutlookFolder.Name & " contains no contacts", _
                        "CIOutlookOM.CCIBackend.FolderContainsContactFolder")
                End If

                'there are no contacts in this folder -
                Dim lNumSubFolders As Long

                Try
                    lNumSubFolders = oOutlookFolder.Folders.Count
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode = True Then
                    GlobalMethods.g_oError.SendToDebug("lNumSubFolders=" & lNumSubFolders, _
                        "CIOutlookOM.CCIBackend.FolderContainsContactFolder")
                End If

                If lNumSubFolders > 0 Then
                    'there are subdirectories - check them
                    For Each oSubFolder In oOutlookFolder.Folders
                        'recurse
                        bRet = FolderContainsContactFolder(oSubFolder)
                        If bRet = True Then
                            FolderContainsContactFolder = True
                            Exit Function
                        End If
                    Next
                Else
                    'no subdirectories - doesn't contain a contact folder
                    FolderContainsContactFolder = False
                End If
            End If
        End Function

        Private Sub ConnectToOutlook()
            'starts and logs on to Outlook
            Dim oItem As Outlook.MailItem

            Try
                If m_oOutApp Is Nothing Then
                    'attempt to get an existing outlook session
                    Try
                        m_oOutApp = GetObject(, "Outlook.Application")
                    Catch
                    End Try

                    If m_oOutApp Is Nothing Then
                        'no existing outlook session open, create new
                        m_oOutApp = CreateObject("Outlook.Application")
                    End If

                    If m_oOutApp Is Nothing Then
                        m_bIsConnected = False
                        Exit Sub
                    Else
                        'GLOG : 8797 : ceh
                        'turn on Security Manager for the session
                        Try
                            SecurityManager = New AddinExpress.Outlook.SecurityManager()
                        Catch
                        End Try

                        If Not (SecurityManager Is Nothing) Then
                            'GLOG : 15762 : ceh
                            Try
                                'get inspector to pass to ConnectTo method
                                oItem = m_oOutApp.CreateItem(Outlook.OlItemType.olMailItem)
                                m_oInspector = oItem.GetInspector()
                            Catch ex As Runtime.InteropServices.COMException
                                m_bIsConnected = False
                                Throw New Runtime.InteropServices.COMException("Could not start an Outlook Session.  Verify that Outlook is setup correctly.")
                            Catch
                            End Try

                            Try
                                'turn warnings off
                                SecurityManager.ConnectTo(m_oInspector)
                                SecurityManager.DisableOOMWarnings = True

                                If GlobalMethods.g_oError.DebugMode Then
                                    GlobalMethods.g_oError.SendToDebug("SecurityManager turned ON", _
                                    "CIOutlookOM.CCIBackend.ConnectToOutlook")
                                End If

                            Catch
                                'OSM may not be registered, do nothing
                            End Try
                        Else
                            If GlobalMethods.g_oError.DebugMode Then
                                GlobalMethods.g_oError.SendToDebug("SecurityManager Is Nothing", _
                                "CIOutlookOM.CCIBackend.ConnectToOutlook")
                            End If
                        End If
                    End If

                End If

                'set an outlook global object
                GlobalMethods.g_oOutlook = m_oOutApp

                If (m_oOutApp.Session Is Nothing) Then
                    'raise error if no session
                    m_bIsConnected = False
                    Throw New Exception("Could not start an Outlook Session.  " & Err.Description)
                End If
                m_bIsConnected = True
            Catch ex As Exception
                m_bIsConnected = False
                'Throw New Exception("Could not start an Outlook Session.  " & Err.Description)
                [Error].Show(ex)
            End Try
        End Sub

        Private Function HasAddresses(oItem As Outlook.ContactItem) As Boolean
            'returns true if there are address
            'fields that are not empty
            Dim xAddress As String
            On Error Resume Next
            xAddress = ""
            With oItem
                xAddress = xAddress & .ItemProperties("BusinessAddressStreet").Value
                xAddress = xAddress & .ItemProperties("BusinessAddressCity").Value
                xAddress = xAddress & .ItemProperties("BusinessAddressState").Value
                xAddress = xAddress & .ItemProperties("BusinessAddressPostalCode").Value
                xAddress = xAddress & .ItemProperties("BusinessAddressCountry").Value

                If xAddress <> "" Then
                    HasAddresses = True
                    Err.Clear()
                    Exit Function
                End If

                xAddress = xAddress & .ItemProperties("HomeAddressStreet").Value
                xAddress = xAddress & .ItemProperties("HomeAddressCity").Value
                xAddress = xAddress & .ItemProperties("HomeAddressState").Value
                xAddress = xAddress & .ItemProperties("HomeAddressPostalCode").Value
                xAddress = xAddress & .ItemProperties("HomeAddressCountry").Value

                If xAddress <> "" Then
                    HasAddresses = True
                    Err.Clear()
                    Exit Function
                End If

                xAddress = xAddress & .ItemProperties("OtherAddressStreet").Value
                xAddress = xAddress & .ItemProperties("OtherAddressCity").Value
                xAddress = xAddress & .ItemProperties("OtherAddressState").Value
                xAddress = xAddress & .ItemProperties("OtherAddressPostalCode").Value
                xAddress = xAddress & .ItemProperties("OtherAddressCountry").Value
            End With
            HasAddresses = (xAddress <> "")
            Err.Clear()

        End Function

        Private Function GetContacts(oListing As CListing, _
            oAddress As CAddress, _
            Optional ByVal vAddressType As Object = Nothing, _
            Optional ByVal iIncludeData As ciRetrieveData = 1&, _
            Optional ByVal iAlerts As ciAlerts = 1&) As CContacts

            Dim oContacts As CContacts
            Dim oContact As CContact
            Dim oDL As Outlook.DistListItem
            Dim oMems As Outlook.Recipients
            Dim oRec As Outlook.Recipient
            Dim oae As Outlook.AddressEntry

            'create a contacts collection
            oContacts = New CContacts

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                ";iIncludeData=" & iIncludeData & ";iAlerts=" & iAlerts, _
                "CIOutlookOM.CCIBackend.ICIBackend_GetContacts")
            End If

            If oListing.ListingType = ciListingType.ciListingType_Group Then
                'cehrecode
                ''        If m_oOutApp is nothing Then
                ''            ConnectToOutlook
                ''        End If
                ''
                ''        Set oDL = m_oOutApp.Session.GetItemFromID(oListing.ID, oListing.StoreID)
                ''        Set oRec = oDL.GetMember(11)
                ''        Set oae = oRec.AddressEntry
                ''
                ''       get listing
                ''        Dim oMsg As MAPI.Message
                ''        Set oMsg = g_oMAPISession.GetMessage(oae.ID, oListing.StoreID)

            Else
                'get the contact associated with the listing
                oContact = GetContact(oListing, oAddress, iIncludeData, iAlerts)

                If oContact Is Nothing Then
                    '           don't add contact
                    GetContacts = Nothing
                    Exit Function
                Else
                    '           add the contact to the collection
                    oContacts.Add(oContact)
                    oContact = Nothing
                End If
            End If

            GetContacts = oContacts
        End Function

        Private Function GetPhoneNumbers(oListing As CListing, _
                                         Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'returns all phone numbers as a collection of contact numbers
            Dim oNums As CContactNumbers
            Dim oItem As Outlook.ContactItem
            Dim xTempNum As String
            Dim vPhones(,) As Object
            Dim i As Integer

            oNums = New CContactNumbers

            'get mapi message from the ci listing
            With oListing
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("BEFORE GET MESSAGE - oListing.ID=" & oListing.ID & _
                    "; oListing.StoreID= " & oListing.StoreID, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetPhoneNumbers")
                End If

                oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("AFTER GET MESSAGE - oListing.UNID=" & oListing.UNID & _
                    "; oItem Is Nothing " & CStr(oItem Is Nothing), _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetPhoneNumbers")
                End If
            End With

            If oItem Is Nothing Then
                'could not get the requested mapi message
                Throw New Exception("Could not find contact '" & oListing.DisplayName & "'.")
            End If

            '   find address used as mailing address
            If IsNothing(vAddressType) Then
                vAddressType = 0
            End If

            If vAddressType = ciMAPIAddresses.ciMAPIAddress_Mailing Then
                Dim iMailingAddress As Integer

                Try
                    iMailingAddress = oItem.SelectedMailingAddress
                Catch
                End Try

                If iMailingAddress = 0 Then 'no mailing address
                    vAddressType = 0
                ElseIf iMailingAddress = 1 Then 'home
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Home
                ElseIf iMailingAddress = 2 Then 'business
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Business
                Else    'other
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Other
                End If
            End If

            ' get phones for this address
            Select Case vAddressType
                Case ciMAPIAddresses.ciMAPIAddress_Business
                    ReDim vPhones(4, 1)
                    vPhones(0, 0) = "Primary"
                    vPhones(0, 1) = "PrimaryTelephoneNumber"
                    vPhones(1, 0) = "Business"
                    vPhones(1, 1) = "BusinessTelephoneNumber"
                    vPhones(2, 0) = "Business 2"
                    vPhones(2, 1) = "Business2TelephoneNumber"
                    vPhones(3, 0) = "Company Main"
                    vPhones(3, 1) = "CompanyMainTelephoneNumber"
                    vPhones(4, 0) = "Callback"
                    vPhones(4, 1) = "CallbackTelephoneNumber"
                Case ciMAPIAddresses.ciMAPIAddress_Home
                    ReDim vPhones(1, 1)
                    vPhones(0, 0) = "Home"
                    vPhones(0, 1) = "HomeTelephoneNumber"
                    vPhones(1, 0) = "Home2"
                    vPhones(1, 1) = "Home2TelephoneNumber"
                Case ciMAPIAddresses.ciMAPIAddress_Other
                    ReDim vPhones(0, 1)
                    vPhones(0, 0) = "Other"
                    vPhones(0, 1) = "HomeTelephoneNumber"
                Case Else
                    ReDim vPhones(7, 1)
                    vPhones(0, 0) = "Primary"
                    vPhones(0, 1) = "PrimaryTelephoneNumber"
                    vPhones(1, 0) = "Business"
                    vPhones(1, 1) = "BusinessTelephoneNumber"
                    vPhones(2, 0) = "Business 2"
                    vPhones(2, 1) = "Business2TelephoneNumber"
                    vPhones(3, 0) = "Company Main"
                    vPhones(3, 1) = "CompanyMainTelephoneNumbe"
                    vPhones(4, 0) = "Callback"
                    vPhones(4, 1) = "CallbackTelephoneNumber"
                    vPhones(5, 0) = "Home"
                    vPhones(5, 1) = "HomeTelephoneNumber"
                    vPhones(6, 0) = "Home2"
                    vPhones(6, 1) = "Home2TelephoneNumber"
                    vPhones(7, 0) = "Other"
                    vPhones(7, 1) = "OtherTelephoneNumber"
            End Select

            With oItem
                '       retrieve each phone - if there is a value for the phone field
                '       add to the collection of contact numbers
                For i = LBound(vPhones, 1) To UBound(vPhones, 1)
                    xTempNum = ""

                    Try
                        xTempNum = .ItemProperties(vPhones(i, 1)).Value
                    Catch
                    End Try
                    If xTempNum <> "" Then
                        oNums.Add(ciContactNumberTypes.ciContactNumberType_Phone & "|" & vPhones(i, 0), _
                            vPhones(i, 0), xTempNum, "", ciContactNumberTypes.ciContactNumberType_Phone, "")
                    End If
                Next i
            End With

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("vAddressType=" & vAddressType & _
                "; oNums.count=" & oNums.Count, _
                "CIOutlookOM.CCIBackend.ICIBackend_GetPhoneNumbers")
            End If

            'return collection of phone numbers
            GetPhoneNumbers = oNums
        End Function

        Private Function GetFaxNumbers(oListing As CListing, _
                                       Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            'returns all phone numbers as a collection of contact numbers
            Dim oNums As CContactNumbers
            Dim oItem As Outlook.ContactItem
            Dim xTempNum As String
            Dim vFaxes(,) As Object
            Dim i As Integer

            oNums = New CContactNumbers

            'get mapi message from the ci listing
            With oListing
                oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                    "; oMsg Is Nothing= " & CStr(oItem Is Nothing), _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
                End If
            End With

            If oItem Is Nothing Then
                'could not get the requested mapi message
                Throw New Exception("Could not find contact '" & oListing.DisplayName & "'.")
            End If

            '   find address used as mailing address
            If IsNothing(vAddressType) Then
                vAddressType = 0
            End If

            If vAddressType = ciMAPIAddresses.ciMAPIAddress_Mailing Then
                Dim iMailingAddress As Integer


                Try
                    iMailingAddress = oItem.SelectedMailingAddress
                Catch
                End Try

                If iMailingAddress = 0 Then 'no mailing address
                    vAddressType = 0
                ElseIf iMailingAddress = 1 Then 'home
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Home
                ElseIf iMailingAddress = 2 Then 'business
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Business
                Else    'other
                    vAddressType = ciMAPIAddresses.ciMAPIAddress_Other
                End If
            End If

            'get faxes for this address
            Select Case vAddressType
                Case ciMAPIAddresses.ciMAPIAddress_Business
                    ReDim vFaxes(0, 1)
                    vFaxes(0, 0) = "Business"
                    vFaxes(0, 1) = "BusinessFaxNumber"
                Case ciMAPIAddresses.ciMAPIAddress_Home
                    ReDim vFaxes(0, 1)
                    vFaxes(0, 0) = "Home"
                    vFaxes(0, 1) = "HomeFaxNumber"
                Case ciMAPIAddresses.ciMAPIAddress_Other
                    ReDim vFaxes(0, 1)
                    vFaxes(0, 0) = "Other"
                    vFaxes(0, 1) = "OtherFaxNumber"
                Case Else
                    ReDim vFaxes(2, 1)
                    vFaxes(0, 0) = "Business"
                    vFaxes(0, 1) = "BusinessFaxNumber"
                    vFaxes(1, 0) = "Home"
                    vFaxes(1, 1) = "HomeFaxNumber"
                    vFaxes(2, 0) = "Other"
                    vFaxes(2, 1) = "OtherFaxNumber"
            End Select

            If GlobalMethods.g_oError.DebugMode Then
                GlobalMethods.g_oError.SendToDebug("vAddressType=" & vAddressType & _
                "; UBound(vFaxes, 1)= " & UBound(vFaxes, 1), _
                "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
            End If

            With oItem
                'retrieve each phone - if there is a value for the phone field
                'add to the collection of contact numbers
                For i = LBound(vFaxes, 1) To UBound(vFaxes, 1)
                    xTempNum = ""
                    Try
                        xTempNum = .ItemProperties(vFaxes(i, 1)).Value
                    Catch
                    End Try
                    If xTempNum <> "" Then
                        oNums.Add(ciContactNumberTypes.ciContactNumberType_Fax & "|" & vFaxes(i, 0), _
                            vFaxes(i, 0), xTempNum, "", ciContactNumberTypes.ciContactNumberType_Fax, "")
                    End If
                Next i
            End With

            'return collection of phone numbers
            GetFaxNumbers = oNums
        End Function

        Private Function GetAddresses(Optional oListing As CListing = Nothing) As CAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Dim oAddress As CAddress
            Dim oAddresses As CAddresses

            oAddresses = New CAddresses

            '4 types of addresses - Mailing, Business, Home, Other
            oAddress = New CAddress
            With oAddress
                .Name = "Mailing"
                If Not oListing Is Nothing Then
                    .UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Mailing
                Else
                    .UNID = ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Mailing
                End If
            End With

            oAddresses.Add(oAddress)

            oAddress = New CAddress
            With oAddress
                .Name = "Business"
                If Not oListing Is Nothing Then
                    .UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Business
                Else
                    .UNID = ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Business
                End If
            End With

            oAddresses.Add(oAddress)

            oAddress = New CAddress
            With oAddress
                .Name = "Home"
                If Not oListing Is Nothing Then
                    .UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Home
                Else
                    .UNID = ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Home
                End If
            End With

            oAddresses.Add(oAddress)

            oAddress = New CAddress
            With oAddress
                .Name = "Other"
                If Not oListing Is Nothing Then
                    .UNID = oListing.UNID & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Other
                Else
                    .UNID = ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & GlobalMethods.g_oConstants.UNIDSep & ciMAPIAddresses.ciMAPIAddress_Other
                End If
            End With

            oAddresses.Add(oAddress)
            GetAddresses = oAddresses
        End Function

        Private Function GetEMailNumbers(oListing As CListing, Optional ByVal vAddressType As Object = Nothing) As CContactNumbers
            Dim oNums As CContactNumbers
            Dim oItem As Outlook.ContactItem
            Dim xNum As String

            oNums = New CContactNumbers

            'get mapi message from the ci listing
            With oListing
                oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)
            End With

            With oItem
                'get email 1 - first try in IA field -
                'IA stores their synched email addresses in this field

                Try
                    xNum = .ItemProperties("IAREmail1").Value
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".Item(IAREmail1)=" & xNum, _
                    "CIOutlookOM.CCIBackend.GetEMailNumbers")
                End If

                If xNum = "" Then
                    Try
                        xNum = .ItemProperties("Email1Address").Value
                    Catch
                    End Try

                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".itemproperties(Email1Address)=" & xNum, _
                    "CIOutlookOM.CCIBackend.GetEMailNumbers")
                End If

                'test for Active Directory path and get original email display name if necessary
                If UCase(xNum) Like "*=RECIPIENTS/*" Then
                    Try
                        xNum = xGetSMTPAddress(xNum)
                    Catch
                    End Try
                End If

                If xNum <> "" Then
                    'email 1 exists - add to collection
                    oNums.Add(ciContactNumberTypes.ciContactNumberType_EMail & "|" & "EMail 1", _
                        "EMail 1", xNum, "", ciContactNumberTypes.ciContactNumberType_EMail, "")
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("EMail 1=" & xNum, _
                    "CIOutlookOM.CCIBackend.GetEMailNumbers")
                End If

                xNum = ""

                'get email 2
                Try
                    xNum = .Item("IAREmail2")
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".Item(IAREmail2)=" & xNum, _
                    "CIOutlookOM.CCIBackend.GetEMailNumbers")
                End If

                If xNum = "" Then
                    Try
                        xNum = .ItemProperties("Email2Address").Value
                    Catch
                    End Try
                End If

                'test for Active Directory path and get original email display name if necessary
                If UCase(xNum) Like "*=RECIPIENTS/*" Then
                    Try
                        xNum = xGetSMTPAddress(xNum)
                    Catch
                    End Try
                End If

                If xNum <> "" Then
                    'email 2 exists - add to collection
                    oNums.Add(ciContactNumberTypes.ciContactNumberType_EMail & "|" & "EMail 2", _
                        "EMail 2", xNum, "", ciContactNumberTypes.ciContactNumberType_EMail, "")
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("EMail 2=" & xNum, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetEMailNumbers")
                End If

                xNum = ""

                'get email 3
                Try
                    xNum = .Item("IAREmail3")
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug(".Item(IAREmail3)=" & xNum, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetEMailNumbers")
                End If

                If xNum = "" Then
                    Try
                        xNum = .ItemProperties("Email3Address").Value
                    Catch
                    End Try
                End If

                'test for Active Directory path and get original email display name if necessary
                If UCase(xNum) Like "*=RECIPIENTS/*" Then
                    Try
                        xNum = xGetSMTPAddress(xNum)
                    Catch
                    End Try
                End If

                If xNum <> "" Then
                    'email 3 exists - add to collection
                    oNums.Add(ciContactNumberTypes.ciContactNumberType_EMail & "|" & "EMail 3", _
                        "EMail 3", xNum, "", ciContactNumberTypes.ciContactNumberType_EMail, "")
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("EMail 3=" & xNum, _
                    "CIOutlookOM.CCIBackend.GetEMailNumbers")
                End If
            End With

            'return
            GetEMailNumbers = oNums
        End Function

        Public Function UsesMAPIFavoriteMailboxes() As Boolean
            'returns TRUE if CI is using the concept of Favorite Outlook Mailboxes
            Dim xGAL As String

            Try
                xGAL = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "AddressList")
                UsesMAPIFavoriteMailboxes = (xGAL <> "")
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

#End Region
#Region "******************ICIBackend***********************"
        Private Function ICIBackend_CustomMenuItem1() As String Implements ICIBackend.CustomMenuItem1
            'returns TRUE if CI is using the concept of Favorite Outlook Mailboxes

            Try
                If UsesMAPIFavoriteMailboxes() Then
                    ICIBackend_CustomMenuItem1 = "Manage &Favorites..."
                Else
                    ICIBackend_CustomMenuItem1 = Nothing
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_CustomMenuItem2() As String Implements ICIBackend.CustomMenuItem2
            ICIBackend_CustomMenuItem2 = ""
        End Function

        Private Function ICIBackend_CustomMenuItem3() As String Implements ICIBackend.CustomMenuItem3
            ICIBackend_CustomMenuItem3 = ""
        End Function

        Private Function ICIBackend_CustomMenuItem4() As String Implements ICIBackend.CustomMenuItem4
            ICIBackend_CustomMenuItem4 = ""
        End Function

        Private Function ICIBackend_CustomMenuItem5() As String Implements ICIBackend.CustomMenuItem5
            ICIBackend_CustomMenuItem5 = ""
        End Function

        Private Sub ICIBackend_CustomProcedure1() Implements ICIBackend.CustomProcedure1
            'Const mpThisFunction As String = "CIOutlookOM.CCIBackend.ICIBackend_CustomProcedure1"
            'Dim oFavs As CCIFavorites
            'Dim l As Long

            'On Error GoTo ProcError
            'g_oCIFav.RemoveFavorites()

            ''prompt user to select favorites
            'g_oCIFav.ManageFavorites()

            'g_oCIFav.AttachFavorites()

            ''    'refresh the outlook session
            ''    With g_oSession
            ''        .Logoff
            ''        .Logon , , True, False
            ''    End With
            '            Exit Sub
            'ProcError:
            '            GlobalMethods.g_oError.RaiseError(mpThisFunction)
            '            Exit Sub
        End Sub

        Private Sub ICIBackend_CustomProcedure2() Implements ICIBackend.CustomProcedure2
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure3() Implements ICIBackend.CustomProcedure3
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure4() Implements ICIBackend.CustomProcedure4
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Sub ICIBackend_CustomProcedure5() Implements ICIBackend.CustomProcedure5
            Throw New System.NotImplementedException("Procedure not implemented")
        End Sub

        Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders Implements ICIBackend.GetFolders
            'retrieves root folders in store oStore -
            'returns them as a CFolders collection
            Dim oCIFolder As CFolder
            Dim oCIFolders As CFolders
            Dim oOutlookStore As Outlook.Store
            Dim oUNID As CUNID
            Dim bShowFolder As Boolean
            Dim oOutlookFolder As Outlook.Folder = Nothing
            Dim oOutlookFolders As Outlook.Folders
            Dim oRoot As Outlook.Folder = Nothing
            Dim iFoldercount As Integer
            Dim i As Integer

            Try
                ICIBackend_GetFolders = Nothing

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("START   oStore.ID=" & oStore.ID & "; oStore.Name=" & oStore.Name, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                End If

                oUNID = New CUNID

                If m_oOutApp Is Nothing Then
                    ConnectToOutlook()
                End If

                oOutlookStore = m_oOutApp.Session.GetStoreFromID(oStore.ID)

                oCIFolders = New CFolders

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oOutlookStore.DisplayName=" & oOutlookStore.DisplayName, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                    GlobalMethods.g_oError.SendToDebug("oOutlookStore.StoreID=" & oOutlookStore.StoreID, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                End If

                Try
                    oRoot = oOutlookStore.GetRootFolder
                Catch
                End Try

                If oRoot Is Nothing Then
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oRoot is nothing", _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                    End If

                    'alert
                    MsgBox("You might not have adequate rights to open this mailbox." & vbCr & vbCr & _
                    "Please contact your administrator.", _
                        vbExclamation, GlobalMethods.g_oSession.AppTitle)
                    Cursor.Current = Cursors.Default
                    Exit Function
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("After Set oRoot", _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                End If

                oOutlookFolders = oRoot.Folders

                Try
                    iFoldercount = oOutlookFolders.Count
                Catch
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("iFoldercount=" & iFoldercount, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                End If

                'Check if there are any folders below oFolder
                If iFoldercount Then
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("before loop", _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                    End If

                    For Each oOutlookFolder In oOutlookFolders
                        'EnumerateFolders Folder
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oOutlookFolder.Name=" & oOutlookFolder.Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                        End If

                        If ShowContactFoldersOnly() Then
                            'show folder only if it is/contains a contact folder
                            bShowFolder = FolderContainsContactFolder(oOutlookFolder)
                        Else
                            bShowFolder = True
                        End If

                        If bShowFolder Then
                            For i = 0 To UBound(m_xExclusionArray)
                                If UCase(Trim(m_xExclusionArray(i))) = UCase(oOutlookFolder.Name) Then
                                    bShowFolder = False
                                    Exit For
                                End If
                            Next i
                        End If

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("bShowFolder= " & CStr(bShowFolder), _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                        End If

                        If bShowFolder Then
                            oCIFolder = oUNID.GetFolder(oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & _
                                oOutlookFolder.EntryID, oOutlookFolder.Name)
                            oCIFolders.Add(oCIFolder)
                        End If
                    Next oOutlookFolder
                Else    'try getting default contact folder
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oOutlookFolders.Count=0", _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetFolders")
                    End If

                    Try
                        oOutlookFolder = oOutlookStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts)
                    Catch
                    End Try

                    If Not (oOutlookFolder Is Nothing) Then
                        oCIFolder = oUNID.GetFolder(oStore.UNID & GlobalMethods.g_oConstants.UNIDSep & _
                                                        oOutlookFolder.EntryID, oOutlookFolder.Name)
                        oCIFolders.Add(oCIFolder)
                    Else
                        MsgBox("Unable to display folders.  The Contacts folder could not be found.", vbExclamation, GlobalMethods.g_oSession.AppTitle)
                    End If
                End If
                ICIBackend_GetFolders = oCIFolders
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStores() As CStores Implements ICIBackend.GetStores
            'retrieves infostores in current session -
            'returns them as a CStores collection
            Dim oStore As CStore
            Dim oStores As CStores
            Dim oUNID As CUNID
            Dim xExclusionList As String
            Dim oOutlookStore As Outlook.Store
            Dim xName As String
            Dim bExclude As Boolean
            Dim vExclusionList As Object
            Dim k As Integer
            Dim xExcluded As String
            Dim i As Integer
            Dim j As Integer
            Dim bExisting As Boolean


            Try
                ICIBackend_GetStores = Nothing
                vExclusionList = Nothing

                If m_oOutApp Is Nothing Then
                    ConnectToOutlook()
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_IsConnected()= " & ICIBackend_IsConnected(), _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                End If

                If Not ICIBackend_IsConnected() Then
                    Exit Function
                End If

                oUNID = New CUNID

                oStores = New CStores

                'get exclusion list
                xExclusionList = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "StoreExclusions")

                If xExclusionList <> "" Then
                    vExclusionList = Split(xExclusionList, ",", , vbTextCompare)
                End If

                For i = 1 To m_oOutApp.Session.Stores.Count
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("Top of For loop - i = " & i, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    Try
                        oOutlookStore = m_oOutApp.Session.Stores.Item(i)
                    Catch
                    End Try

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("After Set oOutlookStore - " & i, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    If Err.Number <> 0 Or (oOutlookStore Is Nothing) Then
                        Err.Clear()
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("Before GoTo NextStore, Err.Number= " & Err.Number & " - " & Err.Description, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If
                        GoTo NextStore
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oOutlookStore  .ID= " & oOutlookStore.StoreID & "; .DisplayName= " & oOutlookStore.DisplayName, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    xName = oOutlookStore.DisplayName

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("after xName = " & xName, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    bExclude = False
                    bExisting = False

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("xExclusionList = " & xExclusionList, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If


                    If xExclusionList <> "" Then
                        'cycle through exclusion list, seeing if there's a match
                        For k = 0 To UBound(vExclusionList)
                            xExcluded = Trim(vExclusionList(k))
                            If UCase(xName) Like "*" & UCase(xExcluded) & "*" Then
                                bExclude = True
                                Exit For
                            End If
                        Next k
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("bExclude = " & bExclude, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("before adding store", _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    For j = 1 To oStores.Count
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("before existing store loop. oStores.Item(j).ID=" & oStores.Item(j).ID & ", oOutlookStore.StoreID=" & oOutlookStore.StoreID, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If
                        If oStores.Item(j).ID = oOutlookStore.StoreID Then
                            bExisting = True
                            Exit For
                        End If
                    Next j

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("bExisting=" & bExisting, _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If

                    'add store if it is not in the exclusion list
                    If (Not bExclude) And (Not bExisting) Then
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("before Set oStore: " & xName, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If
                        oStore = oUNID.GetStore(ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & oOutlookStore.StoreID, xName)
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("before oStores.Add: " & xName, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If
                        oStores.Add(oStore)

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("xExclusionList = " & xExclusionList, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("after oStores.Add: " & xName, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                        End If
                    End If
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("before Next oOutlookStore", _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetStores")
                    End If
NextStore:
                Next i
                ICIBackend_GetStores = oStores
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_SupportsMultipleStores() As Boolean Implements ICIBackend.SupportsMultipleStores
            Get
                ICIBackend_SupportsMultipleStores = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNativeSearch() As Boolean Implements ICIBackend.SupportsNativeSearch
            Get
                ICIBackend_SupportsNativeSearch = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsNestedFolders() As Boolean Implements ICIBackend.SupportsNestedFolders
            Get
                ICIBackend_SupportsNestedFolders = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Name() As String Implements ICIBackend.Name
            Get
                Dim xName As String
                Dim xDesc As String
                Dim oIni As CIni

                ICIBackend_Name = ""

                Try
                    oIni = New CIni

                    'get from ini
                    xName = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Name", GlobalMethods.g_oIni.CIIni)

                    'raise error if ini value is missing
                    If Len(xName) = 0 Then
                        xDesc = "Invalid Backend" & GlobalMethods.g_iID & "\Name key in ci.ini."
                        Throw New Exception(xDesc)
                        ICIBackend_Name = Nothing
                        Exit Property
                    End If

                    ICIBackend_Name = xName

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Name=" & ICIBackend_Name, _
                        "CIOutlookOM.CCIBackend.ICIBackend_Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat Implements ICIBackend.NumberPromptFormat
            ICIBackend_NumberPromptFormat = Me
        End Function

        Private ReadOnly Property ICIBackend_SearchOperators() As ciSearchOperators Implements ICIBackend.SearchOperators
            Get
                ICIBackend_SearchOperators = ciSearchOperators.ciSearchOperator_BeginsWith + _
                    ciSearchOperators.ciSearchOperator_Contains + ciSearchOperators.ciSearchOperator_Equals
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsContactAdd() As Boolean Implements ICIBackend.SupportsContactAdd
            Get
                'backend allows adding contacts
                Dim xAllow As String

                ICIBackend_SupportsContactAdd = False
                Try
                    xAllow = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "AllowNewContact")

                    If UCase$(xAllow) = "FALSE" Then
                        ICIBackend_SupportsContactAdd = False
                    Else
                        ICIBackend_SupportsContactAdd = True
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsContactEdit() As Boolean Implements ICIBackend.SupportsContactEdit
            Get
                ICIBackend_SupportsContactEdit = True
            End Get
        End Property

        Private ReadOnly Property ICIBackend_SupportsFolders() As Boolean Implements ICIBackend.SupportsFolders
            Get
                ICIBackend_SupportsFolders = True
            End Get
        End Property

        'used to refer back to the ci.ini in the functions of this class
        Private Property ICIBackend_ID() As Integer Implements ICIBackend.ID
            Set(value As Integer)
                GlobalMethods.g_iID = value
            End Set
            Get
                ICIBackend_ID = GlobalMethods.g_iID
            End Get
        End Property

        Private Function ICIBackend_GetAddresses(oListing As CListing) As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing
            Try
                ICIBackend_GetAddresses = GetAddresses(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetAddresses() As CAddresses Implements ICIBackend.GetAddresses
            'returns a CAddresses collection that represents
            'the available addresses for the supplied listing

            Try
                ICIBackend_GetAddresses = GetAddresses()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders Implements ICIBackend.GetSubFolders
            'retrieves root folders in store oStore -
            'returns them as a CFolders collection
            Dim oOutlookFolder As Outlook.Folder
            Dim oRootFolder As Outlook.Folder
            Dim oCIFolder As CFolder
            Dim oCIFolders As CFolders
            Dim oUNID As CUNID
            Dim bShowFolder As Boolean
            Dim i As Integer

            Try
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("START", _
                    "CCIBackend.ICIBackend_GetSubFolders")
                End If

                oUNID = New CUNID

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oFolder.ID=" & oFolder.ID, _
                    "CCIBackend.ICIBackend_GetSubFolders")
                    GlobalMethods.g_oError.SendToDebug("oFolder.StoreID=" & oFolder.StoreID, _
                    "CCIBackend.ICIBackend_GetSubFolders")
                    GlobalMethods.g_oError.SendToDebug("oFolder.Name=" & oFolder.Name, _
                    "CCIBackend.ICIBackend_GetSubFolders")
                End If

                'get folder
                oRootFolder = m_oOutApp.Session.GetFolderFromID(oFolder.ID, oFolder.StoreID)

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oRootFolder.name=" & oRootFolder.Name, _
                    "CCIBackend.ICIBackend_GetSubFolders")
                End If

                oCIFolders = New CFolders

                'cycle through all subfolders
                For Each oOutlookFolder In oRootFolder.Folders
                    If ShowContactFoldersOnly() Then
                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("Before FolderContainsContactfolder oOutlookFolder.Name=" & oOutlookFolder.Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                        End If
                        'show folder only if it is/contains a contact folder
                        bShowFolder = FolderContainsContactFolder(oOutlookFolder)
                    Else
                        bShowFolder = True
                    End If

                    'implement folder exclusions
                    If bShowFolder Then
                        For i = 0 To UBound(m_xExclusionArray)
                            If UCase(Trim(m_xExclusionArray(i))) = UCase(oOutlookFolder.Name) Then
                                bShowFolder = False
                                Exit For
                            End If
                        Next i
                    End If

                    If bShowFolder Then
                        'add the folder to the collection of folders

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("After FolderContainsContactfolder oOutlookFolder.Name=" & oOutlookFolder.Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                            GlobalMethods.g_oError.SendToDebug("oOutlookFolder.StoreID=" & oOutlookFolder.StoreID, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                        End If

                        oCIFolder = oUNID.GetFolder(ciOutlookInternalID & GlobalMethods.g_oConstants.UNIDSep & oFolder.StoreID & _
                                        GlobalMethods.g_oConstants.UNIDSep & oOutlookFolder.EntryID, oOutlookFolder.Name)

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("oCIFolder.Name=" & oCIFolder.Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                            GlobalMethods.g_oError.SendToDebug("oCIFolder.UNID=" & oCIFolder.UNID, _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                        End If

                        oCIFolders.Add(oCIFolder)

                        If GlobalMethods.g_oError.DebugMode Then
                            GlobalMethods.g_oError.SendToDebug("After oCIFolders.Add oCIFolder", _
                            "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                        End If
                    End If
                Next oOutlookFolder
                ICIBackend_GetSubFolders = oCIFolders
                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("End", _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetSubFolders")
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean Implements ICIBackend.HasAddresses
            Dim oItem As Outlook.ContactItem

            'get mapi message from the ci listing
            Try
                With oListing
                    oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                        "; oItem is nothing= " & CStr(oItem Is Nothing), _
                        "CIOutlookOM.CCIBackend.ICIBackend_HasAddresses")
                    End If
                End With


                If oItem Is Nothing Then
                    ICIBackend_HasAddresses = Nothing
                    Exit Function
                Else
                    ICIBackend_HasAddresses = HasAddresses(oItem)
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_HasAddresses=" & ICIBackend_HasAddresses, _
                    "CIOutlookOM.CCIBackend.ICIBackend_HasAddresses")
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_Exists() As Boolean Implements ICIBackend.Exists
            Get
                Dim bExists As Boolean
                Dim oApp As Object = Nothing

                bExists = True

                'attempt to get an existing outlook session

                Try
                    oApp = GetObject(, "Outlook.Application")
                Catch
                End Try

                If oApp Is Nothing Then
                    'no existing outlook session open, create new
                    oApp = CreateObject("Outlook.Application")
                End If

                If (oApp Is Nothing) Then
                    bExists = False
                Else
                    oApp = Nothing
                End If

                ICIBackend_Exists = bExists

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_Exists=" & bExists, _
                    "CIOutlookOM.CCIBackend.ICIBackend_Exists")
                End If
            End Get
        End Property

        Private Sub ICIBackend_Initialize(iID As Integer) Implements ICIBackend.Initialize

            Try
                GlobalMethods.g_iID = iID

                GetColumnInfoArray(m_vCols)

                'todo
                'If UsesMAPIFavoriteMailboxes() Then
                '    'If g_oCIFav Is Nothing Then
                '    '    g_oCIFav = New CIOutlookOM.CCIFavorites
                '    '    g_oCIFav.Refresh()
                '    'End If
                '    If GlobalMethods.g_oOutlook Is Nothing Then
                '        ConnectToOutlook()
                '    End If
                '    'If m_bIsConnected Then
                '    '    g_oCIFav.AttachFavorites()
                '    'End If
                'End If

                'get exclusion list
                m_xExclusionList = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "FolderExclusions")

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("FolderExclusion list =" & m_xExclusionList, _
                    "CIOutlookOM.CCIBackend.ICIBackend_Initialize")
                End If

                m_xExclusionArray = Split(m_xExclusionList, ",")
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Function ICIBackend_SearchStore(oStore As CStore) As CListings Implements ICIBackend.SearchStore
            Dim oFilter As CFilter

            Try
                oFilter = GetFilter()

                If Not (oFilter Is Nothing) Then
                    ICIBackend_SearchStore = _
                        ICIBackend_GetStoreListings(oStore, oFilter)
                Else
                    ICIBackend_SearchStore = Nothing
                End If

                m_oEvents.RaiseAfterStoreSearched()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing, vAddressType As Object) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetEMailNumbers
            Try
                ICIBackend_GetEMailNumbers = GetEMailNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'returns all phone numbers as a collection of contact numbers
            Dim oNums As CContactNumbers
            Dim oItem As Outlook.ContactItem
            Dim xTempNum As String
            Dim vFaxes(,) As Object
            Dim i As Integer
            Dim vAddressType As Object


            Try
                oNums = New CContactNumbers

                'get mapi message from the ci listing
                With oListing
                    oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                        "; oMsg Is Nothing= " & CStr(oItem Is Nothing), _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
                    End If
                End With

                If oItem Is Nothing Then
                    'could not get the requested mapi message
                    Throw New Exception("Could not find contact '" & oListing.DisplayName & "'.")
                End If

                'find address used as mailing address
                vAddressType = 0

                If vAddressType = ciMAPIAddresses.ciMAPIAddress_Mailing Then
                    Dim iMailingAddress As Integer


                    Try
                        iMailingAddress = oItem.SelectedMailingAddress
                    Catch
                    End Try

                    If iMailingAddress = 0 Then 'no mailing address
                        vAddressType = 0
                    ElseIf iMailingAddress = 1 Then 'home
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Home
                    ElseIf iMailingAddress = 2 Then 'business
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Business
                    Else    'other
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Other
                    End If
                End If

                '   get faxes for this address
                Select Case vAddressType
                    Case ciMAPIAddresses.ciMAPIAddress_Business
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Business"
                        vFaxes(0, 1) = "BusinessFaxNumber"
                    Case ciMAPIAddresses.ciMAPIAddress_Home
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Home"
                        vFaxes(0, 1) = "HomeFaxNumber"
                    Case ciMAPIAddresses.ciMAPIAddress_Other
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Other"
                        vFaxes(0, 1) = "OtherFaxNumber"
                    Case Else
                        ReDim vFaxes(2, 1)
                        vFaxes(0, 0) = "Business"
                        vFaxes(0, 1) = "BusinessFaxNumber"
                        vFaxes(1, 0) = "Home"
                        vFaxes(1, 1) = "HomeFaxNumber"
                        vFaxes(2, 0) = "Other"
                        vFaxes(2, 1) = "OtherFaxNumber"
                End Select

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("vAddressType=" & vAddressType & _
                    "; UBound(vFaxes, 1)= " & UBound(vFaxes, 1), _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
                End If

                With oItem
                    '       retrieve each phone - if there is a value for the phone field
                    '       add to the collection of contact numbers
                    For i = LBound(vFaxes, 1) To UBound(vFaxes, 1)
                        xTempNum = ""
                        Try
                            xTempNum = .ItemProperties(vFaxes(i, 1)).Value
                        Catch
                        End Try
                        If xTempNum <> "" Then
                            oNums.Add(ciContactNumberTypes.ciContactNumberType_Fax & "|" & vFaxes(i, 0), _
                                vFaxes(i, 0), xTempNum, "", ciContactNumberTypes.ciContactNumberType_Fax, "")
                        End If
                    Next i
                End With

                '   return collection of phone numbers
                ICIBackend_GetFaxNumbers = oNums
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumbers(oListing As CListing, _
                                                  ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetFaxNumbers
            'returns all phone numbers as a collection of contact numbers
            Dim oNums As CContactNumbers
            Dim oItem As Outlook.ContactItem
            Dim xTempNum As String
            Dim vFaxes(,) As Object
            Dim i As Integer

            Try
                oNums = New CContactNumbers

                'get mapi message from the ci listing
                With oListing
                    oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                        "; oMsg Is Nothing= " & CStr(oItem Is Nothing), _
                        "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
                    End If
                End With

                If oItem Is Nothing Then
                    'could not get the requested mapi message
                    Throw New Exception("Could not find contact '" & oListing.DisplayName & "'.")
                End If

                'find address used as mailing address
                If IsNothing(vAddressType) Then
                    vAddressType = 0
                End If

                If vAddressType = ciMAPIAddresses.ciMAPIAddress_Mailing Then
                    Dim iMailingAddress As Integer


                    Try
                        iMailingAddress = oItem.SelectedMailingAddress
                    Catch
                    End Try

                    If iMailingAddress = 0 Then 'no mailing address
                        vAddressType = 0
                    ElseIf iMailingAddress = 1 Then 'home
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Home
                    ElseIf iMailingAddress = 2 Then 'business
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Business
                    Else    'other
                        vAddressType = ciMAPIAddresses.ciMAPIAddress_Other
                    End If
                End If

                '   get faxes for this address
                Select Case vAddressType
                    Case ciMAPIAddresses.ciMAPIAddress_Business
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Business"
                        vFaxes(0, 1) = "BusinessFaxNumber"
                    Case ciMAPIAddresses.ciMAPIAddress_Home
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Home"
                        vFaxes(0, 1) = "HomeFaxNumber"
                    Case ciMAPIAddresses.ciMAPIAddress_Other
                        ReDim vFaxes(0, 1)
                        vFaxes(0, 0) = "Other"
                        vFaxes(0, 1) = "OtherFaxNumber"
                    Case Else
                        ReDim vFaxes(2, 1)
                        vFaxes(0, 0) = "Business"
                        vFaxes(0, 1) = "BusinessFaxNumber"
                        vFaxes(1, 0) = "Home"
                        vFaxes(1, 1) = "HomeFaxNumber"
                        vFaxes(2, 0) = "Other"
                        vFaxes(2, 1) = "OtherFaxNumber"
                End Select

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("vAddressType=" & vAddressType & _
                    "; UBound(vFaxes, 1)= " & UBound(vFaxes, 1), _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumbers")
                End If

                With oItem
                    '       retrieve each phone - if there is a value for the phone field
                    '       add to the collection of contact numbers
                    For i = LBound(vFaxes, 1) To UBound(vFaxes, 1)
                        xTempNum = ""
                        Try
                            xTempNum = .ItemProperties(vFaxes(i, 1)).Value
                        Catch
                        End Try
                        If xTempNum <> "" Then
                            oNums.Add(ciContactNumberTypes.ciContactNumberType_Fax & "|" & vFaxes(i, 0), _
                                vFaxes(i, 0), xTempNum, "", ciContactNumberTypes.ciContactNumberType_Fax, "")
                        End If
                    Next i
                End With

                '   return collection of phone numbers
                ICIBackend_GetFaxNumbers = oNums
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, _
                                                    ByVal vAddressType As Object) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers

            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing, vAddressType)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumbers(oListing As CListing) As CContactNumbers Implements ICIBackend.GetPhoneNumbers
            'returns all phone numbers as a collection of contact numbers
            Try
                ICIBackend_GetPhoneNumbers = GetPhoneNumbers(oListing)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore, oFilter As CFilter) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore, oFilter)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetStoreListings(oStore As CStore) As CListings Implements ICIBackend.GetStoreListings
            Try
                ICIBackend_GetStoreListings = GetStoreListings(oStore)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_SearchNative() As CListings Implements ICIBackend.SearchNative
            Try
                ICIBackend_SearchNative = SearchWithOutlook()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Friend Property ICIBackend_DefaultSortColumn As ciListingCols Implements ICIBackend.DefaultSortColumn
            Get
                ICIBackend_DefaultSortColumn = Nothing
                Try
                    If m_iSortCol = vbNull Then
                        Try
                            m_iSortCol = GlobalMethods.g_oIni.GetIni("CIOutlookOM", "SortColumn", g_oIni.CIUserIni())
                        Catch
                        End Try
                        If m_iSortCol = vbNull Then
                            m_iSortCol = ciListingCols.ciListingCols_DisplayName
                        End If
                    End If

                    ICIBackend_DefaultSortColumn = m_iSortCol

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_DefaultSortColumn= " & m_iSortCol, _
                        "CIOutlookOM.CCIBackend.ICIBackend_DefaultSortColumn")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
            Set(value As ciListingCols)
                Try
                    'Dim iNew As Integer
                    m_iSortCol = value
                    GlobalMethods.g_oIni.SetIni("CIOutlookOM", "SortColumn", CStr(value), GlobalMethods.g_oIni.CIUserIni())
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("Set ci.ini.[CIOutlookOM]\SortColumn= " & value, _
                        "CIOutlookOM.CCIBackend.ICIBackend_DefaultSortColumn")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Set
        End Property

        Private Sub ICIBackend_AddContact() Implements ICIBackend.AddContact
            'displays the contact explorer for a new listing

            Try
                Cursor.Current = Cursors.WaitCursor

                If m_oOutApp Is Nothing Then
                    ConnectToOutlook()
                End If

                Dim hwnd As Long

                'get handle of contact dlg
                hwnd = GetForegroundWindow()

                'show active explorer

                Try
                    Err.Clear()
                    m_oOutApp.Session.Application _
                        .CreateItem(2).Display()
                Catch
                    'alert to error raised when trying to open the dialog
                    MsgBox("Could not open 'New Contact' dialog.  The following " & _
                        "message was returned by Outlook: " & vbCr & Err.Description, _
                        vbExclamation, GlobalMethods.g_oSession.AppTitle)
                    Cursor.Current = Cursors.Default
                    Exit Sub
                Finally
                    Cursor.Current = Cursors.Default
                End Try

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("Displayed Outlook Contact Inspector", _
                    "CIOutlookOM.CCIBackend.ICIBackend_AddContact")
                End If

                'cycle until we return to contact dlg
                'While GetForegroundWindow() <> hwnd
                '    DoEvents()
                'End While

            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private ReadOnly Property ICIBackend_Col1Name() As String Implements ICIBackend.Col1Name
            Get
                ICIBackend_Col1Name = ""
                Try
                    ICIBackend_Col1Name = m_vCols(0, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col1Name=" & ICIBackend_Col1Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_Col1Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col2Name() As String Implements ICIBackend.Col2Name
            Get
                ICIBackend_Col2Name = ""
                Try
                    ICIBackend_Col2Name = m_vCols(1, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col2Name=" & ICIBackend_Col2Name, _
                            "CIOutlookOM.CCIBackend.ICIBackend_Col2Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col3Name() As String Implements ICIBackend.Col3Name
            Get
                ICIBackend_Col3Name = ""
                Try
                    ICIBackend_Col3Name = m_vCols(2, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col3Name=" & ICIBackend_Col3Name, _
                        "CIOutlookOM.CCIBackend.ICIBackend_Col3Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_Col4Name() As String Implements ICIBackend.Col4Name
            Get
                ICIBackend_Col4Name = ""
                Try
                    ICIBackend_Col4Name = m_vCols(3, 0)
                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_Col4Name=" & ICIBackend_Col4Name, _
                        "CIOutlookOM.CCIBackend.ICIBackend_Col4Name")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICIBackend_DisplayName() As String Implements ICIBackend.DisplayName
            Get
                ICIBackend_DisplayName = ""
                Try
                    If m_xDisplayName = "" Then
                        'get from ini
                        m_xDisplayName = GlobalMethods.g_oIni.GetIni("Backend" & GlobalMethods.g_iID, "Name")

                        If m_xDisplayName = "" Then
                            'no value in ini - use default name
                            m_xDisplayName = ICIBackend_Name()
                        End If
                    End If
                    ICIBackend_DisplayName = m_xDisplayName

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_DisplayName=" & m_xDisplayName, _
                        "CIOutlookOM.CCIBackend.ICIBackend_DisplayName")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Sub ICIBackend_EditContact(oListing As CListing) Implements ICIBackend.EditContact
            'displays the contact explorer in
            'Outlook for the specified listing

            Try
                Cursor.Current = Cursors.WaitCursor

                If m_oOutApp Is Nothing Then
                    ConnectToOutlook()
                End If

                '   show active explorer
                With oListing

                    Try
                        m_oOutApp.Session.GetItemFromID(.ID, .StoreID).Display()
                    Catch
                        'alert to error raised when trying to open the dialog
                        MsgBox("Could not edit the contact.  It is possible that you " & _
                        "don't have rights to edit contacts in this folder." & vbCr & vbCr & "The following " & _
                            "message was returned by Outlook: " & vbCr & Err.Description, _
                            vbExclamation, GlobalMethods.g_oSession.AppTitle)
                    End Try

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("Displayed Outlook Inspector for =" & .UNID, _
                        "CIOutlookOM.CCIBackend.ICIBackend_EditContact")
                    End If
                End With

            Catch ex As Exception
                [Error].Show(ex)
            Finally
                Cursor.Current = Cursors.Default
            End Try
        End Sub

        Private Function ICIBackend_Events() As CEventGenerator Implements ICIBackend.Events
            Try
                ICIBackend_Events = m_oEvents
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                        oAddress As CAddress, _
                                        ByVal vAddressType As Object, _
                                        ByVal iIncludeData As ciRetrieveData, _
                                        ByVal iAlerts As ciAlerts) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData, iAlerts)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object, _
                                                ByVal iIncludeData As ciRetrieveData) As ICContacts Implements ICIBackend.GetContacts
            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress, _
                                                ByVal vAddressType As Object) As ICContacts Implements ICIBackend.GetContacts

            Try
                If Not IsNothing(vAddressType) Then
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType)
                Else
                    ICIBackend_GetContacts = GetContacts(oListing, oAddress)
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetContacts(oListing As CListing, _
                                                oAddress As CAddress) As ICContacts Implements ICIBackend.GetContacts

            Try
                ICIBackend_GetContacts = GetContacts(oListing, oAddress)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Object) As CContactNumber Implements ICIBackend.GetEMailNumber
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber

            Try
                oNum = Nothing

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                    ";vEMailID=" & vEMailID, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetEMailNumber")
                End If

                'get all eaddresses
                oNums = ICIBackend_GetEMailNumbers(oListing)

                If Not oNums Is Nothing Then
                    'get the eaddress with the specified ID
                    oNum = oNums.Item(vEMailID)

                    'return
                    ICIBackend_GetEMailNumber = oNum
                End If

                ICIBackend_GetEMailNumber = oNum
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Object) As CContactNumber Implements ICIBackend.GetFaxNumber
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber

            Try
                oNum = Nothing

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                    ";vFaxID=" & vFaxID, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetFaxNumber")
                End If

                'get all fax numbers
                oNums = ICIBackend_GetFaxNumbers(oListing)

                If Not oNums Is Nothing Then
                    'get the fax with the specified ID
                    oNum = oNums.Item(vFaxID)

                    'return
                    ICIBackend_GetFaxNumber = oNum
                End If

                ICIBackend_GetFaxNumber = oNum
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder, _
                                                      oFilter As CFilter) As CListings Implements ICIBackend.GetFolderListings
            'returns a collection of listings that match the filter criteria and are stored in the specified folder
            Dim oListings As CListings

            Try
                'get new listings
                oListings = New CListings

                GetFolderListings(oFolder, oListings, oFilter)

                ICIBackend_GetFolderListings = oListings
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetFolderListings(oFolder As CFolder) As CListings Implements ICIBackend.GetFolderListings
            'returns a collection of listings that match the filter criteria and are stored in the specified folder
            Dim oListings As CListings

            Try
                'get new listings
                oListings = New CListings

                GetFolderListings(oFolder, oListings)

                ICIBackend_GetFolderListings = oListings
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Object) As CContactNumber Implements ICIBackend.GetPhoneNumber
            Dim oNums As CContactNumbers
            Dim oNum As CContactNumber

            Try
                oNum = Nothing

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oListing.UNID=" & oListing.UNID & _
                    "; vPhoneID=" & vPhoneID, _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetPhoneNumber")
                End If

                'get all phones
                oNums = ICIBackend_GetPhoneNumbers(oListing)

                If Not oNums Is Nothing Then
                    'get the phone with the specified ID
                    oNum = oNums.Item(vPhoneID)

                    'return
                End If

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("oNums Is Nothing=" & CStr(oNums Is Nothing), _
                    "CIOutlookOM.CCIBackend.ICIBackend_GetPhoneNumber")
                End If
                ICIBackend_GetPhoneNumber = oNum
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private ReadOnly Property ICIBackend_InternalID() As Integer Implements ICIBackend.InternalID
            Get
                ICIBackend_InternalID = ciOutlookInternalID
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsConnected() As Boolean Implements ICIBackend.IsConnected
            Get
                ICIBackend_IsConnected = m_bIsConnected

                If GlobalMethods.g_oError.DebugMode Then
                    GlobalMethods.g_oError.SendToDebug("ICIBackend_IsConnected=" & m_bIsConnected, _
                    "CIOutlookOM.CCIBackend.ICIBackend_IsConnected")
                End If
            End Get
        End Property

        Private ReadOnly Property ICIBackend_IsLoadableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsLoadableEntity
            Get
                'returns TRUE iff the specified UNID can contain contacts-
                'all folders and only folders are loadable entities in MAPI
                Dim oUNID As CUNID
                ICIBackend_IsLoadableEntity = False
                Try
                    oUNID = New CUNID
                    ICIBackend_IsLoadableEntity = (oUNID.GetUNIDType(xUNID) = ciUNIDTypes.ciUNIDType_Folder)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_IsLoadableEntity=" & ICIBackend_IsLoadableEntity & _
                        "; xUNID=" & xUNID, "CIOutlookOM.CCIBackend.ICIBackend_IsLoadableEntity")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get

        End Property

        Private ReadOnly Property ICIBackend_IsSearchableEntity(ByVal xUNID As String) As Boolean Implements ICIBackend.IsSearchableEntity
            'returns TRUE iff the specified UNID can contain contacts
            Get
                'all folders and stores are searchable entities in MAPI
                Dim oUNID As CUNID
                Dim iType As ciUNIDTypes

                Try
                    oUNID = New CUNID
                    iType = oUNID.GetUNIDType(xUNID)
                    ICIBackend_IsSearchableEntity = (iType = ciUNIDTypes.ciUNIDType_Folder) Or (iType = ciUNIDTypes.ciUNIDType_Store)

                    If GlobalMethods.g_oError.DebugMode Then
                        GlobalMethods.g_oError.SendToDebug("ICIBackend_IsSearchableEntity=" & ICIBackend_IsSearchableEntity & _
                            "; xUNID=" & xUNID, "CIOutlookOM.CCIBackend.ICIBackend_IsSearchableEntity")
                    End If
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings Implements ICIBackend.SearchFolder
            Dim oFilter As CFilter

            ICIBackend_SearchFolder = Nothing
            Try
                oFilter = GetFilter()

                If Not (oFilter Is Nothing) Then
                    'pass filter into function to get listings
                    ICIBackend_SearchFolder = _
                        ICIBackend_GetFolderListings(oFolder, oFilter)
                Else
                    bCancel = True
                    ICIBackend_SearchFolder = Nothing
                End If
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_Filter() As CFilter Implements ICIBackend.Filter
            'returns the filter object for this backend

            Try
                ICIBackend_Filter = CreateFilter()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function

        Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields Implements ICIBackend.GetCustomFields
            Dim oItem As Outlook.ContactItem

            'Outlook item from the ci listing
            ICIBackend_GetCustomFields = Nothing
            Try
                With oListing
                    oItem = m_oOutApp.Session.GetItemFromID(.ID, .StoreID)
                End With

                If oItem Is Nothing Then
                    'could not get the requested mapi message
                    Throw New Exception("Could not find contact '" & oListing.DisplayName & "'.")
                End If

                ICIBackend_GetCustomFields = GetCustomFields(oItem)
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Function
#End Region
#Region "******************ICINumberPromptFormat***********************"
        Public Property ICINumberPromptFormat_ContactName() As String Implements ICINumberPromptFormat.ContactName
            Set(value As String)
                m_xName = value
            End Set
            Get
                ICINumberPromptFormat_ContactName = m_xName
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_AddressTypeHeading() As String Implements ICINumberPromptFormat.AddressTypeHeading
            Get
                ICINumberPromptFormat_AddressTypeHeading = "Type"
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumberDialogDescriptionText() As String Implements ICINumberPromptFormat.MissingNumberDialogDescriptionText
            Get
                Const ciText As String = "No <text> exist for this address type.  " & _
                    "Please select an alternate:"

                Dim xText As String

                ICINumberPromptFormat_MissingNumberDialogDescriptionText = ""

                Try
                    xText = Nothing

                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select

                    ICINumberPromptFormat_MissingNumberDialogDescriptionText = _
                        Replace(ciText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumbersDialogDescriptionText() As String Implements ICINumberPromptFormat.MissingNumbersDialogDescriptionText
            Get
                Dim xBaseText As String
                Dim xText As String

                ICINumberPromptFormat_MissingNumbersDialogDescriptionText = ""

                Try
                    xText = Nothing

                    xBaseText = "No <text> exist for this address type for '" & ICINumberPromptFormat_ContactName() & _
                        "'.  Please select an alternate:"

                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select

                    ICINumberPromptFormat_MissingNumbersDialogDescriptionText = _
                        Replace(xBaseText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try


                Exit Property
ProcError:
                GlobalMethods.g_oError.RaiseError("CIOutlookOM.CCIBackend.ICINumberPromptFormat_MissingNumberDialogDescriptionText")
                Exit Property
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MissingNumbersDialogTitle() As String Implements ICINumberPromptFormat.MissingNumbersDialogTitle
            Get
                ICINumberPromptFormat_MissingNumbersDialogTitle = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select a Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select a Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_MissingNumbersDialogTitle = "Select an EMail Address"
                        Case Else
                            ICINumberPromptFormat_MissingNumbersDialogTitle = Nothing
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MultipleNumbersDialogDescriptionText() As String Implements ICINumberPromptFormat.MultipleNumbersDialogDescriptionText
            Get
                Dim xBaseText As String
                Dim xText As String

                ICINumberPromptFormat_MultipleNumbersDialogDescriptionText = ""

                Try
                    xText = Nothing

                    xBaseText = "Multiple <text> exist for '" & ICINumberPromptFormat_ContactName() & _
                        "'.  Please select an alternate:"

                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            xText = "phone numbers"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            xText = "fax numbers"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            xText = "e-mail addresses"
                        Case Else
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select

                    ICINumberPromptFormat_MultipleNumbersDialogDescriptionText = _
                        Replace(xBaseText, "<text>", xText)
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private ReadOnly Property ICINumberPromptFormat_MultipleNumbersDialogTitle() As String Implements ICINumberPromptFormat.MultipleNumbersDialogTitle
            Get
                ICINumberPromptFormat_MultipleNumbersDialogTitle = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select a Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select a Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = "Select an EMail Address"
                        Case Else
                            ICINumberPromptFormat_MultipleNumbersDialogTitle = Nothing
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Private Property ICINumberPromptFormat_NumberType As ciContactNumberTypes Implements ICINumberPromptFormat.NumberType
            Set(value As ciContactNumberTypes)
                m_iNumberType = value
            End Set
            Get
                ICINumberPromptFormat_NumberType = m_iNumberType
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_CategoryHeading() As String Implements ICINumberPromptFormat.CategoryHeading
            Get
                ICINumberPromptFormat_CategoryHeading = "Type"
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_DescriptionHeading() As String Implements ICINumberPromptFormat.DescriptionHeading
            Get
                ICINumberPromptFormat_DescriptionHeading = "Description"
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_NumberHeading() As String Implements ICINumberPromptFormat.NumberHeading
            Get
                ICINumberPromptFormat_NumberHeading = ""
                Try
                    Select Case m_iNumberType
                        Case ciContactNumberTypes.ciContactNumberType_Phone
                            ICINumberPromptFormat_NumberHeading = "Phone Number"
                        Case ciContactNumberTypes.ciContactNumberType_Fax
                            ICINumberPromptFormat_NumberHeading = "Fax Number"
                        Case ciContactNumberTypes.ciContactNumberType_EMail
                            ICINumberPromptFormat_NumberHeading = "EMail Address"
                        Case Else
                            ICINumberPromptFormat_NumberHeading = Nothing
                            Throw New Exception("You must set ICINumberPromptFormat_NumberType " & _
                                "before attempting to retrieve and NumberFormatProperties")
                    End Select
                Catch ex As Exception
                    [Error].Show(ex)
                End Try
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_ExtensionHeading() As String Implements ICINumberPromptFormat.ExtensionHeading
            Get
                ICINumberPromptFormat_ExtensionHeading = "Extension"
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_AddressTypeWidth() As Single Implements ICINumberPromptFormat.AddressTypeWidth
            Get
                ICINumberPromptFormat_AddressTypeWidth = 0
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_CategoryWidth() As Single Implements ICINumberPromptFormat.CategoryWidth
            Get
                ICINumberPromptFormat_CategoryWidth = 0
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_DescriptionWidth() As Single Implements ICINumberPromptFormat.DescriptionWidth
            Get
                ICINumberPromptFormat_DescriptionWidth = 250
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_NumberWidth() As Single Implements ICINumberPromptFormat.NumberWidth
            Get
                ICINumberPromptFormat_NumberWidth = 500
            End Get
        End Property

        Public ReadOnly Property ICINumberPromptFormat_ExtensionWidth() As Single Implements ICINumberPromptFormat.ExtensionWidth
            Get
                ICINumberPromptFormat_ExtensionWidth = 0
            End Get
        End Property

        Private Property ICINumberPromptFormat_PromptType As ICINumberPromptFormat.ciNumberPromptTypes Implements ICINumberPromptFormat.PromptType
            Get
                ICINumberPromptFormat_PromptType = m_iPromptType
            End Get
            Set(value As ICINumberPromptFormat.ciNumberPromptTypes)
                m_iPromptType = value
            End Set
        End Property
#End Region
    End Class
End Namespace
