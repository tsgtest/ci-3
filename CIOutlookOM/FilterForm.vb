﻿Option Explicit On

Imports System.Windows.Forms
Imports LMP

Namespace TSG.CI.OutlookOM
    Public Class FilterForm
#Region "******************fields***********************"
        Public Enum ciMAPIFilterActions
            ciMAPIFilterAction_Cancel = 0
            ciMAPIFilterAction_Search = 1
            ciMAPIFilterAction_OutlookSearch = 2
            ciMAPIFilterAction_ClearSearch = 3
        End Enum

        Private m_bCancelled As Boolean
        Private m_bSearchNow As Boolean
        Private xMsg As String
        Private m_oBackend As ICIBackend
        Private m_iFilterAction As ciMAPIFilterActions
        Private m_iSortCol As ciListingCols

        'control arrays
        Public txtFilterField(3) As TextBox
        Dim lblFilterField(3) As Label
#End Region
#Region "******************initializer***********************"
        Public Sub New()
            InitializeComponent()
            Try
                'initialize control arrays
                txtFilterField(0) = Me.txtFilterField0
                txtFilterField(1) = Me.txtFilterField1
                txtFilterField(2) = Me.txtFilterField2
                txtFilterField(3) = Me.txtFilterField3

                lblFilterField(0) = Me.lblFilterField0
                lblFilterField(1) = Me.lblFilterField1
                lblFilterField(2) = Me.lblFilterField2
                lblFilterField(3) = Me.lblFilterField3
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************properties***********************"
        Public WriteOnly Property Backend As ICIBackend
            Set(value As ICIBackend)
                m_oBackend = value
            End Set
        End Property

        Public Property Action() As ciMAPIFilterActions
            Set(value As ciMAPIFilterActions)
                m_iFilterAction = value
            End Set
            Get
                Action = m_iFilterAction
            End Get
        End Property

        Public Property SortColumn() As ciListingCols
            Set(value As ciListingCols)
                m_iSortCol = value
            End Set
            Get
                SortColumn = m_iSortCol
            End Get
        End Property
#End Region
#Region "******************events***********************"
        Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
            Dim i As Integer
            Dim xTemp As String = ""

            Try
                For i = 0 To 3
                    xTemp += Me.txtFilterField(i).Text
                Next

                If xTemp <> "" Then
                    Action = ciMAPIFilterActions.ciMAPIFilterAction_Search
                Else
                    Action = ciMAPIFilterActions.ciMAPIFilterAction_Cancel
                End If
                GlobalMethods.g_oIni.SetUserIni("CIOutlookOM", "SearchType", Me.cmbSearchTypes.SelectedValue)
            Catch ex As Exception
                [Error].Show(ex)
            Finally
                Me.Hide()
            End Try
        End Sub

        Private Sub FilterForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
            Dim i As Integer
            Dim xSearchType As String

            Try
                If TSG.CI.OutlookOM.GlobalMethods.g_oIni Is Nothing Then
                    TSG.CI.OutlookOM.GlobalMethods.g_oIni = New CIni
                End If

                'select contents of first criterion
                Me.txtFilterField0.SelectAll()

                'set sort field
                With Me.cbxSortBy
                    For i = 0 To .Items.Count - 1
                        If .SelectedValue = m_oBackend.Filter.SortColumn Then
                            .SelectedIndex = i
                            Exit For
                        End If
                    Next i
                    .SelectedIndex = m_iSortCol
                End With

                xSearchType = TSG.CI.OutlookOM.GlobalMethods.g_oIni.GetUserIni("CIOutlookOM", "SearchType")

                If xSearchType = Nothing Then
                    'no default set - use first col (Display Name)
                    xSearchType = ciSearchOperators.ciSearchOperator_BeginsWith
                End If

                Me.cmbSearchTypes.SelectedValue = xSearchType
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub FilterForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Dim i As Integer
            Dim iNumFields As Integer
            Dim dtSortType As DataTable

            Try
                dtSortType = New DataTable

                With dtSortType
                    'define columns
                    .Columns.Add("Display", GetType(String))
                    .Columns.Add("Operator", GetType(Integer))

                    'add rows
                    .Rows.Add("That begin with...", ciSearchOperators.ciSearchOperator_BeginsWith)
                    .Rows.Add("That contain...", ciSearchOperators.ciSearchOperator_Contains)
                    .Rows.Add("That have the value...", ciSearchOperators.ciSearchOperator_Equals)
                End With

                With Me.cmbSearchTypes
                    .SetList(dtSortType)
                    '.SelectedIndex = 0
                End With

                Action = ciMAPIFilterActions.ciMAPIFilterAction_Cancel

                With m_oBackend.Filter
                    iNumFields = .CountFields

                    'show only those controls corresponding
                    'to existing filter fields
                    For i = 0 To iNumFields - 1
                        Me.lblFilterField(i).Text = "&" & .FilterFields(i).Name & ":"
                        Me.lblFilterField(i).Visible = i + 1 <= iNumFields
                        Me.txtFilterField(i).Visible = i + 1 <= iNumFields
                    Next

                    With Me.fraFilter
                        'adjust frame for number of filter field controls
                        If iNumFields < 4 Then
                            .Height = .Height - ((4 - iNumFields) * (.Height / 4)) + _
                                      (4 - iNumFields) * 100
                        End If
                    End With
                End With
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub txtFilterField_Click(sender As Object, e As EventArgs) _
            Handles txtFilterField0.Click, txtFilterField1.Click, txtFilterField2.Click, txtFilterField3.Click

            Try
                Dim txt As TextBox = DirectCast(sender, TextBox)

                txt.SelectAll()
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

            Try
                Me.Hide()
                Action = ciMAPIFilterActions.ciMAPIFilterAction_Cancel
            Catch ex As Exception
                [Error].Show(ex)
            End Try

        End Sub

        Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
            Try
                Me.ResetFilter()
                Action = ciMAPIFilterActions.ciMAPIFilterAction_Search
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
#Region "******************methods***********************"
        Public Sub ResetFilter()
            Try
                For i = 0 To 3
                    txtFilterField(i).Text = ""
                Next
            Catch ex As Exception
                [Error].Show(ex)
            End Try
        End Sub
#End Region
    End Class
End Namespace