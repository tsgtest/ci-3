Option Explicit On

Namespace TSG.CI.OutlookOM
    Friend Class CError
        Public Enum ciMAPIErrs
            ciMAPIErr_NoMAPISession = 10 + 512 + 1
            ciMAPIErr_InvalidMAPIMessage
            ciMAPIErr_InvalidIniSetup
        End Enum
    End Class
End Namespace